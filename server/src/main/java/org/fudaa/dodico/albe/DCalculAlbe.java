/*
 * @creation 19 oct. 06
 * @modification $Date: 2007-03-27 16:09:55 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.dodico.albe;

import java.io.File;

import org.fudaa.dodico.corba.albe.ICalculAlbe;
import org.fudaa.dodico.corba.albe.ICalculAlbeOperations;
import org.fudaa.dodico.corba.albe.IParametresAlbe;
import org.fudaa.dodico.corba.albe.IParametresAlbeHelper;
import org.fudaa.dodico.corba.albe.IResultatsAlbe;
import org.fudaa.dodico.corba.albe.IResultatsAlbeHelper;
import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.SResultatsAlbe;
import org.fudaa.dodico.corba.objet.IConnexion;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.objet.CExec;

/**
 * Gestion du lancement du code de calcul <code>Albe</code>.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class DCalculAlbe extends DCalcul implements ICalculAlbe, ICalculAlbeOperations {

  public DCalculAlbe() {
    super();
  }

  public final Object clone() throws CloneNotSupportedException {
    return new DCalculAlbe();
  }

  public String toString() {
    return "DCalculAlbe()";
  }

  /**
   * Description du serveur de calcul <code>Albe</code>.
   */
  public String description() {
    return "Albe, serveur de calcul: " + super.description();
  }

  /**
   * 
   */
  public void calcul(final IConnexion _c) {

    try {
      if (!verifieConnexion(_c)) {
        throw new Exception();
      }
    }
    catch (final Exception _e1) {
      System.out.println("Connexion non valide, execution du code de calcul impossible");
      return;
    }

    final IParametresAlbe params = IParametresAlbeHelper.narrow(parametres(_c));
    try {
      if (params == null) {
        throw new Exception();
      }
    }
    catch (final Exception _e2) {
      System.out.println("Aucun param�tre d'entr�e pour le code de calcul");
      return;
    }
    final IResultatsAlbe results = IResultatsAlbeHelper.narrow(resultats(_c));
    try {
      if (results == null) {
        throw new Exception();
      }
    }
    catch (final Exception _e3) {
      System.out.println("Aucun r�sultat renvoy� par le code de calcul");
      return;
    }
    final String os = System.getProperty("os.name");
    final String path = cheminServeur();
    final SParametresAlbe _p = params.parametresAlbe();
    String etude = "albe";
    SResultatsAlbe albeResults = null;
    try {
      DParametresAlbe.ecritParametresAlbe(etude, _p);
      String[] cmd;
      if (os.startsWith("Windows")) {
    	(new File("tmp")).mkdir();
        cmd = new String[2];
        cmd[0] = path + "bin" + "\\win\\albe_win.exe";
        cmd[1] = path + "bin" + "\\win\\";
        //cmd[2] = etude;
        System.out.println(cmd[0] + " " + cmd[1]);
      } else {
    	(new File("tmp")).mkdir();
        cmd = new String[2];
        cmd[0] = path + "bin" + "/linux/albe_linux.x";
        cmd[1] = path + "bin" + "/linux/";
        //cmd[2] = etude;
        System.out.println(cmd[0] + " " + cmd[1]);
      }
      try {
        final CExec ex = new CExec();
        ex.setCommand(cmd);
        ex.setOutStream(System.out);
        ex.setErrStream(System.err);
        ex.exec();
      }
      catch (final Throwable _e1) {
        System.out.println("Erreur rencontr�e lors de l'execution du code de calcul");
        _e1.printStackTrace();
      }
      albeResults = DResultatsAlbe.resultatsAlbe(etude, _p);
      results.resultatsAlbe(albeResults);
    }
    catch (final Exception ex) {
      System.out.println("Erreur lors de l'execution du code de calcul");
    }

  }

}
