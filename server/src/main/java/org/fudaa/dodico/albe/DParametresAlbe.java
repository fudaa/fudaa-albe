/*
 * @creation 19 oct. 06
 * @modification $Date: 2007-04-06 07:47:36 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.dodico.albe;

import java.io.FileWriter;
import java.io.IOException;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.albe.IParametresAlbe;
import org.fudaa.dodico.corba.albe.IParametresAlbeOperations;
import org.fudaa.dodico.corba.albe.SParametresAlbe;

import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * Classe impl�mentant l'interface IParametresAlbe g�rant les param�tres du code
 * de calcul albe.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class DParametresAlbe extends DParametres implements IParametresAlbe,
    IParametresAlbeOperations {

  /**
   * structure d�crivant les param�tres du calcul.
   */
  private SParametresAlbe parametreAlbe_;

  /**
   * constructeur.
   */
  public DParametresAlbe() {
    super();
  }

  /**
   * 
   */
  public final Object clone() throws CloneNotSupportedException {
    return new DParametresAlbe();
  }

  /**
   * cha�ne descriptive de l'objet.
   */
  public String toString() {
    return "DParametresAlbe";
  }

  /**
   * retourne la structure utilisee pour d�crire les parametres.
   */
  public SParametresAlbe parametresAlbe() {
    return parametreAlbe_;
  }

  /**
   * modifie la structure des parametres.
   * 
   * @param _p la nouvelle structure.
   */
  public void parametresAlbe(final SParametresAlbe _p) {
    parametreAlbe_ = _p;
  }

  /**
   * Ecrit les param�tres <code>_params</code> dans le fichier de nom
   * <code>_nomfichier</code> (ajoute l'extension ".albe"). Le format
   * "fortran" est utilis�.
   * 
   * @param _nomFichier le fichier destination (sans extension).
   * @param _params les param�tres a �crire dans le fichier.
   * @throws IOException
   */
  public static void ecritParametresAlbe(final String _nomFichier, final SParametresAlbe _params)
      throws IOException {
    int[] fmtI;
    final FortranWriter fw = new FortranWriter(new FileWriter(_nomFichier + ".albe"));
    fw.setLineSeparator(CtuluLibString.LINE_SEP);
    fmtI = new int[] { 40, 100 };
    // Titre
    fw.stringField(0, _params.projet);
    fw.writeFields(fmtI);
    // Cote de la t�te de l'ouvrage
    fw.stringField(0, String.valueOf(_params.geo.coteTeteOuvrage));
    fw.stringField(1, " COTE DE LA TETE DE L'OUVRAGE (m)");
    fw.writeFields(fmtI);
    // Tron�ons
    ecritParametresTroncons(_params, fmtI, fw);
    // Sol
    ecritParametresSol(_params, fmtI, fw);
    // Action
    ecritParametresAction(_params, fmtI, fw);
    // D�fense
    ecritParametresDefense(_params, fmtI, fw);
    // Coefficients partiels
    ecritParametresCoefficientsPartiels(_params, fmtI, fw);
    // Param�tres de calculs
    ecritParametresCalculs(_params, fmtI, fw);

    fw.flush();
    fw.close();
  }

  /**
   * 
   */
  public static void ecritParametresTroncons(final SParametresAlbe _params, int[] _fmtI,
      final FortranWriter _fw) throws IOException {
    // Type du pieu
    _fw.stringField(0, _params.pieu.typePieu);
    _fw.stringField(1, " TYPE DE DUC D'ALBE");
    _fw.writeFields(_fmtI);

    // Si caisson
    if (_params.pieu.typePieu.equals("C")) {
      _fw.stringField(0, String.valueOf(_params.pieu.excentricite));
      _fw.stringField(1, " EXCENTRICITE DU CAISSON (cm)");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.pieu.largeurEffectivePieu / 100));
      _fw.stringField(1, " LARGEUR EFFECTIVE (m)");
      _fw.writeFields(_fmtI);
    }
    // Si tubulaire
    else if (_params.pieu.typePieu.equals("T")) {
      _fw.stringField(0, String.valueOf(_params.pieu.diametreTroncons));
      _fw.stringField(1, " DIAMETRE DES TUBES (mm)");
      _fw.writeFields(_fmtI);
    }

    // Nombre de tron�ons
    _fw.stringField(0, String.valueOf(_params.pieu.nombreTroncons));
    _fw.stringField(1, " NOMBRE DE TRONCONS");
    _fw.writeFields(_fmtI);

    // Boucle sur les tron�ons
    if (_params.pieu.typePieu.equals("T")) {
      for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
        // Longueur du tron�on i
        _fw.stringField(0, String.valueOf(_params.pieu.tronconsTubulaire[i].longueur));
        _fw.stringField(1, " HAUTEUR DU TRONCON " + (i + 1) + " (m)");
        _fw.writeFields(_fmtI);

        // Epaisseur du tron�on i
        _fw.stringField(0, String.valueOf(_params.pieu.tronconsTubulaire[i].epaisseur));
        _fw.stringField(1, " EPAISSEUR DU TRONCON " + (i + 1) + " (mm)");
        _fw.writeFields(_fmtI);

        // Perte d'�paisseur par corrosion du tron�on i
        _fw.stringField(0, String
            .valueOf(_params.pieu.tronconsTubulaire[i].perteEpaisseurCorrosion));
        _fw.stringField(1, " PERTE D'EPAISSEUR PAR CORROSION DU TRONCON " + (i + 1) + " (mm)");
        _fw.writeFields(_fmtI);

        // Contrainte maxi du tron�on i
        _fw.stringField(0, String
            .valueOf(_params.pieu.tronconsTubulaire[i].valeurCaracteristiqueLimiteElastiqueAcier));
        _fw.stringField(1, " CONTRAINTE MAXI DU TRONCON " + (i + 1) + " (MPa)");
        _fw.writeFields(_fmtI);
      }
    }

    else if (_params.pieu.typePieu.equals("C")) {
      for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
        // Longueur du tron�on i
        _fw.stringField(0, String.valueOf(_params.pieu.tronconsCaisson[i].longueur));
        _fw.stringField(1, " HAUTEUR DU TRONCON " + (i + 1) + " (m)");
        _fw.writeFields(_fmtI);
        // Inertie non corrod�e du tron�on i
        _fw.stringField(0, String.valueOf(_params.pieu.tronconsCaisson[i].inertieNonCorrodee));
        _fw.stringField(1, " INERTIE NON CORRODEE DU TRONCON " + (i + 1) + " (cm4)");
        _fw.writeFields(_fmtI);
        // Inertie corrod�e du tron�on i
        _fw.stringField(0, String.valueOf(_params.pieu.tronconsCaisson[i].inertieCorrodee));
        _fw.stringField(1, " INERTIE CORRODEE DU TRONCON " + (i + 1) + " (cm4)");
        _fw.writeFields(_fmtI);
        // Section non corrod�e du tron�on i
        _fw.stringField(0, String.valueOf(_params.pieu.tronconsCaisson[i].sectionNonCorrodee));
        _fw.stringField(1, " SECTION NON CORRODEE DU TRONCON " + (i + 1) + " (cm2)");
        _fw.writeFields(_fmtI);
        // Section corrod�e du tron�on i
        _fw.stringField(0, String.valueOf(_params.pieu.tronconsCaisson[i].sectionCorrodee));
        _fw.stringField(1, " SECTION CORRODEE DU TRONCON " + (i + 1) + " (cm2)");
        _fw.writeFields(_fmtI);
        // Contrainte maxi du tron�on i
        _fw.stringField(0, String
            .valueOf(_params.pieu.tronconsCaisson[i].valeurCaracteristiqueLimiteElastiqueAcier));
        _fw.stringField(1, " CONTRAINTE MAXI DU TRONCON " + (i + 1) + " (MPa)");
        _fw.writeFields(_fmtI);
      }
    }
  }

  /**
   * 
   */
  public static void ecritParametresSol(final SParametresAlbe _params, int[] _fmtI,
      final FortranWriter _fw) throws IOException {
    // Coefficient pour convertir les pressions (kPa -> t/m2)
    double coefConvPression = 10;
    // Nombre de points de la courbe
    _fw.stringField(0, String.valueOf(_params.sol.nombrePoints));
    _fw.stringField(1, " NOMBRE DE POINTS DES COURBES");
    _fw.writeFields(_fmtI);

    // Cote de dragage
    _fw.stringField(0, String.valueOf(_params.geo.coteDragageBassin));
    _fw.stringField(1, " COTE DE DRAGAGE (m)");
    _fw.writeFields(_fmtI);
    // Cote de calcul
    _fw.stringField(0, String.valueOf(_params.geo.coteCalcul));
    _fw.stringField(1, " COTE DE CALCUL (m)");
    _fw.writeFields(_fmtI);
    // Nombre de couches de sol
    _fw.stringField(0, String.valueOf(_params.geo.nombreCouchesSol));
    _fw.stringField(1, " NOMBRE DE COUCHES DE SOL");
    _fw.writeFields(_fmtI);
    // Boucles sur les couches
    for (int i = 0; i < _params.geo.nombreCouchesSol; i++) {
      // Epaisseur de la couche i
      if (_params.sol.modeleComportementSol.equals("M")) {
        _fw.stringField(0, String.valueOf(_params.sol.epaisseursCourbeManuelle[i]));
        _fw.stringField(1, " EPAISSEUR (m) DE LA COUCHE " + (i + 1));
        _fw.writeFields(_fmtI);
      } else {
        _fw.stringField(0, String.valueOf(_params.sol.courbe[i].epaisseur));
        _fw.stringField(1, " EPAISSEUR (m) DE LA COUCHE " + (i + 1));
        _fw.writeFields(_fmtI);
      }
      // Boucle sur les points de la courbe
      for (int j = 0; j < _params.sol.nombrePoints; j++) {
        _fw.stringField(0, String.valueOf(_params.sol.points[i][j].deplacement));
        _fw.stringField(1, " POINT " + (j + 1) + " : DEPLACEMENT (cm)");
        _fw.writeFields(_fmtI);
        _fw.stringField(0, String.valueOf(_params.sol.points[i][j].pression / coefConvPression));
        _fw.stringField(1, " POINT " + (j + 1) + " : PRESSION (t/m2)");
        _fw.writeFields(_fmtI);
      }
    }// Fin de boucle sur les couches
  }

  /**
   * 
   */

  public static void ecritParametresAction(final SParametresAlbe _params, int[] _fmtI,
      final FortranWriter _fw) throws IOException {
    // coefficient pour convertir les �nergies et les r�actions
    double coefConv = 10;

    // Type action
    _fw.stringField(0, String.valueOf(_params.action.typeAction));
    _fw.stringField(1, " TYPE D'ACTION");
    _fw.writeFields(_fmtI);

    // Si accostage
    if (_params.action.typeAction.equals("C")) {
      _fw.stringField(0, String.valueOf(_params.action.coteAccostageHaut));
      _fw.stringField(1, " COTE ACCOSTAGE HAUT (m)");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.action.coteAccostageBas));
      _fw.stringField(1, " COTE ACCOSTAGE BAS (m)");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.action.energieAccostageFrequente / coefConv));
      _fw.stringField(1, " ENERGIE D'ACCOSTAGE FREQUENTE (tm)");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.action.energieAccostageCaracteristique / coefConv));
      _fw.stringField(1, " ENERGIE D'ACCOSTAGE CARACTERISTIQUE (tm)");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.action.energieAccostageCalcul / coefConv));
      _fw.stringField(1, " ENERGIE D'ACCOSTAGE DE CALCUL (tm)");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.action.energieAccostageAccidentelle / coefConv));
      _fw.stringField(1, " ENERGIE D'ACCOSTAGE ACCIDENTELLE (tm)");
      _fw.writeFields(_fmtI);
    }
    // Si amarrage
    else if (_params.action.typeAction.equals("M")) {
      _fw.stringField(0, String.valueOf(_params.action.coteAmarrage));
      _fw.stringField(1, " COTE AMARRAGE (m)");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.action.effortAmarrageFrequent / coefConv));
      _fw.stringField(1, " EFFORT D'AMARRAGE FREQUENT (t)");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.action.effortAmarrageCaracteristique / coefConv));
      _fw.stringField(1, " EFFORT D'AMARRAGE CARACTERISTIQUE (t)");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.action.effortAmarrageCalcul / coefConv));
      _fw.stringField(1, " EFFORT D'AMARRAGE DE CALCUL (t)");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.action.effortAmarrageAccidentel / coefConv));
      _fw.stringField(1, " EFFORT D'AMARRAGE ACCIDENTEL (t)");
      _fw.writeFields(_fmtI);
    }
  }

  /**
   * 
   */
  public static void ecritParametresDefense(final SParametresAlbe _params, int[] _fmtI,
      final FortranWriter _fw) throws IOException {

    if (_params.action.typeAction.equals("M") || _params.defense.presenceDefense == "N") {
      // Nombre de points = 0
      _fw.stringField(0, "0");
      _fw.stringField(1, " NOMBRE DE POINTS DE LA COURBE DE LA DEFENSE");
      _fw.writeFields(_fmtI);
    }

    else if (_params.defense.presenceDefense == "O") {
      double coefConv = 10;
      // Nombre de points
      _fw.stringField(0, String.valueOf(_params.defense.nombrePoints));
      _fw.stringField(1, " NOMBRE DE POINTS DE LA COURBE DE LA DEFENSE");
      _fw.writeFields(_fmtI);
      // Hauteur de la d�fense
      _fw.stringField(0, String.valueOf(_params.defense.hauteur));
      _fw.stringField(1, " HAUTEUR DE LA DEFENSE (mm)");
      _fw.writeFields(_fmtI);
      // Boucle sur les d�fenses
      for (int i = 0; i < _params.defense.nombrePoints; i++) {
        _fw.stringField(0, String.valueOf(_params.defense.points[i].deformation));
        _fw.stringField(1, " POINT " + (i + 1) + " : DEFLEXION (%)");
        _fw.writeFields(_fmtI);
        _fw.stringField(0, String.valueOf(_params.defense.points[i].effort / coefConv));
        _fw.stringField(1, " POINT " + (i + 1) + " : REACTION (t)");
        _fw.writeFields(_fmtI);
      }
    }
  }

  /**
   * 
   */
  public static void ecritParametresCoefficientsPartiels(final SParametresAlbe _params,
      int[] _fmtI, final FortranWriter _fw) throws IOException {
    // Coefficients sol
    _fw.stringField(0, String.valueOf(_params.coefficientsPartiels.coeffSol[0].favorable));
    _fw.stringField(1, " COEFFICIENT SOL ELU FONDA FAVORABLE");
    _fw.writeFields(_fmtI);
    _fw.stringField(0, String.valueOf(_params.coefficientsPartiels.coeffSol[0].defavorable));
    _fw.stringField(1, " COEFFICIENT SOL ELU FONDA DEFAVORABLE");
    _fw.writeFields(_fmtI);
    // Coefficients d�fense
    if (_params.action.typeAction.equals("C") && _params.defense.presenceDefense == "O") {
      _fw.stringField(0, String.valueOf(_params.coefficientsPartiels.coeffDefense[0].favorable));
      _fw.stringField(1, " COEFFICIENT DEFENSE ELU FONDA FAVORABLE");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.coefficientsPartiels.coeffDefense[0].defavorable));
      _fw.stringField(1, " COEFFICIENT DEFENSE ELU FONDA DEFAVORABLE");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.coefficientsPartiels.coeffDefense[1].favorable));
      _fw.stringField(1, " COEFFICIENT DEFENSE ELU ACC FAVORABLE");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.coefficientsPartiels.coeffDefense[1].defavorable));
      _fw.stringField(1, " COEFFICIENT DEFENSE ELU ACC DEFAVORABLE");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.coefficientsPartiels.coeffDefense[2].favorable));
      _fw.stringField(1, " COEFFICIENT DEFENSE ELS RARE FAVORABLE");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.coefficientsPartiels.coeffDefense[2].defavorable));
      _fw.stringField(1, " COEFFICIENT DEFENSE ELS RARE DEFAVORABLE");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.coefficientsPartiels.coeffDefense[3].favorable));
      _fw.stringField(1, " COEFFICIENT DEFENSE ELS FREQUENT FAVORABLE");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.coefficientsPartiels.coeffDefense[3].defavorable));
      _fw.stringField(1, " COEFFICIENT DEFENSE ELS FREQUENT DEFAVORABLE");
      _fw.writeFields(_fmtI);
    }

  }

  /**
   * 
   */
  public static void ecritParametresCalculs(final SParametresAlbe _params, int[] _fmtI,
      final FortranWriter _fw) throws IOException {

    double coefConv = 10;
    boolean accostage = false;

    // Mode de lancement des calculs
    _fw.stringField(0, String.valueOf(_params.lancementCalculs.modeLancementCalcul));
    _fw.stringField(1, " MODE DE LANCEMENT DES CALCULS");
    _fw.writeFields(_fmtI);

    if (_params.lancementCalculs.modeLancementCalcul == 0) {
      _fw.stringField(0, String.valueOf(_params.lancementCalculs.cote));
      if (_params.action.typeAction.equals("C")) {
        accostage = true;
        _fw.stringField(1, " COTE ACCOSTAGE (m)");
      } else
        _fw.stringField(1, " COTE AMARRAGE (m)");
      _fw.writeFields(_fmtI);
      if (accostage) {
        _fw.stringField(0, String.valueOf(_params.lancementCalculs.energieAccostage / coefConv));
        _fw.stringField(1, " ENERGIE D'ACCOSTAGE (tm)");
        _fw.writeFields(_fmtI);
      } else if (_params.action.typeAction.equals("M")) {
        _fw.stringField(1, " EFFORT D'AMARRAGE (t)");
        _fw.stringField(0, String.valueOf(_params.lancementCalculs.effortAmarrage / coefConv));
        _fw.writeFields(_fmtI);
      }
      _fw.stringField(0, String.valueOf(_params.lancementCalculs.gammaMSol));
      _fw.stringField(1, " COEFFICIENT SOL");
      _fw.writeFields(_fmtI);
      if (accostage && _params.defense.presenceDefense.equals("O")) {
        _fw.stringField(0, String.valueOf(_params.lancementCalculs.gammaMDefense));
        _fw.stringField(1, " COEFFICIENT DEFENSE");
        _fw.writeFields(_fmtI);
      }
      _fw.stringField(0, String.valueOf(_params.lancementCalculs.corrosion));
      _fw.stringField(1, " CORROSION");
      _fw.writeFields(_fmtI);

    } else if (_params.lancementCalculs.modeLancementCalcul == 1) {
      _fw.stringField(0, String.valueOf(_params.lancementCalculs.eluFonda));
      _fw.stringField(1, " ELU FONDAMENTAL");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.lancementCalculs.eluAcc));
      _fw.stringField(1, " ELU ACCIDENTEL");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.lancementCalculs.elsRare));
      _fw.stringField(1, " ELS RARE");
      _fw.writeFields(_fmtI);
      _fw.stringField(0, String.valueOf(_params.lancementCalculs.elsFreq));
      _fw.stringField(1, " ELS FREQUENT");
      _fw.writeFields(_fmtI);
    }
  }
}
