/*
 * @creation 19 oct. 06
 * @modification $Date: 2008-02-06 10:22:44 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.dodico.albe;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.corba.albe.IResultatsAlbe;
import org.fudaa.dodico.corba.albe.IResultatsAlbeOperations;
import org.fudaa.dodico.corba.albe.SLigneRes;
import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.SResGeneraux;
import org.fudaa.dodico.corba.albe.SResistanceTroncon;
import org.fudaa.dodico.corba.albe.SResultatsAlbe;
import org.fudaa.dodico.corba.albe.SResultatsCombinaison;
import org.fudaa.dodico.corba.albe.STroncon;
import org.fudaa.dodico.fortran.FortranReader;

/**
 * Les resultats Albe.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class DResultatsAlbe extends DResultats implements IResultatsAlbe, IResultatsAlbeOperations {

  /**
   * nom du fichier r�sultats contenant les donn�es import�es.
   */
  private String nomFichier_;

  /**
   * structure de donn�es contenant les r�sultats import�s.
   */
  private SResultatsAlbe resultatsAlbe_;

  /**
   * constructeur.
   */
  public DResultatsAlbe() {
    super();
  }

  /**
   * renvoie une copie de l'objet. attention la structure de donn�es est vide,
   * lors de l'appel de la fonction <code>resultatsAlbe()</code> sur ce nouvel
   * objet, une relecture compl�te du fichier sera efectu�e.
   */
  public Object clone() {
    final DResultatsAlbe _r = new DResultatsAlbe();
    _r.setFichier(nomFichier_);
    return _r;
  }

  /**
   * renvoie une structure de donn�es contenant les informations du fichier
   * r�sultats. <code>nomFichier_</code>
   */
  public SResultatsAlbe resultatsAlbe() {
    final SResultatsAlbe _r = resultatsAlbe_;
    return _r;
  }

  /**
   * stocke la structure de donn�es r�sultats <code>_resultatsAlbe</code>.
   */
  public void resultatsAlbe(final SResultatsAlbe _resultatsAlbe) {
    resultatsAlbe_ = _resultatsAlbe;
  }

  /**
   * sauvegarde le nom du fichier contenant les donn�es. si
   * <code>_nomFichier</code> est diff�rent de <code>nomFichier_</code>
   * alors la structure de donn�es <code>resultatsAlbe_</code> est
   * r�initialis�e (vid�e).
   * 
   * @param _nomFichier
   */
  public void setFichier(final String _nomFichier) {
    try {
      if (!_nomFichier.equals(nomFichier_)) {
        nomFichier_ = _nomFichier;
        resultatsAlbe_ = null;
      }
    }
    catch (final NullPointerException _e1) {
      System.out.println("Impossible de charger un fichier sans nom");
    }
  }

  /**
   * renvoie une r�pr�sentation cha�ne de caract�res de l'objet.
   */
  public String toString() {
    String s = "Object DResultatsAlbe | ";
    s += "nomFichier_:" + nomFichier_;
    s += ", ";
    try {
      s += "resultatsAlbe_:" + resultatsAlbe_.toString();
    }
    catch (final NullPointerException _e1) {}
    return s;
  }

  /**
   * @param _etude
   * @return une structure de donn�es contenant les informations r�sultats de
   *         l'�tude <code>_etude</code>. Cette m�thode r�cup�re les donn�es
   *         de tous les fichiers r�sultats.
   */
  public static SResultatsAlbe resultatsAlbe(final String _etude, final SParametresAlbe _params) {
    // Initialisation
    final SResultatsAlbe r = new SResultatsAlbe();
    if (_params.lancementCalculs.modeLancementCalcul == 0)
      r.resultatsCombiManuelle = resultatsAlbeCombinaison(_etude + ".0", _params);
    else {
      boolean accostage = _params.action.typeAction.equals("C") ? true : false;
      boolean defense = accostage && _params.defense.presenceDefense.equals("O") ? true : false;

      /** ELU fondamental. */
      if (_params.lancementCalculs.eluFonda) {
        // si accostage avec d�fense
        if (defense) {
          SResultatsCombinaison[] eluFonda = new SResultatsCombinaison[16];
          for (int i = 0; i < 16; i++) {
            eluFonda[i] = resultatsAlbeCombinaison(_etude + "." + (i + 1), _params);
          }
          r.resultatsEluFonda = eluFonda;
        }// si accostage sans d�fense : on ne lit qu'un fichier sur deux
        else if (accostage) {
          SResultatsCombinaison[] eluFonda = new SResultatsCombinaison[8];
          for (int i = 0; i < 2; i++) {
            eluFonda[i] = resultatsAlbeCombinaison(_etude + "." + (i + 1), _params);
          }
          for (int i = 0; i < 2; i++) {
            eluFonda[i + 2] = resultatsAlbeCombinaison(_etude + "." + (i + 5), _params);
          }
          for (int i = 0; i < 2; i++) {
            eluFonda[i + 4] = resultatsAlbeCombinaison(_etude + "." + (i + 9), _params);
          }
          for (int i = 0; i < 2; i++) {
            eluFonda[i + 6] = resultatsAlbeCombinaison(_etude + "." + (i + 13), _params);
          }
          r.resultatsEluFonda = eluFonda;
        }
        // si amarrage
        else {
          SResultatsCombinaison[] eluFonda = new SResultatsCombinaison[4];
          for (int i = 0; i < 4; i++) {
            eluFonda[i] = resultatsAlbeCombinaison(_etude + "." + (i + 41), _params);
          }
          r.resultatsEluFonda = eluFonda;
        }
      }

      /** ELU acc/ */
      if (_params.lancementCalculs.eluAcc) {
        // si accostage avec d�fense
        if (defense) {
          SResultatsCombinaison[] eluAcc = new SResultatsCombinaison[8];
          for (int i = 0; i < 8; i++) {
            eluAcc[i] = resultatsAlbeCombinaison(_etude + "." + (i + 17), _params);
          }
          r.resultatsEluAcc = eluAcc;
        }// si accostage sans d�fense : on ne lit qu'un fichier sur deux
        else if (accostage) {
          SResultatsCombinaison[] eluAcc = new SResultatsCombinaison[4];
          for (int i = 0; i < 2; i++) {
            eluAcc[i] = resultatsAlbeCombinaison(_etude + "." + (i + 17), _params);
          }
          for (int i = 0; i < 2; i++) {
            eluAcc[i + 2] = resultatsAlbeCombinaison(_etude + "." + (i + 21), _params);
          }
          r.resultatsEluAcc = eluAcc;
        }
        // si amarrage
        else {
          SResultatsCombinaison[] eluAcc = new SResultatsCombinaison[2];
          for (int i = 0; i < 2; i++) {
            eluAcc[i] = resultatsAlbeCombinaison(_etude + "." + (i + 45), _params);
          }
          r.resultatsEluAcc = eluAcc;
        }
      }

      /** ELS rare/ */
      if (_params.lancementCalculs.elsRare) {
        // si accostage avec d�fense
        if (defense) {
          SResultatsCombinaison[] elsRare = new SResultatsCombinaison[8];
          for (int i = 0; i < 8; i++) {
            elsRare[i] = resultatsAlbeCombinaison(_etude + "." + (i + 25), _params);
          }
          r.resultatsElsRare = elsRare;
        }// si accostage sans d�fense : on ne lit qu'un fichier sur deux
        else if (accostage) {
          SResultatsCombinaison[] elsRare = new SResultatsCombinaison[4];
          for (int i = 0; i < 2; i++) {
            elsRare[i] = resultatsAlbeCombinaison(_etude + "." + (i + 25), _params);
          }
          for (int i = 0; i < 2; i++) {
            elsRare[i + 2] = resultatsAlbeCombinaison(_etude + "." + (i + 29), _params);
          }
          r.resultatsElsRare = elsRare;
        }
        // si amarrage
        else {
          SResultatsCombinaison[] elsRare = new SResultatsCombinaison[2];
          for (int i = 0; i < 2; i++) {
            elsRare[i] = resultatsAlbeCombinaison(_etude + "." + (i + 47), _params);
          }
          r.resultatsElsRare = elsRare;
        }
      }

      /** ELS fr�quent/ */
      if (_params.lancementCalculs.elsFreq) {
        // si accostage avec d�fense
        if (defense) {
          SResultatsCombinaison[] elsFreq = new SResultatsCombinaison[8];
          for (int i = 0; i < 8; i++) {
            elsFreq[i] = resultatsAlbeCombinaison(_etude + "." + (i + 33), _params);
          }
          r.resultatsElsFreq = elsFreq;
        }// si accostage sans d�fense : on ne lit qu'un fichier sur deux
        else if (accostage) {
          SResultatsCombinaison[] elsFreq = new SResultatsCombinaison[4];
          for (int i = 0; i < 2; i++) {
            elsFreq[i] = resultatsAlbeCombinaison(_etude + "." + (i + 33), _params);
          }
          for (int i = 0; i < 2; i++) {
            elsFreq[i + 2] = resultatsAlbeCombinaison(_etude + "." + (i + 37), _params);
          }
          r.resultatsElsFreq = elsFreq;
        }
        // si amarrage
        else {
          SResultatsCombinaison[] elsFreq = new SResultatsCombinaison[2];
          for (int i = 0; i < 2; i++) {
            elsFreq[i] = resultatsAlbeCombinaison(_etude + "." + (i + 49), _params);
          }
          r.resultatsElsFreq = elsFreq;
        }
      }
    }

    return r;
  }

  /**
   * lecture du fichier de r�sultats pour une combinaison et r�cup�ration de ces
   * r�sultats .
   * 
   * @param _nomFichier
   * @return les resultats pour la combinaison
   */
  public static SResultatsCombinaison resultatsAlbeCombinaison(final String _nomFichier,
      final SParametresAlbe _params) {
    int[] fmtI;
    final SResultatsCombinaison r = new SResultatsCombinaison();
    try {
      FortranReader fr = new FortranReader(new FileReader("tmp" + System.getProperty("file.separator") +  _nomFichier));
      /** R�sultats g�n�raux. */
      final SResGeneraux rg = new SResGeneraux();
      fmtI = new int[] { 12, 12, 12, 12, 12, 12, 12 };
      // effort statique au choc
      fr.readFields(fmtI);
      rg.effortStatiqueChoc = fr.doubleField(0);
      // d�placement au choc
      rg.deplacementChoc = fr.doubleField(1);
      // d�placement en t�te
      rg.deplacementTete = fr.doubleField(2);
      // �nergie des modules
      rg.energieModules = fr.doubleField(3);
      // Si d�fense
      if (_params.action.typeAction.equals("C") && _params.defense.presenceDefense.equals("O")) {
        // �nergie des d�fenses
        rg.energieDefenses = fr.doubleField(4);
        // �nergie totale
        rg.energieTotale = fr.doubleField(5);
        // d�flexion des d�fenses
        rg.deflexionDefenses = fr.doubleField(6);
      }
      // Si les premi�res valeurs sont �gales � -1, il n'y a pas convergence
      if (rg.effortStatiqueChoc == -1 && rg.deplacementChoc == -1 && rg.deplacementTete == -1
          && rg.energieModules == -1) {
        r.convergence = false;
        r.resultatsGeneraux = null;
      } else {
        r.convergence = true;
        r.resultatsGeneraux = rg;
      }

      /** R�sultats sur les tron�ons. */
      final STroncon[] rt = new STroncon[_params.pieu.nombreTroncons];
      for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
        final STroncon t = new STroncon();
        fr.readFields(fmtI);
        // moment maxi
        t.momentMaxi = fr.doubleField(0);
        // cote du moment
        t.coteMoment = fr.doubleField(1);
        // contrainte maxi
        t.contrainteMaxi = fr.doubleField(2);
        // cote de la contrainte maxi
        t.coteContrainteMaxi = fr.doubleField(3);
        rt[i] = t;
      }
      r.troncons = rt;

      /** R�sultats sur la r�sistance des tron�ons. */
      fmtI = new int[] { 3, 12, 12, 12, 12, 12, 12 };
      final SResistanceTroncon[] listeRt = new SResistanceTroncon[_params.pieu.nombreTroncons];
      for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
        final SResistanceTroncon resT = new SResistanceTroncon();
        fr.readFields(fmtI);
        int nblines = fr.intField(0);
        resT.nombreLignesRes = nblines;
        final SLigneRes[] listelr = new SLigneRes[nblines];
        for (int j = 0; j < resT.nombreLignesRes; j++) {
          final SLigneRes lr = new SLigneRes();
          fr.readFields(fmtI);
          // num�ro de couche
          lr.numCouche = fr.intField(0);
          // cote
          lr.cote = fr.doubleField(1);
          // fl�che
          lr.fleche = fr.doubleField(2);
          // rotation
          lr.rotation = fr.doubleField(3);
          // moment
          lr.moment = fr.doubleField(4);
          // effort tranchant
          lr.effortTranchant = fr.doubleField(5);
          // pression
          lr.pression = fr.doubleField(6);
          listelr[j] = lr;
        }
        resT.resTroncon = listelr;
        listeRt[i] = resT;
      }
      r.resistanceTroncons = listeRt;
    }
    catch (final FileNotFoundException _e2) {
      System.out.println("le fichier r�sultats " + _nomFichier + " est introuvable.");
      System.out.println("le chemin courant est : " + System.getProperties().get("user.dir") );
      System.out.println("V�rifiez l'existance du sous-r�pertoire tmp dans le r�pertoire de Fudaa-Albe");
    }
    catch (final IOException _e1) {
      System.out.println("erreur(s) rencontr�es lors de la lecture du fichier r�sultats "
          + _nomFichier + ".");
      System.out.println("V�rifiez l'existance du sous-r�pertoire tmp dans le r�pertoire de Fudaa-Albe");
    }
    return r;
  }
}
