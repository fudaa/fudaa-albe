/*
 * @creation 19 oct. 06
 * @modification $Date: 2006-10-20 09:12:07 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.dodico.albe;

import java.util.Date;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Une classe serveur pour Albe.
 * @version $Version$
 * @author Sabrina Delattre
 */
public final class ServeurAlbe {
	
	public static void main(final String[] _args){
	    final String nom = (_args.length > 0 ? _args[0] : CDodico.generateName("::albe::ICalculAlbe"));
	    //Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donn�e
	    //Cette M�thode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
	    CDodico.rebind(nom, UsineLib.createService(DCalculAlbe.class));
	    System.out.println("Albe server running... ");
	    System.out.println("Name: " + nom);
	    System.out.println("Date: " + new Date());
	  }

	  private ServeurAlbe() {

	  }

}
