/*
 * @creation 26 janv. 07
 * @modification $Date: 2007-01-30 07:36:36 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Gestionnaire de rendu de cellule pour les tableaux sur les coefficients
 * partiels.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeTableColorCellRenderer extends DefaultTableCellRenderer {

  Object[][] donnees_;

  public AlbeTableColorCellRenderer(Object[][] _donnees) {
    super();
    donnees_ = _donnees;
  }

  public Component getTableCellRendererComponent(JTable _table, Object _value, boolean _isSelected,
      boolean _hasFocus, int _row, int _column) {
    Component comp = super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus,
        _row, _column);

    setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

    ((JLabel) comp).setOpaque(true);

    try {
      if (_column > 0) {
        if (Double.parseDouble(_value.toString()) == (Double
            .parseDouble(donnees_[_row][_column - 1].toString())))
          ((JLabel) comp).setBackground(Color.white);
        else
          ((JLabel) comp).setBackground(Color.cyan);
      }
    }
    catch (final NullPointerException _e1) {
      ((JLabel) comp).setBackground(Color.white);
    }

    return comp;
  }
}
