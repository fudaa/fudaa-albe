package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.corba.albe.SPieu;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;

/**
 * Description de l'onglet 'Caract�ristiques du pieu'.
 * 
 * @author Sabrina Delattre
 */

public class AlbeCaracteristiquesPieuParametres extends AlbeAbstractOnglet {

  JComboBox typePieuCombo_;

  BuPanel panelCenter_;

  String[] typeList_ = { "Tubulaire", "Caisson" };

  BuButton aideButton_;

  /**
   * Panneau s'affichant lorsque 'Tubulaire' est s�lectionn� dans la boite
   * combo.
   */
  AlbeCaracteristiquesPieuTubulaireParametres tubulPanel_;

  /**
   * Panneau s'affichant lorsque 'Caisson' est s�lectionn� dans la boite combo.
   */
  AlbeCaracteristiquesPieuCaissonParametres caissonPanel_;

  /**
   * r�f�rence vers le parent.
   */
  AlbeFilleParametres fp_;

  /**
   * Constructeur.
   * 
   * @param _fp
   */

  public AlbeCaracteristiquesPieuParametres(final AlbeFilleParametres _fp) {

    super(_fp.getImplementation(), _fp, AlbeMsg.URL003);

    fp_ = _fp;

    this.setLayout(new BuVerticalLayout());

    BuPanel panelNord = new BuPanel(new BorderLayout());

    BuPanel typePieuPanel = new BuPanel();
    typePieuPanel.setLayout(new FlowLayout());

    JLabel typePieuLabel = new JLabel("Type de pieu");

    // Initialisation de la boite combo
    typePieuCombo_ = new JComboBox(typeList_);
    typePieuCombo_.addActionListener(this);
    // Initialisation du bouton 'aide'
    aideButton_ = AlbeLib.aideButton();
    aideButton_.addActionListener(this);

    typePieuPanel.add(typePieuLabel);
    typePieuPanel.add(typePieuCombo_);
    typePieuPanel.add(aideButton_);

    panelNord.add(typePieuPanel, BorderLayout.CENTER);
    panelNord.add(aideButton_, BorderLayout.EAST);

    this.add(panelNord);

    CardLayout c3 = new CardLayout();
    panelCenter_ = new BuPanel(c3);
    tubulPanel_ = new AlbeCaracteristiquesPieuTubulaireParametres(this);
    caissonPanel_ = new AlbeCaracteristiquesPieuCaissonParametres(this);
    panelCenter_.add("panTubul", tubulPanel_);
    panelCenter_.add("panCaisson", caissonPanel_);
    this.add(panelCenter_);

  }// Fin constructeur

  /**
   * retourne la valeur du diam�tre du pieu en m�tres (ou la largeur pour un
   * pieu en caisson).
   */

  public double getDiametrePieu() {
    double d = 0;
    if (typePieuCombo_.getSelectedIndex() == 0)
      d = AlbeLib.textFieldDoubleValue(tubulPanel_.diamTroncons_) / 1000;
    else if (typePieuCombo_.getSelectedIndex() == 1)
      d = AlbeLib.textFieldDoubleValue(caissonPanel_.largeurPieu_) / 100;
    return d;
  }

  /**
   * retourne la longueur totale du pieu (somme des longueurs de chaque
   * tron�on).
   */

  public double getLongueurPieu() {

    double longueur = 0;
    JTable table = typePieuCombo_.getSelectedIndex() == 0 ? tubulPanel_.tableTub_
        : caissonPanel_.tableCaisson_;

    for (int i = 0; i < table.getRowCount(); i++) {

      Double d = (Double) table.getValueAt(i, 1);
      if (d == null) {
        return VALEUR_NULLE.value;
      }
      longueur += d.doubleValue();

    }
    return longueur;
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans
   * l'onglet 'Caract�ristiques du pieu'.
   */

  public SPieu getParametresPieu() {
    final SPieu p = (SPieu) AlbeLib.createIDLObject(SPieu.class);

    // Si pieu tubulaire
    if (typePieuCombo_.getSelectedIndex() == 0) {
      p.typePieu = AlbeLib.IDL_TYPE_PIEU_TUBULAIRE;
      p.nombreTroncons = tubulPanel_.getNbTroncons();
      p.diametreTroncons = AlbeLib.textFieldDoubleValue(tubulPanel_.diamTroncons_);
      p.tronconsTubulaire = tubulPanel_.getTroncons();

    }

    // Si pieu en caisson
    else if (typePieuCombo_.getSelectedIndex() == 1) {
      p.typePieu = AlbeLib.IDL_TYPE_PIEU_CAISSON;
      p.nombreTroncons = caissonPanel_.getNbTroncons();
      p.largeurEffectivePieu = AlbeLib.textFieldDoubleValue(caissonPanel_.largeurPieu_);
      p.excentricite = AlbeLib.textFieldDoubleValue(caissonPanel_.excentricite_);
      p.tronconsCaisson = caissonPanel_.getTroncons();

    }

    return p;

  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres dans
   * l'onglet 'Caract�ristique du pieu'.
   * 
   * @param _p structure de donn�es contenant les param�tres (donn�es) �
   *          importer
   */
  public synchronized void setParametresPieu(SPieu _p) {
    if (_p == null) {
      _p = (SPieu) AlbeLib.createIDLObject(SPieu.class);
    }
    // Si pieu tubulaire
    if (_p.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE)) {
      typePieuCombo_.setSelectedIndex(0);
      tubulPanel_.nbTronconsTubCombo_.setSelectedIndex(_p.nombreTroncons - 1);
      if (_p.diametreTroncons != VALEUR_NULLE.value) {
        tubulPanel_.diamTroncons_.setValue(new Double(_p.diametreTroncons));
      }
      tubulPanel_.setTroncons(_p.tronconsTubulaire);
    }

    // Si pieu en caisson
    if (_p.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_CAISSON)) {
      typePieuCombo_.setSelectedIndex(1);
      caissonPanel_.nbTronconsCaissonCombo_.setSelectedIndex(_p.nombreTroncons - 1);
      if (_p.largeurEffectivePieu != VALEUR_NULLE.value) {
        caissonPanel_.largeurPieu_.setValue(new Double(_p.largeurEffectivePieu));
      }
      if (_p.excentricite != VALEUR_NULLE.value) {
        caissonPanel_.excentricite_.setValue(new Double(_p.excentricite));
      }
      caissonPanel_.setTroncons(_p.tronconsCaisson);
    }
  }

  /**
   * 
   */

  public boolean paramsPieuModified(SPieu _p) {

    SPieu p2 = getParametresPieu();
    if (!_p.typePieu.equals(p2.typePieu) || _p.nombreTroncons != p2.nombreTroncons)
      return true;
    if (_p.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE)) {
      if (_p.diametreTroncons != p2.diametreTroncons
          || tubulPanel_.tronconsTubulaireModified(_p.tronconsTubulaire))
        return true;
    } else if (_p.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_CAISSON)) {
      if (_p.largeurEffectivePieu != p2.largeurEffectivePieu || _p.excentricite != p2.excentricite
          || caissonPanel_.tronconsCaissonModified(_p.tronconsCaisson))
        return true;
    }

    return false;
  }

  /**
   * 
   */

  public void actionPerformed(ActionEvent _evt) {

    if (_evt.getSource() == typePieuCombo_) {

      // Si 'Tubulaire' s�lectionn�
      if (typePieuCombo_.getSelectedIndex() == 0) {

        // On stoppe l'�dition du tableau pour un pieu en caisson
        if (caissonPanel_.tableCaisson_.isEditing())
          caissonPanel_.tableCaisson_.getCellEditor().stopCellEditing();

        // Si le panneau pour un pieu en caisson est rempli
        if (!(AlbeLib.emptyField(caissonPanel_.largeurPieu_))
            || !(AlbeLib.emptyTable(caissonPanel_.tableCaisson_, caissonPanel_.modelCaisson_))) {
          AlbeLib.dialogError(getApplication(), AlbeMsg.WAR003);

          // On s�lectionne de nouveau 'Caisson'
          typePieuCombo_.setSelectedIndex(1);
        } else {
          CardLayout c3 = (CardLayout) panelCenter_.getLayout();
          c3.show(panelCenter_, "panTubul");
          majMessages();
        }
      }

      // Si 'Caisson' s�lectionn�
      else if (typePieuCombo_.getSelectedIndex() == 1) {

        // On stoppe l'�dition du tableau pour un pieu tubulaire
        if (tubulPanel_.tableTub_.isEditing())
          tubulPanel_.tableTub_.getCellEditor().stopCellEditing();

        // Si le panneau pour un pieu tubulaire est rempli
        if (!(AlbeLib.emptyField(tubulPanel_.diamTroncons_))
            || !(AlbeLib.emptyTable(tubulPanel_.tableTub_, tubulPanel_.modelTub_))) {
          AlbeLib.dialogWarning(getApplication(), AlbeMsg.WAR004);

          // On s�lectionne de nouveau 'Tubulaire'
          typePieuCombo_.setSelectedIndex(0);
        } else {
          CardLayout c3 = (CardLayout) panelCenter_.getLayout();
          c3.show(panelCenter_, "panCaisson");
          majMessages();
        }
      }

    }

    else if (_evt.getSource() == aideButton_)
      aide();

  }

  /**
   * 
   */

  public ValidationMessages validation() {

    final ValidationMessages vm = new ValidationMessages();

    // Cas d'un pieu tubulaire
    if (typePieuCombo_.getSelectedIndex() == 0)
      vm.add(tubulPanel_.validation());

    // Cas d'un pieu en caisson
    else if (typePieuCombo_.getSelectedIndex() == 1)
      vm.add(caissonPanel_.validation());

    return vm;
  }

}
