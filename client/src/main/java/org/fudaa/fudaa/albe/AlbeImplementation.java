package org.fudaa.fudaa.albe;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.print.PrinterJob;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.List;
import java.util.Map;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.fudaa.dodico.albe.DCalculAlbe;
import org.fudaa.dodico.corba.albe.ICalculAlbe;
import org.fudaa.dodico.corba.albe.ICalculAlbeHelper;
import org.fudaa.dodico.corba.albe.IParametresAlbe;
import org.fudaa.dodico.corba.albe.IParametresAlbeHelper;
import org.fudaa.dodico.corba.albe.IResultatsAlbe;
import org.fudaa.dodico.corba.albe.IResultatsAlbeHelper;
import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.SResultatsAlbe;
import org.fudaa.dodico.corba.albe.SResultatsCombinaison;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.ebli.impression.EbliFillePrevisualisation;
import org.fudaa.ebli.impression.EbliMiseEnPageDialog;
import org.fudaa.ebli.impression.EbliPageable;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.aide.FudaaHelpPanel;
import org.fudaa.fudaa.commun.aide.FudaaHelpHtmlPanel;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.projet.FudaaParamChangeLog;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetInformationsFrame;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuActionRemover;
import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuBrowserFrame;
import com.memoire.bu.BuColumn;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuDesktopPreferencesPanel;
import com.memoire.bu.BuDialogWarning;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLanguagePreferencesPanel;
import com.memoire.bu.BuLookPreferencesPanel;
import com.memoire.bu.BuMainPanel;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuMenuRecentFiles;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuPrinter;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTaskOperation;
import com.memoire.bu.BuTaskView;
import com.memoire.bu.BuToolBar;

/**
 * Impl�mentation de l'application.
 * 
 * @author Sabrina Delattre
 */

public class AlbeImplementation extends FudaaImplementation implements PropertyChangeListener {

  /** le logiciel est une application. */
  public final static boolean IS_APPLICATION = true;

  /** titre du logiciel. */
  public final static String SOFTWARE_TITLE = "Albe";

  /** fen�tre principale de l'application. */
  public static java.awt.Frame FRAME_;

  /** code de calcul Serveur Albe. */
  public static ICalculAlbe SERVEUR_ALBE;

  /** informations sur la connexion courante au serveur Albe. */
  public static IConnexion CONNEXION_ALBE;

  /** nombre de fichiers r�cents � afficher dans le menu Fichier > R�ouvrir. */
  protected final static int RECENT_COUNT = 10;

  /** informations sur le logiciel. */
  protected static BuInformationsSoftware isAlbe_ = new BuInformationsSoftware();

  /** informations sur le document. */
  protected static BuInformationsDocument idAlbe_ = new BuInformationsDocument();

  /** donn�es param�tres � transmettre au serveur de calcul Albe (1). */
  private IParametresAlbe albeParams_;

  /** donn�es r�sultats � r�cup�rer du serveur de calcul Albe (1). */
  private IResultatsAlbe albeResults_;

  /** fen�tre de saisie des donn�es param�tres. */
  protected AlbeFilleParametres fp_;

  /** fen�tre contenant le sch�ma de l'ouvrage. */
  protected AlbeFilleSchemaOuvrage fs_;

  /** fen�tre de lancement des calculs. */
  protected AlbeLancementCalculsFrame fl_;

  /** projet en cours (ouvert). */
  protected FudaaProjet projet_;

  /** fen�tre d'affichage de la note de calculs. */
  protected AlbeFilleNoteDeCalculs fRapport_;

  /**
   * tableaux de fen�tres pour l'affichage des r�sultats combinaison par
   * combinaison.
   */
  protected AlbeFilleResultatsCombinaison[] fResCombi_;

  /**
   * fen�tre d'affichage du tableau r�capitulatif des r�sultats pour l'ELU
   * fondamental.
   */
  protected AlbeResultatsRecapitulatifsBrowserFrame fResEluFonda_;

  /**
   * fen�tre d'affichage du tableau r�capitulatif des r�sultats pour l'ELU
   * accidentel.
   */
  protected AlbeResultatsRecapitulatifsBrowserFrame fResEluAcc_;

  /**
   * fen�tre d'affichage du tableau r�capitulatif des r�sultats pour l'ELS rare.
   */
  protected AlbeResultatsRecapitulatifsBrowserFrame fResElsRare_;

  /**
   * fen�tre d'affichage du tableau r�capitulatif des r�sultats pour l'ELS
   * fr�quent.
   */
  protected AlbeResultatsRecapitulatifsBrowserFrame fResElsFreq_;

  /**
   * fen�tre permettant d'afficher un contenu Html (� utiliser avec showHtml()).
   */
  protected BuBrowserFrame fHtml_;

  /** assistant Fudaa. */
  protected BuAssistant assistant_;

  /** panneau des t�ches en cours. */
  protected BuTaskView taches_;

  /** panneau des messages. */
  protected AlbeParamEventView msgView_;

  /** fen�tre de modification des propri�t�s du projet. */
  protected FudaaProjetInformationsFrame fProprietes_;

  /** fermer flag. */
  protected boolean fermerflag_;

  protected SParametresAlbe paramsAlbe_;

  /** informations sur la version courante du logiciel. */
  static {
    /** titre du logiciel */
    isAlbe_.name = "Albe";
    /** version courante du logiciel */
    isAlbe_.version = "1.1.1";
    /** date de la version courante du logiciel */
    isAlbe_.date = "17/12/08";
    /** informations sur le droits r�serv�s */
    isAlbe_.rights = "CETMEF (c) 2003";
    /** email du responsable du logiciel */
    isAlbe_.contact = "Claire.Guiziou@developpement-durable.gouv.fr";
    /** informations sur la licence de d�veloppement */
    isAlbe_.license = "GPL2";
    /** langage support� */
    isAlbe_.languages = "fr";
    /** logo du logiciel */
    isAlbe_.logo = AlbeResource.ALBE.getIcon("albe-logo");
    /** banni�re du logiciel */
    isAlbe_.banner = AlbeResource.ALBE.getIcon("albe-banner");
    /** auteurs-d�veloppeurs du logiciel */
    isAlbe_.authors = new String[] { "Sabrina Delattre" };
    /** contributeurs au d�veloppement du logiciel */
    isAlbe_.contributors = new String[] { "Equipes Dodico, Ebli, Fudaa et autres" };
    /** r�dacteurs de la documentation du logiciel */
    isAlbe_.documentors = new String[] { "Olivier Soulat" };
    /** testeurs du logiciel */
    isAlbe_.testers = new String[] { "Jean-Jacques Trichet", "Olivier Soulat" };
    isAlbe_.man = FudaaLib.LOCAL_MAN;
    BuPrinter.INFO_LOG = isAlbe_;
    BuPrinter.INFO_DOC = idAlbe_;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return isAlbe_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isAlbe_;
  }

  public BuInformationsDocument getInformationsDocument() {
    try {
      return projet_.getInformationsDocument();
    }
    catch (final NullPointerException _e1) {
      return idAlbe_;
    }
  }

  /**
   * Initialisation du MainPanel.
   */

  public void init() {
    super.init();

    projet_ = null;
    fp_ = null;
    aide_ = null;
    fHtml_ = null;
    try {
      // Titre de la fen�tre
      setTitle(SOFTWARE_TITLE);

      // Barre de menu
      BuMenuBar mb = getMainMenuBar();
      mb.addMenu(buildDonneesMenu(IS_APPLICATION));
      mb.addMenu(buildCalculMenu(IS_APPLICATION));
      mb.addMenu(buildResultatsMenu(IS_APPLICATION));
      //On enl�ve l'item 'Pointeur'
      mb.getMenu(6).remove(3);
      // Barre d'outils
      BuToolBar tb = getMainToolBar();
      tb.addToolButton(BuResource.BU.getString("Donn�es"), "Introduction des donn�es",
          "INTRO_DONNEES", BuResource.BU.getToolIcon("parametre"), false);
      tb.addSeparator();
      tb.addToolButton(BuResource.BU.getString("Ouvrage"), "Sch�ma � l'echelle de l'ouvrage",
          "SCHEMA_OUVRAGE", BuResource.BU.getToolIcon("avantplan"), false);
      tb.addSeparator();
      tb.addToolButton(BuResource.BU.getString("Calc."), "Lancer les calculs",
          "CHOIX_MODE_LANCEMENT_CALCULS", BuResource.BU.getToolIcon("executer"), false);
      tb.addSeparator();
      tb.addToolButton(BuResource.BU.getString("Note"), "Note de calculs", "NOTE_DE_CALCULS",
          BuResource.BU.getToolIcon("index"), false);
      tb.addSeparator();
      tb.addToolButton(BuResource.BU.getString("ELU fonda"),
          "R�capitulatif des r�sultats pour l'ELU fondamental", "RECAP_ELU_FONDA", BuResource.BU
              .getToolIcon("document"), false);
      tb.addToolButton(BuResource.BU.getString("ELU acc"),
          "R�capitulatif des r�sultats pour l'ELU accidentel", "RECAP_ELU_ACC", BuResource.BU
              .getToolIcon("document"), false);
      tb.addToolButton(BuResource.BU.getString("ELS rare"),
          "R�capitulatif des r�sultats pour l'ELS rare", "RECAP_ELS_RARE", BuResource.BU
              .getToolIcon("document"), false);
      tb.addToolButton(BuResource.BU.getString("ELS fr�q"),
          "R�capitulatif des r�sultats pour l'ELS fr�quent", "RECAP_ELS_FREQ", BuResource.BU
              .getToolIcon("document"), false);
      final BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
      if (mr != null) {
        mr.setPreferences(AlbePreferences.ALBE);
        mr.setResource(AlbeResource.ALBE);
        mr.setEnabled(true);
      }

      // Allocation de m�moire pour les tableaux de fen�tres de r�sultats
      fResCombi_ = new AlbeFilleResultatsCombinaison[5];
      // Initialisation des conteneurs principaux de la fen�tre
      BuMainPanel mp = getApp().getMainPanel();
      BuColumn rc = mp.getRightColumn();

      assistant_ = new AlbeAssistant();
      taches_ = new BuTaskView();
      msgView_ = new AlbeParamEventView();
      final BuScrollPane sp2 = AlbeLib.columnScrollPane(msgView_);
      BuPanel p = new BuPanel();
      p.add(AlbeLib.symbol(AlbeLib.getIcon("unitview")));
      final BuScrollPane sp3 = AlbeLib.columnScrollPane(p);

      // Ajout des Panneaux dans les Colonnes Gauches et Droites
      rc.addToggledComponent(BuResource.BU.getString("Unit�s"), "UNITES", sp3, this);
      rc.addToggledComponent(BuResource.BU.getString("Messages"), "MESSAGE", sp2, this);

      // Initialisation du Main panel
      mp.setLogo(isAlbe_.logo);
      mp.setTaskView(taches_);
      // D�finit l'afficheur de message
      setParamEventView(msgView_);

      getMainPanel().getDesktop().addPropertyChangeListener(this);

      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("REOUVRIR", true);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("APERCU", false);
      setEnabledForAction("IMPRIMER", false);
      setEnabledForAction("CALCULS", false);
      setEnabledForAction("CHOIX_MODE_LANCEMENT_CALCULS", false);
      setEnabledForAction("DONNEES", false);
      setEnabledForAction("INRO_DONNEES", false);
      setEnabledForAction("RESULTATS", false);
      setEnabledForAction("RECAPITULATIF_RESULTATS", false);
      setEnabledForAction("RECAP_ELU_FONDA", false);
      setEnabledForAction("RECAP_ELU_ACC", false);
      setEnabledForAction("RECAP_ELS_RARE", false);
      setEnabledForAction("RECAP_ELS_FREQ", false);
      setEnabledForAction("DETAILS_COMBINAISON", false);
      setEnabledForAction("NOTE_DE_CALCULS", false);
      BuActionRemover.removeAction(getMainToolBar(), "COPIER");
      BuActionRemover.removeAction(getMainToolBar(), "COUPER");
      BuActionRemover.removeAction(getMainToolBar(), "COLLER");
      BuActionRemover.removeAction(getMainToolBar(), "DEFAIRE");
      BuActionRemover.removeAction(getMainToolBar(), "REFAIRE");
      BuActionRemover.removeAction(getMainToolBar(), "TOUTSELECTIONNER");
      BuActionRemover.removeAction(getMainToolBar(), "RANGERICONES");
      BuActionRemover.removeAction(getMainToolBar(), "RANGERPALETTES");
      BuActionRemover.removeAction(getMainToolBar(), "POINTEURAIDE");
      BuActionRemover.removeAction(getMainToolBar(), "RECHERCHER");
      BuActionRemover.removeAction(getMainToolBar(), "REMPLACER");
      BuActionRemover.removeAction(getMainToolBar(), "CONNECTER");
    }
    catch (Throwable t) {
      System.err.println("$$$ " + t);
    }
  }// Fin m�thode init()

  /**
   * Initialisation des Activations/D�sactivations des diff�rents Menus.
   */
  protected void actionInit() {
    setEnabledForAction("CREER", true);
    setEnabledForAction("OUVRIR", true);
    setEnabledForAction("REOUVRIR", true);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("QUITTER", true);
    setEnabledForAction("APERCU", false);
    setEnabledForAction("IMPRIMER", true); //
    setEnabledForAction("CALCULS", false);
    setEnabledForAction("CHOIX_MODE_LANCEMENT_CALCULS", false);
    setEnabledForAction("DONNEES", false);
    setEnabledForAction("INRO_DONNEES", false);
    setEnabledForAction("RESULTATS", false);
    setEnabledForAction("RECAPITULATIF_RESULTATS", false);
    setEnabledForAction("RECAP_ELU_FONDA", false);
    setEnabledForAction("RECAP_ELU_ACC", false);
    setEnabledForAction("RECAP_ELS_RARE", false);
    setEnabledForAction("RECAP_ELS_FREQ", false);
    setEnabledForAction("DETAILS_COMBINAISON", false);
    setEnabledForAction("NOTE_DE_CALCULS", false);
  }

  /**
   * d�marrage du logiciel : affichage de l'�cran d'accueil,connexion au serveur
   * Albe, initialisation du projet, des pr�f�rences et des propri�t�s...
   */
  public void start() {
    super.start();
    /** Cr�ation du nouveau Projet */
    projet_ = new FudaaProjet(getApp(), AlbeLib.filtreFichier()/*
                                                                 * ,
                                                                 * AlbeLib.defaultDirectory()
                                                                 */);

    final BuMainPanel mp = getApp().getMainPanel();
    mp.doLayout();
    mp.validate();
    assistant_.addEmitters((Container) getApp());
    // Initialisation des propri�t�s du Projet
    fProprietes_ = new FudaaProjetInformationsFrame(this);
    fProprietes_.setProjet(projet_);
    actionInit();
    // Application des pr�f�rences
    BuPreferences.BU.applyOn(this);
    AlbePreferences.ALBE.applyOn(this);
  }

  /**
   * 
   */
  public FudaaAstucesAbstract getAstuces() {
    return AlbeAstuces.ALBE;
  }

  /**
   * 
   */
  public BuPreferences getApplicationPreferences() {
    return AlbePreferences.ALBE;
  }

  /**
   * stocke le message en tant que message courant du panneau des messages.
   * 
   * @param _v message (�v�nement vue)
   */
  public void setParamEventView(final AlbeParamEventView _v) {
    msgView_ = _v;
  }

  /**
   * retourne le message courant du panneau des messages.
   */
  public AlbeParamEventView getParamEventView() {
    return msgView_;
  }

  /**
   * cr�ation du menu donn�es.
   * 
   * @param _app sp�cifie si le parent est une application ou non pour
   *          l'affichage du menu
   */

  protected BuMenu buildDonneesMenu(boolean _app) {
    final BuMenu m = new BuMenu(BuResource.BU.getString("Donn�es"), "DONNEES");
    m.addMenuItem(BuResource.BU.getString("Introduction des donn�es"), "INTRO_DONNEES",
        BuResource.BU.getIcon("parametre"), false);
    return m;
  }

  /**
   * cr�ation du menu "Calculs" ainsi que de ses �l�ments.
   * 
   * @param _app bool�en qui sp�cifie si le parent est une application ou non
   *          pour le mode d'affichage du menu
   */
  protected BuMenu buildCalculMenu(final boolean _app) {
    // Cr�ation du menu
    final BuMenu r = new BuMenu(BuResource.BU.getString("Calculs"), "CALCULS");
    // Ajout des �l�ments du menu
    r.addMenuItem(BuResource.BU.getString("Lancer les calculs"), "CHOIX_MODE_LANCEMENT_CALCULS",
        BuResource.BU.getIcon("executer"), false);
    return r;
  }

  /**
   * cr�ation du menu "Resultats" ainsi que de ses �l�ments.
   * 
   * @param _app bool�en qui sp�cifie si le parent est une application ou non
   *          pour le mode d'affichage du menu
   */
  protected BuMenu buildResultatsMenu(final boolean _app) {
    // Cr�ation du menu
    final BuMenu r = new BuMenu(BuResource.BU.getString("R�sultats"), "RESULTATS");
    // // Sous menu 'R�capitulatif'
    final BuMenu recap = new BuMenu(BuResource.BU.getString("R�capitulatif"),
        "RECAPITULATIF_RESULTATS", BuResource.BU.getIcon("dossier"));
    recap.addMenuItem(BuResource.BU.getString("ELU fondamental"), "RECAP_ELU_FONDA", BuResource.BU
        .getIcon("document"), false);
    recap.addMenuItem(BuResource.BU.getString("ELU accidentel"), "RECAP_ELU_ACC", BuResource.BU
        .getIcon("document"), false);
    recap.addMenuItem(BuResource.BU.getString("ELS rare"), "RECAP_ELS_RARE", BuResource.BU
        .getIcon("document"), false);
    recap.addMenuItem(BuResource.BU.getString("ELS fr�quent"), "RECAP_ELS_FREQ", BuResource.BU
        .getIcon("document"), false);
    // Ajout des �l�ments au menu
    r.addSubMenu(recap, false);
    r.addMenuItem(BuResource.BU.getString("D�tails pour la combinaison manuelle"),
        "DETAILS_COMBINAISON", BuResource.BU.getIcon("liste"), false);
    r.addMenuItem(BuResource.BU.getString("Note de calculs"), "NOTE_DE_CALCULS", BuResource.BU
        .getIcon("index"), false);
    return r;
  }

  /**
   * lancement du calcul des r�sultats � partir des donn�es saisies par
   * l'utilisateur.
   */

  public void oprCalculer() {
    paramsAlbe_ = fp_.getParametres();
    // R�cup�ration des param�tres de lancement des calculs
    paramsAlbe_.lancementCalculs = fl_.getParametresCalculs();
    // obligatoire pour r�cup�rer les data de la fen�tre de saisies des
    // donn�es.
    projet_.addParam(AlbeResource.PARAMETRES, paramsAlbe_);
    // V�rifie si l'utilisateur est connect� � un serveur
    if (!isConnected()) {
      AlbeLib.dialogError(getApp(), AlbeMsg.ERR009);
      return;
    }
    final BuMainPanel mp = getMainPanel();
    mp.setProgression(0);
    /** Transmission des param�tres */
    mp.setMessage("Transmission des param�tres...");
    final SParametresAlbe par = (SParametresAlbe) projet_.getParam(AlbeResource.PARAMETRES);
    albeParams_ = IParametresAlbeHelper.narrow(SERVEUR_ALBE.parametres(CONNEXION_ALBE));
    albeParams_.parametresAlbe(par);
    projet_.clearResults();
    mp.setProgression(25);
    /** Lancement des calculs */
    mp.setMessage("Calculs en cours...");
    try {
      SERVEUR_ALBE.calcul(CONNEXION_ALBE);
      albeResults_ = IResultatsAlbeHelper.narrow(SERVEUR_ALBE.resultats(CONNEXION_ALBE));
    }
    catch (final Throwable _e) {}
    mp.setProgression(75);
    /** R�cup�ration des r�sultats * */
    mp.setMessage("R�cup�ration des r�sultats...");
    mp.setMessage("Calculs termin�s.");

    if (fl_ != null)
      closeModeLancementCalculsFrame();

    if (fRapport_ != null)
      closeFilleNoteDeCalculs();

    for (int i = 0; i < 5; i++) {
      if (fResCombi_[i] != null)
        closeFilleResCombi(i);
    }

    closeAllBrowserFrame();
    projet_.addResult(AlbeResource.RESULTATS, albeResults_.resultatsAlbe());
    mp.setProgression(100);
    setEnabledForAction("CALCULS", false);
    setEnabledForAction("CHOIX_MODE_LANCEMENT_CALCULS", false);
    setEnabledForAction("RESULTATS", true);
    setEnabledForAction("NOTE_DE_CALCULS", true);

    if (paramsAlbe_.lancementCalculs.modeLancementCalcul == AlbeLib.IDL_LANCEMENT_MANUEL) {
      // D�sactivation du menu 'R�capitulatif'
      setEnabledForAction("RECAPITULATIF_RESULTATS", false);
      // D�sactivation des ic�nes 'R�capitulatif'
      setEnabledForAction("RECAP_ELU_FONDA", false);
      setEnabledForAction("RECAP_ELU_ACC", false);
      setEnabledForAction("RECAP_ELS_RARE", false);
      setEnabledForAction("RECAP_ELS_FREQ", false);
      // Activation du menu 'D�tails pour la combinaison manuelle'
      setEnabledForAction("DETAILS_COMBINAISON", true);
      AlbeLib.dialogMessage(getApp(), AlbeMsg.DLG006);
      // Affichage de la fen�tre de d�tails des r�sultats pour la combinaison
      // manuelle
      resultatsCombiManuelle();
    } else {
      // Activation du menu 'R�capitulatif'
      setEnabledForAction("RECAPITULATIF_RESULTATS", true);
      // Activation/d�sactivation des menus 'R�capitulatif' selon les
      // combinaisons calcul�es
      setEnabledForAction("RECAP_ELU_FONDA", paramsAlbe_.lancementCalculs.eluFonda);
      setEnabledForAction("RECAP_ELU_ACC", paramsAlbe_.lancementCalculs.eluAcc);
      setEnabledForAction("RECAP_ELS_RARE", paramsAlbe_.lancementCalculs.elsRare);
      setEnabledForAction("RECAP_ELS_FREQ", paramsAlbe_.lancementCalculs.elsFreq);
      // D�sactivation du menu 'D�tails pour la combinaison manuelle'
      setEnabledForAction("DETAILS_COMBINAISON", false);
      AlbeLib.dialogMessage(getApp(), AlbeMsg.DLG005);
    }
    mp.setProgression(0);
  }

  /**
   * 
   */

  public boolean paramsModified() {
    if (fp_.parametresModifies(paramsAlbe_))
      return true;
    return false;
  }

  /**
   * affichage de l'action en cours sur la console et execution de l'action.
   * 
   * @param _evt est l'�v�nement ayant d�clench� l'action
   */
  public void actionPerformed(final ActionEvent _evt) {
    // R�cup�ration de la commande de l'action en cours
    String action = _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    // R�cup�ration de la commande et de ses arguments
    final String arg = AlbeLib.actionArgumentStringOnly(action);
    action = AlbeLib.actionStringWithoutArgument(action);
    // Execution de la commande
    if (action.equals("CREER"))
      creer();
    else if (action.equals("OUVRIR"))
      ouvrir(null);
    else if (action.equals("REOUVRIR"))
      ouvrir(arg);
    else if (action.equals("ENREGISTRER"))
      enregistrer();
    else if (action.equals("ENREGISTRERSOUS"))
      enregistrerSous();
    else if (action.equals("FERMER"))
      fermer();
    else if (action.equals("PROPRIETE"))
      proprietes();
    else if (action.equals("INTRO_DONNEES"))
      parametre();
    else if (action.equals("SCHEMA_OUVRAGE"))
      schemaOuvrage();
    else if (action.equals("CHOIX_MODE_LANCEMENT_CALCULS"))
      choisirModeLancementCalculs();
    else if (action.equals("LANCER_CALCULS"))
      calculer();
    else if (action.equals("RECAP_ELU_FONDA"))
      recapitulatifResultatsEluFond();
    else if (action.equals("RECAP_ELU_ACC"))
      recapitulatifResultatsEluAcc();
    else if (action.equals("RECAP_ELS_RARE"))
      recapitulatifResultatsElsRare();
    else if (action.equals("RECAP_ELS_FREQ"))
      recapitulatifResultatsElsFrequent();
    else if (action.equals("DETAILS_COMBINAISON"))
      resultatsCombiManuelle();
    else if (action.equals("NOTE_DE_CALCULS"))
      rapport();
    else if (action.equals("QUITTER"))
      quitter();
    else if (action.equals("PREFERENCES"))
      preferences();
    else if (action.equals("UNITES") || action.equals("ASSISTANT") || action.equals("TACHE")
        || action.equals("MESSAGE"))
      enrouleDeroule(action);
    else if (action.equals("IMPRESSION") || action.equals("PREVISUALISER")
        || action.equals("MISEENPAGE"))
      gestionnaireImpression(action);
    else {
      super.actionPerformed(_evt);
    }

  }

  private void gestionnaireImpression(final String _commande) {
    final JInternalFrame frame = getCurrentInternalFrame();
    EbliPageable target = null;
    if (frame instanceof EbliPageable) {
      target = (EbliPageable) frame;
    } else if (frame instanceof EbliFillePrevisualisation) {
      target = ((EbliFillePrevisualisation) frame).getEbliPageable();
    } else {
      new BuDialogWarning(this, getInformationsSoftware(), FudaaResource.FUDAA
          .getString("Cette fen�tre n'est pas imprimable")).activate();
      return;
    }
    if ("IMPRIMER".equals(_commande)) {
      cmdImprimer(target);
    } else if ("MISEENPAGE".equals(_commande)) {
      cmdMiseEnPage(target);
    } else if ("PREVISUALISER".equals(_commande)) {
      cmdPrevisualisation(target);
    }
  }

  public void cmdImprimer(final EbliPageable _target) {
    final PrinterJob printJob = PrinterJob.getPrinterJob();
    printJob.setPageable(_target);
    if (printJob.printDialog()) {
      try {
        printJob.print();
      }
      catch (final Exception printException) {
        printException.printStackTrace();
      }
    }
  }

  public void cmdMiseEnPage(final EbliPageable _target) {
    new EbliMiseEnPageDialog(_target, getApp(), getInformationsSoftware()).activate();
  }

  /**
   * enroulement / d�roulement d'un panneau visible dans l'une des colonnes du
   * bureau.
   * 
   * @param _panneau : cha�ne de caract�res repr�sentant le panneau � enrouler
   *          ou d�rouler
   */
  private void enrouleDeroule(final String _panneau) {
    final BuColumn rc = getMainPanel().getRightColumn();
    if (_panneau.equals("ASSISTANT") || _panneau.equals("TACHE") || _panneau.equals("MESSAGE")
        || _panneau.equals("UNITES")) {
      rc.toggleComponent(_panneau);
      setCheckedForAction(_panneau, rc.isToggleComponentVisible(_panneau));
    }
  }

  /**
   * affiche la fen�tre des propri�t�s du projet.
   */
  private void proprietes() {
    if (fProprietes_.getDesktopPane() != getMainPanel().getDesktop()) {
      addInternalFrame(fProprietes_);
    } else {
      activateInternalFrame(fProprietes_);
    }
  }

  public void showHtml(final String _title, final String _html) {
    if (fHtml_ == null) {
      fHtml_ = new BuBrowserFrame(this);
    }
    fHtml_.setHtmlSource(_html, _title);
    if (fHtml_.getDesktopPane() != getMainPanel().getDesktop()) {
      addInternalFrame(fHtml_);
    } else {
      activateInternalFrame(fHtml_);
    }
    try {
      fHtml_.setMaximum(true);

    }
    catch (final java.beans.PropertyVetoException _e) {}
    catch (final java.lang.Throwable _e1) {}
  }

  public void showHtml(final String _documentUrl) {
    if (fHtml_ == null) {
     fHtml_ = new BuBrowserFrame(this);
    
    }
    
    fHtml_.setDocumentUrl(_documentUrl);
    if (fHtml_.getDesktopPane() != getMainPanel().getDesktop()) {
      addInternalFrame(fHtml_);
    } else {
      activateInternalFrame(fHtml_);
    }
    try {
      fHtml_.setMaximum(true);
    }
    catch (final java.beans.PropertyVetoException _e) {}
    catch (final java.lang.Throwable _e1) {}
  }

  /**
   * met � jour la possibilit� ou non d'activer les commandes apr�s cr�ation ou
   * ouverture d'un projet.
   */
  public void majCommandesOuvertureProjet() {
    setEnabledForAction("DONNEES", true);
    setEnabledForAction("INTRO_DONNEES", true);
    setEnabledForAction("SCHEMA_OUVRAGE", true);
    setEnabledForAction("CALCULS", false);
    setEnabledForAction("CHOIX_MODE_LANCEMENT_CALCULS", false);
    setEnabledForAction("RESULTATS", false);
    setEnabledForAction("RECAPITULATIF_RESULTATS", false);
    setEnabledForAction("RECAP_ELU_FONDA", false);
    setEnabledForAction("RECAP_ELU_ACC", false);
    setEnabledForAction("RECAP_ELS_RARE", false);
    setEnabledForAction("RECAP_ELS_FREQ", false);
    setEnabledForAction("DETAILS_COMBINAISON", false);
    setEnabledForAction("NOTE_DE_CALCULS", false);
    setEnabledForAction("IMPORT", true);
    setEnabledForAction("EXPORTER", true);
    setEnabledForAction("FERMER", true);
    setEnabledForAction("ENREGISTRER", true);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("PROPRIETE", true);
    setEnabledForAction("IMPRIMER", true);
    setEnabledForAction("EXPORTRES", false);
  }

  /**
   * met � jour la possibilit� ou non d'activer les commandes apr�s fermeture
   * d'un projet.
   */
  public void majCommandesFermetureProjet() {
    setEnabledForAction("DONNEES", false);
    setEnabledForAction("INTRO_DONNEES", false);
    setEnabledForAction("SCHEMA_OUVRAGE", false);
    setEnabledForAction("CALCULS", false);
    setEnabledForAction("CHOIX_MODE_LANCEMENT_CALCULS", false);
    setEnabledForAction("RESULTATS", false);
    setEnabledForAction("RECAPITULATIF_RESULTATS", false);
    setEnabledForAction("RECAP_ELU_FONDA", false);
    setEnabledForAction("RECAP_ELU_ACC", false);
    setEnabledForAction("RECAP_ELS_RARE", false);
    setEnabledForAction("RECAP_ELS_FREQ", false);
    setEnabledForAction("DETAILS_COMBINAISON", false);
    setEnabledForAction("NOTE_DE_CALCULS", false);
    setEnabledForAction("IMPORT", false);
    setEnabledForAction("EXPORTER", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("IMPRIMER", false);
    setEnabledForAction("PROPRIETE", false);
    setEnabledForAction("EXPORTRES", false);

  }

  /**
   * cr�ation d'un projet.
   */
  protected void creer() {
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.creer();
    if (!projet_.estConfigure()) {
      projet_.fermer();
    } else {
      majCommandesOuvertureProjet();
      setTitle(SOFTWARE_TITLE + " - " + projet_.getFichier());
      // affichage de la fen�tre de saisie des donn�es
      parametre();
    }
  }

  /**
   * ouverture d'un projet.
   */
  protected void ouvrir(final String _arg) {
    if (!projet_.estVierge())
      fermer();
    projet_.ouvrir(_arg);
    if (!projet_.estConfigure()) {
      projet_.fermer();
      majCommandesFermetureProjet();
    } else {
      majCommandesOuvertureProjet();
      setTitle(SOFTWARE_TITLE + " - " + projet_.getFichier());
      // affichage de la fen�tre de saisie des donn�es
      parametre();
      fp_.updatePanels();
      fp_.loadValidationMsg();
      if (_arg == null) {
        final String r = projet_.getFichier();
        getMainMenuBar().addRecentFile(r, AlbeLib.FILTRE_FICHIER);
        AlbePreferences.ALBE.writeIniFile();
      }
    }
  }

  /**
   * enregistrement du projet en cours.
   */
  protected void enregistrer() {
    projet_.addParam(AlbeResource.PARAMETRES, fp_.getParametres());
    projet_.enregistre();
    boolean found = false;
    final String r = projet_.getFichier();
    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r.equals(AlbePreferences.ALBE.getStringProperty("file.recent." + i + ".path"))) {
        found = true;
      }
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, AlbeLib.FILTRE_FICHIER);
      AlbePreferences.ALBE.writeIniFile();
    }
    setEnabledForAction("REOUVRIR", true);
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", true);
  }

  /**
   * enregistrement du projet sous un autre nom.
   */
  protected void enregistrerSous() {
    projet_.addParam(AlbeResource.PARAMETRES, fp_.getParametres());
    projet_.enregistreSous();
    boolean found = false;
    final String r = projet_.getFichier();
    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r.equals(AlbePreferences.ALBE.getStringProperty("file.recent." + i + ".path"))) {
        found = true;
      }
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, AlbeLib.FILTRE_FICHIER);
      AlbePreferences.ALBE.writeIniFile();
    }
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", true);
    setTitle(SOFTWARE_TITLE + " - " + projet_.getFichier());
  }

  /**
   * fermeture du projet en cours.
   */
  protected boolean fermer() {
    fermerflag_ = true;
    if (projet_.estConfigure()) {
      final int res = AlbeLib
          .dialogAdvancedConfirmation(
              getApp(),
              "Enregistrer",
              AlbeLib
                  .replaceKeyWordBy(
                      "nomfichier",
                      projet_.getFichier(),
                      "Vous �tes sur le point de fermer le projet <NOMFICHIER>.\nSouhaitez-vous l'enregistrer auparavant ?\n"));

      if (res == JOptionPane.YES_OPTION) {
        enregistrer();
      } else if (res == JOptionPane.CANCEL_OPTION) {
        fermerflag_ = false;
        return false;
      }
    }
    if ((previsuFille_ != null) && previsuFille_.isVisible()) {
      previsuFille_.setVisible(false);
    }
    if ((fProprietes_ != null) && fProprietes_.isVisible()) {
      fProprietes_.setVisible(false);
    }
    final BuDesktop dk = getMainPanel().getDesktop();

    if (fl_ != null)
      // Fermeture de la fen�tre de choix de mode de calculs
      closeModeLancementCalculsFrame();

    if (fRapport_ != null) {
      // Fermeture de la fen�tre d'affichage de la note de calculs
      fRapport_.dispose();
      dk.removeInternalFrame(fRapport_);
      fRapport_.removeInternalFrameListener(this);
      fRapport_ = null;
    }

    if (fs_ != null) {
      // Fermeture de la fen�tre du sch�ma de l'ouvrage
      fs_.dispose();
      dk.removeInternalFrame(fs_);
      fs_.removeInternalFrameListener(this);
      fs_ = null;
    }

    if (fp_ != null) {
      // Fermeture de la fen�tre de saisie des param�tres
      try {
        fp_.setClosed(true);
      }
      catch (final PropertyVetoException _e) {}
      dk.removeInternalFrame(fp_);
      fp_.removeInternalFrameListener(this);
      fp_ = null;
    }

    // Fermeture des fen�tres contenant les tableaux r�capitulatifs des
    // r�sultats
    closeAllBrowserFrame();

    // Fermeture des fen�tres contenant les r�sultats d�taill�s pour une
    // combinaison
    for (int i = 0; i < 5; i++) {
      if (fResCombi_[i] != null) {
        fResCombi_[i].dispose();
        dk.removeInternalFrame(fResCombi_[i]);
        fResCombi_[i].removeInternalFrameListener(this);
        fResCombi_[i] = null;
      }
    }

    if (fHtml_ != null) {
      try {
        fHtml_.setClosed(true);
      }
      catch (final PropertyVetoException e) {}
      dk.removeInternalFrame(fHtml_);
      fHtml_.removeInternalFrameListener(this);
      fHtml_ = null;
      fermerflag_ = false;
    }
    projet_.fermer();
    // on vide le panneau des messages :
    msgView_.removeAllMessages();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    dk.repaint();
    majCommandesFermetureProjet();
    setTitle(SOFTWARE_TITLE);
    return true;
  }

  /**
   * fermeture du logiciel.
   */
  public void quitter() {
    exit();
  }

  /**
   * fermeture de la fen�tre de choix de mode de lancement des calculs.
   */
  protected void closeModeLancementCalculsFrame() {

    fl_.dispose();
    fl_ = null;
  }

  /**
   * fermeture de la fen�tre de note de calculs.
   */
  protected void closeFilleNoteDeCalculs() {

    fRapport_.dispose();
    fRapport_ = null;
  }

  /**
   * fermeture de la fen�tre du sch�ma de l'ouvrage.
   */
  protected void closeFilleSchemaOuvrage() {

    fs_.dispose();
    fs_ = null;
  }

  /**
   * fermeture de la fen�tre des r�sultats combinaison par combinaison.
   * 
   * @param _typeCombi type de la combinaison
   */

  protected void closeFilleResCombi(int _typeCombi) {

    fResCombi_[_typeCombi].dispose();
    fResCombi_[_typeCombi] = null;
  }

  /**
   * fermeture de la fen�tre contenant le tableau r�capitulatif des r�sultats
   * pour une combinaison.
   */
  protected void closeBrowserFrame(AlbeResultatsRecapitulatifsBrowserFrame _f) {
    _f.dispose();
    _f = null;
  }

  /**
   * fermeture des fen�tres contenant les tableaux r�capitulatifs.
   */

  protected void closeAllBrowserFrame() {
    if (fResEluFonda_ != null)
      closeBrowserFrame(fResEluFonda_);

    if (fResEluAcc_ != null)
      closeBrowserFrame(fResEluAcc_);

    if (fResElsRare_ != null)
      closeBrowserFrame(fResElsRare_);

    if (fResElsFreq_ != null)
      closeBrowserFrame(fResElsFreq_);
  }

  /**
   * ouverture de la fen�tre de saisie des param�tres (donn�es).
   */
  protected void parametre() {
    iconiserTout();
    if (fp_ == null) {
      fp_ = new AlbeFilleParametres(getApp(), projet_);
      fp_.addPropertyChangeListener("CALCUL_DISPONIBLE", this);
      fp_.addPropertyChangeListener("RESULTATS_DISPONIBLE", this);
      fp_.addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_) {
            fermer();
          }
        }
      });
      addInternalFrame(fp_);
    } else {
      if (fp_.isClosed())
        addInternalFrame(fp_);
      else
        activateInternalFrame(fp_);
    }
  }

  /**
   * ouverture de la fen�tre contenant le sch�ma de l'ouvrage.
   */
  protected void schemaOuvrage() {
    SParametresAlbe params = fp_.getParametres();
    if (params.geo.coteTeteOuvrage == VALEUR_NULLE.value
        || params.geo.coteDragageBassin == VALEUR_NULLE.value
        || (params.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE) ? params.pieu.tronconsTubulaire[0].longueur
            : params.pieu.tronconsCaisson[0].longueur) == VALEUR_NULLE.value) {
      AlbeLib.dialogMessage(app_, AlbeMsg.DLG009);
      closeFilleSchemaOuvrage();
    } else {
      if (fs_ != null)
        closeFilleSchemaOuvrage();
      fs_ = new AlbeFilleSchemaOuvrage(getApp(), projet_, params);
      fs_.addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_) {
            closeFilleSchemaOuvrage();
          }
        }
      });
      addInternalFrame(fs_);
    }
  }

  /**
   * 
   */
  public AlbeFilleParametres getFilleParametres() {
    return fp_;
  }

  /**
   * 
   */
  public AlbeFilleNoteDeCalculs getFilleNoteDeCalculs() {
    return fRapport_;
  }

  /**
   * affichage de la fen�tre de choix de mode de lancement des calculs.
   */
  public void choisirModeLancementCalculs() {
    iconiserTout();
    if (fl_ == null) {
      fl_ = new AlbeLancementCalculsFrame(getApp(), projet_, fp_);
      fl_.addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_) {
            closeModeLancementCalculsFrame();
          }
        }
      });
      addInternalFrame(fl_);
    }

    else if (fl_.isClosed())
      addInternalFrame(fl_);

    else
      activateInternalFrame(fl_);

  }

  /**
   * ouverture de la fen�tre de la note de calculs.
   */
  protected void rapport() {
    iconiserTout();
    if (fRapport_ == null) {
      fRapport_ = new AlbeFilleNoteDeCalculs(getApp(), projet_);
      fRapport_.addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_) {
            closeFilleNoteDeCalculs();
          }
        }
      });
      fRapport_.setClosable(true);
      addInternalFrame(fRapport_);
      try {
        fRapport_.setMaximizable(true);
        fRapport_.setMaximum(true);
      }
      catch (final java.beans.PropertyVetoException _e1) {}
    } else {
      if (fRapport_.isClosed()) {
        addInternalFrame(fRapport_);
      } else {
        activateInternalFrame(fRapport_);
      }
    }
  }

  /**
   * affichage de la fen�tre des r�sultats combinaison par combinaison.
   */
  protected void resultatsCombi(final String _title, final int _typeCombi, final int _numCombi,
      final int _nbCombi) {
    SResultatsCombinaison resultsCombi = AlbeRes.getResCombi((SResultatsAlbe) projet_
        .getResult(AlbeResource.RESULTATS), _typeCombi, _numCombi);
    // S'il n'y a pas convergence, on n'affiche pas les r�sultats
    if (!resultsCombi.convergence)
      AlbeLib.dialogMessage(app_,
          "Il n'y a pas de r�sultats pour cette combinaison.\nLe calcul n'a pas converg�.");
    else {
      if (fResCombi_[_typeCombi] != null)
        fResCombi_[_typeCombi].dispose();
      fResCombi_[_typeCombi] = new AlbeFilleResultatsCombinaison(getApp(), projet_, _title,
          resultsCombi, _typeCombi, _numCombi, _nbCombi);
      fResCombi_[_typeCombi].addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_) {
            closeFilleResCombi(_typeCombi);
          }
        }
      });
      addInternalFrame(fResCombi_[_typeCombi]);
    }

  }

  /**
   * affichage de la fen�tre des r�sultats pour la combinaison manuelle.
   */
  public void resultatsCombiManuelle() {
    if (paramsModified())
      AlbeLib.dialogWarning(getApp(), AlbeMsg.WAR006);
    resultatsCombi("Combinaison manuelle : r�sultats d�taill�s", AlbeLib.COMBINAISON_MANUELLE, 0, 1);
  }

  /**
   * affichage de la fen�tre contenant le tableau r�capitulatif des r�sultats
   * pour l'ELU fondamental.
   */
  protected void recapitulatifResultatsEluFond() {
    iconiserTout();
    if (paramsModified())
      AlbeLib.dialogWarning(getApp(), AlbeMsg.WAR006);
    if (fResEluFonda_ != null && !fResEluFonda_.isClosed()) {
      activateInternalFrame(fResEluFonda_);
    } else {
      fResEluFonda_ = new AlbeResultatsRecapitulatifsBrowserFrame(app_, projet_, AlbeLib.ELU_FONDA);
      fResEluFonda_.addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_)
            closeBrowserFrame(fResEluFonda_);
        }
      });
      addInternalFrame(fResEluFonda_);
    }
  }

  /**
   * affichage de la fen�tre contenant le tableau r�capitulatif des r�sultats
   * pour l'ELU accidentel.
   */
  protected void recapitulatifResultatsEluAcc() {
    iconiserTout();
    if (paramsModified())
      AlbeLib.dialogWarning(getApp(), AlbeMsg.WAR006);
    if (fResEluAcc_ != null && !fResEluAcc_.isClosed()) {
      activateInternalFrame(fResEluAcc_);
    } else {
      fResEluAcc_ = new AlbeResultatsRecapitulatifsBrowserFrame(app_, projet_, AlbeLib.ELU_ACC);
      fResEluAcc_.addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_)
            closeBrowserFrame(fResEluAcc_);
        }
      });
      addInternalFrame(fResEluAcc_);
    }
  }

  /**
   * affichage de la fen�tre contenant le tableau r�capitulatif des r�sultats
   * pour l'ELS rare.
   */
  protected void recapitulatifResultatsElsRare() {
    iconiserTout();
    if (paramsModified())
      AlbeLib.dialogWarning(getApp(), AlbeMsg.WAR006);
    if (fResElsRare_ != null && !fResElsRare_.isClosed()) {
      activateInternalFrame(fResElsRare_);
    } else {
      fResElsRare_ = new AlbeResultatsRecapitulatifsBrowserFrame(app_, projet_, AlbeLib.ELS_RARE);
      fResElsRare_.addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_)
            closeBrowserFrame(fResElsRare_);
        }
      });
      addInternalFrame(fResElsRare_);
    }
  }

  /**
   * affichage de la fen�tre contenant le tableau r�capitulatif des r�sultats
   * pour l'ELS fr�quent.
   */
  protected void recapitulatifResultatsElsFrequent() {
    iconiserTout();
    if (paramsModified())
      AlbeLib.dialogWarning(getApp(), AlbeMsg.WAR006);
    if (fResElsFreq_ != null && !fResElsFreq_.isClosed()) {
      activateInternalFrame(fResElsFreq_);
    } else {
      fResElsFreq_ = new AlbeResultatsRecapitulatifsBrowserFrame(app_, projet_, AlbeLib.ELS_FREQ);
      fResElsFreq_.addInternalFrameListener(new InternalFrameAdapter() {
        public void internalFrameClosing(final InternalFrameEvent _e) {
          if (!fermerflag_)
            closeBrowserFrame(fResElsFreq_);
        }
      });
      addInternalFrame(fResElsFreq_);
    }
  }

  /**
   * 
   */

  public void propertyChange(final PropertyChangeEvent _evt) {
    final String _p = _evt.getPropertyName();
    final Object _nv = _evt.getNewValue();
    if (_p.equals("CALCUL_DISPONIBLE")) {
      final boolean _v = ((Boolean) _nv).booleanValue();
      setEnabledForAction("CALCULS", _v);
      setEnabledForAction("CHOIX_MODE_LANCEMENT_CALCULS", _v);
    }
  }

  /**
   * cr�ation ou affichage de la fen�tre pour les pr�f�rences.
   */
  protected void buildPreferences(final List _prefs) {
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
    _prefs.add(new BuLookPreferencesPanel(this));
  }

  /**
   * lancement de la t�che de calcul.
   */
  protected void calculer() {
    new BuTaskOperation(this, "Calcul", "oprCalculer").start();
  }

  /**
   * iconise toutes les fen�tres du bureau.
   */
  protected void iconiserTout() {
    try {
      final Component[] _cs = getMainPanel().getDesktop().getComponents();
      for (int i = 0; i < _cs.length; i++) {
        final JInternalFrame _o = (JInternalFrame) _cs[i];
        _o.setIcon(true);
      }
    }
    catch (final Throwable _e) {}
  }

  /**
   * fermeture du logiciel.
   */
  public void exit() {
    if (fermer()) {
      closeConnexions();
      super.exit();
    }
  }

  /**
   * appell�e � la fermeture du logiciel.
   */
  public void finalize() {
    closeConnexions();
  }

  /**
   * permet de confirmer ou non la fermeture du logiciel.
   */
  public boolean confirmExit() {

  /*  int res = AlbeLib.dialogAdvancedConfirmation(getApp(), "Fermeture du logiciel",
        "Voulez-vous vraiment quitter ce logiciel ?");

    if (res == JOptionPane.YES_OPTION)
*/      return true;

  //  return false;
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  protected void clearVariables() {
    CONNEXION_ALBE = null;
    SERVEUR_ALBE = null;
  }

  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVEUR_ALBE, CONNEXION_ALBE);
    return new FudaaDodicoTacheConnexion[] { c };
  }

  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculAlbe.class };
  }

  protected void initConnexions(final Map _r) {
    final FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculAlbe.class);
    CONNEXION_ALBE = c.getConnexion();
    SERVEUR_ALBE = ICalculAlbeHelper.narrow(c.getTache());
  }
}
