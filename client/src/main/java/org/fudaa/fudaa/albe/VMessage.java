package org.fudaa.fudaa.albe;

public class VMessage {
  String onglet_;

  String donnees_;

  String etat_;

  String message_;

  int id_;

  public static String WARNING = "warning";

  public static String FATAL_ERROR = "fatal";

  String type_;

  public VMessage(final int _id, final String _o, final String _d, final String _e,
      final String _m, final String _t) {
    onglet_ = _o;
    donnees_ = _d;
    etat_ = _e;
    message_ = _m;
    type_ = _t;
    id_ = _id;
  }

  public String getOnglet() {
    return onglet_;
  }

  public String getDonnees() {
    return donnees_;
  }

  public String getEtat() {
    return etat_;
  }

  public String getMessage() {
    return message_;
  }

  public String getType() {
    return type_;
  }

  public int getId() {
    return id_;
  }
}
