/*
 * @creation 17 nov. 06
 * @modification $Date: 2007-06-01 10:19:58 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicArrowButton;

import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.SResultatsAlbe;
import org.fudaa.fudaa.albe.AlbeFilleNoteDeCalculs.Settings;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;

/**
 * @version $Version$
 * @author Sabrina Delattre
 */
public class WTree extends JPanel {
  WTreeItem pageDeGarde_, presentation_, rappelHypotheses_, resultats_;

  public AlbeFilleNoteDeCalculs.Settings getConfig() {
    final AlbeFilleNoteDeCalculs.Settings _s = new Settings();
    _s.add(pageDeGarde_.getConfig());
    _s.add(presentation_.getConfig());
    _s.add(rappelHypotheses_.getConfig());
    _s.add(resultats_.getConfig());
    return _s;
  }

  public WTree(final FudaaProjet _project) {
    super();
    final JPanel _c = new JPanel();
    _c.setLayout(new BoxLayout(_c, BoxLayout.Y_AXIS));
    _c.setBorder(new EmptyBorder(10, 10, 10, 10));
    pageDeGarde_ = new WTreeItem("Page de garde", new PageDeGarde(_project));
    pageDeGarde_.toggleItemEnable(true);
    presentation_ = new WTreeItem("Pr�sentation", new Presentation(_project));
    presentation_.toggleItemEnable(true);
    rappelHypotheses_ = new WTreeItem("Rappel des hypoth�ses", new RappelHypotheses(_project));
    rappelHypotheses_.toggleItemEnable(true);
    resultats_ = new WTreeItem("R�sultats des calculs", new Re(_project));
    resultats_.toggleItemEnable(true);

    _c.add(pageDeGarde_);
    _c.add(presentation_);
    _c.add(rappelHypotheses_);
    _c.add(resultats_);

    setLayout(new BorderLayout());

    add(_c, BorderLayout.CENTER);
  }

  public static void main(final String[] _args) {
    final JFrame _f = new JFrame("WTreeItem Sample");
    _f.setBounds(50, 50, 400, 400);
    final WTree _t = new WTree((FudaaProjet) null);
    _f.setContentPane(_t);
    _f.setVisible(true);
    _f.addWindowListener(new WindowAdapter() {
      public void windowClosing(final WindowEvent _e) {
        WSpy.Spy("Config : \n" + (_t.getConfig()).toString() + "\n");
        System.exit(0);
      }
    });
  }

  static abstract class TreeItemGUI extends JPanel {
    int i_;

    GridBagLayout gb_;

    GridBagConstraints gbc_;

    protected String base_;

    protected AlbeFilleNoteDeCalculs.Settings s_;

    protected FudaaProjet project_;

    public TreeItemGUI(final String _base, final FudaaProjet _project) {
      super();
      project_ = _project;
      gb_ = new GridBagLayout();
      gbc_ = new GridBagConstraints();
      base_ = _base;
      setLayout(gb_);
      i_ = 0;
      s_ = new Settings();
      setBorder(new EmptyBorder(3, 3, 3, 3));
    }

    protected abstract AlbeFilleNoteDeCalculs.Settings getConfig();

    protected void add(final JComponent _object, final double _weightx, final double _weighty,
        final int _gridx, final int _gridy, final int _gridwidth, final int _gridheight,
        final int _anchor, final int _fill) {
      gbc_.weightx = _weightx;
      gbc_.weighty = _weighty;
      gbc_.gridx = _gridx;
      gbc_.gridy = _gridy;
      gbc_.gridwidth = _gridwidth;
      gbc_.gridheight = _gridheight;
      gbc_.anchor = _anchor;
      gbc_.fill = _fill;
      gb_.setConstraints(_object, gbc_);
      add(_object);
    }
  }

  static class PageDeGarde extends TreeItemGUI {

    JCheckBox cbLogoAlbe_, cbLogoCetmef_, cbTitre_, cbLogiciel_, cbDate_, cbSommaire_;

    JTextField tfTitre_, tfLogiciel_, tfDate_;

    public PageDeGarde(final FudaaProjet _project) {
      super("PAGEDEGARDE", _project);
      final JLabel infoLabel = new JLabel("Informations");
      add(infoLabel, 1.0, 1.0, 0, i_++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // Logo CETMEF
      cbLogoCetmef_ = new JCheckBox("Logo du CETMEF");
      add(cbLogoCetmef_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      cbLogoCetmef_.setSelected(true);
      // Logo Albe
      cbLogoAlbe_ = new JCheckBox("Logo de Albe");
      add(cbLogoAlbe_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      cbLogoAlbe_.setSelected(true);
      // Nom du logiciel
      cbLogiciel_ = new JCheckBox("Nom du logiciel");
      add(cbLogiciel_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      cbLogiciel_.setSelected(true);
      tfLogiciel_ = new JTextField(AlbeImplementation.SOFTWARE_TITLE);
      add(tfLogiciel_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.HORIZONTAL);
      cbLogiciel_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("logiciel");
        }
      });
      // Titre
      cbTitre_ = new JCheckBox("Titre");
      add(cbTitre_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      cbTitre_.setSelected(true);
      tfTitre_ = new JTextField("Note de calculs");
      add(tfTitre_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      cbTitre_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("titre");
        }
      });
      // Date
      cbDate_ = new JCheckBox("Date");
      add(cbDate_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      cbDate_.setSelected(true);
      final Calendar _cal = new GregorianCalendar();
      _cal.setTime(new Date(System.currentTimeMillis()));
      final int dd = _cal.get(Calendar.DAY_OF_MONTH);
      final int mm = _cal.get(Calendar.MONTH) + 1;
      final int yyyy = _cal.get(Calendar.YEAR);
      final String date;
      if (mm < 10)
        date = dd + "/0" + mm + "/" + yyyy;
      else
        date = dd + "/" + mm + "/" + yyyy;
      tfDate_ = new JTextField(date);
      add(tfDate_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      cbDate_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("date");
        }
      });
      // Sommaire
      cbSommaire_ = new JCheckBox("Sommaire");
      add(cbSommaire_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      cbSommaire_.setSelected(true);
    }

    void toggleInfoItem(final String _item) {
      if (_item.equals("titre"))
        tfTitre_.setVisible(cbTitre_.isSelected());
      else if (_item.equals("logiciel"))
        tfLogiciel_.setVisible(cbLogiciel_.isSelected());
      else if (_item.equals("date"))
        tfDate_.setVisible(cbDate_.isSelected());
      setVisible(false);
      setVisible(true);
    }

    protected AlbeFilleNoteDeCalculs.Settings getConfig() {
      s_.stocke(base_ + "_" + "LOGOALBE", new Boolean(cbLogoAlbe_.isSelected()));
      s_.stocke(base_ + "_" + "LOGOCETMEF", new Boolean(cbLogoCetmef_.isSelected()));
      s_.stocke(base_ + "_" + "SOMMAIRE", new Boolean(cbSommaire_.isSelected()));
      s_.stocke(base_ + "_" + "TITRE", (cbTitre_.isSelected()) ? tfTitre_.getText()
          : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "LOGICIEL", (cbLogiciel_.isSelected()) ? tfLogiciel_.getText()
          : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "DATE", (cbDate_.isSelected()) ? tfDate_.getText()
          : AlbeLib.VAL_NULLE);
      return s_;
    }
  }

  static class Presentation extends TreeItemGUI {

    JCheckBox cbFichier_, cbTitre_, cbAuteur_, cbDateCalculs_, cbAdresse_, cbOrganisation_,
        cbDepartement_, cbVersion_, cbDateCreation_, cbDateModification_, cbCommentaires_;

    JTextField tfFichier_, tfTitre_, tfAuteur_, tfDateCalculs_, tfAdresse_, tfOrganisation_,
        tfDepartement_, tfVersion_, tfDateCreation_, tfDateModification_;

    JTextArea taCommentaires_;

    public Presentation(final FudaaProjet _project) {
      super("PRESENTATION", _project);
      final JLabel infoLabel = new JLabel("Informations");
      add(infoLabel, 1.0, 1.0, 0, i_++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
      // Fichier
      cbFichier_ = new JCheckBox("Fichier");
      add(cbFichier_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      cbFichier_.setSelected(true);
      tfFichier_ = new JTextField(project_.getFichier());
      tfFichier_.setVisible(false);
      add(tfFichier_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.HORIZONTAL);
      cbFichier_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("fichier");
        }
      });
      // Titre du projet
      cbTitre_ = new JCheckBox("Titre du projet");
      add(cbTitre_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      cbTitre_.setSelected(true);
      tfTitre_ = new JTextField(project_.getInformations().titre);
      tfTitre_.setVisible(false);
      add(tfTitre_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
      cbTitre_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("titre");
        }
      });
      // Auteur
      cbAuteur_ = new JCheckBox("Auteur");
      add(cbAuteur_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      cbAuteur_.setSelected(true);
      tfAuteur_ = new JTextField(project_.getInformations().auteur);
      tfAuteur_.setVisible(false);
      add(tfAuteur_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.HORIZONTAL);
      cbAuteur_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("auteur");
        }
      });
      // Date de r�alisation des calculs
      cbDateCalculs_ = new JCheckBox("Date de r�alisation des calculs");
      add(cbDateCalculs_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbDateCalculs_.setSelected(true);
      final Calendar _cal = new GregorianCalendar();
      _cal.setTime(new Date(System.currentTimeMillis()));
      final int _dd = _cal.get(Calendar.DAY_OF_MONTH);
      final int _mm = _cal.get(Calendar.MONTH) + 1;
      final int _yyyy = _cal.get(Calendar.YEAR);
      final int _hh = _cal.get(Calendar.HOUR_OF_DAY);
      final int _min = _cal.get(Calendar.MINUTE);
      if (_min < 10)
        tfDateCalculs_ = new JTextField(_dd + "/" + _mm + "/" + _yyyy + ", " + _hh + ":" + "0"
            + _min);
      else
        tfDateCalculs_ = new JTextField(_dd + "/" + _mm + "/" + _yyyy + ", " + _hh + ":" + _min);
      tfDateCalculs_.setVisible(false);
      add(tfDateCalculs_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.HORIZONTAL);
      cbDateCalculs_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("date_calculs");
        }
      });
      // Adresse courriel
      cbAdresse_ = new JCheckBox("Adresse courriel");
      add(cbAdresse_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      cbAdresse_.setSelected(true);
      tfAdresse_ = new JTextField(project_.getInformations().email);
      tfAdresse_.setVisible(false);
      add(tfAdresse_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.HORIZONTAL);
      cbAdresse_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("adresse");
        }
      });
      // Organisation
      cbOrganisation_ = new JCheckBox("Organisation");
      add(cbOrganisation_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbOrganisation_.setSelected(true);
      tfOrganisation_ = new JTextField(project_.getInformations().organisation);
      tfOrganisation_.setVisible(false);
      add(tfOrganisation_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.HORIZONTAL);
      cbOrganisation_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("organisation");
        }
      });
      // D�partement
      cbDepartement_ = new JCheckBox("D�partement");
      add(cbDepartement_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbDepartement_.setSelected(true);
      tfDepartement_ = new JTextField(project_.getInformations().departement);
      tfDepartement_.setVisible(false);
      add(tfDepartement_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.HORIZONTAL);
      cbDepartement_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("departement");
        }
      });
      // Version
      cbVersion_ = new JCheckBox("Version");
      add(cbVersion_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      cbVersion_.setSelected(true);
      tfVersion_ = new JTextField(project_.getInformationsDocument().version);
      tfVersion_.setVisible(false);
      add(tfVersion_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.HORIZONTAL);
      cbVersion_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("version");
        }
      });
      // Date de cr�ation
      cbDateCreation_ = new JCheckBox("Date de cr�ation");
      add(cbDateCreation_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbDateCreation_.setSelected(true);
      tfDateCreation_ = new JTextField(project_.getInformations().dateCreation);
      tfDateCreation_.setVisible(false);
      add(tfDateCreation_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.HORIZONTAL);
      cbDateCreation_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("date_creation");
        }
      });
      // Date de modification
      cbDateModification_ = new JCheckBox("Date de modification");
      add(cbDateModification_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbDateModification_.setSelected(true);
      tfDateModification_ = new JTextField(project_.getInformations().dateModif);
      tfDateModification_.setVisible(false);
      add(tfDateModification_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.HORIZONTAL);
      cbDateModification_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("date_modif");
        }
      });
      // Commentaires
      cbCommentaires_ = new JCheckBox("Commentaires");
      add(cbCommentaires_, 0.0, 1.0, 0, i_, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbCommentaires_.setSelected(true);
      taCommentaires_ = new JTextArea(project_.getInformations().commentaire);
      taCommentaires_.setVisible(false);
      add(taCommentaires_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.HORIZONTAL);
      cbCommentaires_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("commentaires");
        }
      });

      toggleInfoItem("fichier");
      toggleInfoItem("titre");
      toggleInfoItem("auteur");
      toggleInfoItem("date_calculs");
      toggleInfoItem("adresse");
      toggleInfoItem("organisation");
      toggleInfoItem("departement");
      toggleInfoItem("version");
      toggleInfoItem("date_creation");
      toggleInfoItem("date_modif");
      toggleInfoItem("commentaires");
    }

    void toggleInfoItem(final String _item) {
      if (_item.equals("fichier"))
        tfFichier_.setVisible(cbFichier_.isSelected());
      else if (_item.equals("titre"))
        tfTitre_.setVisible(cbTitre_.isSelected());
      else if (_item.equals("auteur"))
        tfAuteur_.setVisible(cbAuteur_.isSelected());
      else if (_item.equals("date_calculs"))
        tfDateCalculs_.setVisible(cbDateCalculs_.isSelected());
      else if (_item.equals("adresse"))
        tfAdresse_.setVisible(cbAdresse_.isSelected());
      else if (_item.equals("organisation"))
        tfOrganisation_.setVisible(cbOrganisation_.isSelected());
      else if (_item.equals("departement"))
        tfDepartement_.setVisible(cbDepartement_.isSelected());
      else if (_item.equals("version"))
        tfVersion_.setVisible(cbVersion_.isSelected());
      else if (_item.equals("date_creation"))
        tfDateCreation_.setVisible(cbDateCreation_.isSelected());
      else if (_item.equals("date_modif"))
        tfDateModification_.setVisible(cbDateModification_.isSelected());
      else if (_item.equals("commentaires"))
        taCommentaires_.setVisible(cbCommentaires_.isSelected());

      setVisible(false);
      setVisible(true);
    }

    protected AlbeFilleNoteDeCalculs.Settings getConfig() {
      s_.stocke(base_ + "_" + "FICHIER", (cbFichier_.isSelected()) ? tfFichier_.getText()
          : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "TITRE", (cbTitre_.isSelected()) ? tfTitre_.getText()
          : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "AUTEUR", (cbAuteur_.isSelected()) ? tfAuteur_.getText()
          : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "DATECALCULS", (cbDateCalculs_.isSelected()) ? tfDateCalculs_
          .getText() : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "ADRESSE", (cbAdresse_.isSelected()) ? tfAdresse_.getText()
          : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "ORGANISATION", (cbOrganisation_.isSelected()) ? tfOrganisation_
          .getText() : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "DEPARTEMENT", (cbDepartement_.isSelected()) ? tfDepartement_
          .getText() : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "VERSION", (cbVersion_.isSelected()) ? tfVersion_.getText()
          : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "DATECREATION", (cbDateCreation_.isSelected()) ? tfDateCreation_
          .getText() : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "DATEMODIFICATION",
          (cbDateModification_.isSelected()) ? tfDateModification_.getText() : AlbeLib.VAL_NULLE);
      s_.stocke(base_ + "_" + "COMMENTAIRES", (cbCommentaires_.isSelected()) ? taCommentaires_
          .getText() : AlbeLib.VAL_NULLE);
      return s_;
    }

  }// Fin classe Presentation

  /**
   * 
   */
  static class RappelHypotheses extends TreeItemGUI {

    JCheckBox cbOuvrage_, cbDonnees_, cbSchema_, cbCouchesSol_, cbActionsOuvrage_,
        cbCoeffPartiels_, cbCoeffMateriauxSol_, cbCoeffModele_, cbCriteresDim_,
        cbLancementCalculs_;

    public RappelHypotheses(final FudaaProjet _project) {
      super("RAPPELHYPOTHESES", _project);

      // Ouvrage
      cbOuvrage_ = new JCheckBox("Ouvrage");
      add(cbOuvrage_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST, GridBagConstraints.VERTICAL);
      cbOuvrage_.setSelected(true);
      cbOuvrage_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("ouvrage");
        }
      });

      // //Donn�es de l'ouvrage
      cbDonnees_ = new JCheckBox("Donn�es");
      add(cbDonnees_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
      cbDonnees_.setSelected(true);

      // //Sch�ma de l'ouvrage
      cbSchema_ = new JCheckBox("Sch�ma de l'ouvrage");
      add(cbSchema_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
      cbSchema_.setSelected(true);

      // Couches de sol
      cbCouchesSol_ = new JCheckBox("Couches de sol");
      add(cbCouchesSol_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbCouchesSol_.setSelected(true);

      // Actions sur l'ouvrage
      cbActionsOuvrage_ = new JCheckBox("Actions sur l'ouvrage");
      add(cbActionsOuvrage_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbActionsOuvrage_.setSelected(true);

      // Coefficients partiels
      cbCoeffPartiels_ = new JCheckBox("Coefficients partiels");
      add(cbCoeffPartiels_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbCoeffPartiels_.setSelected(true);
      cbCoeffPartiels_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleInfoItem("coefficients partiels");
        }
      });

      // // Mat�riaux et sol
      cbCoeffMateriauxSol_ = new JCheckBox("Mat�riaux et sol");
      cbCoeffMateriauxSol_.setSelected(true);
      add(cbCoeffMateriauxSol_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.CENTER,
          GridBagConstraints.BOTH);

      // // Coefficients de mod�le
      cbCoeffModele_ = new JCheckBox("Coefficients de mod�le");
      cbCoeffModele_.setSelected(true);
      add(cbCoeffModele_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.CENTER,
          GridBagConstraints.BOTH);

      // // Crit�res de dimensionnement
      cbCriteresDim_ = new JCheckBox("Crit�res de dimensionnement");
      cbCriteresDim_.setSelected(true);
      add(cbCriteresDim_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.CENTER,
          GridBagConstraints.BOTH);

      // Lancement des calculs
      cbLancementCalculs_ = new JCheckBox("Lancement des calculs");
      add(cbLancementCalculs_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbLancementCalculs_.setSelected(true);
    }

    void toggleInfoItem(final String _item) {
      if (_item.equals("ouvrage")) {
        cbDonnees_.setEnabled(cbOuvrage_.isSelected());
        cbSchema_.setEnabled(cbOuvrage_.isSelected());
      } else if (_item.equals("coefficients partiels")) {
        cbCoeffMateriauxSol_.setEnabled(cbCoeffPartiels_.isSelected());
        cbCoeffModele_.setEnabled(cbCoeffPartiels_.isSelected());
        cbCriteresDim_.setEnabled(cbCoeffPartiels_.isSelected());
      }
    }

    protected AlbeFilleNoteDeCalculs.Settings getConfig() {
      s_.stocke(base_ + "_" + "OUVRAGE", new Boolean(cbOuvrage_.isSelected()));
      s_.stocke(base_ + "_" + "OUVRAGE_DONNEES", new Boolean(cbDonnees_.isSelected()));
      s_.stocke(base_ + "_" + "OUVRAGE_SCHEMA", new Boolean(cbSchema_.isSelected()));
      s_.stocke(base_ + "_" + "SOL", new Boolean(cbCouchesSol_.isSelected()));
      s_.stocke(base_ + "_" + "ACTIONSOUVRAGE", new Boolean(cbActionsOuvrage_.isSelected()));
      s_.stocke(base_ + "_" + "COEFFPARTIELS", new Boolean(cbCoeffPartiels_.isSelected()));
      s_.stocke(base_ + "_" + "COEFFPARTIELS_MATERIAUXETSOL", new Boolean(cbCoeffMateriauxSol_
          .isSelected()));
      s_.stocke(base_ + "_" + "COEFFPARTIELS_MODELE", new Boolean(cbCoeffModele_.isSelected()));
      s_
          .stocke(base_ + "_" + "COEFFPARTIELS_CRITERESDIM", new Boolean(cbCriteresDim_
              .isSelected()));
      s_.stocke(base_ + "_" + "LANCEMENTCALCULS", new Boolean(cbLancementCalculs_.isSelected()));

      return s_;
    }
  }// Fin classe RappelHypotheses

  /**
   * 
   */
  static class Re extends TreeItemGUI {

    JCheckBox cbTableauxRecapitulatifs_, cbResDetailles_, cbCombiEtudiee_, cbResumeRes_,
        cbTableauDetaille_, cbGraphique_;

    BasicArrowButton baEluFonda_, baEluAcc_, baElsRare_, baElsFreq_;

    BuPanel barreEluFonda_, barreEluAcc_, barreElsRare_, barreElsFreq_;

    boolean expandedEluFonda_, expandedEluAcc_, expandedElsRare_, expandedElsFreq_;

    BuPanel contentEluFonda_, contentEluAcc_, contentElsRare_, contentElsFreq_;

    JCheckBox cbGraphDeformee_, cbGraphMmts_, cbGraphEfforts_, cbGraphButee_, cbGraphDefense_;

    // Tableaux de cases � cocher pour chaque type de combinaison
    JCheckBox[] tabCbEluFonda_, tabCbEluAcc_, tabCbElsRare_, tabCbElsFreq_;

    JLabel infoLabel_;

    // Nombre de combinaisons pour l'ELU fondamental
    int nbCombiEluFonda_;

    // Nombre de combinaisons pour les autres types (ELU acc, ELS rare, ELS
    // fr�q)
    int nbCombiAutre_;

    SParametresAlbe params_;

    SResultatsAlbe results_;

    public Re(final FudaaProjet _project) {
      super("RESULTATS", _project);
      // R�cup�ration des param�tres
      params_ = (SParametresAlbe) project_.getParam(AlbeResource.PARAMETRES);
      results_ = (SResultatsAlbe) project_.getResult(AlbeResource.RESULTATS);

      // Nombre de combinaisons
      if (params_.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)) {
        if (params_.defense.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI)) {
          nbCombiEluFonda_ = 16;
          nbCombiAutre_ = 8;
        } else {
          nbCombiEluFonda_ = 8;
          nbCombiAutre_ = 4;
        }
      } else {
        nbCombiEluFonda_ = 4;
        nbCombiAutre_ = 2;
      }

      // Allocation m�moire pour les tableaux de cases � cocher
      tabCbEluFonda_ = new JCheckBox[nbCombiEluFonda_];
      tabCbEluAcc_ = new JCheckBox[nbCombiAutre_];
      tabCbElsRare_ = new JCheckBox[nbCombiAutre_];
      tabCbElsFreq_ = new JCheckBox[nbCombiAutre_];

      // Tableaux r�capitulatifs
      cbTableauxRecapitulatifs_ = new JCheckBox("Tableaux r�capitulatifs");
      add(cbTableauxRecapitulatifs_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbTableauxRecapitulatifs_.setSelected(true);

      // R�sultats d�taill�s
      cbResDetailles_ = new JCheckBox("R�sultats d�taill�s");
      add(cbResDetailles_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL);
      cbResDetailles_.setSelected(true);
      cbResDetailles_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleItemView("resultats detailles");
        }
      });

      // Label 'Choix de la (des) combinaison(s)'
      infoLabel_ = new JLabel("Choix de la (des) combinaison(s) :");
      infoLabel_.setForeground(Color.blue);
      if (params_.lancementCalculs.modeLancementCalcul != AlbeLib.IDL_LANCEMENT_MANUEL) {
        add(infoLabel_, 1.0, 1.0, 0, i_++, 2, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);

        // Cases ELU fonda
        baEluFonda_ = new BasicArrowButton(SwingConstants.SOUTH);
        JLabel eluFondaLabel = new JLabel("  ELU fondamental");
        eluFondaLabel.setForeground(Color.blue);
        barreEluFonda_ = new BuPanel(new BorderLayout());
        barreEluFonda_.add(baEluFonda_, BorderLayout.WEST);
        barreEluFonda_.add(eluFondaLabel, BorderLayout.CENTER);
        add(barreEluFonda_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL);
        contentEluFonda_ = new BuPanel(new BuVerticalLayout());
        if (params_.lancementCalculs.eluFonda) {
          for (int i = 1; i <= nbCombiEluFonda_; i++) {
            tabCbEluFonda_[i - 1] = new JCheckBox(String.valueOf(i));
            contentEluFonda_.add(tabCbEluFonda_[i - 1]);
          }
          add(contentEluFonda_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL);
          contentEluFonda_.setVisible(false);
          baEluFonda_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent _evt) {
              toggleItemView("elu fonda");
            }
          });
          // S'il n'y a pas convergence pour une combinaison, on d�sactive la
          // case � cocher correpondante
          for (int i = 0; i < nbCombiEluFonda_; i++) {
            if (!results_.resultatsEluFonda[i].convergence)
              tabCbEluFonda_[i].setEnabled(false);
          }
        } else
          eluFondaLabel.setEnabled(false);

        // Cases ELU acc
        baEluAcc_ = new BasicArrowButton(SwingConstants.SOUTH);
        JLabel eluAccLabel = new JLabel("  ELU accidentel");
        eluAccLabel.setForeground(Color.blue);
        barreEluAcc_ = new BuPanel(new BorderLayout());
        barreEluAcc_.add(baEluAcc_, BorderLayout.WEST);
        barreEluAcc_.add(eluAccLabel, BorderLayout.CENTER);
        add(barreEluAcc_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL);
        contentEluAcc_ = new BuPanel(new BuVerticalLayout());
        if (params_.lancementCalculs.eluAcc) {
          for (int i = 1; i <= nbCombiAutre_; i++) {
            tabCbEluAcc_[i - 1] = new JCheckBox(String.valueOf(i));
            contentEluAcc_.add(tabCbEluAcc_[i - 1]);
          }
          add(contentEluAcc_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL);
          contentEluAcc_.setVisible(false);
          baEluAcc_.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent _e) {
              toggleItemView("elu acc");
            }
          });
          for (int i = 0; i < nbCombiAutre_; i++) {
            if (!results_.resultatsEluAcc[i].convergence)
              tabCbEluAcc_[i].setEnabled(false);
          }
        } else
          eluAccLabel.setEnabled(false);

        // Cases ELS rare
        baElsRare_ = new BasicArrowButton(SwingConstants.SOUTH);
        JLabel elsRareLabel = new JLabel("  ELS rare");
        elsRareLabel.setForeground(Color.blue);
        barreElsRare_ = new BuPanel(new BorderLayout());
        barreElsRare_.add(baElsRare_, BorderLayout.WEST);
        barreElsRare_.add(elsRareLabel, BorderLayout.CENTER);
        add(barreElsRare_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL);
        contentElsRare_ = new BuPanel(new BuVerticalLayout());
        if (params_.lancementCalculs.elsRare) {
          for (int i = 1; i <= nbCombiAutre_; i++) {
            tabCbElsRare_[i - 1] = new JCheckBox(String.valueOf(i));
            contentElsRare_.add(tabCbElsRare_[i - 1]);
          }
          add(contentElsRare_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL);
          contentElsRare_.setVisible(false);
          baElsRare_.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent _e) {
              toggleItemView("els rare");
            }
          });
          for (int i = 0; i < nbCombiAutre_; i++) {
            if (!results_.resultatsElsRare[i].convergence)
              tabCbElsRare_[i].setEnabled(false);
          }
        } else
          elsRareLabel.setEnabled(false);

        // Cases ELS fr�q
        baElsFreq_ = new BasicArrowButton(SwingConstants.SOUTH);
        JLabel elsFreqLabel = new JLabel("  ELS fr�quent");
        elsFreqLabel.setForeground(Color.blue);
        barreElsFreq_ = new BuPanel(new BorderLayout());
        barreElsFreq_.add(baElsFreq_, BorderLayout.WEST);
        barreElsFreq_.add(elsFreqLabel, BorderLayout.CENTER);
        add(barreElsFreq_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL);
        contentElsFreq_ = new BuPanel(new BuVerticalLayout());

        if (params_.lancementCalculs.elsFreq) {
          for (int i = 1; i <= nbCombiAutre_; i++) {
            tabCbElsFreq_[i - 1] = new JCheckBox(String.valueOf(i));
            contentElsFreq_.add(tabCbElsFreq_[i - 1]);
          }
          add(contentElsFreq_, 0.0, 1.0, 0, i_++, 1, 1, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL);
          contentElsFreq_.setVisible(false);
          baElsFreq_.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent _e) {
              toggleItemView("els freq");
            }
          });
          for (int i = 0; i < nbCombiAutre_; i++) {
            if (!results_.resultatsElsFreq[i].convergence)
              tabCbElsFreq_[i].setEnabled(false);
          }
        } else
          elsFreqLabel.setEnabled(false);
      }

      // // Identification de la combinaison �tudi�e
      cbCombiEtudiee_ = new JCheckBox("Identification de la combinaison");
      cbCombiEtudiee_.setSelected(true);
      add(cbCombiEtudiee_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.CENTER,
          GridBagConstraints.BOTH);

      // // R�sum� des r�sultats
      cbResumeRes_ = new JCheckBox("R�sum� des r�sultats");
      cbResumeRes_.setSelected(true);
      add(cbResumeRes_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH);

      // // Tableau d�taill�
      cbTableauDetaille_ = new JCheckBox("Tableau d�taill�");
      cbTableauDetaille_.setSelected(true);
      add(cbTableauDetaille_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.CENTER,
          GridBagConstraints.BOTH);

      // // Graphiques
      cbGraphique_ = new JCheckBox("Graphiques");
      cbGraphique_.setSelected(true);
      add(cbGraphique_, 1.0, 1.0, 1, i_++, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
      cbGraphique_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          toggleItemView("graphiques");
        }
      });

      // // // Courbe de la d�form�e de l'ouvrage
      cbGraphDeformee_ = new JCheckBox("Courbe de la d�form�e de l'ouvrage");
      cbGraphDeformee_.setSelected(true);
      add(cbGraphDeformee_, 1.0, 1.0, 2, i_++, 1, 1, GridBagConstraints.CENTER,
          GridBagConstraints.BOTH);
      // // // Courbe des moments
      cbGraphMmts_ = new JCheckBox("Courbe des moments");
      cbGraphMmts_.setSelected(true);
      add(cbGraphMmts_, 1.0, 1.0, 2, i_++, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
      // // // Courbe des efforts tranchants
      cbGraphEfforts_ = new JCheckBox("Courbe des efforts tranchants");
      cbGraphEfforts_.setSelected(true);
      add(cbGraphEfforts_, 1.0, 1.0, 2, i_++, 1, 1, GridBagConstraints.CENTER,
          GridBagConstraints.BOTH);
      // // // Courbe de mobilisation de la but�e
      cbGraphButee_ = new JCheckBox("Courbe de mobilisation de la but�e");
      cbGraphButee_.setSelected(true);
      add(cbGraphButee_, 1.0, 1.0, 2, i_++, 1, 1, GridBagConstraints.CENTER,
          GridBagConstraints.BOTH);
      // // // Courbe de d�fense
      cbGraphDefense_ = new JCheckBox("Courbe d�flexion-r�action de la d�fense");
      if (params_.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)
          && params_.defense.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI)) {
        cbGraphDefense_.setSelected(true);
        add(cbGraphDefense_, 1.0, 1.0, 2, i_++, 1, 1, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH);
      }
    }

    protected void toggleItemView(String _item) {

      if (_item.equals("resultats detailles")) {
        if (params_.lancementCalculs.modeLancementCalcul != AlbeLib.IDL_LANCEMENT_MANUEL) {
          infoLabel_.setVisible(cbResDetailles_.isSelected());
          barreEluFonda_.setVisible(cbResDetailles_.isSelected());
          barreEluAcc_.setVisible(cbResDetailles_.isSelected());
          barreElsRare_.setVisible(cbResDetailles_.isSelected());
          barreElsFreq_.setVisible(cbResDetailles_.isSelected());
          if (cbResDetailles_.isSelected()) {
            contentEluFonda_.setVisible(expandedEluFonda_);
            contentEluAcc_.setVisible(expandedEluAcc_);
            contentElsRare_.setVisible(expandedElsRare_);
            contentElsFreq_.setVisible(expandedElsFreq_);
          } else {
            contentEluFonda_.setVisible(false);
            contentEluAcc_.setVisible(false);
            contentElsRare_.setVisible(false);
            contentElsFreq_.setVisible(false);
          }
        }
        cbCombiEtudiee_.setVisible(cbResDetailles_.isSelected());
        cbResumeRes_.setVisible(cbResDetailles_.isSelected());
        cbTableauDetaille_.setVisible(cbResDetailles_.isSelected());
        cbGraphique_.setVisible(cbResDetailles_.isSelected());
        cbGraphDeformee_.setVisible(cbResDetailles_.isSelected());
        cbGraphMmts_.setVisible(cbResDetailles_.isSelected());
        cbGraphEfforts_.setVisible(cbResDetailles_.isSelected());
        cbGraphButee_.setVisible(cbResDetailles_.isSelected());
        cbGraphDefense_.setVisible(cbResDetailles_.isSelected());
      }

      else if (_item.equals("elu fonda")) {
        if (params_.lancementCalculs.eluFonda) {
          contentEluFonda_.setVisible(!expandedEluFonda_);
          expandedEluFonda_ = !expandedEluFonda_;
        }
      } else if (_item.equals("elu acc")) {
        if (params_.lancementCalculs.eluAcc) {
          contentEluAcc_.setVisible(!expandedEluAcc_);
          expandedEluAcc_ = !expandedEluAcc_;
        }
      } else if (_item.equals("els rare")) {
        if (params_.lancementCalculs.elsRare) {
          contentElsRare_.setVisible(!expandedElsRare_);
          expandedElsRare_ = !expandedElsRare_;
        }
      } else if (_item.equals("els freq")) {
        if (params_.lancementCalculs.elsFreq) {
          contentElsFreq_.setVisible(!expandedElsFreq_);
          expandedElsFreq_ = !expandedElsFreq_;
        }
      } else if (_item.equals("graphiques")) {
        cbGraphDeformee_.setEnabled(cbGraphique_.isSelected());
        cbGraphMmts_.setEnabled(cbGraphique_.isSelected());
        cbGraphEfforts_.setEnabled(cbGraphique_.isSelected());
        cbGraphButee_.setEnabled(cbGraphique_.isSelected());
        cbGraphDefense_.setEnabled(cbGraphique_.isSelected());
      }
    }

    protected AlbeFilleNoteDeCalculs.Settings getConfig() {
      s_.stocke(base_ + "_" + "TABLEAUXRECAPITULATIFS", new Boolean(cbTableauxRecapitulatifs_
          .isSelected()));
      s_.stocke(base_ + "_" + "DETAILLES", new Boolean(cbResDetailles_.isSelected()));

      if (params_.lancementCalculs.modeLancementCalcul != AlbeLib.IDL_LANCEMENT_MANUEL) {
        if (params_.lancementCalculs.eluFonda) {
          for (int i = 1; i <= nbCombiEluFonda_; i++) {
            s_.stocke(base_ + "_" + "DETAILLES_ELUFONDA" + i, new Boolean(tabCbEluFonda_[i - 1]
                .isSelected()));
          }
        }
        if (params_.lancementCalculs.eluAcc) {
          for (int i = 1; i <= nbCombiAutre_; i++) {
            s_.stocke(base_ + "_" + "DETAILLES_ELUACC" + i, new Boolean(tabCbEluAcc_[i - 1]
                .isSelected()));
          }
        }
        if (params_.lancementCalculs.elsRare) {
          for (int i = 1; i <= nbCombiAutre_; i++) {
            s_.stocke(base_ + "_" + "DETAILLES_ELSRARE" + i, new Boolean(tabCbElsRare_[i - 1]
                .isSelected()));
          }
        }
        if (params_.lancementCalculs.elsFreq) {
          for (int i = 1; i <= nbCombiAutre_; i++) {
            s_.stocke(base_ + "_" + "DETAILLES_ELSFREQ" + i, new Boolean(tabCbElsFreq_[i - 1]
                .isSelected()));
          }
        }
      }
      s_.stocke(base_ + "_" + "DETAILLES_COMBIETUDIEE", new Boolean(cbCombiEtudiee_.isSelected()));
      s_.stocke(base_ + "_" + "DETAILLES_RESUME", new Boolean(cbResumeRes_.isSelected()));
      s_.stocke(base_ + "_" + "DETAILLES_TABLEAU", new Boolean(cbTableauDetaille_.isSelected()));
      s_.stocke(base_ + "_" + "DETAILLES_GRAPHIQUES", new Boolean(cbGraphique_.isSelected()));
      s_.stocke(base_ + "_" + "DETAILLES_GRAPHIQUES_DEFORMEE", new Boolean(cbGraphDeformee_
          .isSelected()));
      s_.stocke(base_ + "_" + "DETAILLES_GRAPHIQUES_MOMENTS",
          new Boolean(cbGraphMmts_.isSelected()));
      s_.stocke(base_ + "_" + "DETAILLES_GRAPHIQUES_EFFORTS", new Boolean(cbGraphEfforts_
          .isSelected()));
      s_
          .stocke(base_ + "_" + "DETAILLES_GRAPHIQUES_BUTEE", new Boolean(cbGraphButee_
              .isSelected()));
      s_.stocke(base_ + "_" + "DETAILLES_GRAPHIQUES_DEFENSE", new Boolean(cbGraphDefense_
          .isSelected()));
      s_.stocke("COMBINAISONMANUELLE", new Boolean(
          params_.lancementCalculs.modeLancementCalcul == AlbeLib.IDL_LANCEMENT_MANUEL));
      return s_;
    }

  }
}
