package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import org.fudaa.dodico.corba.albe.SCoefficientsPartiels;
import org.fudaa.dodico.corba.albe.SCombinaisonMateriaux;
import org.fudaa.dodico.corba.albe.SCombinaisonModele;
import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * Description de l'onglet 'Crit�res de dimensionnement'.
 * 
 * @author Sabrina Delattre
 */

public class AlbeCoefficientsPartielsParametres extends AlbeAbstractOnglet {

  /**
   * Valeurs par d�faut des coeffients de sol pour un accostage.
   */
  Object[][] donneesSolAccost_ = { { "0.714", "1.40" } };

  /**
   * Valeurs par d�faut des coeffients de sol pour un amarrage.
   */
  Object[][] donneesSolAmar_ = { { "1.00", "1.00" } };

  /**
   * Valeurs par d�faut des coeffients de d�fense (forc�ment accostage).
   */
  Object[][] donneesDefense_ = { { "0.90", "1.80" }, { "1.00", "1.40" }, { "0.95", "1.60" },
      { "0.95", "1.60" } };

  /**
   * Valeurs par d�faut des coeffients d'acier pour un accostage.
   */
  Object[][] donneesAcierAccost_ = { { "1.00" }, { "1.00" }, { "1.25" } };

  /**
   * Valeurs par d�faut des coeffients d'acier pour un amarrage.
   */
  Object[][] donneesAcierAmar_ = { { "1.10" }, { "1.00" }, { "1.25" } };

  /**
   * Valeurs par d�faut des coeffients de mod�le pour un accostage.
   */
  Object[][] donneesModeleAccost_ = { { "1.10", "1.00", "-", "1.20" },
      { "1.125", "1.00", "1.00", "-" } };

  /**
   * Valeurs par d�faut des coeffients de mod�le pour un amarrage.
   */
  Object[][] donneesModeleAmar_ = { { "1.10", "1.00", "-", "1.30" },
      { "1.125", "1.00", "1.00", "-" } };

  /**
   * noms des premi�res colonnes pour les tableaux de coefficients sur les
   * mat�riaux.
   */
  Object[] donneesColonne1_ = { "ELU fondamental", "ELU accidentel", "ELS rare", "ELS fr�quent" };

  BuPanel materiauxPanel_, solPanel_, defensePanel_, acierPanel_, modelePanel_, buttonPanel_;

  BuButton retablirButton_, aideButton_;

  JTable solTable_, defenseTable_, acierTable_, modeleTable_;

  DefaultTableModel modelSol_, modelDefense_, modeleAcier_, modele_;

  BuTextField tfGammaSollicitation_, tfGammaResistance_;

  boolean accostage_;

  /**
   * r�f�rence vers la fen�tre parente.
   */

  AlbeFilleParametres fp_;

  /**
   * Constructeur.
   * 
   * @param _fp
   */
  public AlbeCoefficientsPartielsParametres(final AlbeFilleParametres _fp) {

    super(_fp.getImplementation(), _fp, AlbeMsg.URL007);

    fp_ = _fp;

    accostage_ = fp_.action_.actionCombo_.getSelectedIndex() == 0 ? true : false;

    BuPanel panelCenter = new BuPanel(new BuVerticalLayout());

    // Panneau contenant le bouton 'aide'
    BuPanel aidePanel = new BuPanel(new BorderLayout());
    aideButton_ = AlbeLib.aideButton();
    aideButton_.addActionListener(this);
    aidePanel.add(aideButton_, BorderLayout.EAST);

    // Panneau pour les coefficients sur les mat�riaux
    materiauxPanel_ = AlbeLib.titledPanel("Coefficients sur les mat�riaux", AlbeLib.ETCHED_BORDER);
    materiauxPanel_.setLayout(new BuVerticalLayout());

    initPanelSol();
    initPanelDefense();
    initPanelAcier();

    materiauxPanel_.add(solPanel_);
    materiauxPanel_.add(defensePanel_);
    materiauxPanel_.add(acierPanel_);

    // Panneau pour les coefficients de mod�le
    initPanelModele();

    // Panneau pour l'approche 2 de la NF EN 1997-1 (non visible pour le moment)
    BuPanel pApprocheNF = AlbeLib.titledPanel("ELU fondamental approche 2 de la NF EN 1997-1",
        AlbeLib.ETCHED_BORDER);
    pApprocheNF.setLayout(new BorderLayout());
    BuPanel tmpPanel = new BuPanel(new SpringLayout());
    tfGammaSollicitation_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    tfGammaResistance_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    tfGammaSollicitation_.setText("1.35");
    tfGammaResistance_.setText("1.4");
    tfGammaSollicitation_.setHorizontalAlignment(SwingConstants.CENTER);
    tfGammaResistance_.setHorizontalAlignment(SwingConstants.CENTER);
    tfGammaSollicitation_.setColumns(6);
    tfGammaResistance_.setColumns(6);
    tmpPanel.add(new JLabel("Coefficient sur la sollicitation calcul�e dans la structure"));
    tmpPanel.add(AlbeLib.symbol(AlbeLib.getIcon("gamma-sollicitation")));
    tmpPanel.add(tfGammaSollicitation_);
    tmpPanel.add(new JLabel("Coefficient de r�duction de la r�sistance en but�e"));
    tmpPanel.add(AlbeLib.symbol(AlbeLib.getIcon("gamma-resistance-sol")));
    tmpPanel.add(tfGammaResistance_);

    SpringUtilities.makeCompactGrid(tmpPanel, 2, 3, 5, 5, 5, 5);

    pApprocheNF.add(tmpPanel, BorderLayout.WEST);
    pApprocheNF.setVisible(false);

    // Panneau contenant le bouton pour r�tablir les valeurs par d�faut
    buttonPanel_ = new BuPanel();
    retablirButton_ = AlbeLib.button("R�tablir les valeurs pas d�faut", "reinitialiser");
    retablirButton_.addActionListener(this);
    buttonPanel_.add(retablirButton_);

    // Ajout des panneaux
    BuPanel p = AlbeLib.titledPanel("Coefficients partiels", AlbeLib.COMPOUND_BORDER);
    p.setLayout(new BuVerticalLayout());
    p.add(materiauxPanel_);
    p.add(modelePanel_);
    p.add(pApprocheNF);

    panelCenter.add(aidePanel);
    panelCenter.add(p);
    panelCenter.add(buttonPanel_);

    /*
     * //Ajout d'un scrollBar dans le cas o� tous les composants ne tiennent pas
     * dans la fen�tre JScrollPane sp = new JScrollPane(panelCenter); JScrollBar
     * sb = sp.getVerticalScrollBar(); this.add(sp, BorderLayout.CENTER);
     * this.add(sb, BorderLayout.EAST);
     */
    this.add(panelCenter);

  }// Fin constructeur

  /**
   * initialise le panneau pour les coefficients de sol.
   */

  public void initPanelSol() {
    solPanel_ = AlbeLib.titledPanel("Sol", AlbeLib.ETCHED_BORDER);

    // Cr�ation du tableau
    modelSol_ = new AlbeModeleTablePieuEtCoeffMateriaux(new Object[] { "", "Mou (favorable)",
        "Raide (d�favorable)" }, 1, this);
    solTable_ = new JTable(modelSol_);
    AlbeLib.changeParametresTable(solTable_);
    for (int i = 1; i < solTable_.getColumnCount(); i++) {
      solTable_.getColumnModel().getColumn(i).setCellRenderer(
          new AlbeTableColorCellRenderer(accostage_ ? donneesSolAccost_ : donneesSolAmar_));
    }

    // Initialisation du tableau
    initColumn1(solTable_);
    AlbeLib.setValuesInTable(solTable_, donneesSolAccost_);

    JScrollPane scrollPane = new JScrollPane(solTable_);
    scrollPane.setPreferredSize(new Dimension(500, 38));
    solPanel_.add(scrollPane);

  }

  /**
   * initialise le panneau pour les coefficients de d�fense.
   */

  public void initPanelDefense() {

    defensePanel_ = AlbeLib.titledPanel("D�fense", AlbeLib.ETCHED_BORDER);

    // Cr�ation du tableau
    modelDefense_ = new AlbeModeleTablePieuEtCoeffMateriaux(new Object[] { "", "Molle", "Raide" },
        4, this);
    defenseTable_ = new JTable(modelDefense_);
    AlbeLib.changeParametresTable(defenseTable_);
    for (int i = 1; i < defenseTable_.getColumnCount(); i++) {
      defenseTable_.getColumnModel().getColumn(i).setCellRenderer(
          new AlbeTableColorCellRenderer(donneesDefense_));
    }

    // Initialisation du tableau
    initColumn1(defenseTable_);
    AlbeLib.setValuesInTable(defenseTable_, donneesDefense_);

    JScrollPane scrollPane2 = new JScrollPane(defenseTable_);
    scrollPane2.setPreferredSize(new Dimension(500, 98));

    defensePanel_.add(scrollPane2);
  }

  /**
   * initialise le panneau pour les coefficients d'acier.
   */

  public void initPanelAcier() {

    acierPanel_ = AlbeLib.titledPanel("Acier", AlbeLib.ETCHED_BORDER);

    // Cr�ation du tableau
    modeleAcier_ = new AlbeModeleTablePieuEtCoeffMateriaux(new Object[] { "", "gamma-fy" }, 3, this);
    acierTable_ = new JTable(modeleAcier_);
    AlbeLib.changeParametresTable(acierTable_);
    acierTable_.getTableHeader().setDefaultRenderer(new AlbeTableCellRenderer());
    for (int i = 1; i < acierTable_.getColumnCount(); i++) {
      acierTable_.getColumnModel().getColumn(i).setCellRenderer(
          new AlbeTableColorCellRenderer(accostage_ ? donneesAcierAccost_ : donneesAcierAmar_));
    }

    // Initialisation du tableau
    initColumn1(acierTable_);
    AlbeLib.setValuesInTable(acierTable_, donneesAcierAccost_);

    JScrollPane scrollPane3 = new JScrollPane(acierTable_);
    scrollPane3.setPreferredSize(new Dimension(400, 83));
    acierPanel_.add(scrollPane3);
  }

  /**
   * initialise le panneau pour les coefficients de mod�le.
   */

  public void initPanelModele() {

    modelePanel_ = AlbeLib.titledPanel("Coefficients de mod�le", AlbeLib.ETCHED_BORDER);

    // Cr�ation du tableau
    modele_ = new AlbeModeleTableCoeffModele(new Object[] { "", "header-gamma-d",
        "header-gamma-d-acc", "header-gamma-d-rare", "header-gamma-d-freq" }, 2, this);

    modeleTable_ = new JTable(modele_);
    AlbeLib.changeParametresTable(modeleTable_);
    modeleTable_.getTableHeader().setDefaultRenderer(new AlbeTableCellRenderer());
    for (int i = 1; i < modeleTable_.getColumnCount(); i++) {
      modeleTable_.getColumnModel().getColumn(i).setCellRenderer(
          new AlbeTableColorCellRenderer(accostage_ ? donneesModeleAccost_ : donneesModeleAmar_));
    }

    // Initialisation du tableau
    modeleTable_.setValueAt("Mobilisation de la but�e du sol", 0, 0);
    modeleTable_.setValueAt("R�sistance structurale des pieux", 1, 0);
    AlbeLib.setValuesInTable(modeleTable_, donneesModeleAccost_);

    // Largeur de la premi�re colonne
    modeleTable_.getColumnModel().getColumn(0).setPreferredWidth(160);

    JScrollPane scrollPane4 = new JScrollPane(modeleTable_);
    scrollPane4.setPreferredSize(new Dimension(700, 63));
    modelePanel_.add(scrollPane4);

  }

  /**
   * initialise les premi�res colonnes des tableaux sur les coefficients de
   * mat�riaux.
   */

  public void initColumn1(JTable _t) {
    for (int i = 0; i < _t.getRowCount(); i++) {
      _t.setValueAt(donneesColonne1_[i], i, 0);
    }
  }

  /**
   * retourne une structure de donn�es contenant les coefficients sur les
   * mat�riaux d'une combinaison du tableau.
   * 
   * @param _n num�ro de la combinaison (commence � 1)
   * @param _model mod�le du tableau concern� (sol, d�fense ou acier)
   */
  public SCombinaisonMateriaux getCombiMateriaux(final int _n, DefaultTableModel _model) {
    SCombinaisonMateriaux c = null;
    try {
      c = (SCombinaisonMateriaux) AlbeLib.createIDLObject(SCombinaisonMateriaux.class);
      try {
        c.favorable = ((Double) _model.getValueAt(_n - 1, 1)).doubleValue();
      }
      catch (final Exception _e2) {
        c.favorable = VALEUR_NULLE.value;
      }

      try {
        c.defavorable = ((Double) _model.getValueAt(_n - 1, 2)).doubleValue();
      }
      catch (final Exception _e3) {
        c.defavorable = VALEUR_NULLE.value;
      }

    }
    catch (final ArrayIndexOutOfBoundsException _e1) {
      System.err.println(AlbeMsg.CONS011);
    }

    return c;
  }

  /**
   * retourne une structure de donn�es contenant les coefficients sur les
   * mat�riaux.
   * 
   * @param _model mod�le du tableau concern� (sol ou d�fense)
   * @param _t tableau concern� (sol ou d�fense)
   */
  public SCombinaisonMateriaux[] getCoefMateriaux(final DefaultTableModel _model, final JTable _t) {
    if (_t.isEditing()) {
      _t.getCellEditor().stopCellEditing();
    }
    SCombinaisonMateriaux[] c = null;
    int nb = _t.getRowCount();
    try {
      c = new SCombinaisonMateriaux[nb];
    }
    catch (final NegativeArraySizeException _e2) {
      System.err.println(AlbeMsg.CONS012);
    }
    for (int i = 0; i < nb; i++) {
      try {
        c[i] = getCombiMateriaux(i + 1, _model);
      }
      catch (final NullPointerException _e1) {
        System.err.println(AlbeMsg.CONS013);
      }
    }

    return c;
  }

  /**
   * retourne un tableau de double contenant les coefficients sur l'acier.
   */
  public double[] getCoefAcier() {
    if (acierTable_.isEditing()) {
      acierTable_.getCellEditor().stopCellEditing();
    }
    int nbRow = acierTable_.getRowCount();
    double[] d = new double[nbRow];

    for (int i = 0; i < nbRow; i++) {
      try {
        d[i] = ((Double) modeleAcier_.getValueAt(i, 1)).doubleValue();
      }
      catch (final NullPointerException _e1) {
        System.err.println(AlbeMsg.CONS019);
      }
    }

    return d;
  }

  /**
   * charge les combinaisons dans un des tableaux sur les coefficients de
   * mat�riaux.
   * 
   * @param _c tableau contenant les combinaisons � importer
   * @param _t tableau concern� (sol ou d�fense)
   */
  public synchronized void setCoefMateriaux(final SCombinaisonMateriaux[] _c, final JTable _t) {
    if (_t.isEditing()) {
      _t.getCellEditor().stopCellEditing();
    }
    try {

      for (int i = 0; i < _t.getRowCount(); i++) {
        if (_c[i].favorable != VALEUR_NULLE.value)
          _t.setValueAt(new Double(_c[i].favorable), i, 1);

        if (_c[i].defavorable != VALEUR_NULLE.value)
          _t.setValueAt(new Double(_c[i].defavorable), i, 2);
      }
    }
    catch (final NullPointerException _e1) {
      System.err.println(AlbeMsg.CONS014);
    }
  }

  /**
   * charge les coefficients dans le tableau sur l'acier.
   */
  public synchronized void setCoefAcier(final double[] _d) {
    if (acierTable_.isEditing()) {
      acierTable_.getCellEditor().stopCellEditing();
    }
    try {
      for (int i = 0; i < acierTable_.getRowCount(); i++) {
        if (_d[i] != VALEUR_NULLE.value)
          acierTable_.setValueAt(new Double(_d[i]), i, 1);
      }
    }
    catch (final NullPointerException _e1) {
      System.err.println(AlbeMsg.CONS020);
    }
  }

  /**
   * retourne une structure de donn�es contenant les coefficients de mod�le
   * d'une combinaison du tableau.
   * 
   * @param _n num�ro de la combinaison (commence � 1)
   */
  public SCombinaisonModele getCombiModele(final int _n) {
    SCombinaisonModele c = null;
    try {
      c = (SCombinaisonModele) AlbeLib.createIDLObject(SCombinaisonModele.class);
      try {
        c.mobilisationButeeSol = ((Double) modele_.getValueAt(0, _n)).doubleValue();
      }
      catch (final Exception _e2) {
        c.mobilisationButeeSol = VALEUR_NULLE.value;
      }

      try {
        c.resistanceStructuralePieux = ((Double) modele_.getValueAt(1, _n)).doubleValue();
      }
      catch (final Exception _e3) {
        c.resistanceStructuralePieux = VALEUR_NULLE.value;
      }

    }
    catch (final ArrayIndexOutOfBoundsException _e1) {
      System.err.println(AlbeMsg.CONS011);
    }

    return c;
  }

  /**
   * retourne une structure de donn�es contenant les coefficients de mod�le.
   */
  public SCombinaisonModele[] getCoefModele() {
    if (modeleTable_.isEditing()) {
      modeleTable_.getCellEditor().stopCellEditing();
    }
    SCombinaisonModele[] c = null;
    try {
      c = new SCombinaisonModele[4];
    }
    catch (final NegativeArraySizeException _e2) {
      System.err.println(AlbeMsg.CONS012);
    }
    for (int i = 0; i < 4; i++) {
      try {
        c[i] = getCombiModele(i + 1);
      }
      catch (final NullPointerException _e1) {
        System.err.println(AlbeMsg.CONS013);
      }
    }

    return c;
  }

  /**
   * charge les combinaisons dans un des tableaux sur les coefficients de
   * mod�le.
   * 
   * @param _c tableau contenant les combinaisons � importer
   */
  public synchronized void setCoefModele(final SCombinaisonModele[] _c) {
    if (modeleTable_.isEditing()) {
      modeleTable_.getCellEditor().stopCellEditing();
    }
    try {

      for (int i = 0; i < 4; i++) {
        if (_c[i].mobilisationButeeSol != VALEUR_NULLE.value)
          modeleTable_.setValueAt(new Double(_c[i].mobilisationButeeSol), 0, i + 1);

        if (_c[i].resistanceStructuralePieux != VALEUR_NULLE.value)
          modeleTable_.setValueAt(new Double(_c[i].resistanceStructuralePieux), 1, i + 1);
      }
    }
    catch (final NullPointerException _e1) {
      System.err.println(AlbeMsg.CONS014);
    }
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans
   * l'onglet 'Coefficients partiels'.
   */

  public SCoefficientsPartiels getParametresCoef() {
    final SCoefficientsPartiels p = (SCoefficientsPartiels) AlbeLib
        .createIDLObject(SCoefficientsPartiels.class);
    p.coeffSol = getCoefMateriaux(modelSol_, solTable_);
    p.coeffDefense = getCoefMateriaux(modelDefense_, defenseTable_);
    p.coeffAcier = getCoefAcier();
    p.coeffModele = getCoefModele();
    return p;
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres dans
   * l'onglet 'Coefficients partiels'.
   * 
   * @param _p structure de donn�es contenant les param�tres (donn�es) �
   *          importer
   */
  public synchronized void setParametresCoef(SCoefficientsPartiels _c) {
    setCoefMateriaux(_c.coeffSol, solTable_);
    setCoefMateriaux(_c.coeffDefense, defenseTable_);
    setCoefAcier(_c.coeffAcier);
    setCoefModele(_c.coeffModele);
  }

  /**
   * 
   */
  public boolean paramsCombiMateriauxModified(SCombinaisonMateriaux _p, DefaultTableModel _model,
      int _n) {
    SCombinaisonMateriaux p2 = getCombiMateriaux(_n, _model);
    if (_p.favorable != p2.favorable || _p.defavorable != p2.defavorable)
      return true;
    return false;
  }

  /**
   * 
   */

  public boolean paramsCombiModeleModified(SCombinaisonModele _p, int _n) {
    SCombinaisonModele p2 = getCombiModele(_n);
    if (_p.mobilisationButeeSol != p2.mobilisationButeeSol
        || _p.resistanceStructuralePieux != p2.resistanceStructuralePieux)
      return true;
    return false;
  }

  /**
   * 
   */
  public boolean paramsCoeffMateriauxModified(SCombinaisonMateriaux[] _p, DefaultTableModel _model,
      JTable _t) {
    if (_t.isEditing())
      _t.getCellEditor().stopCellEditing();

    for (int i = 0; i < _t.getRowCount(); i++) {
      if (paramsCombiMateriauxModified(_p[i], _model, i + 1))
        return true;
    }
    return false;
  }

  /**
   * 
   */
  public boolean paramsCoeffAcierModified(double[] _d) {
    if (acierTable_.isEditing())
      acierTable_.getCellEditor().stopCellEditing();
    double[] d2 = getCoefAcier();

    for (int i = 0; i < acierTable_.getRowCount(); i++) {
      if (_d[i] != d2[i])
        return true;
    }
    return false;
  }

  /**
   * 
   */
  public boolean paramsCoeffModeleModified(SCombinaisonModele[] _p, JTable _t) {
    if (_t.isEditing())
      _t.getCellEditor().stopCellEditing();

    for (int i = 0; i < _t.getColumnCount() - 1; i++) {
      if (paramsCombiModeleModified(_p[i], i + 1))
        return true;
    }
    return false;
  }

  /**
   * 
   */

  public boolean paramsCoeffPartielsModified(SParametresAlbe _pAlbe, SCoefficientsPartiels _p) {

    if (paramsCoeffMateriauxModified(_p.coeffSol, modelSol_, solTable_)
        || paramsCoeffAcierModified(_p.coeffAcier)
        || paramsCoeffModeleModified(_p.coeffModele, modeleTable_))
      return true;

    if (_pAlbe.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)
        && _pAlbe.defense.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI)) {
      if (paramsCoeffMateriauxModified(_p.coeffDefense, modelDefense_, defenseTable_))
        return true;
    }
    return false;
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {

    if (_evt.getSource() == retablirButton_) {
      // On stoppe l'�dition pour les tableaux sur les coefficients de
      // sol, d'acier et de mod�le
      if (solTable_.isEditing())
        solTable_.getCellEditor().stopCellEditing();
      if (acierTable_.isEditing())
        acierTable_.getCellEditor().stopCellEditing();
      if (modeleTable_.isEditing())
        modeleTable_.getCellEditor().stopCellEditing();

      // Si structure soumise � un accostage
      if (fp_.action_.actionCombo_.getSelectedIndex() == 0) {
        // On stoppe l'�dition pour le tableau sur les coefficients de
        // d�fense
        if (defenseTable_.isEditing())
          defenseTable_.getCellEditor().stopCellEditing();
        AlbeLib.setValuesInTable(solTable_, donneesSolAccost_);
        AlbeLib.setValuesInTable(defenseTable_, donneesDefense_);
        AlbeLib.setValuesInTable(acierTable_, donneesAcierAccost_);
        AlbeLib.setValuesInTable(modeleTable_, donneesModeleAccost_);
      }
      // Si structure soumise � un amarrage
      else if (fp_.action_.actionCombo_.getSelectedIndex() == 1) {
        AlbeLib.setValuesInTable(solTable_, donneesSolAmar_);
        AlbeLib.setValuesInTable(acierTable_, donneesAcierAmar_);
        AlbeLib.setValuesInTable(modeleTable_, donneesModeleAmar_);
      }
    }

    else if (_evt.getSource() == aideButton_)
      aide();
  }

  /**
   * 
   */
  public ValidationMessages validation() {

    final ValidationMessages vm = new ValidationMessages();

    // On stoppe l'�dition des tableaux
    /*
     * if (solTable_.isEditing()) solTable_.getCellEditor().stopCellEditing();
     * if (acierTable_.isEditing())
     * acierTable_.getCellEditor().stopCellEditing(); if
     * (modeleTable_.isEditing())
     * modeleTable_.getCellEditor().stopCellEditing();
     */

    // On v�rifie que tous les coefficients sur le sol ont �t� saisis
    if (!AlbeLib.fullTable(solTable_, modelSol_))
      vm.add(AlbeMsg.VMSG092);

    // On v�rifie que les coefficients sur le sol sont positifs
    if (!AlbeLib.valeursPositivesOuNulles(solTable_))
      vm.add(AlbeMsg.VMSG096);

    /** Si pr�sence de d�fense */
    if (fp_.action_.actionCombo_.getSelectedIndex() == 0
        && fp_.defense_.presenceCombo_.getSelectedIndex() == 0) {

      /*
       * if (defenseTable_.isEditing())
       * defenseTable_.getCellEditor().stopCellEditing();
       */

      // On v�rifie que tous les coefficients de d�fense ont �t� saisis
      if (!AlbeLib.fullTable(defenseTable_, modelDefense_))
        vm.add(AlbeMsg.VMSG093);

      // On v�rifie que les coefficients sur la d�fense sont positifs
      if (!AlbeLib.valeursPositivesOuNulles(defenseTable_))
        vm.add(AlbeMsg.VMSG097);
    }

    // On v�rifie que tous les coefficients sur l'acier ont �t� saisis
    if (!AlbeLib.fullTable(acierTable_, modeleAcier_))
      vm.add(AlbeMsg.VMSG094);

    // On v�rifie que les coefficients sur l'acier sont positifs
    if (!AlbeLib.valeursStrictPositives(acierTable_))
      vm.add(AlbeMsg.VMSG098);

    // On v�rifie que tous les coefficients de mod�le ont �t� saisis
    if (!AlbeLib.fullTableCellsEditables(modeleTable_, modele_))
      vm.add(AlbeMsg.VMSG095);

    // On v�rifie que les coefficients de mod�le sont positifs
    if (!AlbeLib.valeursStrictPositives(modeleTable_))
      vm.add(AlbeMsg.VMSG099);

    return vm;
  }

}
