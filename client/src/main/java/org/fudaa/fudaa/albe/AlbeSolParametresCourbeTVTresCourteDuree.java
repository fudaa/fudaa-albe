package org.fudaa.fudaa.albe;

import java.awt.event.KeyEvent;

/**
 * Panneau s'affichant lorsque l'utilisateur choisit la courbe du fascicule 62
 * Titre V tr�s courte dur�e dans l'onglet 'Param�tres de sol'.
 * 
 * @author Sabrina Delattre
 */
public class AlbeSolParametresCourbeTVTresCourteDuree extends
    AlbeAbstractSolCourbeElastoSetraFascicule{

  /**
   * Constructeur.
   */

  public AlbeSolParametresCourbeTVTresCourteDuree(final AlbeSolParametres _sol) {

    super(_sol, 7, true, true);

    initButton_.addActionListener(this);
  }

  /**
   * calcule et retourne la valeur de x2.
   */

  public double calculx2(int _indexCouche, double _dimPieu) {

    double denom = 2 * (AlbeLib.textFieldDoubleValue(pressionLimite_[_indexCouche]) - AlbeLib
        .textFieldDoubleValue(pressionFluage_[_indexCouche]));
    return calculx1CourteTresCourteDuree(_indexCouche, _dimPieu)
        + (denom / calculKfCourteTresCourteDuree(_indexCouche, _dimPieu));
  }

  /**
   * remplit le tableau de l'onglet _index si toutes les valeurs utiles sont
   * renseign�es.
   */
  public void genereTableauTresCourteDuree(int _index) {
    try {

      double pl = AlbeLib.textFieldDoubleValue(pressionLimite_[_index]);
      double pf = AlbeLib.textFieldDoubleValue(pressionFluage_[_index]);
      double module = AlbeLib.textFieldDoubleValue(modulePressiometrique_[_index]);
      double alpha = AlbeLib.textFieldDoubleValue(coeffRheo_[_index]);
      double dimPieu = sol_.fp_.pieu_.getDiametrePieu();
      // Coefficient permettant de convertir les m�tres en centim�tres
      int coefConv = 100;

      if (pf <= 0 || pl <= 0 || pl <= pf || module <= 0 || alpha < 0  || alpha > 1 || dimPieu <= 0) {
        graphiqueButton_[_index].setEnabled(false);
        AlbeLib.initialiseTableVide(table_[_index]);
      } else {
        // Calcul de x1 et de x2
        double x1 = calculx1CourteTresCourteDuree(_index, dimPieu) * coefConv;
        double x2 = calculx2(_index, dimPieu) * coefConv;
        // G�n�ration des valeurs des d�placements (premi�re colonne)
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(-3 * x2)), 0, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(-x2)), 1, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(-x1)), 2, 1);
        table_[_index].setValueAt("0", 3, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(x1)), 4, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(x2)), 5, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(3 * x2)), 6, 1);
        // G�n�ration des valeurs des pressions (deuxi�me colonne)
        table_[_index].setValueAt(Double.toString(-pl), 0, 2);
        table_[_index].setValueAt(Double.toString(-pl), 1, 2);
        table_[_index].setValueAt(Double.toString(-pf), 2, 2);
        table_[_index].setValueAt("0", 3, 2);
        table_[_index].setValueAt(Double.toString(pf), 4, 2);
        table_[_index].setValueAt(Double.toString(pl), 5, 2);
        table_[_index].setValueAt(Double.toString(pl), 6, 2);
        graphiqueButton_[_index].setEnabled(true);
      }
    }
    catch (final NullPointerException _e1) {
      graphiqueButton_[_index].setEnabled(false);
      AlbeLib.initialiseTableVide(table_[_index]);
    }
  }

  /**
   * 
   */
  public void afficheGraphique(int _indexCouche) {
    afficheGraphiqueCourbe7Pts(_indexCouche);
  }

  /**
   *
   */
  public void keyReleased(KeyEvent _evt) {
    genereTableauTresCourteDuree(getCurrentTab());
  }

}
