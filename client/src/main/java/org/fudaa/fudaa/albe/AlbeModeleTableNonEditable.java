/*
 * @creation 25 janv. 07
 * @modification $Date: 2007-01-25 17:39:31 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import javax.swing.table.DefaultTableModel;

/**
 * Cr�ation d'un mod�le de Table (d�rivant de DefaultTableModel) non �ditable.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeModeleTableNonEditable extends DefaultTableModel {

  /**
   * cr�e un mod�le de donn�es.
   * 
   * @param _data donn�es � ins�rer dans le tableau
   * @param _columnNames tableau de string contenant les noms des colonnes �
   *          cr�er
   */
  public AlbeModeleTableNonEditable(Object[][] _data, String[] _columnNames) {
    super(_data, _columnNames);

  }

  /**
   * cr�e un mod�le de donn�es.
   * 
   * @param _ob tableau d'objets contenant les noms des colonnes � cr�er
   * @param _row nombre de lignes du tableau
   */

  public AlbeModeleTableNonEditable(Object[] _ob, int _row) {
    super(_ob, _row);
  }

  /**
   * d�finit si la cellule situ�e � l'emplacement sp�cifi�e est �ditable par
   * l'utilisateur ou non.
   * 
   * @param row indice de la ligne de la cellule concern�e
   * @param column indice de la colonne de la cellule concern�e
   */

  public boolean isCellEditable(int _row, int _col) {

    // Aucune cellule n'est �ditable
    return false;

  }

  /**
   * retourne l'objet valeur de la cellule sp�cifi�e.
   * 
   * @param _row indice de la ligne de la cellule concern�e
   * @param _column indice de la colonne de la cellule concern�e
   */
  public Object getValueAt(final int _row, final int _column) {
    Object o = null;
    Object v = null;
    v = super.getValueAt(_row, _column);
    try {
      if ((_row >= 0) && (_column > 0)) {
        o = new Double(v.toString());
      } else {
        o = v;
      }
    }
    catch (final NumberFormatException e1) {}
    catch (final NullPointerException e2) {}
    return o;
  }

}