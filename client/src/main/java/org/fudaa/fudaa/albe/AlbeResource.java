package org.fudaa.fudaa.albe;

import com.memoire.bu.BuResource;

/**
 * Le gestionnaire de ressources d'Albe.
 * 
 * @author Sabrina Delattre
 */

public class AlbeResource extends BuResource {

  public final static String PARAMETRES = "Parametres";

  public final static String RESULTATS = "Resultats";

  public final static String NOTEDECALCULS = "NoteDeCalculs";

  // Renvoie un nouvel objet AlbeResource
  public final static AlbeResource ALBE = new AlbeResource();
}
