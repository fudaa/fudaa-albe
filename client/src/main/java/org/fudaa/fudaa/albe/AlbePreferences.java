package org.fudaa.fudaa.albe;

import com.memoire.bu.BuPreferences;

/**
 * Préférences de Albe.
 * 
 * @author Sabrina Delattre
 */
public class AlbePreferences extends BuPreferences {
  public final static AlbePreferences ALBE = new AlbePreferences();

}
