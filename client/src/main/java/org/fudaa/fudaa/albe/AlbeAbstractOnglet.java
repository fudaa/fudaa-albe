/*
 * @creation 11 oct. 06
 * @modification $Date: 2008-03-06 17:05:13 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

/**
 * Classe abstraite de base pour les onglets.
 * 
 * @author Sabrina Delattre
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuPanel;

import org.fudaa.fudaa.commun.aide.FudaaHelpPanel;

public class AlbeAbstractOnglet extends BuPanel implements ActionListener, ChangeListener,
    PropertyChangeListener, KeyListener, FocusListener {
  /**
   * application.
   */
  private BuCommonInterface appli_;

  /**
   * composant conteneur parent principal de l'onglet.
   */
  private JComponent parent_;

  /**
   * lien vers l'aide contextuelle concernant cet onglet.
   */
  private String helpfile_;

  /**
   * construit un onglet d�pendant du classeur d'onglet _parent.
   * 
   * @param _parent composant parent conteneur de cet onglet
   */
  protected AlbeAbstractOnglet(final BuCommonInterface _appli, final JComponent _parent,final String _helpfile) {
    super();
    parent_ = _parent;
    appli_ = _appli;
    helpfile_ = _helpfile;
  }

  /**
   * m�thode appell�e lors d'un changement de propri�t� espionn�e par ce
   * composant.
   * 
   * @param _evt �v�nement qui d�crit le changement de propri�t�
   */
  public void propertyChange(final PropertyChangeEvent _evt) {}

  /**
   * m�thode appell�e � l'initialisation de l'objet pour importer les donn�es
   * �ventuelles dans les champs de l'onglet.
   */
  protected void importerDonnees() {}

  /**
   * imprime le contenu de l'onglet.
   */
  protected void imprimer() {}

  /**
   * 
   */
  protected void sauvegarder() {}

  /**
   * m�thode appell�e lorsque l'utilisateur arrive ou quitte l'onglet.
   * 
   * @param _et �v�nement d'onglet qui a engendr� l'action
   */
  protected void changeOnglet(final ChangeEvent _evt) {
    majMessages();
  }

  /**
   * m�thode appell�e lorsque l'utilisateur quitte ou arrive dans l'onglet.
   * 
   * @param _evt �v�nement d'onglet qui a engendr� l'action
   */
  public void stateChanged(final ChangeEvent _evt) {

  }

  /**
   * retourne le composant parent de cet onglet.
   * 
   * @return le conteneur parent_
   */
  protected JComponent getParentComponent() {
    return parent_;
  }

  /**
   * 
   */
  protected BuCommonInterface getApplication() {
    return appli_;
  }

  /**
   * 
   */
  protected AlbeImplementation getImplementation() {
    return (AlbeImplementation) appli_.getImplementation();
  }

  /**
   * 
   */
  protected AlbeFilleParametres getFilleParametres() {
    return getImplementation().getFilleParametres();
  }

  /**
   * affiche l'aide concernant l'onglet courant dans le navigateur.
   */
  protected void aide() {
	  String path = //"file:" + 
		  AlbeImplementation.informationsSoftware().man + AlbeImplementation.informationsSoftware().name.toLowerCase() + System.getProperty("file.separator") + helpfile_+".html";
	  //String path = "aide/albe/" + helpfile_ + ".html";
	  //getImplementation().showHtml(path);
	  getImplementation().displayURL(path);
  }

  /**
   * 
   */

  public void majMessages() {

    try {
      final AlbeParamEventView mv = getImplementation().getParamEventView();
      mv.removeAllMessages();
      mv.addMessages(((AlbeImplementation) (appli_.getImplementation())).fp_.validation()
          .getMessages());
    }
    catch (final NullPointerException _e) {}
  }

  /**
   *
   */
  public void actionPerformed(ActionEvent _evt) {}

  /**
   * appell�e automatiquement lorsqu'une touche est press�e.
   * 
   * @param _evt �v�nement qui a engendr� l'action
   */
  public void keyPressed(KeyEvent _evt) {
    if (_evt.getKeyCode() == KeyEvent.VK_ENTER)
      _evt.getComponent().transferFocus();
  }

  /**
   * appell�e automatiquement lorsqu'une touche est relach�e.
   * 
   * @param _evt �v�nement qui a engendr� l'action
   */
  public void keyReleased(KeyEvent _evt) {}

  /**
   * appell�e automatiquement lorsque l'utilisateur tape sur une touche du
   * clavier.
   * 
   * @param _evt �v�nement qui a engendr� l'action
   */
  public void keyTyped(KeyEvent _evt) {}

  /**
   * 
   */
  public void focusGained(FocusEvent _evt) {}

  /**
   * 
   */
  public void focusLost(FocusEvent _evt) {
    majMessages();
  }

}
