package org.fudaa.fudaa.albe;

import org.fudaa.fudaa.commun.FudaaAstuces;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;

import com.memoire.bu.BuPreferences;

/**
 * Classe pour les astuces du logiciel Fudaa-Albe (Albe).
 * 
 * @author Sabrina Delattre
 */
public class AlbeAstuces extends FudaaAstucesAbstract {
  /**
   * renvoie un nouvel objet AlbeAstuces.
   */
  public static AlbeAstuces ALBE = new AlbeAstuces();

  /**
   * renvoie un nouvel objet FudaaAstuces (parent de AlbeAstuces).
   * 
   * @return une instance d'objet <code>FudaaAstuces</code>
   */
  protected FudaaAstucesAbstract getParent() {
    final FudaaAstuces _fa = FudaaAstuces.FUDAA;
    return _fa;
  }

  /**
   * renvoie un nouvel objet AlbePreferences.
   * 
   * @return une instance d'objet <code>AlbePreferences</code>
   */
  protected BuPreferences getPrefs() {
    final BuPreferences bp = AlbePreferences.ALBE;
    return bp;
  }
}
