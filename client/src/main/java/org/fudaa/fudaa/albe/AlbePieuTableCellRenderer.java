/*
 * @creation 20 oct. 06
 * @modification $Date: 2006-10-26 06:41:07 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */

package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTableCellRenderer;

/**
 * Gestionnaire de rendu de cellule pour les tableaux sur les pieux.
 * 
 * @author Sabrina Delattre
 */

public class AlbePieuTableCellRenderer extends BuTableCellRenderer {
  public AlbePieuTableCellRenderer() {
    super();
  }

  public Component getTableCellRendererComponent(final JTable _table, final Object _value,
      final boolean _isSelected, final boolean _hasFocus, final int _row, final int _column) {

    Component r = null;
    BuLabel lb = null;
    if (_row < 0) { // entetes de colonnes
      lb = new BuLabel();
      lb.setIcon(AlbeResource.ALBE.getIcon((String) _value + ".gif"));
      lb.setHorizontalAlignment(SwingConstants.CENTER);
      lb.setOpaque(false);
      lb.setPreferredSize(new Dimension(40, 50));
      final BuPanel _p = new BuPanel();
      _p.setLayout(new BorderLayout());
      _p.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
      _p.setBackground(UIManager.getColor("TableHeader.background"));
      _p.setOpaque(true);
      _p.add(lb, BorderLayout.CENTER);
      r = _p;
    }

    return r;

  }
}
