/*
 * @creation 28 nov. 06
 * @modification $Date: 2007-01-30 07:41:43 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

/**
 * Cr�ation d'un mod�le de table pour les coefficients de mod�le (onglet
 * 'Coefficients partiels').
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeModeleTableCoeffModele extends DefaultTableModel implements TableModelListener {

  AlbeCoefficientsPartielsParametres coeff_;

  /**
   * cr�e un mod�le de donn�es utilis� pour les tableaux.
   */

  public AlbeModeleTableCoeffModele(Object[] _ob, int _row,
      AlbeCoefficientsPartielsParametres _coeff) {
    super(_ob, _row);
    coeff_ = _coeff;
    this.addTableModelListener(this);
  }

  /**
   * d�finit si la cellule situ�e � l'emplacement sp�cifi�e est �ditable par
   * l'utilisateur ou non.
   * 
   * @param row indice de la ligne de la cellule concern�e
   * @param column indice de la colonne de la cellule concern�e
   */

  public boolean isCellEditable(int _row, int _col) {

    // Les cellules d'indice 0,3 et 1,4 ne sont pas �ditables
    if (_col == 0 || (_row == 0 && _col == 3) || (_row == 1 && _col == 4))
      return false;

    return true;
  }

  /**
   * retourne l'objet valeur de la cellule sp�cifi�e.
   * 
   * @param _row indice de la ligne de la cellule concern�e
   * @param _col indice de la colonne de la cellule concern�e
   */
  public Object getValueAt(final int _row, final int _col) {
    Object o = null;
    Object v = null;
    v = super.getValueAt(_row, _col);
    try {
      if ((_row >= 0) && (_col > 0)) {
        o = new Double(v.toString());
      } else {
        o = v;
      }
    }
    catch (final NumberFormatException _e1) {}
    catch (final NullPointerException _e2) {}
    return o;
  }

  public void tableChanged(TableModelEvent _evt) {
    coeff_.majMessages();
  }

}
