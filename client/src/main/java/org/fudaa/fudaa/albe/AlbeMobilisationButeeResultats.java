/*
 * @creation 1 d�c. 06
 * @modification $Date: 2007-05-25 10:06:07 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.StringReader;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Lecteur;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuVerticalLayout;

/**
 * classe de l'onglet 'Mobilisation de la but�e' des r�sultats combinaison par
 * combinaison.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeMobilisationButeeResultats extends AlbeAbstractResultatsOnglet {

  BuButton courbeButton_;

  BuTable tbPression_;

  DefaultTableModel dmPression_;

  String pressionNonDef_ = "X";

  // Indice � partir duquel la pression est d�finie
  int indPres_;

  /** r�f�rence vers la fen�tre parente. */
  AlbeFilleResultatsCombinaison fp_;

  public AlbeMobilisationButeeResultats(final AlbeFilleResultatsCombinaison _fp) {

    super(_fp.getApplication(), _fp);

    fp_ = _fp;

    this.setLayout(new BuVerticalLayout());

    /** Bloc informations. */
    final BuPanel infoPanel = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    infoPanel.setLayout(new BorderLayout());
    BuLabelMultiLine infoLabel = AlbeLib
        .labelMultiLine("Vous trouverez ici un tableau r�capitulant la valeur calcul�e de la pression exerc�e par le sol sur chaque tron�on ainsi que la valeur de calcul de la pression limite du sol au niveau de ce tron�on."
            + "\nVous pouvez exporter ce tableau et visualiser le graphique.");
    infoPanel.add(infoLabel);

    /** Bloc 'Mobilisation de la but�e */
    final BuPanel p = AlbeLib.titledPanel("Mobilisation de la but�e", AlbeLib.BEVEL_BORDER);
    p.setLayout(new BuVerticalLayout());

    // Bloc boutons
    final BuPanel buttonPanel = new BuPanel(new FlowLayout());
    imprimerButton_ = AlbeLib.buttonId("Imprimer", "IMPRIMER");
    exporterButton_ = AlbeLib.buttonId("Exporter", "EXPORTER");
    courbeButton_ = AlbeLib.buttonId("Graphique", "VOIR");
    imprimerButton_.setToolTipText("Imprimer la fen�tre");
    exporterButton_.setToolTipText("Exporter le tableau");
    imprimerButton_.addActionListener(this);
    exporterButton_.addActionListener(this);
    courbeButton_
        .setToolTipText("Visualiser le graphique (pression calcul�e et pression limite de calcul)");
    courbeButton_.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        afficheGraphique();
      }
    });
    buttonPanel.add(imprimerButton_);
    buttonPanel.add(exporterButton_);
    buttonPanel.add(courbeButton_);

    // Bloc tableau
    final BuPanel tablePanel = new BuPanel(new FlowLayout());
    dmPression_ = new AlbeMobilisationButeeResultatsTableModel(new Object[] { "Cote (m)",
        "Presssion (kPa)", "Pression lim (kPa)" });
    tbPression_ = new BuTable(dmPression_);

    for (int i = 0; i < fp_.params_.pieu.nombreTroncons; i++) {
      for (int j = 0; j < fp_.resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
        // Calcul des pressions en kPa
        double pression = fp_.resCombi_.resistanceTroncons[i].resTroncon[j].pression * 10
            / AlbeRes.getLargeurPieu(fp_.params_);
        // Calcul des pressions limites en kPa
        int numCouche = fp_.resCombi_.resistanceTroncons[i].resTroncon[j].numCouche;
        if (numCouche == 0)
          dmPression_.addRow(new Object[] {
              new Double(AlbeLib
                  .arronditDouble(fp_.resCombi_.resistanceTroncons[i].resTroncon[j].cote)),
              new Double(AlbeLib.arronditDouble(pression)), pressionNonDef_ });
        else
          dmPression_
              .addRow(new Object[] {
                  new Double(AlbeLib
                      .arronditDouble(fp_.resCombi_.resistanceTroncons[i].resTroncon[j].cote)),
                  new Double(AlbeLib.arronditDouble(pression)),
                  new Double(AlbeLib.arronditDouble(AlbeRes.getPressionPalier(fp_.params_, fp_.typeCombi_, fp_.numCombi_,
                          numCouche))) });

      }
    }
    tbPression_.setRowHeight(AlbeLib.TABLE_ROWHEIGHT);

    /** Alignement du texte au centre des cellules */
    DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
    renderer.setHorizontalAlignment(SwingConstants.CENTER);
    int nbColonnes = tbPression_.getColumnCount();
    for (int i = 0; i < nbColonnes; i++) {
      tbPression_.getColumnModel().getColumn(i).setCellRenderer(renderer);
    }

    final JScrollPane sp = new JScrollPane(tbPression_);
    sp.setPreferredSize(new Dimension(450, 300));
    tablePanel.add(sp);

    p.add(buttonPanel);
    p.add(tablePanel);

    this.add(infoPanel);
    this.add(p);
  }

  /**
   * retourne le minimum des pressions (colonnes 1 et 2 du tableau).
   */
  public double getMinPression() {

    double min = ((Double) (tbPression_.getValueAt(0, 1))).doubleValue();
    for (int i = 1; i < tbPression_.getRowCount(); i++) {
      double d = ((Double) (tbPression_.getValueAt(i, 1))).doubleValue();
      if (d < min)
        min = d;
    }
    for (int i = indPres_; i < tbPression_.getRowCount(); i++) {
      double d = ((Double) (tbPression_.getValueAt(i, 2))).doubleValue();
      if (d < min)
        min = d;
    }
    return min;
  }

  /**
   * retourne le maximum des pressions (colonnes 1 et 2 du tableau).
   */
  public double getMaxPression() {
    int k = 0;
    while (tbPression_.getValueAt(k, 2).equals(pressionNonDef_)) {
      k = k + 1;
    }
    indPres_ = k;
    double max = ((Double) (tbPression_.getValueAt(indPres_, 2))).doubleValue();
    for (int i = indPres_ + 1; i < tbPression_.getRowCount(); i++) {
      double d = ((Double) (tbPression_.getValueAt(i, 2))).doubleValue();
      if (d > max)
        max = d;
    }
    for (int i = 0; i < tbPression_.getRowCount(); i++) {
      double d = ((Double) (tbPression_.getValueAt(i, 1))).doubleValue();
      if (d > max)
        max = d;
    }
    return max;
  }

  /**
   * 
   */
  public String getScriptPression() {
    final StringBuffer s = new StringBuffer(4096);
    double coordMaxX = getMaxPression();
    double coordMinX = getMinPression();
    double pasX = AlbeLib.getPas(coordMaxX, coordMinX);
    double minX = AlbeLib.getMin(pasX, coordMinX);
    double maxX = AlbeLib.getMax(pasX, coordMaxX);
    double coordMinY = fp_.getMinTableau(tbPression_, 0);
    double coordMaxY = fp_.getMaxTableau(tbPression_, 0);
    double pasY = AlbeLib.getPas(coordMaxY, coordMinY);
    double minY = AlbeLib.getMin(pasY, coordMinY);
    double maxY = AlbeLib.getMax(pasY, coordMaxY);

    s.append("graphe \n{\n");
    s.append("  titre \"Courbe des pressions du sol\"\n");
    s.append("  legende oui\n");
    s.append("  animation non\n");
    s.append("  marges\n{\n");
    s.append("    gauche 80\n");
    s.append("    droite 120\n");
    s.append("    haut 50\n");
    s.append("    bas 30\n");
    s.append("  }\n");
    s.append("  \n");
    s.append("  axe\n{\n");
    s.append("    titre \"pression\"\n");
    s.append("    unite \"kPa\"\n");
    s.append("    orientation horizontal\n");
    s.append("    graduations oui\n");
    s.append("    minimum " + minX + "\n");
    s.append("    maximum " + maxX + "\n");
    s.append("    pas " + pasX + "\n");
    s.append("  }\n");
    s.append("  axe\n{\n");
    s.append("    titre \"cote\"\n");
    s.append("    unite \"m\"\n");
    s.append("    orientation vertical\n");
    s.append("    graduations oui\n");
    s.append("    minimum " + minY + "\n");
    s.append("    maximum " + maxY + "\n");
    s.append("    pas " + pasY + "\n");
    s.append("  }\n");
    s.append("  courbe\n{\n");
    s.append("    aspect\n{\n");
    s.append("    contour.couleur FF00FF\n");
    s.append("    }\n");
    s.append("    valeurs\n{\n");
    for (int i = 0; i < tbPression_.getRowCount(); i++) {
      s.append("      " + tbPression_.getValueAt(i, 1) + " " + tbPression_.getValueAt(i, 0) + "\n");
    }
    s.append("    }\n");
    s.append("    }\n");
    s.append("  courbe\n{\n");
    s.append("    aspect\n{\n");
    s.append("    contour.couleur 0000FF\n");
    s.append("    }\n");
    s.append("    valeurs\n{\n");
    for (int i = indPres_; i < tbPression_.getRowCount(); i++) {
      s.append("      " + tbPression_.getValueAt(i, 2) + " " + tbPression_.getValueAt(i, 0) + "\n");
    }
    s.append("    }\n");
    s.append("    }\n");
    s.append("  }\n");
    return s.toString();
  }

  /**
   * affiche le graphique avec une l�gende.
   */
  public void afficheGraphique() {
    final Lecteur lin = new Lecteur(new StringReader(getScriptPression()));
    final Graphe g = Graphe.parse(lin, (Applet) null);
    final BGraphe bg = new BGraphe();
    bg.setGraphe(g);
    final AlbeGraphiqueDialog agf = new AlbeGraphiqueDialog(getApplication(),
        "Repr�sentation graphique des pressions du sol", bg);
    final BuPanel tmpPanelLegende = new BuPanel(new BuVerticalLayout());
    tmpPanelLegende.setBackground(Color.white);
    final BuPanel panelLegende = new BuPanel(new BuVerticalLayout());
    panelLegende.setBorder(BorderFactory.createTitledBorder(
        BorderFactory.createRaisedBevelBorder(), "L�gende"));
    final BuPanel panPression = new BuPanel();
    JLabel pressionLabel = new JLabel("_____");
    pressionLabel.setForeground(Color.magenta);
    panPression.add(pressionLabel);
    panPression.add(new JLabel("Pression calcul�e"));
    final BuPanel panPressionLim = new BuPanel();
    JLabel pressionLimLabel = new JLabel("_____");
    pressionLimLabel.setForeground(Color.blue);
    panPressionLim.add(pressionLimLabel);
    panPressionLim.add(new JLabel("Pression limite  "));
    panelLegende.add(panPression);
    panelLegende.add(panPressionLim);
    tmpPanelLegende.add(new JLabel(" "));
    tmpPanelLegende.add(new JLabel(" "));
    tmpPanelLegende.add(new JLabel(" "));
    tmpPanelLegende.add(new JLabel(" "));
    tmpPanelLegende.add(panelLegende);
    agf.conteneur_.add(tmpPanelLegende, BorderLayout.EAST);
    agf.setVisible(true);
  }

  /**
   * exporte le tableau dans un fichier .csv.
   */
  public void exporter() {
    final String f = AlbeLib.getFileChoosenByUser(this, "Exporter les donn�es", "Exporter",
        AlbeLib.USER_HOME
            + "Albe_resultatsMobilisationButee_combinaison"
            + (fp_.typeCombi_ == AlbeLib.COMBINAISON_MANUELLE ? "manuelle"
                : (fp_.numCombi_ + "_" + AlbeRes.nomCombinaison(fp_.typeCombi_))) + ".csv");
    if (f == null) {
      return;
    }
    String txt = "";
    txt += "Cote (m);Pression (kPa);Pression lim (kPa)" + CtuluLibString.LINE_SEP;
    for (int i = 0; i < dmPression_.getRowCount(); i++) {
      txt += dmPression_.getValueAt(i, 0) + ";" + dmPression_.getValueAt(i, 1) + ";"
          + dmPression_.getValueAt(i, 2) + CtuluLibString.LINE_SEP;
    }
    AlbeLib.writeFile(f, txt);
  }

  /**
   * construit la partie HTML des r�sultats sur la mobilisation de la but�e.
   */
  public String buildPartieHtml() {
    String html = "";
    html += "<table border=\"2\" cellspacing=\"0\" cellpadding=\"5\">" + CtuluLibString.LINE_SEP;
    html += "<th bgcolor=\"#DDDDDD\">Cote (m)</th><th bgcolor=\"#DDDDDD\">Pression (kPa)</th><th bgcolor=\"#DDDDDD\">Pression lim (kPa)</th>";
    for (int i = 0; i < tbPression_.getRowCount(); i++) {
      html += "<tr align=center>" + CtuluLibString.LINE_SEP;
      for (int j = 0; j < tbPression_.getColumnCount(); j++) {
        html += "<td>" + tbPression_.getValueAt(i, j) + "</td>" + CtuluLibString.LINE_SEP;
      }
      html += "</tr>" + CtuluLibString.LINE_SEP;
    }
    html += "</table><br><br>" + CtuluLibString.LINE_SEP;
    html += fp_.addImgFromScript(getScriptPression(), "courbe_pressions.gif",
        "Courbe des pressions");
    html += "<br><br>";

    return html;
  }

  /**
   *
   */
  public class AlbeMobilisationButeeResultatsTableModel extends DefaultTableModel {
    /**
     *
     */
    public AlbeMobilisationButeeResultatsTableModel(final Object[] _columnNames) {
      super(_columnNames, 0);
    }

    /**
     *
     */
    public boolean isCellEditable(final int _row, final int _column) {
      return false;
    }
  }

}
