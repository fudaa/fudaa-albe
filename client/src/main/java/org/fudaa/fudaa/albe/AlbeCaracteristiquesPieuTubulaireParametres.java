package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

import org.fudaa.dodico.corba.albe.STronconTubulaire;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * Panneau � ins�rer dans l'onglet 'Caract�ristiques pieu' si le pieu est
 * tubulaire.
 * 
 * @author Sabrina Delattre
 */

public class AlbeCaracteristiquesPieuTubulaireParametres extends BuPanel implements ActionListener {

  BuTextField diamTroncons_;

  JComboBox nbTronconsTubCombo_;

  DefaultTableModel modelTub_;

  BuPanel tablePanelTub_;

  BuTable tableTub_;

  JScrollPane scrollPaneTub_;

  BuButton initButton_;

  /**
   * r�f�rence vers le parent.
   */

  AlbeCaracteristiquesPieuParametres pieu_;

  /**
   * Constructeur.
   */

  public AlbeCaracteristiquesPieuTubulaireParametres(AlbeCaracteristiquesPieuParametres _pieu) {

    pieu_ = _pieu;

    this.setLayout(new BuVerticalLayout());
    Border b = BorderFactory.createTitledBorder(
        BorderFactory.createBevelBorder(BevelBorder.RAISED), "Caract�ristiques du pieu tubulaire");
    this.setBorder(b);

    /**
     * Panneau contenant les deux champs texte.
     */

    BuPanel tubulPanelNord = new BuPanel();
    tubulPanelNord.setLayout(new SpringLayout());

    diamTroncons_ = AlbeLib.doubleField(AlbeLib.FORMAT002);
    diamTroncons_.setHorizontalAlignment(SwingConstants.CENTER);
    diamTroncons_.addKeyListener(pieu_);
    diamTroncons_.addFocusListener(pieu_);

    // Initialisation de la boite combo
    nbTronconsTubCombo_ = new JComboBox();
    for (int i = 0; i < AlbeLib.NBMAX_TRONCONS_PIEU_TUBULAIRE; i++) {
      String s = Integer.toString(i + 1);
      nbTronconsTubCombo_.addItem(s);
    }

    nbTronconsTubCombo_.addActionListener(this);
    tubulPanelNord.add(new JLabel("Nombre de tron�on(s) (de 1 � "
        + AlbeLib.NBMAX_TRONCONS_PIEU_TUBULAIRE + ")"));
    tubulPanelNord.add(nbTronconsTubCombo_);
    tubulPanelNord.add(new JLabel("Diam�tre des tron�ons (en mm)"));
    tubulPanelNord.add(diamTroncons_);

    SpringUtilities.makeCompactGrid(tubulPanelNord, 2, 2, 5, 5, 10, 10);

    BuPanel tempPanelTub = AlbeLib.borderPanel(AlbeLib.ETCHED_BORDER);
    tempPanelTub.setLayout(new BorderLayout());
    tempPanelTub.add(tubulPanelNord, BorderLayout.WEST);

    /**
     * Panneau contenant le tabelau des tron�ons.
     */

    tablePanelTub_ = AlbeLib
        .titledPanel("Propri�t�s de chacun des tron�ons", AlbeLib.ETCHED_BORDER);
    modelTub_ = new AlbeModeleTablePieuEtCoeffMateriaux(new Object[] { "header-numero",
        "header-longueur", "header-epaisseur", "header-corrosion", "header-fyk" }, 1, pieu_);
    tableTub_ = new BuTable(modelTub_);
    tableTub_.getTableHeader().setDefaultRenderer(new AlbePieuTableCellRenderer());

    scrollPaneTub_ = new JScrollPane(tableTub_);
    scrollPaneTub_.setPreferredSize(new Dimension(550, 77));

    // Largeur de la premi�re colonne
    tableTub_.getColumnModel().getColumn(0).setPreferredWidth(30);

    AlbeLib.initialiseColonne1JTable(tableTub_);
    AlbeLib.initialiseTableVide(tableTub_);
    AlbeLib.changeParametresTable(tableTub_);

    tablePanelTub_.add(scrollPaneTub_);

    /**
     * Panneau contenant le bouton 'Initialiser'.
     */

    BuPanel bp = new BuPanel(new FlowLayout());
    initButton_ = AlbeLib.button("Initialiser", "reinitialiser");
    initButton_.setToolTipText("Effacer les donn�es saisies");
    initButton_.addActionListener(this);
    bp.add(initButton_);

    this.add(tempPanelTub);
    this.add(tablePanelTub_);
    this.add(bp);

  }// Fin constructeur

  /**
   * efface toutes les donn�es entr�es pour un pieu tubulaire.
   */
  public void eraseData() {
    diamTroncons_.setText("");
    diamTroncons_.requestFocus();
    diamTroncons_.transferFocus();
    // if (tableTub_.isEditing())
    // tableTub_.getCellEditor().stopCellEditing();
    AlbeLib.initialiseTableVide(tableTub_);
  }

  /**
   * retourne le nombre de tron�ons contenus dans le tableau.
   */
  public int getNbTroncons() {
    return tableTub_.getRowCount();
  }

  /**
   * retourne une structure de donn�es contenant les informations du tron�on _n
   * du tableau. retourne null si le tron�on n'existe pas dans le tableau.
   * 
   * @param _n num�ro du tron�on � retourner (commence � 1)
   */
  public STronconTubulaire getTroncon(final int _n) {
    STronconTubulaire t = null;
    try {
      t = (STronconTubulaire) AlbeLib.createIDLObject(STronconTubulaire.class);
      try {
        t.longueur = ((Double) modelTub_.getValueAt(_n - 1, 1)).doubleValue();
      }
      catch (final Exception _e2) {
        t.longueur = VALEUR_NULLE.value;
      }

      try {
        t.epaisseur = ((Double) modelTub_.getValueAt(_n - 1, 2)).doubleValue();
      }
      catch (final Exception _e3) {
        t.epaisseur = VALEUR_NULLE.value;
      }
      try {
        t.perteEpaisseurCorrosion = ((Double) modelTub_.getValueAt(_n - 1, 3)).doubleValue();
      }
      catch (final Exception _e4) {
        t.perteEpaisseurCorrosion = VALEUR_NULLE.value;
      }
      try {
        t.valeurCaracteristiqueLimiteElastiqueAcier = ((Double) modelTub_.getValueAt(_n - 1, 4))
            .doubleValue();
      }
      catch (final Exception _e5) {
        t.valeurCaracteristiqueLimiteElastiqueAcier = VALEUR_NULLE.value;
      }
    }
    catch (final ArrayIndexOutOfBoundsException _e1) {
      System.err.println(AlbeMsg.CONS001);
    }

    return t;
  }

  /**
   * retourne une structure de donn�es contenant les informations de tous les
   * tron�ons.
   */
  public STronconTubulaire[] getTroncons() {
    if (tableTub_.isEditing()) {
      tableTub_.getCellEditor().stopCellEditing();
    }
    STronconTubulaire[] t = null;
    final int nb = getNbTroncons();
    try {
      t = new STronconTubulaire[nb];
    }
    catch (final NegativeArraySizeException _e2) {
      System.err.println(AlbeMsg.CONS002);
    }
    for (int i = 0; i < nb; i++) {
      try {
        t[i] = getTroncon(i + 1);
      }
      catch (final NullPointerException _e1) {
        System.err.println(AlbeMsg.CONS003);
      }
      catch (final ArrayIndexOutOfBoundsException _e2) {
        System.err.println(AlbeMsg.CONS004);
        return null;
      }
    }

    return t;
  }

  /**
   * charge les tron�ons sp�cifi�s dans le tableau des tron�ons.
   * 
   * @param _t tableau contenant les tron�ons � importer
   */
  public synchronized void setTroncons(final STronconTubulaire[] _t) {
    if (tableTub_.isEditing()) {
      tableTub_.getCellEditor().stopCellEditing();
    }
    int n = 0;
    try {
      n = _t.length;
      AlbeLib.majNbLignes(tableTub_, n);
      AlbeLib.initialiseColonne1JTable(tableTub_);
      for (int i = 0; i < n; i++) {
        if (_t[i].longueur != VALEUR_NULLE.value)
          tableTub_.setValueAt(new Double(_t[i].longueur), i, 1);

        if (_t[i].epaisseur != VALEUR_NULLE.value)
          tableTub_.setValueAt(new Double(_t[i].epaisseur), i, 2);

        if (_t[i].perteEpaisseurCorrosion != VALEUR_NULLE.value)
          tableTub_.setValueAt(new Double(_t[i].perteEpaisseurCorrosion), i, 3);

        if (_t[i].valeurCaracteristiqueLimiteElastiqueAcier != VALEUR_NULLE.value)
          tableTub_.setValueAt(new Double(_t[i].valeurCaracteristiqueLimiteElastiqueAcier), i, 4);

      }
    }
    catch (final NullPointerException _e1) {
      System.err.println(AlbeMsg.CONS005);
    }
  }

  /**
   * 
   */

  public boolean tronconTubulaireModified(STronconTubulaire _p, int _n) {
    STronconTubulaire p2 = getTroncon(_n);
    if (_p.longueur != p2.longueur
        || _p.epaisseur != p2.epaisseur
        || _p.valeurCaracteristiqueLimiteElastiqueAcier != p2.valeurCaracteristiqueLimiteElastiqueAcier
        || _p.perteEpaisseurCorrosion != p2.perteEpaisseurCorrosion)
      return true;
    return false;
  }

  /**
   * 
   */

  public boolean tronconsTubulaireModified(STronconTubulaire[] _p) {
    if (tableTub_.isEditing()) {
      tableTub_.getCellEditor().stopCellEditing();
    }

    for (int i = 0; i < _p.length; i++) {
      if (tronconTubulaireModified(_p[i], i + 1))
        return true;
    }
    return false;
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {

    /**
     * Gestion de la s�lection du nombre de tron�ons (et donc du nombre de
     * lignes du tableau).
     */
    if (_evt.getSource() == nbTronconsTubCombo_) {

      if (tableTub_.isEditing())
        tableTub_.getCellEditor().stopCellEditing();

      // Pendant la mise � jour du nb de lignes du tableau, on n'effectue pas
      // les v�rifications
      AlbeLib.verificationPieu_ = false;
      int nbRow = nbTronconsTubCombo_.getSelectedIndex() + 1;
      AlbeLib.majNbLignes(tableTub_, nbRow);
      AlbeLib.initialiseColonne1JTable(tableTub_);
      pieu_.majMessages();
      AlbeLib.verificationPieu_ = true;

      /**
       * Si le nombre de tron�ons est inf�rieur ou �gal � 16, adaptation de la
       * taille du tableau pour ne pas avoir de barre de d�filement.
       */
      if (nbRow <= 16)
        scrollPaneTub_.setPreferredSize(new Dimension(550, 57 + 20 * nbRow));
      /**
       * Si le nombre de tron�ons est sup�rieur � 16, il y aura une barre de
       * d�filement.
       */
      else
        scrollPaneTub_.setPreferredSize(new Dimension(550, 377));
      // Rafra�chissement du panneau
      revalidate();
    }

    /** Clic sur le bouton 'Initialiser'. */
    else if (_evt.getSource() == initButton_) {
      // Pendant l'effacement des donn�es, on n'effectue pas les v�rifications
      AlbeLib.verificationPieu_ = false;
      eraseData();
      AlbeLib.verificationPieu_ = true;
    }
  }

  /**
   * 
   */
  public ValidationMessages validation() {

    final ValidationMessages vm = new ValidationMessages();

    int nbTroncons = nbTronconsTubCombo_.getSelectedIndex() + 1;

    // On stoppe l'�dition du tableau
    /*
     * if (tableTub_.isEditing()) tableTub_.getCellEditor().stopCellEditing();
     */

    // V�rification diam�tre
    if (AlbeLib.emptyField(diamTroncons_))
      vm.add(AlbeMsg.VMSG105);
    else if (AlbeLib.textFieldDoubleValue(diamTroncons_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG106);
    else if (AlbeLib.textFieldDoubleValue(diamTroncons_) <= 0)
      vm.add(AlbeMsg.VMSG114);

    // On v�rifie que toutes les cases du tableau sont remplies
    if (!AlbeLib.fullTable(tableTub_, modelTub_))
      vm.add(AlbeMsg.VMSG107);

    // On v�rifie que les longueurs sont strictement positives
    if (!AlbeLib.valeursColonneStrictPositives(tableTub_, 1))
      vm.add(AlbeMsg.VMSG117);

    // On v�rifie que les �paisseurs sont strictement positives
    if (!AlbeLib.valeursColonneStrictPositives(tableTub_, 2))
      vm.add(AlbeMsg.VMSG118);

    // On v�rifie que les corrosions sont positives ou nulles
    if (!AlbeLib.valeursColonnePositivesOuNulles(tableTub_, 3))
      vm.add(AlbeMsg.VMSG119);

    // On v�rifie que les fyk sont strictement positives
    if (!AlbeLib.valeursColonneStrictPositives(tableTub_, 4))
      vm.add(AlbeMsg.VMSG120);

    // On v�rifie que la somme des longueurs des tron�ons est strictement
    // sup�rieure � la cote de la t�te de l'ouvrage moins la cote de calcul
    if (pieu_.getLongueurPieu() != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(pieu_.fp_.geo_.coteTete_) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(pieu_.fp_.geo_.coteCalcul_) != VALEUR_NULLE.value
        && pieu_.getLongueurPieu() <= (AlbeLib.textFieldDoubleValue(pieu_.fp_.geo_.coteTete_) - AlbeLib
            .textFieldDoubleValue(pieu_.fp_.geo_.coteCalcul_)))
      vm.add(AlbeMsg.VMSG111);

    // On v�rifie que l'�paisseur de chaque tron�on est inf�rieure ou �gale �
    // son rayon
    for (int i = 0; i < nbTroncons; i++) {
      try {
        Double d = (Double) tableTub_.getValueAt(i, 2);
        if (d != null && AlbeLib.textFieldDoubleValue(diamTroncons_) != VALEUR_NULLE.value
            && d.doubleValue() > AlbeLib.textFieldDoubleValue(diamTroncons_) / 2)
          vm.add(AlbeMsg.getMsgPieuEpaisseur(i + 1));
      }
      catch (final ArrayIndexOutOfBoundsException _e1) {}
    }
    // On v�rifie que la perte d'�paisseur par corrosion de chaque tron�on est
    // strictement inf�rieure � son rayon
    for (int i = 0; i < nbTroncons; i++) {
      try {
        Double d1 = (Double) tableTub_.getValueAt(i, 3);
        Double d2 = (Double) tableTub_.getValueAt(i, 2);
        if (d1 != null && d2 != null && d1.doubleValue() >= d2.doubleValue())
          vm.add(AlbeMsg.getMsgPieuCorrosion(i + 1));
      }
      catch (final ArrayIndexOutOfBoundsException _e2) {}

    }

    return vm;
  }
}