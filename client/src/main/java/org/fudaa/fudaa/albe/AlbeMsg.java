package org.fudaa.fudaa.albe;

/**
 * Librairie Albe des messages.
 * 
 * @author Sabrina Delattre
 */
public final class AlbeMsg {
  // Messages affich�s dans des boites de dialogues Erreur

  public final static String ERR001 = "Impossible de visualiser les courbes :\nvous n'avez pas saisi la hauteur de la d�fense.";

  public final static String ERR002 = "Impossible de visualiser les courbes :\nla hauteur de la d�fense n'est pas valide.";

  public final static String ERR003 = "Impossible de visualiser les courbes :\nvous n'avez pas saisi toutes les coordonn�es des points.";

  public final static String ERR004 = "Impossible de visualiser les courbes :\ncertaines coordonn�es de points ne sont pas valides.";

  public final static String ERR005 = "Vous devez entrer les valeurs des d�formations dans l'ordre strictement croissant.";

  public final static String ERR006 = "Pour une d�formation strictement sup�rieure � une autre, la r�action doit �tre strictement sup�rieure � la pr�c�dente."
      + "\nAutrement dit, la courbe doit �tre strictement croissante.";

  public final static String ERR007 = "Impossible de visualiser les courbes :\nles valeurs des d�formations doivent �tre comprises entre 0 et 100.";

  public final static String ERR008 = "Impossible de visualiser les courbes :\nles valeurs des efforts doivent �tre positives.\n";

  public final static String ERR009 = "Vous n'�tes pas connect� � un serveur Albe !";

  public final static String ERR010 = "Impossible d'�crire un fichier temporaire sur le disque. La note de calculs ne peut pas �tre visualis�e. Veuillez r�essayer.";

  public final static String ERR011 = "Erreur lors de l'exportation.\nLa note de calculs n'a pas pu �tre export�e.\nVeuillez r�essayer.";

  public final static String ERR012 = "La cote d'accostage doit �tre inf�rieure ou �gale � la cote de la t�te de l'ouvrage et strictement sup�rieure � la cote de dragage.\nVous devez rectifier cette erreur avant de lancer les calculs.";

  public final static String ERR013 = "La cote d'amarrage doit �tre inf�rieure ou �gale � la cote de la t�te de l'ouvrage et strictement sup�rieure � la cote de dragage.\nVous devez rectifier cette erreur avant de lancer les calculs.";

  public final static String ERR014 = "Vous ne pouvez pas lancer les calculs.\nVos donn�es comportent des erreurs ou vous avez omis de saisir des donn�es obligatoires.\n"
      + "Veuillez consulter le rapport d'erreurs ci-dessous pour en connaitre les d�tails.";

  public final static String ERR015 = "Impossible de visualiser les courbes :\nla hauteur de la d�fense doit �tre strictement positive.";

  public final static String ERR016 = "L'�nergie d'accostage doit �tre positive.";

  public final static String ERR017 = "L'effort d'amarrage doit �tre positif.";

  public final static String ERR018 = "Le coefficient sur le sol doit �tre positif.";

  public final static String ERR019 = "Le coefficient sur la d�fense doit �tre positif.";

  public final static String ERR020 = "Le coefficient sur l'acier doit �tre strictement positif.";

  public final static String ERR021 = "La r�action maximale doit �tre positive.";

  public final static String ERR022 = "La d�flexion maximale doit �tre comprise entre 0 et 100.";

  public final static String ERR023 = "Le d�placement maximal doit �tre positif.";

  public final static String ERR024 = "Le coefficient sur la mobilisation de la but�e doit �tre strictement positif.";

  public final static String ERR025 = "Le coefficient sur la r�sistance des pieux doit �tre strictement positif.";

  // Messages affich�s dans des boites de dialogues Warning

  public final static String WAR001 = "Vous ne pouvez pas remplir � la fois les donn�es pour un accostage et un amarrage.\n"
      + "Si votre structure est effectivement soumise � un accostage, veuillez d'abord effacer les donn�es saisies pour l'amarrage (pour cel�, cliquez sur 'Initialiser').";

  public final static String WAR002 = "Vous ne pouvez pas remplir � la fois les donn�es pour un accostage et un amarrage.\n"
      + "Si votre structure est effectivement soumise � un amarrage, veuillez d'abord effacer les donn�es saisies pour l'accostage (pour cel�, cliquez sur 'Initialiser').";

  public final static String WAR003 = "Vous ne pouvez pas remplir � la fois les donn�es pour un pieu tubulaire et un pieu en caisson.\n"
      + "Si le pieu est effectivement tubulaire, veuillez d'abord effacer les donn�es saisies pour un pieu en caisson (pour cel�, cliquez sur 'Initialiser').";

  public final static String WAR004 = "Vous ne pouvez pas remplir � la fois les donn�es pour un pieu tubulaire et un pieu en caisson.\n"
      + "Si le pieu est effectivement en caisson, veuillez d'abord effacer les donn�es saisies pour un pieu tubulaire (pour cel�, cliquez sur 'Initialiser').";

  public final static String WAR005 = "Vous ne pouvez pas remplir les donn�es pour deux comportements de sol diff�rents.\n"
      + "Si vous voulez utiliser une autre courbe de r�action du sol, veuillez d'abord effacer les donn�es actuellement affich�es (pour cel�, cliquez sur 'Initialiser toutes les couches').";

  public final static String WAR006 = "Vous avez modifi� des param�tres depuis la r�alisation des calculs.\nLes r�sultats que vous allez visualiser correspondent aux anciennes donn�es.\n"
      + "Pour acc�der aux nouveaux r�sultats, vous devez � nouveau valider vos donn�es.";

  public final static String WAR007 = "Certains param�tres ont �t� modifi�s depuis la r�alisation des calculs.\n"
      + "La note de calculs que vous allez visualiser correspond aux anciennes donn�es.\n"
      + "Pour acc�der aux nouveaux r�sultats, vous devez � nouveau valider vos donn�es.";

  public final static String WAR008 = "Certains param�tres ont �t� modifi�s depuis la r�alisation des calculs.\n"
      + "La note de calculs que vous allez sauvegarder correspond aux anciennes donn�es.\n"
      + "Pour acc�der aux nouveaux r�sultats, vous devez � nouveau valider vos donn�es.";

  // Messages affich�s dans des boites de dialogues Message ou Confirmation

  public final static String DLG001 = "Des erreurs bloquantes ont �t� rencontr�es, la validation n'a pas pu �tre effectu�e.\n\n"
      + "Il est possible que vos donn�es comportent des erreurs ou que vous ayez omis de saisir des donn�es obligatoires.\n"
      + "Veuillez consulter le rapport d'erreurs ci-dessous pour en connaitre les d�tails.";

  public final static String DLG002 = "La validation est termin�e.\n\nVos donn�es comportent des avertissements mais cel� ne bloque pas le lancement des calculs."
      + "\n\nCliquez sur 'Oui' pour lancer les calculs."
      + "\n\nCliquez sur 'Non' pour ne pas lancer les calculs et afficher le d�tail des avertissements.";

  public final static String DLG003 = "La validation est termin�e.\n\nVos donn�es ne comportent � priori aucune erreur.\nCliquez sur 'Continuer' pour lancer les calculs.";

  public final static String DLG004 = "Vous avez modifi� un param�tre.\nLes r�sultats ne sont plus valides:\nils vont �tre effac�s.\n";

  public final static String DLG005 = "Les calculs sont termin�s.\n\nVous pouvez visualiser les r�sultats en allant dans le menu 'R�sultats'.";

  public final static String DLG006 = "Les calculs sont termin�s.\n\nCliquez sur 'Continuer' pour voir les r�sultats d�taill�s.";

  public final static String DLG007 = "Exportation termin�e avec succ�s.\nLa note de calculs vient d'�tre enregistr�e au format HTML dans le fichier <FILE>.\nLes images sont situ�es dans le r�pertoire <IMGDIR>.";

  public final static String DLG008 = "La d�flexion maximale devrait �tre inf�rieure ou �gale � la d�flexion maximale introduite dans l'onglet 'D�fense'."
      + "\nCet avertissement ne bloque pas le lancement des calculs.\nCliquez sur 'Oui' si vous voulez lancer les calculs.\nCliquez sur non si vous souhaitez modifier vos param�tres.";

  public final static String DLG009 = "Afin de visualiser le sch�ma � l'echelle de l'ouvrage, vous devez au minimum rentrer la cote de la t�te de l'ouvrage "
      + "et la cote de dragage (onglet 'Param�tres g�om�triques') ainsi que la longueur du premier tron�on (onglet 'Caract�ristiques du pieu').";

  // Messages d'erreurs affich�s sur la console

  public final static String CONS001 = "Num�ro de tron�on introuvable dans le tableau";

  public final static String CONS002 = "Aucun tron�on dans la plage sp�cifi�e par les indices";

  public final static String CONS003 = "Impossible de r�cup�rer les informations du tron�on";

  public final static String CONS004 = "Le nombre de tron�ons dans le tableau a chang�, impossible de r�cup�rer la liste des tron�ons";

  public final static String CONS005 = "Impossible de charger une structure de tron�ons nulle";

  public final static String CONS006 = "Point introuvable";

  public final static String CONS007 = "Aucun point correspondant";

  public final static String CONS008 = "Impossible de r�cup�rer les informations du point";

  public final static String CONS009 = "Le nombre de points a chang�, impossible de r�cup�rer la liste des points";

  public final static String CONS010 = "Impossible de charger une structure de points nulle";

  public final static String CONS011 = "Num�ro de combinaison introuvable dans le tableau";

  public final static String CONS012 = "Aucune combinaison dans la plage sp�cifi�e par les indices";

  public final static String CONS013 = "Impossible de r�cup�rer les informations de la combinaison";

  public final static String CONS014 = "Impossible de charger une structure de combinaison nulle";

  public final static String CONS015 = "Aucune couche dans la plage sp�cifi�e par les indices";

  public final static String CONS016 = "Impossible de r�cup�rer les informations de la couche";

  public final static String CONS017 = "Le nombre de couches a chang�, impossible de r�cup�rer la liste des couches";

  public final static String CONS018 = "Impossible de charger une structure de couche nulle";

  public final static String CONS019 = "Impossible de r�cup�rer le coefficient sur l'acier";

  public final static String CONS020 = "Impossible de charger un tableau nul de coefficients sur l'acier";

  // Cat�gories de messages pour la validation (correspondent aux diff�rents
  // onglets possibles)

  public final static String CAT001 = "Param�tres g�om�triques";

  public final static String CAT002 = "Sol";

  public final static String CAT003 = "Pieu";

  public final static String CAT004 = "Action";

  public final static String CAT005 = "D�fenses d'accostage";

  public final static String CAT006 = "Crit�res de dimensionnement";

  public final static String CAT007 = "Coefficients partiels";

  // URL
  public final static String URL001 = "geometrie";
  
  public final static String URL002 = "sol";
  
  public final static String URL003 = "pieu";
  
  public final static String URL004 = "actions";
  
  public final static String URL005 = "defensesaccostage";
  
  public final static String URL006 = "criteresdimensionnement";
  
  public final static String URL007 = "coefficientspartiels";
  
  public final static String URL008 = "validation";
  
  public final static String URL009 = "lancementcalculs";

  // Messages pour la validation des onglets

  // Onglet 'Param�tres g�om�triques'
  public final static VMessage VMSG001 = new VMessage(1, CAT001, "cote t�te ouvrage", "manquante",
      "Vous devez saisir la cote de la t�te de l'ouvrage.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG002 = new VMessage(2, CAT001, "cote dragage", "manquante",
      "Vous devez saisir la cote de dragage du bassin.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG003 = new VMessage(3, CAT001, "cote calcul", "manquante",
      "Vous devez saisir la cote de calcul.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG004 = new VMessage(4, CAT001, "cote t�te ouvrage", "non valide",
      "La cote de la t�te de l'ouvrage doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG005 = new VMessage(5, CAT001, "cote dragage", "non valide",
      "La cote de dragage doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG006 = new VMessage(6, CAT001, "cote calcul", "non valide",
      "La cote de calcul doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG007 = new VMessage(7, CAT001, "cote dragage",
      "sup�rieure ou �gale � cote t�te ouvrage",
      "La cote de dragage doit �tre strictement inf�rieure � la cote de la t�te de l'ouvrage.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG008 = new VMessage(8, CAT001, "cote calcul",
      "sup�rieure ou �gale � cote t�te ouvrage",
      "La cote de calcul doit �tre strictement inf�rieure � la cote de la t�te de l'ouvrage.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG009 = new VMessage(
      9,
      CAT001,
      "cote calcul",
      "sup�rieure ou �gale � cote dragage",
      "La cote de calcul permet de tenir compte des probl�mes de surdragage et d'affouillement. Elle est habituellement strictement inf�rieure � la cote de dragage.",
      VMessage.WARNING);

  public final static VMessage VMSG010 = new VMessage(
      10,
      CAT001 + "/" + CAT002,
      "cote dragage - cote calcul en valeur absolue",
      "sup�rieure ou �gale � epaisseur couche 1",
      "La cote de dragage - la cote de calcul en valeur absolue doit �tre strictement inf�rieure � l'�paisseur de la premi�re couche de sol.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG011 = new VMessage(
      11,
      CAT001 + "/" + CAT002 + "/" + CAT003,
      "cote du bas du pieu",
      "inf�rieure � cote du bas de la derni�re couche",
      "La cote du bas du pieu doit �tre sup�rieure ou �gale � la cote du bas de la derni�re couche de sol (= cote dragage - somme des �paisseurs des couches).",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG012 = new VMessage(
      12,
      CAT001 + "/" + CAT003,
      "cote du bas du pieu",
      "sup�rieure au max de (cote dragage, cote calcul)",
      "La cote du bas du pieu doit �tre inf�rieure ou �gale au maximum de la cote de dragage et de la cote de calcul.",
      VMessage.FATAL_ERROR);

  // Onglet 'Action'
  public final static VMessage VMSG020 = new VMessage(20, CAT004, "accostage haut", "manquant",
      "Vous devez saisir la cote d'accostage haut.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG021 = new VMessage(21, CAT004, "accostage haut", "non valide",
      "La cote d'accostage haut doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG022 = new VMessage(22, CAT004, "accostage bas", "manquant",
      "Vous devez saisir la cote d'accostage bas.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG023 = new VMessage(23, CAT004, "accostage bas", "non valide",
      "La cote d'accostage bas doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG024 = new VMessage(24, CAT004, "accostage fr�quent", "manquant",
      "Vous devez saisir l'�nergie d'accostage fr�quente.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG025 = new VMessage(25, CAT004, "accostage fr�quent",
      "non valide", "L'�nergie d'accostage fr�quente doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG026 = new VMessage(26, CAT004, "accostage caract�ristique",
      "manquant", "Vous devez saisir l'�nergie d'accostage caract�ristique.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG027 = new VMessage(27, CAT004, "accostage caract�ristique",
      "non valide", "L'�nergie d'accostage caract�ristique doit �tre un nombre.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG028 = new VMessage(28, CAT004, "accostage de calcul",
      "manquant", "Vous devez saisir l'�nergie d'accostage de calcul.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG029 = new VMessage(29, CAT004, "accostage de calcul",
      "non valide", "L'�nergie d'accostage de calcul doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG030 = new VMessage(30, CAT004, "accostage accidentel",
      "manquant", "Vous devez saisir l'�nergie d'accostage accidentelle.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG031 = new VMessage(31, CAT004, "accostage accidentel",
      "non valide", "L'�nergie d'accostage accidentelle doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG032 = new VMessage(32, CAT004, "cote amarrage", "manquante",
      "Vous devez saisir la cote d'amarrage.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG033 = new VMessage(33, CAT004, "cote amarrage", "non valide",
      "La cote d'amarrage doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG034 = new VMessage(34, CAT004, "amarrage fr�quent", "manquant",
      "Vous devez saisir l'effort d'amarrage fr�quent.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG035 = new VMessage(35, CAT004, "amarrage fr�quent",
      "non valide", "L'effort d'amarrage fr�quent doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG036 = new VMessage(36, CAT004, "amarrage caract�ristique",
      "manquant", "Vous devez saisir l'effort d'amarrage caract�ristique.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG037 = new VMessage(37, CAT004, "amarrage caract�ristique",
      "non valide", "L'effort d'amarrage caract�ristique doit �tre un nombre.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG038 = new VMessage(38, CAT004, "amarrage de calcul", "manquant",
      "Vous devez saisir l'effort d'amarrage de calcul.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG039 = new VMessage(39, CAT004, "amarrage de calcul",
      "non valide", "L'effort d'amarrage de calcul doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG040 = new VMessage(40, CAT004, "amarrage accidentel",
      "manquant", "Vous devez saisir l'effort d'amarrage accidentel.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG041 = new VMessage(41, CAT004, "amarrage accidentel",
      "non valide", "L'effort d'amarrage accidentel doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG042 = new VMessage(42, CAT004, "accostage fr�quent", "n�gatif",
      "L'�nergie d'accostage fr�quente doit �tre positive.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG043 = new VMessage(43, CAT004, "accostage caract�ristique",
      "n�gatif", "L'�nergie d'accostage caract�ristique doit �tre positive.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG044 = new VMessage(44, CAT004, "accostage de calcul", "n�gatif",
      "L'�nergie d'accostage de calcul doit �tre positive.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG045 = new VMessage(45, CAT004, "accostage accidentel",
      "n�gatif", "L'�nergie d'accostage accidentelle doit �tre positive.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG046 = new VMessage(46, CAT004, "amarrage fr�quent", "n�gatif",
      "L'effort d'amarrage fr�quent doit �tre positif.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG047 = new VMessage(47, CAT004, "amarrage caract�ristique",
      "n�gatif", "L'effort d'amarrage caract�ristique doit �tre positif.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG048 = new VMessage(48, CAT004, "amarrage de calcul", "n�gatif",
      "L'effort d'amarrage de calcul doit �tre positif.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG049 = new VMessage(49, CAT004, "amarrage accidentel", "n�gatif",
      "L'effort d'amarrage accidentel doit �tre positif.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG050 = new VMessage(
      50,
      CAT004,
      "cote accostage haut",
      "inf�rieure ou �gale � cote accostage bas",
      "La cote d'accostage haut est inf�rieure ou �gale � la cote d'accostage bas. Vous les avez probablement invers�es.",
      VMessage.WARNING);

  public final static VMessage VMSG051 = new VMessage(
      51,
      CAT001 + "/" + CAT004,
      "max de (cote accostage haut, cote accostage bas)",
      "sup�rieur � cote t�te ouvrage",
      "Le maximum de la cote d'accostage haut et de la cote d'accostage bas doit �tre inf�rieur ou �gal � la cote de la t�te de l'ouvrage.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG052 = new VMessage(52, CAT001 + "/" + CAT004, "cote amarrage",
      "sup�rieure � cote t�te ouvrage",
      "La cote d'amarrage doit �tre inf�rieure ou �gale � la cote de la t�te de l'ouvrage.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG053 = new VMessage(
      53,
      CAT001 + "/" + CAT004,
      "cote amarrage",
      "inf�rieure au max de (cote dragage, cote calcul)",
      "La cote d'amarrage doit �tre sup�rieure ou �gale au maximum de la cote de dragage et de la cote de calcul.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG054 = new VMessage(
      54,
      CAT001 + "/" + CAT004,
      "min de (cote accostage haut, cote accostage bas)",
      "inf�rieur � max de (cote dragage, cote calcul)",
      "Le minimum de la cote d'accostage haut et de la cote d'accostage bas doit �tre sup�rieur ou �gal au maximum de la cote de dragage et de la cote de calcul.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG055 = new VMessage(55, CAT004, "effort vertical fr�quent",
      "manquant", "Vous devez saisir l'effort vertical fr�quent.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG056 = new VMessage(56, CAT004, "effort vertical fr�quent",
      "non valide", "L'effort vertical fr�quent doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG057 = new VMessage(57, CAT004,
      "effort vertical caract�ristique", "manquant",
      "Vous devez saisir l'effort vertical caract�ristique.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG058 = new VMessage(58, CAT004,
      "effort vertical caract�ristique", "non valide",
      "L'effort vertical caract�ristique doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG059 = new VMessage(59, CAT004, "effort vertical de calcul",
      "manquant", "Vous devez saisir l'effort vertical de calcul.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG060 = new VMessage(60, CAT004, "effort vertical de calcul",
      "non valide", "L'effort vertical de calcul doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG061 = new VMessage(61, CAT004, "effort vertical accidentel",
      "manquant", "Vous devez saisir l'effort vertical accidentel.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG062 = new VMessage(62, CAT004, "effort vertical accidentel",
      "non valide", "L'effort vertical accidentel doit �tre un nombre.", VMessage.FATAL_ERROR);
  
  public final static VMessage VMSG063 = new VMessage(63, CAT004, "effort vertical en t�te de pieu",
      "manquant", "Vous devez saisir l'effort vertical en t�te de pieu.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG064 = new VMessage(64, CAT004, "effort vertical en t�te de pieu",
      "non valide", "L'effort vertical en t�te de pieu doit �tre un nombre.", VMessage.FATAL_ERROR);

  // Onglet 'Crit�res de dimensionnement'
  public final static VMessage VMSG070 = new VMessage(70, CAT006, "d�placement en t�te d'ouvrage",
      "manquant", "Vous devez saisir le d�placement en t�te d'ouvrage.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG071 = new VMessage(71, CAT006, "d�placement en t�te d'ouvrage",
      "non valide", "Le d�placement en t�te d'ouvrage doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG072 = new VMessage(72, CAT006,
      "d�flexion de la d�fense (ELS rare)", "manquante",
      "Vous devez saisir la d�flexion de la d�fense pour l'ELS rare.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG073 = new VMessage(73, CAT006,
      "d�flexion de la d�fense (ELS rare)", "non valide",
      "La d�flexion de la d�fense pour l'ELS rare doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG074 = new VMessage(74, CAT006,
      "d�flexion de la d�fense (ELU fonda)", "manquante",
      "Vous devez saisir la d�flexion de la d�fense pour l'ELU fondamental.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG075 = new VMessage(75, CAT006,
      "d�flexion de la d�fense (ELU fonda)", "non valide",
      "La d�flexion de la d�fense pour l'ELU fondamental doit �tre un nombre.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG076 = new VMessage(76, CAT006,
      "d�flexion de la d�fense (ELU acc)", "manquante",
      "Vous devez saisir la d�flexion de la d�fense pour l'ELU accidentel.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG077 = new VMessage(77, CAT006,
      "d�flexion de la d�fense (ELU acc)", "non valide",
      "La d�flexion de la d�fense pour l'ELU accidentel doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG078 = new VMessage(78, CAT006,
      "r�action en t�te d'ouvrage (ELU fonda)", "manquante",
      "Vous devez saisir la r�action en t�te d'ouvrage pour l'ELU fondamental.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG079 = new VMessage(79, CAT006,
      "r�action en t�te d'ouvrage (ELU fonda)", "non valide",
      "La r�action en t�te d'ouvrage pour l'ELU fondamental doit �tre un nombre.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG080 = new VMessage(80, CAT006,
      "r�action en t�te d'ouvrage (ELU acc)", "manquante",
      "Vous devez saisir la r�action en t�te d'ouvrage pour l'ELU accidentel.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG081 = new VMessage(81, CAT006,
      "r�action en t�te d'ouvrage (ELU acc)", "non valide",
      "La r�action en t�te d'ouvrage pour l'ELU accidentel doit �tre un nombre.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG082 = new VMessage(82, CAT006, "d�placement en t�te d'ouvrage",
      "n�gatif", "Le d�placement en t�te d'ouvrage doit �tre positif.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG083 = new VMessage(83, CAT006,
      "d�flexion de la d�fense (ELS rare)", "n�gative",
      "La d�flexion de la d�fense pour l'ELS rare doit �tre positive.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG084 = new VMessage(84, CAT006,
      "d�flexion de la d�fense (ELU fonda)", "n�gative",
      "La d�flexion de la d�fense pour l'ELU fondamental doit �tre positive.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG085 = new VMessage(85, CAT006,
      "d�flexion de la d�fense (ELU acc)", "n�gative",
      "La d�flexion de la d�fense pour l'ELU accidentel doit �tre positive.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG086 = new VMessage(86, CAT006,
      "r�action en t�te d'ouvrage (ELU fonda)", "n�gative",
      "La r�action en t�te d'ouvrage l'ELU fondamental doit �tre positive.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG087 = new VMessage(87, CAT006,
      "r�action en t�te d'ouvrage (ELU acc)", "n�gative",
      "La r�action en t�te d'ouvrage pour l'ELU accidentel doit �tre positive.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG088 = new VMessage(
      88,
      CAT005 + "/" + CAT006,
      "d�flexion de la d�fense (ELS rare)",
      "sup�rieure � la d�flexion maximale",
      "La d�flexion de d�fense pour l'ELS rare devrait �tre inf�rieure ou �gale � la d�flexion maximale introduite dans l'onglet 'D�fense'.",
      VMessage.WARNING);

  public final static VMessage VMSG089 = new VMessage(
      89,
      CAT005 + "/" + CAT006,
      "d�flexion de la d�fense (ELU fonda)",
      "sup�rieure � la d�flexion maximale",
      "La d�flexion de d�fense pour l'ELU fondamental devrait �tre inf�rieure ou �gale � la d�flexion maximale introduite dans l'onglet 'D�fense'.",
      VMessage.WARNING);

  public final static VMessage VMSG090 = new VMessage(
      90,
      CAT005 + "/" + CAT006,
      "d�flexion de la d�fense (ELU acc)",
      "sup�rieure � la d�flexion maximale",
      "La d�flexion de d�fense pour l'ELU accidentel devrait �tre inf�rieure ou �gale � la d�flexion maximale introduite dans l'onglet 'D�fense'.",
      VMessage.WARNING);

  public final static VMessage VMSG091 = new VMessage(91, CAT006, "d�flexions de la d�fense",
      "non croissantes", "Les d�flexions devraient �tre croissantes.", VMessage.WARNING);

  // Onglet 'Coefficients partiels'
  public final static VMessage VMSG092 = new VMessage(92, CAT007, "coefficient(s) sol",
      "manquant(s)", "Vous devez saisir l'int�gralit� des coefficients sur le sol.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG093 = new VMessage(93, CAT007, "coefficient(s) d�fense",
      "manquant(s)", "Vous devez saisir l'int�gralit� des coefficients sur la d�fense.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG094 = new VMessage(94, CAT007, "coefficient(s) acier",
      "manquant(s)", "Vous devez saisir l'int�gralit� des coefficients sur l'acier.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG095 = new VMessage(95, CAT007, "coefficient(s) mod�le",
      "manquant(s)", "Vous devez saisir l'int�gralit� des coefficients de mod�le.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG096 = new VMessage(96, CAT007, "coefficient(s) sol",
      "n�gatif(s)", "Les coefficients sur le sol doivent �tre positifs.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG097 = new VMessage(97, CAT007, "coefficient(s) d�fense",
      "n�gatif(s)", "Les coefficients sur la d�fense doivent �tre positifs.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG098 = new VMessage(98, CAT007, "coefficient(s) acier",
      "n�gatif(s) ou nul(s)", "Les coefficients sur l'acier doivent �tre strictements positifs.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG099 = new VMessage(99, CAT007, "coefficient(s) mod�le",
      "n�gatif(s) ou nul(s)", "Les coefficients de mod�le doivent �tre strictements positifs.",
      VMessage.FATAL_ERROR);

  // Onglet 'Caract�ristiques du pieu'
  public final static VMessage VMSG105 = new VMessage(105, CAT003, "diam�tre", "manquant",
      "Vous devez saisir le diam�tre des tron�ons.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG106 = new VMessage(106, CAT003, "diam�tre", "non valide",
      "Le diam�tre des tron�ons doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG107 = new VMessage(
      107,
      CAT003,
      "propri�t�(s) des tron�ons",
      "manquante(s)",
      "Vous devez saisir l'int�gralit� des propri�t�s de chaque tron�on. (Toutes les cellules du tableau doivent �tre remplies.)",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG109 = new VMessage(109, CAT003, "largeur", "manquante",
      "Vous devez saisir la largeur effective du pieu.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG110 = new VMessage(110, CAT003, "largeur", "non valide",
      "La largeur effective du pieu doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG111 = new VMessage(
      111,
      CAT001 + "/" + CAT003,
      "somme longueurs tron�ons",
      "inf�rieure ou �gale � cote t�te ouvrage - cote calcul",
      "La somme des longueurs des tron�ons doit �tre strictement sup�rieure � la cote de la t�te de l'ouvrage moins la cote de calcul (introduites dans l'onglet 'Param�tres g�om�triques').",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG112 = new VMessage(112, CAT003, "excentricit� du caisson",
      "manquante", "Vous devez saisir l'excentricit� du caisson.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG113 = new VMessage(113, CAT003, "excentricit� du caisson",
      "non valide", "L'excentricit� du caisson doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG114 = new VMessage(114, CAT003, "diam�tre", "n�gatif ou nul",
      "Le diam�tre des tron�ons doit �tre strictement positif.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG115 = new VMessage(115, CAT003, "largeur", "n�gative ou nulle",
      "La largeur effective du pieu doit �tre strictement positive.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG116 = new VMessage(116, CAT003, "excentricit� du caisson",
      "n�gative ou nulle", "L'excentricit� du caisson doit �tre strictement positive.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG117 = new VMessage(117, CAT003, "longueur tron�on(s)",
      "n�gative ou nulle", "La longueur de chaque tron�on doit �tre strictement positive.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG118 = new VMessage(118, CAT003, "�paisseur tron�on(s)",
      "n�gative ou nulle", "L'�paisseur de chaque tron�on doit �tre strictement positive.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG119 = new VMessage(119, CAT003, "corrosion tron�on(s)",
      "n�gative", "La perte d'�paisseur par corrosion de chaque tron�on doit �tre positive.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG120 = new VMessage(
      120,
      CAT003,
      "fyk tron�on(s)",
      "n�gative ou nulle",
      "La valeur caract�ristique de la limite �lastique de l'acier de chaque tron�on doit �tre strictement positive.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG121 = new VMessage(121, CAT003,
      "section non corrod�e tron�on(s)", "n�gative ou nulle",
      "La section non corrod�e de chaque tron�on doit �tre strictement positive.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG122 = new VMessage(122, CAT003,
      "inertie non corrod�e tron�on(s)", "n�gative ou nulle",
      "L'inertie non corrod�e de chaque tron�on doit �tre strictement positive.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG123 = new VMessage(123, CAT003, "section corrod�e tron�on(s)",
      "n�gative ou nulle", "La section corrod�e de chaque tron�on doit �tre strictement positive.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG124 = new VMessage(124, CAT003, "inertie corrod�e tron�on(s)",
      "n�gative ou nulle", "L'inertie corrod�e de chaque tron�on doit �tre strictement positive.",
      VMessage.FATAL_ERROR);

  // Onglet 'D�fense'
  public final static VMessage VMSG131 = new VMessage(131, CAT005, "hauteur", "manquante",
      "Vous devez saisir la hauteur de la d�fense.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG132 = new VMessage(132, CAT005, "hauteur", "non valide",
      "La hauteur de la d�fense doit �tre un nombre.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG133 = new VMessage(133, CAT005, "hauteur", "n�gative ou nulle",
      "La hauteur de la d�fense doit �tre strictement positive.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG134 = new VMessage(134, CAT005, "d�formation(s)",
      "manquante(s)", "Vous devez saisir les d�formations pour chaque point.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG135 = new VMessage(135, CAT005, "effort(s)", "manquant(s)",
      "Vous devez saisir les efforts pour chaque point.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG136 = new VMessage(136, CAT005, "d�formation(s)",
      "non valide(s)", "Les d�formations doivent �tre des nombres.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG137 = new VMessage(137, CAT005, "effort(s)", "non valide(s)",
      "Les efforts doivent �tre des nombres.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG138 = new VMessage(138, CAT005, "d�formations",
      "non strictement croissantes",
      "Vous devez entrer les valeurs des d�formations dans l'ordre strictement croissant.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG139 = new VMessage(139, CAT005, "courbe d�flexion-r�action",
      "non strictement croissante",
      "La courbe d�flexion-r�action doit �tre strictement croissante.", VMessage.FATAL_ERROR);

  public final static VMessage VMSG140 = new VMessage(140, CAT005, "d�formation(s)",
      "non comprise(s) entre 0 et 100", "Les d�formations doivent �tre comprises entre 0 et 100.",
      VMessage.FATAL_ERROR);

  public final static VMessage VMSG141 = new VMessage(141, CAT005, "effort(s)", "n�gatif(s)",
      "Les efforts doivent �tre positifs.", VMessage.FATAL_ERROR);

  // Onglet 'Sol'
  /**
   * les m�thodes qui suivent retournent le message sp�cifique � une couche de
   * sol.
   * 
   * @param _numCouche num�ro de la couche concern�e
   */
  public static VMessage getMsgEpaisseurSolManquante(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 200, CAT002 + " (couche " + _numCouche + ")",
        "�paisseur", "manquante", "Vous devez saisir l'�paisseur de la couche " + _numCouche + ".",
        VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgEpaisseurSolNonValide(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 300, CAT002 + " (couche " + _numCouche + ")",
        "�paisseur", "non valide", "L'�paisseur de la couche " + _numCouche
            + " n'est pas valide ; la valeur doit �tre un nombre.", VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgCoordSol(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 400, CAT002 + " (couche " + _numCouche + ")",
        "d�placement(s) / pression(s)", "manquant(s)", "Couche " + _numCouche
            + ":\nVous devez saisir toutes les coordonn�es des points de la courbe.",
        VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgDeplacementSol(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 500, CAT002 + " (couche " + _numCouche + ")",
        "d�placements", "non strictement croissants", "Les d�placements concernant la couche "
            + _numCouche + " doivent �tre entr�s par ordre strictement croissant.",
        VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgCourbeSol(int _numCouche) {
    VMessage vMsg = new VMessage(
        _numCouche + 600,
        CAT002 + " (couche " + _numCouche + ")",
        "courbe d�placement - pression",
        "non croissante",
        "Couche "
            + _numCouche
            + ":\npour une valeur de d�placement sup�rieure � une autre, la pression obtenue doit �tre sup�rieure ou �gale � la pr�c�dente.\nAutrement dit, la courbe doit �tre croissante.",
        VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgDonneesManquantesSol(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 700, CAT002 + " (couche " + _numCouche + ")",
        "donn�e(s)", "manquante(s)",
        "Vous devez saisir l'int�gralit� des propri�t�s concernant la couche " + _numCouche + ".",
        VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgDonneesNonValidesSol(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 800, CAT002 + " (couche " + _numCouche + ")",
        "donn�e(s)", "non valide(s)", "Certaines donn�es concernant la couche " + _numCouche
            + " ne sont pas valides (les valeurs ne sont pas des nombres).", VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgPfSupPl(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 900, CAT002 + " (couche " + _numCouche + ")",
        "pression de fluage", "sup�rieure ou �gale � pression limite",
        "La pression de fluage de la couche " + _numCouche
            + " doit �tre srictement inf�rieure � sa pression limite.", VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgEpaisseurNeg(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 1000, CAT002 + " (couche " + _numCouche + ")",
        "�paisseur", "n�gative ou  nulle", "L'�paisseur de la couche " + _numCouche
            + " doit �tre strictement positive.", VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgPressionLimiteNeg(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 1100, CAT002 + " (couche " + _numCouche + ")",
        "pression limite", "n�gative ou nulle", "La pression limite de la couche " + _numCouche
            + " doit �tre strictement positive.", VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgPressionFluageNeg(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 1200, CAT002 + " (couche " + _numCouche + ")",
        "pression de fluage", "n�gative ou nulle", "La pression de fluage de la couche "
            + _numCouche + " doit �tre strictement positive.", VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgModuleNeg(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 1300, CAT002 + " (couche " + _numCouche + ")",
        "module", "n�gatif ou nul", "Le module pressiom�trique de la couche " + _numCouche
            + " doit �tre strictement positif.", VMessage.FATAL_ERROR);
    return vMsg;
  }

  public static VMessage getMsgAlphaNeg(int _numCouche) {
    VMessage vMsg = new VMessage(_numCouche + 1400, CAT002 + " (couche " + _numCouche + ")",
        "coefficient rh�ologique", "non compris entre 0 et 1", "Le coefficient rh�ologique de la couche "
            + _numCouche + " doit �tre compris entre 0 et 1.", VMessage.FATAL_ERROR);
    return vMsg;
  }

  // Onglet 'Pieu'
  /**
   * les m�thodes qui suivent retournent le message sp�cifique � un tron�on.
   * 
   * @param _num num�ro du tron�on concern�
   */

  public static VMessage getMsgPieuEpaisseur(int _num) {
    VMessage msg = new VMessage(_num + 1500, CAT003, "�paisseur tron�on " + _num,
        "sup�rieure � rayon", "L'�paisseur du tron�on " + _num
            + " doit �tre inf�rieure ou �gale � son rayon.", VMessage.FATAL_ERROR);
    return msg;
  }

  public static VMessage getMsgPieuCorrosion(int _num) {
    VMessage msg = new VMessage(_num + 1700, CAT003, "corrosion tron�on " + _num,
        "sup�rieure ou �gale � �paisseur", "La perte d'�paisseur par corrosion du tron�on " + _num
            + " doit �tre strictement inf�rieure � son �paisseur.", VMessage.FATAL_ERROR);
    return msg;
  }

}
