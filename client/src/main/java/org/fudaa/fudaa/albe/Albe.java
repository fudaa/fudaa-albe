package org.fudaa.fudaa.albe;

import org.fudaa.fudaa.commun.impl.Fudaa;

import com.memoire.bu.BuResource;

/**
 * Classe principale du logiciel Fudaa-Albe.
 * 
 * @author Sabrina Delattre
 */

public class Albe {

  /**
   * M�thode principale de la classe, appel�e en premier au d�marrage de
   * l'application.
   */

  public static void main(String[] _args) {
    BuResource.BU.setIconFamily("crystal");
    Fudaa f = new Fudaa();
    f.launch(_args, AlbeImplementation.informationsSoftware(), false);
    f.startApp(new AlbeImplementation());

  }

}
