package org.fudaa.fudaa.albe;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.util.Map;

import javax.swing.JPanel;

import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;

import com.memoire.bu.BuCommonInterface;

public class AlbeSchemaOuvragePanel extends JPanel implements CtuluImageProducer {

  /** param�tres de Albe. */
  SParametresAlbe params_;

  /** Hauteur du sch�ma en pixels. */
  int hSchemaP_;

  /** Hauteur du sch�ma dans la r�alit� (en m�tres). */
  double hSchemaM_;

  /** T�te de l'ouvrage. */
  double teteOuvrage_;

  /** Cote de dragage. */
  double coteDragage_;

  /** Ordonn�e de la cote de dragage. */
  int yCoteDragage_;

  /** Tableaux de couleurs. */
  Color[] couleursTroncons_;

  Color[] couleursCouches_;

  /** entier permettant de se d�placer dans les tableaux de couleurs. */
  int numCouleur_;

  /**
   * bool�en permettant de savoir si la cote de calcul est renseign�e.
   */
  boolean coteCalculRenseignee_;

  BuCommonInterface appli_;

  /**
   * constructeur.
   * 
   * @param _params param�tres de Albe
   */
  public AlbeSchemaOuvragePanel(final BuCommonInterface _appli, SParametresAlbe _params) {
    super();
    setBackground(Color.WHITE);
    setPreferredSize(new Dimension(630, 600));
    appli_ = _appli;
    params_ = _params;
    hSchemaP_ = 540;
    teteOuvrage_ = params_.geo.coteTeteOuvrage;
    coteDragage_ = params_.geo.coteDragageBassin;
    couleursTroncons_ = new Color[] { Color.magenta, Color.green, Color.yellow, Color.blue };
    couleursCouches_ = new Color[] { Color.orange, Color.red, Color.cyan, Color.gray };
    coteCalculRenseignee_ = false;
  }

  /**
   * retourne la somme des �paisseurs (si renseign�es) des couches de sol.
   */
  public double getSommeEpaisseursCouches() {
    double res = 0;

    if (params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_MANUELLE)) {
      for (int i = 0; i < params_.geo.nombreCouchesSol; i++) {
        if (params_.sol.epaisseursCourbeManuelle[i] != VALEUR_NULLE.value)
          res += params_.sol.epaisseursCourbeManuelle[i];
      }
    } else {
      for (int i = 0; i < params_.geo.nombreCouchesSol; i++) {
        if (params_.sol.courbe[i].epaisseur != VALEUR_NULLE.value)
          res += params_.sol.courbe[i].epaisseur;
      }
    }
    return res;
  }

  /**
   * dessine les couches de sol.
   */

  public void paintCouchesSol(Graphics _g) {

    int hautCouche = yCoteDragage_;
    int epaisseurCoucheP;
    numCouleur_ = 0;
    // Bool�en permettant de savoir si au moins une couche a �t� dessin�e
    boolean coucheDessinee = false;

    if (params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_MANUELLE)) {
      for (int i = 0; i < params_.geo.nombreCouchesSol; i++) {
        if (params_.sol.epaisseursCourbeManuelle[i] != VALEUR_NULLE.value) {
          coucheDessinee = true;
          epaisseurCoucheP = (int) (hSchemaP_ * (params_.sol.epaisseursCourbeManuelle[i] / hSchemaM_));
          if (numCouleur_ == 4)
            numCouleur_ = 0;
          _g.setColor(couleursCouches_[numCouleur_++]);
          _g.fillRect(150, hautCouche, 350, epaisseurCoucheP);
          hautCouche += epaisseurCoucheP;
        }
      }
    } else {
      for (int i = 0; i < params_.geo.nombreCouchesSol; i++) {
        if (params_.sol.courbe[i].epaisseur != VALEUR_NULLE.value
            && params_.sol.courbe[i].epaisseur != 0) {
          coucheDessinee = true;
          epaisseurCoucheP = (int) (hSchemaP_ * (params_.sol.courbe[i].epaisseur / hSchemaM_));
          if (numCouleur_ == 4)
            numCouleur_ = 0;
          _g.setColor(couleursCouches_[numCouleur_++]);
          _g.fillRect(150, hautCouche, 350, epaisseurCoucheP);
          hautCouche += epaisseurCoucheP;
        }
      }
    }

    // On affiche le bas du sol (si au moins une couche a �t� dessin�e)
    if (coucheDessinee) {
      _g.setColor(Color.black);
      _g.drawString("Bas du sol", 180, hautCouche - 3);
    }
  }

  /**
   * dessine les tron�ons.
   */

  public void paintTroncons(Graphics _g) {
    int tmpLongeur = 30;
    double hauteurTronconM;
    int hauteurTronconP;
    numCouleur_ = 0;
    double longueurPieu = 0;

    for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
      hauteurTronconM = params_.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE) ? params_.pieu.tronconsTubulaire[i].longueur
          : params_.pieu.tronconsCaisson[i].longueur;
      if (hauteurTronconM != VALEUR_NULLE.value) {
        longueurPieu += hauteurTronconM;
        hauteurTronconP = (int) (hSchemaP_ * (hauteurTronconM / hSchemaM_));
        if (numCouleur_ == 4)
          numCouleur_ = 0;
        _g.setColor(couleursTroncons_[numCouleur_++]);
        _g.fillRect(300, tmpLongeur, 30, hauteurTronconP);
        tmpLongeur += hauteurTronconP;
      }
    }
    // On affiche le pied du pieu
    _g.setColor(Color.black);
    _g.drawString("Pied du pieu : " + AlbeLib.arronditDouble(teteOuvrage_ - longueurPieu) + " m",
        335, tmpLongeur + 2);
  }

  /**
   * dessine les cotes d'accostage (ou d'amarrage).
   */

  public void paintCotesAccostageAmarrage(Graphics _g) {

    if (params_.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)) {
      boolean coteAccostageHautRenseignee = false;
      boolean coteAccostageBasRenseignee = false;
      int yCoteAccostageHaut = 0;
      int yCoteAccostageBas = 0;
      if (params_.action.coteAccostageHaut != VALEUR_NULLE.value) {
        coteAccostageHautRenseignee = true;
        yCoteAccostageHaut = (int) ((teteOuvrage_ - params_.action.coteAccostageHaut) * hSchemaP_ / hSchemaM_) + 30;
        _g.setColor(Color.black);
        _g.drawLine(260, yCoteAccostageHaut, 300, yCoteAccostageHaut);

      }
      if (params_.action.coteAccostageBas != VALEUR_NULLE.value) {
        coteAccostageBasRenseignee = true;
        yCoteAccostageBas = (int) ((teteOuvrage_ - params_.action.coteAccostageBas) * hSchemaP_ / hSchemaM_) + 30;
        _g.drawLine(260, yCoteAccostageBas, 300, yCoteAccostageBas);

      }
      if (coteAccostageHautRenseignee && coteAccostageBasRenseignee
          && params_.action.coteAccostageHaut == params_.action.coteAccostageBas)
        _g.drawString("AC haut/AC bas : " + params_.action.coteAccostageHaut + " m", 100,
            yCoteAccostageHaut + 3);
      else {
        if (coteAccostageHautRenseignee) {
          _g.drawString("AC haut : " + params_.action.coteAccostageHaut + " m", 150,
              yCoteAccostageHaut + 3);
        }
        if (coteAccostageBasRenseignee) {
          _g.drawString("AC bas : " + params_.action.coteAccostageBas + " m", 150,
              yCoteAccostageBas + 3);
        }
      }

    } else if (params_.action.coteAmarrage != VALEUR_NULLE.value) {
      int yCoteAmarrage = (int) ((teteOuvrage_ - params_.action.coteAmarrage) * hSchemaP_ / hSchemaM_) + 30;
      _g.setColor(Color.black);
      _g.drawLine(260, yCoteAmarrage, 300, yCoteAmarrage);
      _g.drawString("Amarrage : " + params_.action.coteAmarrage + " m", 140, yCoteAmarrage + 3);
    }
  }

  /**
   * 
   */
  public void paintComponent(Graphics _g) {

    // Calcul de la hauteur r�elle du sch�ma en m�tres (= t�te de l'ouvrage -
    // cote de dragage + somme des �paisseurs des couches)
    hSchemaM_ = teteOuvrage_ - coteDragage_ + getSommeEpaisseursCouches();

    // Calcul de l'ordonn�e de la cote de dragage
    yCoteDragage_ = (int) ((teteOuvrage_ - coteDragage_) * hSchemaP_ / hSchemaM_) + 30;

    // Ordonn�e de la cote de calcul
    int yCoteCalcul = 0;
    if (params_.geo.coteCalcul != VALEUR_NULLE.value) {
      coteCalculRenseignee_ = true;
      yCoteCalcul = (int) ((teteOuvrage_ - params_.geo.coteCalcul) * hSchemaP_ / hSchemaM_) + 30;
    }

    // On dessine les couches de sol
    paintCouchesSol(_g);

    // On dessine les tron�ons
    paintTroncons(_g);

    // On dessine la cote de dragage
    _g.setColor(Color.blue);
    _g.drawLine(100, yCoteDragage_, 550, yCoteDragage_);
    _g.drawString("Cote de dragage : " + params_.geo.coteDragageBassin + " m", 450,
        yCoteDragage_ - 3);

    // On dessine la cote de calcul (si renseign�e)
    if (coteCalculRenseignee_) {
      _g.setColor(Color.red);
      _g.drawLine(100, yCoteCalcul, 550, yCoteCalcul);
      _g.drawString("Cote de calcul : " + params_.geo.coteCalcul + " m", 30, yCoteCalcul - 3);
    }

    // On affiche la t�te de l'ouvrage
    _g.setColor(Color.black);
    _g.drawString("T�te de l'ouvrage : " + params_.geo.coteTeteOuvrage + " m", 260, 28);

    // On dessine les cotes d'accostage (ou amarrage)
    paintCotesAccostageAmarrage(_g);
  }

  /**
   * exporte le sch�ma.
   */
  public void exporter() {
    this.setDoubleBuffered(false);
    // CtuluImageExport.exportImageFor((FudaaImplementation)
    // appli_.getImplementation(), this);
    final String f = AlbeLib.getFileChoosenByUser(this, "Exporter le sch�ma", "Exporter",
        AlbeLib.USER_HOME + "schema_ouvrage.png");
    CtuluImageExport.export(this.produceImage(null), new File(f), "png", (FudaaImplementation) appli_.getImplementation());
  }

  /**
   * 
   */
  public Dimension getDefaultImageDimension() {
    return null;
  }

  /**
   * m�thode d'impl�mentation de CtuluImageProducer afin de pouvoir exporter.
   */
  public BufferedImage produceImage(Map _params) {
    return CtuluLibImage.produceImageForComponent(this, _params);
  }

  /**
   * 
   */
  public BufferedImage produceImage(int _w, int _h, Map _params) {
    return CtuluLibImage.produceImageForComponent(this, _w, _h, _params);
  }
}
