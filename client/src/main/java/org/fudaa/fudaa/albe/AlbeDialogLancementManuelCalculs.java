package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import org.fudaa.dodico.corba.albe.SLancementCalculs;
import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * Description de la boite de dialogue pour le lancement manuel des calculs.
 * 
 * @author Sabrina Delattre
 */

public class AlbeDialogLancementManuelCalculs extends JDialog implements ActionListener,
    KeyListener {

  BuTextField energieAccostage_, effortAmarrage_, cote_, gMSol_, gMDefense_, gfy_, reactionMax_,
      deflexionMax_, deplacementMax_, gdButee_, gdPieux_;

  JRadioButton ouiRadio_, nonRadio_;

  JButton lancerCalculButton_, annulerButton_;

  SpringLayout layout_;

  SParametresAlbe params_;

  BuCommonInterface appli_;

  /** bool�en pr�cisant le type d'action (true = accostage / false = amarrage). */
  boolean accostage_;

  /**
   * bool�en pr�cisant s'il y a des d�fenses (true = accostage et defense /
   * false = pas de d�fense).
   */
  boolean defense_;

  /**
   * Constructeur.
   */

  public AlbeDialogLancementManuelCalculs(final BuCommonInterface _appli) {

    super(_appli.getImplementation().getFrame(), "Lancement manuel", true);

    appli_ = _appli;
    params_ = ((AlbeImplementation) _appli.getImplementation()).fp_.getParametres();

    Container c = getContentPane();
    c.setLayout(new BuVerticalLayout());

    layout_ = new SpringLayout();

    defense_ = false;

    initTextField();
    initRadioButton();
    initButton();

    // Bloc 1 : param�tres g�om�triques
    BuPanel b1 = AlbeLib.titledPanel("Param�tres g�om�triques", AlbeLib.ETCHED_BORDER);
    b1.setLayout(new BuVerticalLayout());

    // // Bloc 1.0 : information
    BuPanel b10 = new BuPanel();
    b10
        .add(new BuLabel(
            "<html><font color = \"red\">La cote de dragage consid�r�e est la cote de dragage.</font></html>"));

    // // Bloc 1.1 : boutons radio 'Corrosion'
    BuPanel b11 = new BuPanel(new BorderLayout());
    BuPanel tempPanel = new BuPanel();
    tempPanel.add(new JLabel("Corrosion"));
    tempPanel.add(ouiRadio_);
    tempPanel.add(nonRadio_);
    b11.add(tempPanel, BorderLayout.WEST);

    // // Bloc 1.2
    BuPanel b12 = new BuPanel();
    b12.setLayout(layout_);
    // Si accostage
    if (params_.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)) {
      accostage_ = true;
      b12.add(new JLabel("Energie d'accostage"));
      b12.add(energieAccostage_);
      b12.add(new JLabel("kJ"));
      b12.add(new JLabel("Cote d'accostage"));
      b12.add(cote_);
      b12.add(new JLabel("m"));
    } else {
      accostage_ = false;
      b12.add(new JLabel("Effort d'amarrage"));
      b12.add(effortAmarrage_);
      b12.add(new JLabel("kN"));
      b12.add(new JLabel("Cote d'amarrage"));
      b12.add(cote_);
      b12.add(new JLabel("m"));
    }
    SpringUtilities.makeCompactGrid(b12, 2, 3, 5, 10, 10, 10);

    b1.add(b10);
    b1.add(b11);
    b1.add(b12);

    // Bloc 2 : coefficients partiels
    BuPanel b2 = AlbeLib.titledPanel("Coefficients partiels", AlbeLib.ETCHED_BORDER);
    b2.setLayout(layout_);
    b2.add(AlbeLib.symbol(AlbeLib.getIcon("gamma-m")));
    b2.add(new JLabel("sur le comportement du sol"));
    b2.add(gMSol_);
    b2.add(new JLabel("(-)"));
    if (accostage_ && params_.defense.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI)) {
      defense_ = true;
      b2.add(AlbeLib.symbol(AlbeLib.getIcon("gamma-m")));
      b2.add(new JLabel("sur le comportement de la d�fense"));
      b2.add(gMDefense_);
      b2.add(new JLabel("(-)"));
    }
    b2.add(AlbeLib.symbol(AlbeLib.getIcon("gamma-fy")));
    b2.add(new JLabel("sur la r�sistance �lastique de l'acier"));
    b2.add(gfy_);
    b2.add(new JLabel("(-)"));

    SpringUtilities.makeCompactGrid(b2, defense_ ? 3 : 2, 4, 5, 10, 10, 10);

    // Bloc 3 : crit�res de dimensionnement
    BuPanel b3 = AlbeLib.titledPanel("Crit�res de dimensionnement", AlbeLib.ETCHED_BORDER);
    b3.setLayout(new BuVerticalLayout());

    // // Bloc 3.1
    BuPanel b31 = new BuPanel();
    b31.setLayout(layout_);
    if (accostage_) {
      b31.add(new JLabel("R�action maximale"));
      b31.add(reactionMax_);
      b31.add(new JLabel("kN"));
      if (defense_) {
        b31.add(new JLabel("D�flexion maximale"));
        b31.add(deflexionMax_);
        b31.add(new JLabel("%"));
      }
    }
    b31.add(new JLabel("D�placement maximal"));
    b31.add(deplacementMax_);
    b31.add(new JLabel("m"));

    SpringUtilities.makeCompactGrid(b31, defense_ ? 3 : (accostage_ ? 2 : 1), 3, 5, 10, 10, 10);

    // Bloc 3.2
    BuPanel b32 = new BuPanel();
    b32.setLayout(layout_);
    b32.add(AlbeLib.symbol(AlbeLib.getIcon("gamma-d")));
    b32.add(new JLabel("mobilisation de la but�e"));
    b32.add(gdButee_);
    b32.add(new JLabel("(-)"));
    b32.add(AlbeLib.symbol(AlbeLib.getIcon("gamma-d")));
    b32.add(new JLabel("r�sistance des pieux"));
    b32.add(gdPieux_);
    b32.add(new JLabel("(-)"));
    SpringUtilities.makeCompactGrid(b32, 2, 4, 5, 10, 10, 10);

    b3.add(b31);
    b3.add(b32);

    // Bloc 4 : boutons 'Annuler' et 'Lancer le calcul'
    BuPanel b4 = new BuPanel(new FlowLayout());
    b4.add(lancerCalculButton_);
    b4.add(annulerButton_);

    // Bloc regroupant les param�tres
    BuPanel bParam = AlbeLib.titledPanel("Param�tres du lancement manuel", AlbeLib.COMPOUND_BORDER);
    bParam.setLayout(new BuVerticalLayout());
    bParam.add(b1);
    bParam.add(b2);
    bParam.add(b3);

    // Ajout au conteneur
    c.add(bParam);
    c.add(b4);

    pack();
  }

  /**
   * initialise les zones de texte.
   */
  public void initTextField() {
    energieAccostage_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    effortAmarrage_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    cote_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    gMSol_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    gMSol_.setColumns(5);
    gMDefense_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    gfy_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    reactionMax_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    deflexionMax_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    deplacementMax_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    gdButee_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    gdPieux_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    energieAccostage_.setHorizontalAlignment(SwingConstants.CENTER);
    effortAmarrage_.setHorizontalAlignment(SwingConstants.CENTER);
    cote_.setHorizontalAlignment(SwingConstants.CENTER);
    gMSol_.setHorizontalAlignment(SwingConstants.CENTER);
    gMDefense_.setHorizontalAlignment(SwingConstants.CENTER);
    gfy_.setHorizontalAlignment(SwingConstants.CENTER);
    reactionMax_.setHorizontalAlignment(SwingConstants.CENTER);
    deflexionMax_.setHorizontalAlignment(SwingConstants.CENTER);
    deplacementMax_.setHorizontalAlignment(SwingConstants.CENTER);
    gdButee_.setHorizontalAlignment(SwingConstants.CENTER);
    gdPieux_.setHorizontalAlignment(SwingConstants.CENTER);
    energieAccostage_.addKeyListener(this);
    effortAmarrage_.addKeyListener(this);
    cote_.addKeyListener(this);
    gMSol_.addKeyListener(this);
    gMDefense_.addKeyListener(this);
    gfy_.addKeyListener(this);
    reactionMax_.addKeyListener(this);
    deflexionMax_.addKeyListener(this);
    deplacementMax_.addKeyListener(this);
    gdButee_.addKeyListener(this);
    gdPieux_.addKeyListener(this);
  }

  /**
   * initialise les boutons radio.
   */

  public void initRadioButton() {
    ouiRadio_ = new JRadioButton("Oui");
    nonRadio_ = new JRadioButton("Non");
    ouiRadio_.setSelected(true);
    ouiRadio_.addActionListener(this);
    nonRadio_.addActionListener(this);

    ButtonGroup group = new ButtonGroup();
    group.add(ouiRadio_);
    group.add(nonRadio_);
  }

  /**
   * initialise les boutons de commande.
   */

  public void initButton() {
    annulerButton_ = AlbeLib.buttonId("Annuler", "ANNULER");
    annulerButton_.addActionListener(this);
    lancerCalculButton_ = AlbeLib.buttonId("Lancer le calcul", "VALIDER");
    lancerCalculButton_.setEnabled(false);
    lancerCalculButton_.addActionListener(this);
  }

  /**
   * ajoute � la structure _p les param�tres concernant le lancement manuel des
   * calculs et retourne cette structure.
   */
  public SLancementCalculs addParametresLancementManuel(SLancementCalculs _p) {

    if (params_.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)) {
      _p.energieAccostage = AlbeLib.textFieldDoubleValue(energieAccostage_);
      _p.reactionMax = AlbeLib.textFieldDoubleValue(reactionMax_);

      if (params_.defense.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI)) {
        _p.gammaMDefense = AlbeLib.textFieldDoubleValue(gMDefense_);
        _p.deflexionMax = AlbeLib.textFieldDoubleValue(deflexionMax_);
      }
    } else
      _p.effortAmarrage = AlbeLib.textFieldDoubleValue(effortAmarrage_);
    _p.cote = AlbeLib.textFieldDoubleValue(cote_);
    _p.gammaMSol = AlbeLib.textFieldDoubleValue(gMSol_);

    if (params_.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)
        && params_.defense.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI)) {
      _p.gammaMDefense = AlbeLib.textFieldDoubleValue(gMDefense_);
      _p.deflexionMax = AlbeLib.textFieldDoubleValue(deflexionMax_);
    }
    _p.gammaFyAcier = AlbeLib.textFieldDoubleValue(gfy_);
    _p.deplacementMax = AlbeLib.textFieldDoubleValue(deplacementMax_);
    _p.gammaDButee = AlbeLib.textFieldDoubleValue(gdButee_);
    _p.gammaDPieu = AlbeLib.textFieldDoubleValue(gdPieux_);
    _p.corrosion = nonRadio_.isSelected() ? false : true;

    return _p;
  }

  /**
   * v�rifie les donn�es saisies et affiche un message dans le cas o� des
   * erreurs sont pr�sentes. Retourne vrai si les calculs peuvent �tre lanc�s.
   */
  public boolean lancerCalculs() {
    // bool�en pr�cisant si les calculs peuvent �tre lanc�s
    boolean lancerCalcul = false;

    // bool�en pr�cisant si un msg a d�j� �t� affich� (afin de n'afficher qu'un
    // seul msg m�me s'il y a plusieurs erreurs)
    boolean msg = false;

    /** V�rifications � effectuer. */

    if (accostage_) {
      // Energie d'accostage
      if (AlbeLib.textFieldDoubleValue(energieAccostage_) < 0) {
        AlbeLib.dialogError(appli_, AlbeMsg.ERR016);
        msg = true;
      }
    }
    // Effort d'amarrage
    else if (AlbeLib.textFieldDoubleValue(effortAmarrage_) < 0) {
      msg = true;
      AlbeLib.dialogError(appli_, AlbeMsg.ERR017);
    }

    if (!msg) {
      // Coeff sol
      if (AlbeLib.textFieldDoubleValue(gMSol_) < 0)
        AlbeLib.dialogError(appli_, AlbeMsg.ERR018);
      // Coeff d�fense
      else if (defense_ && AlbeLib.textFieldDoubleValue(gMDefense_) < 0)
        AlbeLib.dialogError(appli_, AlbeMsg.ERR019);
      // Coeff acier
      else if (AlbeLib.textFieldDoubleValue(gfy_) <= 0)
        AlbeLib.dialogError(appli_, AlbeMsg.ERR020);

      // R�action maximale
      else if (accostage_ && AlbeLib.textFieldDoubleValue(reactionMax_) < 0)
        AlbeLib.dialogError(appli_, AlbeMsg.ERR021);
      // D�flexion maximale
      else if (defense_ && AlbeLib.textFieldDoubleValue(deflexionMax_) < 0 || defense_
          && AlbeLib.textFieldDoubleValue(deflexionMax_) > 100)
        AlbeLib.dialogError(appli_, AlbeMsg.ERR022);
      // D�placement maximal
      else if (AlbeLib.textFieldDoubleValue(deplacementMax_) < 0)
        AlbeLib.dialogError(appli_, AlbeMsg.ERR023);

      // Coeff but�e
      else if (AlbeLib.textFieldDoubleValue(gdButee_) <= 0)
        AlbeLib.dialogError(appli_, AlbeMsg.ERR024);
      // Coeff pieux
      else if (AlbeLib.textFieldDoubleValue(gdPieux_) <= 0)
        AlbeLib.dialogError(appli_, AlbeMsg.ERR025);

      else {
        double valeurCote = AlbeLib.textFieldDoubleValue(cote_);
        if (valeurCote > params_.geo.coteTeteOuvrage || valeurCote <= params_.geo.coteDragageBassin)
          AlbeLib.dialogError(appli_, accostage_ ? AlbeMsg.ERR012 : AlbeMsg.ERR013);
        else if (defense_) {
          if (AlbeLib.textFieldDoubleValue(deflexionMax_) > params_.defense.points[params_.defense.nombrePoints - 1].deformation) {
            int res = AlbeLib.dialogConfirmation(appli_, "Avertissement", AlbeMsg.DLG008);
            if (res == JOptionPane.YES_OPTION)
              lancerCalcul = true;
          } else
            lancerCalcul = true;
        } else
          lancerCalcul = true;
      }
    }

    return lancerCalcul;
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {

    // Clic sur le bouton 'Annuler'
    if (_evt.getSource() == annulerButton_)
      setVisible(false);

    // Clic sur le bouton 'Lancer le calcul'
    else if (_evt.getSource() == lancerCalculButton_) {

      if (lancerCalculs()) {
        ((AlbeImplementation) appli_.getImplementation()).actionPerformed(new ActionEvent(this, 3,
            "LANCER_CALCULS"));
        this.dispose();
      }
    }
  }

  /**
   * appell�e automatiquement lorsqu'une touche est press�e. Si l'utilisateur
   * appuie sur la touche 'Entr�e', on transfert le focus.
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyPressed(KeyEvent _evt) {
    if (_evt.getKeyCode() == KeyEvent.VK_ENTER)
      _evt.getComponent().transferFocus();
  }

  /**
   * appell�e automatiquement lorsqu'une touche est relach�e. A chaque fois que
   * cette m�thode est appell�e, on v�rifie si tous les champs sont remplis ; si
   * c'est le cas, on rend actif le bouton 'Lancer le calcul'.
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyReleased(KeyEvent _evt) {
    try {
      if (accostage_) {
        AlbeLib.textFieldDoubleValue2(energieAccostage_);
        AlbeLib.textFieldDoubleValue2(reactionMax_);
      } else
        AlbeLib.textFieldDoubleValue2(effortAmarrage_);

      AlbeLib.textFieldDoubleValue2(cote_);
      AlbeLib.textFieldDoubleValue2(gMSol_);
      AlbeLib.textFieldDoubleValue2(gfy_);
      AlbeLib.textFieldDoubleValue2(deplacementMax_);
      AlbeLib.textFieldDoubleValue2(gdButee_);
      AlbeLib.textFieldDoubleValue2(gdPieux_);

      if (defense_) {
        AlbeLib.textFieldDoubleValue2(gMDefense_);
        AlbeLib.textFieldDoubleValue2(deflexionMax_);
      }
      lancerCalculButton_.setEnabled(true);
    }
    catch (final NullPointerException _e1) {
      lancerCalculButton_.setEnabled(false);
    }

  }

  /**
   * appell�e automatiquement lorsque l'utilisateur tape sur une touche du
   * clavier.
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyTyped(KeyEvent _evt) {

  }

}
