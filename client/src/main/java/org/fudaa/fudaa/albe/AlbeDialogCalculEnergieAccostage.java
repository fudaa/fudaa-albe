package org.fudaa.fudaa.albe;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * Description de la boite de dialogue pour le calcul des �nergies d'accostage
 * (qui apparait lorsque l'utilisateur clique sur le bouton 'Calculatrice' lors
 * de la saisie des param�tres d'accostage (onglet 'Action')).
 * 
 * @author Sabrina Delattre
 */

public class AlbeDialogCalculEnergieAccostage extends JDialog implements ActionListener,
    KeyListener {

  BuTextField deplacementNavire_, vitesseAccostage_, coefMasseAjoutee_, coefLaminage_,
      coefExcentricite_, coefRigidite_, energieCalculee_;

  BuButton lancementCalculButton_, annulerButton_;

  int num_;

  /**
   * r�f�rence vers le parent.
   */
  AlbeActionParametres parent_;

  /**
   * Constructeur.
   * 
   * @param _parent
   * @param i entier compris entre 1 et 4 et correpondant au num�ro du bouton
   *          'Calcul' s�lectionn�
   */

  public AlbeDialogCalculEnergieAccostage(final BuCommonInterface _app,
      final AlbeActionParametres _parent, final int _i) {

    super(_app.getImplementation().getFrame(), "Calcul de l'�nergie d'accostage", true);
    parent_ = _parent;
    num_ = _i;

    Container conteneur = getContentPane();

    conteneur.setLayout(new BuVerticalLayout());

    // Champs texte
    deplacementNavire_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    vitesseAccostage_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    coefMasseAjoutee_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    coefMasseAjoutee_.setColumns(5);
    coefLaminage_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    coefExcentricite_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    coefRigidite_ = AlbeLib.doubleField(AlbeLib.FORMAT001);

    deplacementNavire_.setHorizontalAlignment(SwingConstants.CENTER);
    vitesseAccostage_.setHorizontalAlignment(SwingConstants.CENTER);
    coefMasseAjoutee_.setHorizontalAlignment(SwingConstants.CENTER);
    coefLaminage_.setHorizontalAlignment(SwingConstants.CENTER);
    coefExcentricite_.setHorizontalAlignment(SwingConstants.CENTER);
    coefRigidite_.setHorizontalAlignment(SwingConstants.CENTER);

    deplacementNavire_.addKeyListener(this);
    vitesseAccostage_.addKeyListener(this);
    coefMasseAjoutee_.addKeyListener(this);
    coefLaminage_.addKeyListener(this);
    coefExcentricite_.addKeyListener(this);
    coefRigidite_.addKeyListener(this);

    // Boutons
    annulerButton_ = AlbeLib.buttonId("Annuler", "ANNULER");
    annulerButton_.addActionListener(this);

    lancementCalculButton_ = AlbeLib.buttonId("Lancement du calcul de l'�nergie", "VALIDER");
    lancementCalculButton_.setEnabled(false);
    lancementCalculButton_.addActionListener(this);

    /** Panneau contenant les param�tres du navire. */
    BuPanel p1 = AlbeLib.titledPanel("Param�tres du navire", AlbeLib.ETCHED_BORDER);
    p1.setLayout(new SpringLayout());
    p1.add(new JLabel("D�placement du navire"));
    p1.add(deplacementNavire_);
    p1.add(new JLabel("t"));
    p1.add(new JLabel("Vitesse d'accostage du navire"));
    p1.add(vitesseAccostage_);
    p1.add(new JLabel("m/s"));

    SpringUtilities.makeCompactGrid(p1, 2, 3, 5, 5, 10, 10);

    /** Panneau contenant les champs pour les coefficients. */
    BuPanel p2 = AlbeLib.titledPanel("Coefficients", AlbeLib.ETCHED_BORDER);
    p2.setLayout(new SpringLayout());
    p2.add(new JLabel("Coefficient de masse ajout�e"));
    p2.add(AlbeLib.symbol(AlbeLib.getIcon("coeff-cm")));
    p2.add(coefMasseAjoutee_);
    p2.add(new JLabel("(-)"));
    p2.add(new JLabel("Coefficient de laminage"));
    p2.add(AlbeLib.symbol(AlbeLib.getIcon("coeff-cc")));
    p2.add(coefLaminage_);
    p2.add(new JLabel("(-)"));
    p2.add(new JLabel("Coefficient d'excentricit�"));
    p2.add(AlbeLib.symbol(AlbeLib.getIcon("coeff-ce")));
    p2.add(coefExcentricite_);
    p2.add(new JLabel("(-)"));
    p2.add(new JLabel("Coefficient d'affaissement (rigidit� de la coque)"));
    p2.add(AlbeLib.symbol(AlbeLib.getIcon("coeff-ca")));
    p2.add(coefRigidite_);
    p2.add(new JLabel("(-)"));

    SpringUtilities.makeCompactGrid(p2, 4, 4, 5, 5, 10, 10);

    /** Panneau contenant les deux boutons. */
    BuPanel p3 = new BuPanel();
    p3.setLayout(new FlowLayout());
    p3.add(annulerButton_);
    p3.add(lancementCalculButton_);

    /** Panneau regroupant les param�tres. */
    BuPanel p = AlbeLib.borderPanel(AlbeLib.COMPOUND_BORDER);
    p.setLayout(new BuVerticalLayout());
    p.add(p1);
    p.add(p2);

    conteneur.add(p);
    conteneur.add(p3);

    pack();

  }

  /**
   * calcule la valeur de l'�nergie � partir des donn�es plac�es en param�tres
   * et retourne sa valeur.
   */
  public double calculEnergie(double _deplacement, double _vitesse, double _coef1, double _coef2,
      double _coef3, double _coef4) {

    return 0.5 * _deplacement * _vitesse * _vitesse * _coef1 * _coef2 * _coef3 * _coef4;
  }

  /**
   * 
   */
  public JComponent getComponent() {
    return null;
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {

    if (_evt.getSource() == annulerButton_)
      setVisible(false);

    else if (_evt.getSource() == lancementCalculButton_) {

      /**
       * D'apr�s l'entier "num" r�cup�r� dans le constructeur, on affiche la
       * valeur de l'�nergie calcul�e dans le bon champ de texte.
       */

      // Calcul de l'�nergie
      double valeurEnergie = calculEnergie(AlbeLib.textFieldDoubleValue(deplacementNavire_),
          AlbeLib.textFieldDoubleValue(vitesseAccostage_), AlbeLib
              .textFieldDoubleValue(coefMasseAjoutee_),
          AlbeLib.textFieldDoubleValue(coefLaminage_), AlbeLib
              .textFieldDoubleValue(coefExcentricite_), AlbeLib.textFieldDoubleValue(coefRigidite_));

      if (num_ == 1)
        // Affichage du r�sultat dans le premier champ (ie �nergie
        // d'accostage fr�quente)
        parent_.energieAccostFreq_.setText(String.valueOf(valeurEnergie));

      else if (num_ == 2)
        parent_.energieAccostCaract_.setText(String.valueOf(valeurEnergie));

      else if (num_ == 3)
        parent_.energieAccostCalcul_.setText(String.valueOf(valeurEnergie));

      else if (num_ == 4)
        parent_.energieAccostAcc_.setText(String.valueOf(valeurEnergie));

      parent_.majMessages();

      setVisible(false);
    }
  }

  /**
   * appell�e automatiquement lorsqu'une touche est press�e. Si l'utilisateur
   * appuie sur la touche 'Entr�e', on transfert le focus.
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyPressed(KeyEvent _evt) {
    if (_evt.getKeyCode() == KeyEvent.VK_ENTER)
      _evt.getComponent().transferFocus();
  }

  /**
   * appell�e automatiquement lorsqu'une touche est relach�e. A chaque fois que
   * cette m�thode est appell�e, on v�rifie si l'�nergie peut �tre recalcul�e
   * (mise � jour par rapport � la saisie); si c'est le cas, on rend actif le
   * bouton 'Lancement du calcul de l'�nergie'.
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyReleased(KeyEvent _evt) {
    try {
      AlbeLib.textFieldDoubleValue2(deplacementNavire_);
      AlbeLib.textFieldDoubleValue2(vitesseAccostage_);
      AlbeLib.textFieldDoubleValue2(coefMasseAjoutee_);
      AlbeLib.textFieldDoubleValue2(coefLaminage_);
      AlbeLib.textFieldDoubleValue2(coefExcentricite_);
      AlbeLib.textFieldDoubleValue2(coefRigidite_);
      lancementCalculButton_.setEnabled(true);
    }
    catch (final NullPointerException _e1) {
      lancementCalculButton_.setEnabled(false);
    }

  }

  /**
   * appell�e automatiquement lorsque l'utilisateur tape sur une touche du
   * clavier.
   * 
   * @param _e �v�nement qui a engendr� l'action
   */
  public void keyTyped(KeyEvent _evt) {}

}
