package org.fudaa.fudaa.albe;

import java.util.Vector;

public class ValidationMessages {
  Vector messages_;

  public ValidationMessages() {
    messages_ = new Vector();
  }

  public void add(final ValidationMessages _vm) {
    try {
      final VMessage[] ms = _vm.getMessages();
      for (int i = 0; i < ms.length; i++) {
        add(ms[i]);
      }
    }
    catch (final NullPointerException _e1) {}
  }

  public void add(final VMessage _m) {
    messages_.addElement(_m);
  }

  public VMessage[] getMessages() {
    try {
      final Object[] _m = messages_.toArray();
      final VMessage[] _vm = new VMessage[_m.length];
      for (int i = 0; i < _m.length; i++) {
        _vm[i] = (VMessage) _m[i];
      }
      return _vm;
    }
    catch (final NullPointerException _e1) {
      return null;
    }
  }

  public VMessage[] getWarningMessages() {
    int j = 0;
    try {
      final Object[] _m = messages_.toArray();
      final Vector _vm = new Vector();
      for (int i = 0; i < _m.length; i++) {
        if (((VMessage) _m[i]).getType().equals(VMessage.WARNING)) {
          _vm.addElement(_m[i]);
        }
      }
      final Object[] _vme = _vm.toArray();
      final VMessage[] _vmsg = new VMessage[_vme.length];
      for (j = 0; j < _vme.length; j++) {
        _vmsg[j] = (VMessage) _vme[j];
      }
      return _vmsg;
    }
    catch (final NullPointerException _e1) {
      return null;
    }
  }

  public VMessage[] getFatalErrorMessages() {
    int j = 0;
    try {
      final Object[] _m = messages_.toArray();
      final Vector _vm = new Vector();
      for (int i = 0; i < _m.length; i++) {
        if (((VMessage) _m[i]).getType().equals(VMessage.FATAL_ERROR)) {
          _vm.addElement(_m[i]);
        }
      }
      final Object[] _vme = _vm.toArray();
      final VMessage[] _vmsg = new VMessage[_vme.length];
      for (j = 0; j < _vme.length; j++) {
        _vmsg[j] = (VMessage) _vme[j];
      }
      return _vmsg;
    }
    catch (final NullPointerException _e1) {
      return null;
    }
  }

  public boolean hasWarning() {
    final VMessage[] _vm = getMessages();
    boolean found = false;
    for (int i = 0; i < _vm.length; i++) {
      if (_vm[i].getType().equals(VMessage.WARNING)) {
        found = true;
        break;
      }
    }
    return found;
  }

  public boolean hasFatalError() {
    final VMessage[] _vm = getMessages();
    boolean found = false;
    for (int i = 0; i < _vm.length; i++) {
      if (_vm[i].getType().equals(VMessage.FATAL_ERROR)) {
        found = true;
        break;
      }
    }
    return found;
  }

}
