/*
 * @creation 24 janv. 07
 * @modification $Date: 2007-02-07 08:27:12 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

/**
 * Cr�ation d'un mod�le de Table (d�rivant de DefaultTableModel) pour les
 * tableaux de la courbe manuelle (onglet 'Sol').
 * 
 * @author Sabrina Delattre
 */
public class AlbeModeleTableCourbeManuelle extends DefaultTableModel implements TableModelListener {

  AlbeSolParametresCourbeManuelle courbeManuelle_;

  /**
   * cr�e un mod�le de donn�es utilis� pour les tableaux.
   * 
   * @param _columnNames tableau d'Objets contenant les noms des colonnes �
   *          cr�er
   * @param _rowCount nombre de lignes du tableau
   */

  public AlbeModeleTableCourbeManuelle(Object[] _columnNames, int _rowCount,
      AlbeSolParametresCourbeManuelle _onglet) {
    super(_columnNames, _rowCount);
    courbeManuelle_ = _onglet;
    this.addTableModelListener(this);
  }

  /**
   * d�finit si la cellule situ�e � l'emplacement sp�cifi�e est �ditable par
   * l'utilisateur ou non.
   * 
   * @param _row indice de la ligne de la cellule concern�e
   * @param _column indice de la colonne de la cellule concern�e
   */
  public boolean isCellEditable(final int _row, final int _column) {
    boolean b = false;
    if ((_row >= 0) && (_column >= 1)) {
      b = true;
    }
    return b;
  }

  /**
   * retourne l'objet valeur de la cellule sp�cifi�e.
   * 
   * @param _row indice de la ligne de la cellule concern�e
   * @param _column indice de la colonne de la cellule concern�e
   */
  public Object getValueAt(final int _row, final int _column) {
    Object o = null;
    Object v = null;
    v = super.getValueAt(_row, _column);
    try {
      if ((_row >= 0) && (_column > 0)) {
        o = new Double(v.toString());
      } else {
        o = v;
      }
    }
    catch (final NumberFormatException _e1) {}
    catch (final NullPointerException _e2) {}
    return o;
  }

  /**
   * 
   */
  public void tableChanged(TableModelEvent _evt) {
    if (AlbeLib.verificationCourbeManuelle_)
      courbeManuelle_.majMessages();
  }

}
