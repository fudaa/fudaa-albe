/*
 * @creation 1 d�c. 06
 * @modification $Date: 2007-04-25 15:18:06 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;

import javax.swing.JOptionPane;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gif.AcmeGifEncoder;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Lecteur;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;

/**
 * classe de l'onglet 'Mobilisation de la d�fense' des r�sultats combinaison par
 * combinaison.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeMobilisationDefenseResultats extends AlbeAbstractResultatsOnglet {

  /** r�f�rence vers la fen�tre parente. */
  AlbeFilleResultatsCombinaison fp_;

  /** graphique. */
  BGraphe bg_;

  public AlbeMobilisationDefenseResultats(final AlbeFilleResultatsCombinaison _fp) {

    super(_fp.getApplication(), _fp);

    fp_ = _fp;

    this.setLayout(new BuVerticalLayout());

    /** Bloc informations. */
    final BuPanel infoPanel = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    infoPanel.setLayout(new BorderLayout());
    BuLabelMultiLine infoLabel = AlbeLib
        .labelMultiLine("Vous trouverez ici le graphique de la courbe de calcul d�flexion-r�action de la d�fense.");
    infoPanel.add(infoLabel);

    /** Bloc 'Mobilisation de la d�fense */
    final BuPanel p = AlbeLib.titledPanel("Mobilisation de la d�fense", AlbeLib.BEVEL_BORDER);
    p.setLayout(new BorderLayout());

    // Bloc 1 : boutons
    final BuPanel b1 = new BuPanel(new FlowLayout());
    imprimerButton_ = AlbeLib.buttonId("Imprimer", "IMPRIMER");
    exporterButton_ = AlbeLib.buttonId("Exporter", "EXPORTER");
    imprimerButton_.setToolTipText("Imprimer la fen�tre");
    exporterButton_.setToolTipText("Exporter la courbe");
    imprimerButton_.addActionListener(this);
    exporterButton_.addActionListener(this);
    b1.add(imprimerButton_);
    b1.add(exporterButton_);

    // Bloc 2 : graphique
    final BuPanel b2 = new BuPanel(new FlowLayout());
    final Lecteur lin = new Lecteur(new StringReader(getScript()));
    final Graphe g = Graphe.parse(lin, (Applet) null);
    bg_ = new BGraphe();
    bg_.setGraphe(g);
    bg_.setPreferredSize(new Dimension(550, 350));
    b2.add(bg_);

    // Bloc 3 : valeurs des d�flexions
    final BuPanel b3 = new BuPanel(new FlowLayout());
    b3.setLayout(new FlowLayout());
    BuPanel tmpb3 = AlbeLib.borderPanel(AlbeLib.BEVEL_BORDER);
    tmpb3.setLayout(new BuVerticalLayout());
    BuLabelMultiLine lab = AlbeLib.labelMultiLine(" D�flexion calcul�e : " + fp_.deflexions_[0]
        + "\n D�flexion admissible : " + fp_.deflexions_[1] + "\n Facteur de dimensionnement : "
        + fp_.deflexions_[2]);
    lab.setForeground(Color.blue);
    tmpb3.add(lab);
    b3.add(tmpb3);

    p.add(b1, BorderLayout.NORTH);
    p.add(b2, BorderLayout.CENTER);
    p.add(b3, BorderLayout.EAST);

    this.add(infoPanel);
    this.add(p);
  }

  protected String getScript() {
    final StringBuffer s = new StringBuffer(4096);
    double coefDefense = AlbeRes.getCoefDefense(fp_.params_, fp_.typeCombi_, fp_.numCombi_);
    double pas = AlbeLib.getPas(coefDefense
        * fp_.params_.defense.points[fp_.params_.defense.nombrePoints - 1].effort, 0);
    s.append("graphe \n{\n");
    s.append("  titre \"D�fenses d'accostage\"\n");
    s.append("  sous-titre \"Courbe d�flexion-r�action\"\n");
    s
        .append(((AlbeFilleParametres) fp_.getInternalFrame(AlbeLib.INTERNAL_FRAME_PARAMETRES)).defense_
            .getScriptCommun());
    s.append("    titre \"effort\"\n");
    s.append("    unite \"kN\"\n");
    s.append("    orientation vertical\n");
    s.append("    graduations oui\n");
    s.append("    minimum 0\n");
    s.append("    maximum " + 10 * pas + "\n");
    s.append("    pas " + pas + "\n");
    s.append("  }\n");
    s.append("  courbe\n{\n");
    s.append("    aspect\n{\n");
    s.append("    contour.couleur 0000FF\n");
    s.append("    }\n");
    s.append("    valeurs\n{\n");
    s.append("      " + 0 + " " + 0 + "\n");
    for (int i = 0; i < fp_.params_.defense.nombrePoints; i++) {
      s.append("      " + fp_.params_.defense.points[i].deformation + " "
          + AlbeLib.arronditDouble(coefDefense * fp_.params_.defense.points[i].effort) + "\n");
    }
    s.append("    }\n");
    s.append("    }\n");
    s.append("  courbe\n{\n");
    s.append("    aspect\n{\n");
    s.append("    contour.couleur 008000\n");
    s.append("    }\n");
    s.append("    valeurs\n{\n");
    s.append("      " + fp_.resCombi_.resultatsGeneraux.deflexionDefenses + " " + 0 + "\n");
    s.append("      " + fp_.resCombi_.resultatsGeneraux.deflexionDefenses + " " + 10 * pas + "\n");
    s.append("    }\n");
    s.append("  }\n");
    s.append("  }\n");
    return s.toString();
  }

  /**
   * construit la partie HTML des r�sultats sur la mobilisation de la d�fense.
   */
  public String buildPartieHtml() {
    String html = "";
    html += fp_.addImgFromScript(getScript(), "courbe_deflexion_reaction.gif",
        "Courbe d&eacute;flexion r&eacute;action");
    html += "<br><br><ul type = square>" + CtuluLibString.LINE_SEP;
    html += "<li>D&eacute;flexion calcul&eacute;e : " + fp_.deflexions_[0] + CtuluLibString.LINE_SEP;
    html += "<li>D&eacute;flexion admissible : " + fp_.deflexions_[1] + CtuluLibString.LINE_SEP;
    html += "<li>Facteur de dimensionnement : " + fp_.deflexions_[2] + CtuluLibString.LINE_SEP;
    html += "</ul><br>" + CtuluLibString.LINE_SEP;

    return html;
  }

  public void exporter() {
    final String _f = AlbeLib.getFileChoosenByUser(bg_, "Exporter l'image", "Exporter",
        AlbeLib.USER_HOME + "courbe_deflexion_reaction.gif");
    if (_f == null) {
      return;
    }
    if ((new File(_f)).exists()) {
      final int _i = AlbeLib.dialogConfirmation((BuCommonInterface) null, null,
          "Le fichier cible existe d�j�. Voulez-vous l'�craser ?");
      if (_i != JOptionPane.YES_OPTION) {
        return;
      }
    }
    try {
      final AcmeGifEncoder _age = new AcmeGifEncoder(bg_.getImageCache(), new FileOutputStream(_f));
      _age.encode();
    }
    catch (final Exception _e1) {}
  }
}
