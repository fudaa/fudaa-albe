/*
 * @creation 30 nov. 06
 * @modification $Date: 2007-04-24 14:55:37 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.SpringLayout;

import org.fudaa.ctulu.CtuluLibString;

import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * classe de l'onglet 'D�placements et r�action' des r�sultats combinaison par
 * combinaison.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */

public class AlbeDeplacementReactionResultats extends AlbeAbstractResultatsOnglet {

  BuTextField deplacementTeteVal_, deplacementTetemax_, deplacementTeteDim_, reactionChocVal_,
      reactionChocMax_, reactionChocDim_;

  /** r�f�rence vers la fen�tre parente. */
  AlbeFilleResultatsCombinaison fp_;

  /**
   * constructeur.
   */
  public AlbeDeplacementReactionResultats(AlbeFilleResultatsCombinaison _fp) {

    super(_fp.getApplication(), _fp);
    fp_ = _fp;

    this.setLayout(new BuVerticalLayout());

    /** Bloc informations. */
    final BuPanel infoPanel = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    infoPanel.setLayout(new BorderLayout());
    BuLabelMultiLine infoLabel = AlbeLib
        .labelMultiLine("Cette page donne les valeurs calcul�es de la r�action au choc et du d�placement en t�te de l'ouvrage, rappelle les valeurs maximales et donne les facteurs de dimensionnement.\n"
            + "Vous pouvez exporter ces r�sultats dans un fichier texte.");
    infoPanel.add(infoLabel);

    /** Bloc 'D�placements et r�action' */
    final BuPanel panelRes = AlbeLib.titledPanel("D�placements et r�action", AlbeLib.BEVEL_BORDER);
    panelRes.setLayout(new BuVerticalLayout());

    // Bloc boutons
    final BuPanel b1 = new BuPanel(new FlowLayout());
    imprimerButton_ = AlbeLib.buttonId("Imprimer", "IMPRIMER");
    exporterButton_ = AlbeLib.buttonId("Exporter", "EXPORTER");
    imprimerButton_.setToolTipText("Imprimer la fen�tre");
    exporterButton_.setToolTipText("Exporter les r�sultats");
    imprimerButton_.addActionListener(this);
    exporterButton_.addActionListener(this);
    b1.add(imprimerButton_);
    b1.add(exporterButton_);

    // Bloc contenant les r�sultats
    initTextField();
    final BuPanel b2 = new BuPanel(new SpringLayout());
    b2.add(new JLabel(""));
    b2.add(new JLabel("    Valeur calcul�e    "));
    b2.add(new JLabel("    Valeur maximale    "));
    b2.add(new JLabel("Facteur de dimensionnement"));
    b2.add(new JLabel("D�placement en t�te"));
    b2.add(deplacementTeteVal_);
    b2.add(deplacementTetemax_);
    b2.add(deplacementTeteDim_);
    JLabel reactionLabel = new JLabel("R�action au choc");
    b2.add(reactionLabel);
    b2.add(reactionChocVal_);
    b2.add(reactionChocMax_);
    b2.add(reactionChocDim_);

    SpringUtilities.makeCompactGrid(b2, 3, 4, 5, 5, 5, 5);

    if (!fp_.accostage_)
      reactionLabel.setEnabled(false);

    panelRes.add(b1);
    panelRes.add(b2);

    BuPanel tmpPanel = new BuPanel(new FlowLayout());
    tmpPanel.add(panelRes);

    this.add(infoPanel);
    this.add(tmpPanel);

    // Ecriture des r�sultats dans les champs texte
    setResults();
  }

  /**
   * initialise les zones de texte.
   */
  public void initTextField() {
    deplacementTeteVal_ = new BuTextField(6);
    AlbeRes.setTextField(deplacementTeteVal_);
    deplacementTetemax_ = new BuTextField(6);
    AlbeRes.setTextField(deplacementTetemax_);
    deplacementTeteDim_ = new BuTextField(6);
    AlbeRes.setTextField(deplacementTeteDim_);
    reactionChocVal_ = new BuTextField(6);
    AlbeRes.setTextField(reactionChocVal_);
    reactionChocMax_ = new BuTextField(6);
    AlbeRes.setTextField(reactionChocMax_);
    reactionChocDim_ = new BuTextField(6);
    AlbeRes.setTextField(reactionChocDim_);
  }

  /**
   * remplit les champs de texte avec les r�sultats.
   */
  public void setResults() {
    // DEPLACEMENT EN TETE
    deplacementTeteVal_.setText(fp_.deplacements_[0]);
    deplacementTetemax_.setText(fp_.deplacements_[1]);
    deplacementTeteDim_.setText(fp_.deplacements_[2]);
    // REACTION AU CHOC
    if (fp_.accostage_) {
      reactionChocVal_.setText(fp_.reactions_[0]);
      reactionChocMax_.setText(fp_.reactions_[1]);
      reactionChocDim_.setText(fp_.reactions_[2]);
    }
  }

  /**
   * construit la partie HTML des r�sultats sur les d�placements et r�actions.
   */
  public String buildPartieHtml() {
    String html = "";

    html += "<table border=\"2\" cellspacing=\"0\" cellpadding=\"5\">" + CtuluLibString.LINE_SEP;
    html += "<th></th><th bgcolor=\"#DDDDDD\">Valeur calcul&eacute;e</th><th bgcolor=\"#DDDDDD\">Valeur maximale</th><th bgcolor=\"#DDDDDD\">Facteur de dimensionnement</th>";
    html += "<tr align=center>" + CtuluLibString.LINE_SEP;
    html += "<th bgcolor=\"#DDDDDD\">D&eacute;placement en t&ecirc;te</th>" + CtuluLibString.LINE_SEP;
    html += "<td>" + deplacementTeteVal_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "<td>" + deplacementTetemax_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "<td>" + deplacementTeteDim_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "</tr>" + CtuluLibString.LINE_SEP;
    if (fp_.accostage_){
    html += "<tr align=center>" + CtuluLibString.LINE_SEP;
    html += "<th bgcolor=\"#DDDDDD\">R&eacute;action au choc</th>" + CtuluLibString.LINE_SEP;
    html += "<td>" + reactionChocVal_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "<td>" + reactionChocMax_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "<td>" + reactionChocDim_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "</tr>" + CtuluLibString.LINE_SEP;
    }
    html += "</table><br>" + CtuluLibString.LINE_SEP;

    return html;
  }

  /**
   * exporte les r�sultats de l'onglet dans un fichier .txt.
   */
  public void exporter() {
    final String f = AlbeLib.getFileChoosenByUser(this, "Exporter les donn�es", "Exporter",
        AlbeLib.USER_HOME
            + "Albe_resultats_deplacement_reaction_combinaison"
            + (fp_.typeCombi_ == AlbeLib.COMBINAISON_MANUELLE ? "manuelle"
                : (fp_.numCombi_ + "_" + AlbeRes.nomCombinaison(fp_.typeCombi_))) + ".txt");
    if (f == null) {
      return;
    }
    String txt = "";
    txt += "ALBE : combinaison "
        + (fp_.typeCombi_ == AlbeLib.COMBINAISON_MANUELLE ? "manuelle"
            : (fp_.numCombi_ + " " + AlbeRes.getCombinaison(fp_.typeCombi_)))
        + CtuluLibString.LINE_SEP;
    txt += "R�sultats sur le d�placement en t�te d'ouvrage et la r�action au choc"
        + CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP;

    // D�placement en t�te d'ouvrage
    txt += "D�placement en t�te d'ouvrage" + CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP;
    txt += "Valeur calcul�e : " + deplacementTeteVal_.getText() + CtuluLibString.LINE_SEP;
    txt += "Valeur maximale : " + deplacementTetemax_.getText() + CtuluLibString.LINE_SEP;
    txt += "Facteur de dimensionnement : " + deplacementTeteDim_.getText()
        + CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP;

    // R�action au choc
    if (fp_.accostage_) {
      txt += "R�action au choc" + CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP;
      txt += "Valeur calcul�e : " + reactionChocVal_.getText() + CtuluLibString.LINE_SEP;
      txt += "Valeur maximale : " + reactionChocMax_.getText() + CtuluLibString.LINE_SEP;
      txt += "Facteur de dimensionnement : " + reactionChocDim_.getText();
    }

    AlbeLib.writeFile(f, txt);
  }
}
