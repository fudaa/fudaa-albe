package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableModel;

import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;

/**
 * Panneau s'affichant lorsque l'utilisateur choisit la courbe manuelle dans
 * l'onglet 'Param�tres de sol'.
 * 
 * @author Sabrina Delattre
 */

public class AlbeSolParametresCourbeManuelle extends AlbeAbstractSolCourbe {

  /** set d'onglets. */

  JComboBox nbPointsCombo_;

  JTable[] table_;

  JScrollPane[] scrollPane_;

  DefaultTableModel[] modele_;

  /**
   * fen�tre contenant le graphique.
   */
  AlbeGraphiqueDialog fGraphe_;

  /**
   * tableaux de double pour r�cup�rer les abscisses et ordonn�es des points
   * saisis.
   */

  double[] abs_;

  double[] ord_;

  /**
   * Constructeur.
   * 
   * @param _sol
   */

  public AlbeSolParametresCourbeManuelle(final AlbeSolParametres _sol) {

    super(_sol);

    sol_ = _sol;

    abs_ = new double[AlbeLib.NBMAX_POINTS_COURBEMANUELLE];
    ord_ = new double[AlbeLib.NBMAX_POINTS_COURBEMANUELLE];

    initTabbedPane();

    // Ajout du bouton et des onglets au panneau
    this.add(panelSud_);
    this.add(tp_);

  }// Fin constructeur

  /**
   * initialise les composants.
   */
  protected void initComposants() {

    table_ = new JTable[AlbeLib.NBMAX_COUCHES];
    scrollPane_ = new JScrollPane[AlbeLib.NBMAX_COUCHES];
    modele_ = new AlbeModeleTableCourbeManuelle[AlbeLib.NBMAX_COUCHES];

  }

  /**
   * cr�e et retourne le panneau qui va contenir le tableau.
   */
  public BuPanel getPanelTable(JTable _t, JScrollPane _sp) {
    BuPanel panel = AlbeLib.titledPanel("Saisie des points de la courbe", AlbeLib.ETCHED_BORDER);
    panel.setLayout(new BuVerticalLayout());
    // Panneau contenant l'information
    final BuPanel p1 = new BuPanel(new FlowLayout());
    p1
        .add(new JLabel(
            "<html><font color = \"red\">Le graphique doit �tre introduit pour les d�placements n�gatifs comme pour les d�placements positifs.</font></html>"));
    // Panneau contenant le tableau
    final BuPanel p2 = new BuPanel();
    _sp.setPreferredSize(new Dimension(370, 63));
    _t.getTableHeader().setDefaultRenderer(new AlbeTableCellRenderer());
    AlbeLib.initialiseColonne1JTable(_t);
    AlbeLib.initialiseTableVide(_t);
    AlbeLib.changeParametresTable(_t);
    _t.getColumnModel().getColumn(0).setPreferredWidth(30);
    p2.add(_sp);
    // Ajout des deux panneaux
    panel.add(p1);
    panel.add(p2);
    return panel;
  }

  /**
   * initialise un panneau pour une couche de sol.
   * 
   * @param _pf
   * @param _i num�ro de la couche de sol
   */
  public void initCouchePanel(BuPanel _pf, int _i) {

    _pf.setLayout(new BuVerticalLayout());

    /** Panneau nord. */
    panelNord_[_i] = AlbeLib.borderPanel(AlbeLib.ETCHED_BORDER);
    panelNord_[_i].setLayout(new BorderLayout());

    // Initialisation de la boite combo
    if (_i == 0) {
      nbPointsCombo_ = new JComboBox();
      for (int nb = 1; nb < AlbeLib.NBMAX_POINTS_COURBEMANUELLE; nb++) {
        nbPointsCombo_.addItem(Integer.toString(nb + 1));
      }
      nbPointsCombo_.addActionListener(this);
    }

    // Panneau nord-ouest contenant les champs
    BuPanel panelNordOuest = new BuPanel();
    panelNordOuest.setLayout(new SpringLayout());
    panelNordOuest.add(new JLabel("Epaisseur"));
    panelNordOuest.add(epaisseur_[_i]);
    panelNordOuest.add(new JLabel("m"));
    if (_i == 0) {
      panelNordOuest.add(new JLabel("Nombre de points"));
      panelNordOuest.add(nbPointsCombo_);
      panelNordOuest.add(new JLabel("(de 2 � " + AlbeLib.NBMAX_POINTS_COURBEMANUELLE + ")"));
    }
    SpringUtilities.makeCompactGrid(panelNordOuest, _i == 0 ? 2 : 1, 3, 5, 15, 10, 10);

    // Panneau nord-est contenant les remarques
    panelNordEst_[_i] = new BuPanel(new BuVerticalLayout());

    panelNord_[_i].add(panelNordOuest, BorderLayout.WEST);
    panelNord_[_i].add(panelNordEst_[_i], BorderLayout.CENTER);

    /** Panneau centre contenant le tableau pour la saisie des points. */
    modele_[_i] = new AlbeModeleTableCourbeManuelle(new Object[] { "header-point",
        "header-deplacement", "header-pression" }, 2, this);
    table_[_i] = new JTable(modele_[_i]);
    scrollPane_[_i] = new JScrollPane(table_[_i]);

    /** Panneau sud contenant le bouton 'Graphique'. */
    BuPanel pb = new BuPanel();
    graphiqueButton_[_i] = AlbeLib.buttonId("Graphique", "graphe_32");
    graphiqueButton_[_i].setToolTipText("Visualiser la courbe d�placement-pression");
    graphiqueButton_[_i].addActionListener(this);
    pb.add(graphiqueButton_[_i]);

    /** Ajout des 3 panneaux au panneau final. */
    _pf.add(panelNord_[_i]);
    _pf.add(getPanelTable(table_[_i], scrollPane_[_i]));
    _pf.add(pb);
  }

  /**
   * efface les donn�es saisies pour chaque couche de sol � partir de la couche
   * _n1 jusqu'� la couche _n2 (si _n1 = 1 et _n2 = 3, on efface les donn�es des
   * couches n�1, n�2 et n�3).
   */
  public void eraseDataCouches(int _n1, int _n2) {
    for (int i = _n1 - 1; i < _n2; i++) {
      epaisseur_[i].setText("0");
      if (table_[i].isEditing())
        table_[i].getCellEditor().stopCellEditing();
      AlbeLib.initialiseTableVide(table_[i]);
    }
  }

  /**
   * v�rifie que la valeur contenue dans chaque cellule de la colonne 1 est
   * sup�rieure strictement � celle de la ligne pr�c�dente.
   */

  public boolean cellulescolonne1Croissantes(JTable _table) {

    // R�cup�ration du nombre de lignes
    int rowCount = _table.getRowCount();

    // On parcourt toute la colonne 1
    for (int i = 0; i < rowCount - 1; i++) {

      if (((Double) _table.getValueAt(i, 1)).doubleValue() >= ((Double) _table.getValueAt(i + 1, 1))
          .doubleValue())
        return false;

    }
    return true;

  }

  /**
   * v�rifie que la valeur contenue dans chaque cellule de la colonne 2 est
   * sup�rieure ou �gale � celle de la ligne pr�c�dente.
   */

  public boolean cellulescolonne2Croissantes(JTable _table) {

    // R�cup�ration du nombre de lignes
    int rowCount = _table.getRowCount();

    // On parcourt toute la colonne 2
    for (int i = 0; i < rowCount - 1; i++) {

      if (((Double) _table.getValueAt(i, 2)).doubleValue() > ((Double) _table.getValueAt(i + 1, 2))
          .doubleValue())
        return false;
    }
    return true;
  }

  /**
   * affiche un message si au moins une cellule de la JTable n'est pas remplie
   * ou ne comprend pas un nombre.
   */

  public boolean verifTable(JTable _t, DefaultTableModel _model) {

    if (!AlbeLib.fullTable(_t, _model)) {

      AlbeLib.dialogError(getApplication(),
          "Vous devez saisir toutes les coordonn�es des points de la courbe.");
      return false;
    } else if (!cellulescolonne1Croissantes(_t)) {

      AlbeLib.dialogError(getApplication(),
          "Vous devez entrer les valeurs des d�placements dans l'ordre strictement croissant.");
      return false;
    } else if (!cellulescolonne2Croissantes(_t)) {

      AlbeLib
          .dialogError(
              getApplication(),
              "Attention : pour une valeur de d�placement sup�rieure � une autre, la pression obtenue doit �tre sup�rieure ou �gale � la pr�c�dente.\nAutrement dit, la courbe doit �tre croissante.");

      return false;
    } else

      return true;
  }

  /**
   * r�cup�re les abscisses et ordonn�es des points saisis par l'utilisateur et
   * place celles - ci dans les tableaux abs_ et ord_.
   */

  public void getCoordPoints(JTable _t) {

    for (int i = 0; i < _t.getRowCount(); i++) {
      abs_[i] = ((Double) _t.getValueAt(i, 1)).doubleValue();
      ord_[i] = ((Double) _t.getValueAt(i, 2)).doubleValue();
    }
  }

  /**
   * 
   */
  public String getScriptCourbeManuelle(JTable _t, int _numCouche) {
    final StringBuffer s = new StringBuffer();
    double pasX = AlbeLib.getPas(abs_[_t.getRowCount() - 1], abs_[0]);
    double pasY = AlbeLib.getPas(ord_[_t.getRowCount() - 1], ord_[0]);
    s.append(sol_.getScriptCourbeSol(_numCouche, AlbeLib.getMin(pasX, abs_[0]), AlbeLib.getMax(
        pasX, abs_[_t.getRowCount() - 1]), pasX, AlbeLib.getMin(pasY, ord_[0]), AlbeLib.getMax(
        pasY, ord_[_t.getRowCount() - 1]), pasY));
    for (int i = 0; i < _t.getRowCount(); i++) {
      s.append("      " + abs_[i] + " " + ord_[i] + "\n");
    }
    s.append("    }\n");
    s.append("  }\n");
    return s.toString();

  }

  /**
   * retourne vrai si les donn�es concernant la courbe manuelle ont commenc� �
   * �tre remplies par l'utilisateur.
   */

  public boolean courbeCompletee() {
    for (int i = 0; i < sol_.nbCouches_; i++) {
      if (table_[i].isEditing())
        table_[i].getCellEditor().stopCellEditing();
      try {
        if ((!AlbeLib.emptyField(epaisseur_[i]) && AlbeLib.textFieldDoubleValue(epaisseur_[i]) != 0)
            || !AlbeLib.emptyTable(table_[i], modele_[i])) {
          return true;
        }
      }
      catch (NullPointerException _e) {
        return true;
      }
    }
    return false;
  }

  /**
   * renvoie l'�paisseur de la couche de sol _n.
   */

  public double getEpaisseurCouche(int _n) {
    return AlbeLib.textFieldDoubleValue(epaisseur_[_n - 1]);
  }

  /**
   * renvoie un tableau de double contenant les �paisseurs de toutes les couches
   * de la courbe manuelle.
   */

  public double[] getEpaisseursCourbeManuelle() {

    double[] d = null;

    try {
      d = new double[sol_.nbCouches_];
    }
    catch (final NegativeArraySizeException _e2) {
      System.err.println(AlbeMsg.CONS015);
    }
    for (int i = 0; i < sol_.nbCouches_; i++) {
      try {
        d[i] = getEpaisseurCouche(i + 1);
      }
      catch (final NullPointerException _e1) {
        System.err.println(AlbeMsg.CONS016);
      }
      catch (final ArrayIndexOutOfBoundsException _e2) {
        System.err.println(AlbeMsg.CONS017);
        return null;
      }
    }

    return d;
  }

  /**
   * importe les donn�es (�paisseurs) contenues dans le tableau pass� en
   * param�tres pour la courbe manuelle.
   */

  public synchronized void setEpaisseursCourbeManuelle(double[] _d) {

    int n = 0;
    try {
      n = _d.length;

      for (int i = 0; i < n; i++) {
        if (_d[i] != VALEUR_NULLE.value)
          epaisseur_[i].setValue(new Double(_d[i]));
      }
    }
    catch (final NullPointerException _e1) {
      System.err.println(AlbeMsg.CONS018);
    }

    // Si l'�paisseur de la couche 1 est remplie, on affiche le label (sinon il
    // apparaitra lorsque la zone de texte obtiendra le focus)
    if (!AlbeLib.emptyField(epaisseur_[0]))
      infoEpaisseurLabel_.setVisible(true);

  }

  /**
   * 
   */
  public boolean paramsCourbeManuelleModified(double[] _d) {
    double[] d2 = getEpaisseursCourbeManuelle();
    for (int i = 0; i < sol_.nbCouches_; i++) {
      if (_d[i] != d2[i])
        return true;
    }
    return false;
  }

  /**
   * 
   */
  public ValidationMessages validation() {
    final ValidationMessages vm = new ValidationMessages();

    for (int i = 0; i < sol_.nbCouches_; i++) {
      // On stoppe l'�dition des tableaux pour chaque couche de sol
      // if (table_[i].isEditing())
      // table_[i].getCellEditor().stopCellEditing();

      if (AlbeLib.emptyField(epaisseur_[i]))
        vm.add(AlbeMsg.getMsgEpaisseurSolManquante(i + 1));
      else if (AlbeLib.textFieldDoubleValue(epaisseur_[i]) == VALEUR_NULLE.value)
        vm.add(AlbeMsg.getMsgEpaisseurSolNonValide(i + 1));
      else if (AlbeLib.textFieldDoubleValue(epaisseur_[i]) <= 0)
        vm.add(AlbeMsg.getMsgEpaisseurNeg(i + 1));

      if (!AlbeLib.fullTable(table_[i], modele_[i]))
        vm.add(AlbeMsg.getMsgCoordSol(i + 1));
      else if (!cellulescolonne1Croissantes(table_[i]))
        vm.add(AlbeMsg.getMsgDeplacementSol(i + 1));
      else if (!cellulescolonne2Croissantes(table_[i]))
        vm.add(AlbeMsg.getMsgCourbeSol(i + 1));
    }
    return vm;
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {

    int currentTab = tp_.getSelectedIndex();

    /**
     * Gestion de la s�lection du nombre de points de la courbe manuelle.
     */
    if (_evt.getSource() == nbPointsCombo_) {
      // On stoppe l'�dition du tableau
      if (table_[currentTab].isEditing())
        table_[currentTab].getCellEditor().stopCellEditing();
      // Pendant la mise � jour du nb de lignes du tableau, on n'effectue pas
      // les v�rifications
      AlbeLib.verificationCourbeManuelle_ = false;

      int index = nbPointsCombo_.getSelectedIndex();
      int nbRow = index + 2;
      for (int i = 0; i < AlbeLib.NBMAX_COUCHES; i++) {
        // nbPointsCombo_[i].setSelectedIndex(index);
        AlbeLib.majTableSol(table_[i], scrollPane_[i], nbRow);
        // Rafra�chissement du panneau
        revalidate();
      }

      majMessages();
      AlbeLib.verificationCourbeManuelle_ = true;
    }

    /**
     * Clic sur le bouton 'Initialiser toutes les couches'.
     */
    else if (_evt.getSource() == initButton_) {
      // Pendant l'effacement des donn�es, on n'effectue pas les v�rifications
      AlbeLib.verificationCourbeManuelle_ = false;
      eraseDataCouches(1, sol_.nbCouches_);
      majMessages();
      AlbeLib.verificationCourbeManuelle_ = true;
    }

    /**
     * Clic sur un des boutons 'Graphique'.
     */
    else if (_evt.getSource() == graphiqueButton_[currentTab]) {
      // On stoppe l'�dition du tableau
      if (table_[currentTab].isEditing())
        table_[currentTab].getCellEditor().stopCellEditing();

      if (verifTable(table_[currentTab], modele_[currentTab])) {
        getCoordPoints(table_[currentTab]);
        // Affichage du graphique
        AlbeLib.showGraphViewer(getApplication(), getImplementation(),
            "Mod�lisation du comportement du sol", getScriptCourbeManuelle(table_[currentTab],
                currentTab + 1));
      }
    }
  }
}
