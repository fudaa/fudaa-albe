/*
 * @creation 30 nov. 06
 * @modification $Date: 2007-06-01 10:18:13 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JComponent;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.SResultatsAlbe;
import org.fudaa.dodico.corba.albe.SResultatsCombinaison;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuVerticalLayout;

/**
 * fen�tre contenant les r�sultats combinaison par combinaison.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeFilleResultatsCombinaison extends AlbeInternalFrameAdapter {

  /** onglet 'R�capitulatif'. */
  protected AlbeRecapitulatifResultats recap_;

  /** onglet 'R�sistance du pieu'. */
  protected AlbeResistancePieuResultats resistance_;

  /** onglet 'Mobilisation de la but�e'. */
  protected AlbeMobilisationButeeResultats butee_;

  /** onglet 'Mobilisation de la d�fense'. */
  protected AlbeMobilisationDefenseResultats defense_;

  /** onglet 'D�placements et r�action'. */
  protected AlbeDeplacementReactionResultats deplacement_;

  /** set d'onglets. */
  protected BuTabbedPane tp_;

  /** boutons : exporter et fermer. */
  protected BuButton exporterButton_, fermerButton_;

  /** param�tres de Albe. */
  SParametresAlbe params_;

  /** r�sultats de Albe. */
  SResultatsAlbe results_;

  /** structure de r�sultats pour la combinaison. */
  SResultatsCombinaison resCombi_;

  /**
   * type de combinaison (0:manuelle/1:ELU fonda/2:ELU acc/3:ELS rare/4:ELS
   * fr�q).
   */
  int typeCombi_;

  /** num�ro de la combinaison concern�e. */
  int numCombi_;

  /** nombre de combinaisons possibles pour le type. */
  int nbCombi_;

  /** bool�en pr�cisant le type d'action (true = accostage / false = amarrage). */
  boolean accostage_;

  /** bool�en pr�cisant s'il y a des d�fenses. */
  boolean presenceDefense_;

  /** bool�en sp�cifiant s'il y a corrosion. */
  boolean cor_;

  /** r�sultats � afficher. */
  String[] deplacements_;

  String[] reactions_;

  String[] deflexions_;

  /** nom du fichier pour l'exportation html des r�sultats.
   * @f_ : chemin complet
   * @fRel_ : chemin relatif
   */
  String f_;
  String fRel_;
  
  /**
   * constructeur.
   */

  public AlbeFilleResultatsCombinaison(final BuCommonInterface _appli, final FudaaProjet _project,
      final String _title, final SResultatsCombinaison _resCombi, final int _typeCombi,
      final int _numCombi, final int _nbCombi) {

    super(_title, "liste", _appli, _project);
    setSize(800, 800);
    setResizable(true);

    typeCombi_ = _typeCombi;
    numCombi_ = _numCombi;
    nbCombi_ = _nbCombi;

    params_ = (SParametresAlbe) project_.getParam(AlbeResource.PARAMETRES);
    results_ = (SResultatsAlbe) project_.getResult(AlbeResource.RESULTATS);

    resCombi_ = _resCombi;

    accostage_ = false;
    presenceDefense_ = false;
    if (params_.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE))
      accostage_ = true;
    if (accostage_ && params_.defense.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI))
      presenceDefense_ = true;

    if (params_.lancementCalculs.modeLancementCalcul == AlbeLib.IDL_LANCEMENT_AUTOMATIQUE) {
      // il y a corrosion si le num�ro de combinaison est impair
      if (numCombi_ % 2 == 0)
        cor_ = false;
      else
        cor_ = true;
    } else
      cor_ = params_.lancementCalculs.corrosion;

    // Calcul des r�sultats
    deplacements_ = AlbeRes.getDeplacements(params_, resCombi_, typeCombi_);
    if (accostage_)
      reactions_ = AlbeRes.getReactions(params_, resCombi_, typeCombi_);
    if (presenceDefense_)
      deflexions_ = AlbeRes.getDeflexions(params_, resCombi_, typeCombi_);

    JComponent content = (JComponent) getContentPane();
    content.setLayout(new BuVerticalLayout());

    /** Bloc informations */
    final BuPanel infoPanel = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    infoPanel.setLayout(new BorderLayout());
    BuLabelMultiLine infoLabel = AlbeLib
        .labelMultiLine("Voici le d�tail des r�sultats pour la combinaison "
            + (_typeCombi == AlbeLib.COMBINAISON_MANUELLE ? "manuelle" : numCombi_ + " du type "
                + AlbeRes.getCombinaison(typeCombi_))
            + ".\n"
            + "Pour sauvegarder l'int�gralit� des r�sultats en format HTML, cliquez sur 'Exporter'.\n"
            + "Vous pouvez �galement g�n�rer une note de calculs compl�te (donn�es et r�sultats) en cliquant sur le bouton 'Note' ci dessus.");
    infoPanel.add(infoLabel);

    /** Bloc boutons */
    final BuPanel buttonPanel = new BuPanel(new FlowLayout());
    exporterButton_ = AlbeLib.buttonId("Exporter", "EXPORTER");
    fermerButton_ = AlbeLib.buttonId("Fermer", "FERMER");
    exporterButton_.setToolTipText("Sauvegarder l'int�gralit� des r�sultats dans un fichier HTML");
    fermerButton_.setToolTipText("Fermer la fen�tre");
    exporterButton_.addActionListener(this);
    fermerButton_.addActionListener(this);
    buttonPanel.add(exporterButton_);
    buttonPanel.add(fermerButton_);

    /** set d'onglets */
    tp_ = new BuTabbedPane();

    recap_ = new AlbeRecapitulatifResultats(this);
    resistance_ = new AlbeResistancePieuResultats(this);
    butee_ = new AlbeMobilisationButeeResultats(this);
    if (presenceDefense_)
      defense_ = new AlbeMobilisationDefenseResultats(this);
    deplacement_ = new AlbeDeplacementReactionResultats(this);

    tp_.addTab("R�capitulatif", null, recap_, "Page r�capitulative des r�sultats");
    tp_.addTab("R�sistance du pieu", null, resistance_,
        "R�sultats concernant la r�sistance du tube ou caisson");
    tp_.addTab("Mobilisation de la but�e", null, butee_,
        "R�sultats concernant la mobilisation de la but�e du sol");
    tp_.addTab("Mobilisation de la d�fense", null, defense_,
        "R�sultats concernant la mobilisation de la d�fense");
    tp_.addTab("D�placements et r�action", null, deplacement_,
        "R�sultats concernant le d�placement en t�te d'ouvrage et la r�action au choc");

    // D�sactivation de l'onglet 'Mobilisation de la d�fense' si pas de
    // d�fense
    if (!presenceDefense_)
      tp_.setEnabledAt(3, false);

    content.add(infoPanel);
    content.add(buttonPanel);
    content.add(tp_);

  }// Fin constructeur

  /**
   * sauvegarde tous les r�sultats dans un fichier HTML.
   */
  protected void sauvegarder() {
    f_ = AlbeLib.getFileChoosenByUser(this, "Exporter les r�sultats d�taill�s", "Enregistrer",
        AlbeLib.USER_HOME
            + "resultatsAlbe_combinaison"
            + (typeCombi_ == AlbeLib.COMBINAISON_MANUELLE ? "manuelle" : (numCombi_ + "_" + AlbeRes.nomCombinaison(typeCombi_))) + ".html");
    fRel_ = "resultatsAlbe_combinaison"+ (typeCombi_ == AlbeLib.COMBINAISON_MANUELLE ? "manuelle" : (numCombi_ + "_" + AlbeRes.nomCombinaison(typeCombi_))) + ".html";
    if (f_ == null) {
      return;
    }
    try {
      (new File(f_ + "files" + AlbeLib.FILE_SEPARATOR)).mkdirs();
    }
    catch (final Exception _e1) {}
    int i = 1;
    String html = "";
    html += "<html>" + CtuluLibString.LINE_SEP + "";
    html += "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"><title>R&eacute;sultats combinaison "
        + (typeCombi_ == AlbeLib.COMBINAISON_MANUELLE ? "manuelle" : (numCombi_ + " " + AlbeRes
            .getCombinaison(typeCombi_))) + "</title></head>" + CtuluLibString.LINE_SEP + "";
    html += "<body>" + CtuluLibString.LINE_SEP + "";
    html += "<hr>" + CtuluLibString.LINE_SEP;
    html += "<CENTER><h1><FONT COLOR = \"#FF0000\">R&eacute;sultats</FONT></h1></CENTER>"
        + CtuluLibString.LINE_SEP + "";
    html += "<hr>" + CtuluLibString.LINE_SEP;
    // Ligne 'Information'
    html += "<br><p>Cette page pr&eacute;sente le d&eacute;tail des r&eacute;sultats du logiciel <b><i>"
        + AlbeImplementation.SOFTWARE_TITLE
        + "</i></b> pour la combinaison "
        + (typeCombi_ == AlbeLib.COMBINAISON_MANUELLE ? "manuelle"
            : (numCombi_ + " du type " + AlbeRes.getCombinaison(typeCombi_))) + ".<br></p>"
        + CtuluLibString.LINE_SEP;
    // Sommaire
    html += "<ul type = none>" + CtuluLibString.LINE_SEP;
    html += "<br><h2><FONT COLOR = \"#3333CC\"> Sommaire</FONT></h2>" + CtuluLibString.LINE_SEP;
    html += "<a href = \"#recapitulatif\">" + (i++) + ". R&eacute;capitulatif des r&eacute;sultats</a><br>"
        + CtuluLibString.LINE_SEP;
    html += "<a href = \"#resistance_pieu\">" + (i++) + ". R&eacute;sistance du pieu</a><br>"
        + CtuluLibString.LINE_SEP;
    html += "<a href = \"#mobilisation_butee\">" + (i++) + ". Mobilisation de la but&eacute;e</a><br>"
        + CtuluLibString.LINE_SEP;
    if (presenceDefense_)
      html += "<a href = \"#mobilisation_defense\">" + (i++)
          + ". Mobilisation de la d&eacute;fense</a><br>" + CtuluLibString.LINE_SEP;
    html += "<a href = \"#deplacement_reaction\">" + (i++) + ". D&eacute;placement et r&eacute;action</a><br>"
        + CtuluLibString.LINE_SEP;

    i = 1;

    // R�capitulatif des r�sultats
    html += "<br><br><h2><FONT COLOR = \"#3333CC\"><li><a name =recapitulatif>" + (i++)
        + ". R&eacute;capitulatif des r&eacute;sultats</a></FONT></h2>" + CtuluLibString.LINE_SEP;
    html += recap_.buildPartieHtml();

    // R�sistance du pieu
    html += "<h2><FONT COLOR = \"#3333CC\"><li><a name =resistance_pieu>" + (i++)
        + ". R&eacute;sistance du pieu</a></FONT></h2>" + CtuluLibString.LINE_SEP;
    html += resistance_.buildPartieHtml();

    // Mobilisation de la but�e
    html += "<h2><FONT COLOR = \"#3333CC\"><li><a name =mobilisation_butee>" + (i++)
        + ". Mobilisation de la but&eacute;e</a></FONT></h2>" + CtuluLibString.LINE_SEP;
    html += butee_.buildPartieHtml();

    // Mobilisation de la d�fense
    if (presenceDefense_) {
      html += "<h2><FONT COLOR = \"#3333CC\"><li><a name =mobilisation_defense>" + (i++)
          + ". Mobilisation de la d&eacute;fense</a></FONT></h2>" + CtuluLibString.LINE_SEP;
      html += defense_.buildPartieHtml();
    }

    // D�placements et r�action
    html += "<h2><FONT COLOR = \"#3333CC\"><li><a name =deplacement_reaction>" + (i++)
        + ". D&eacute;placement et r&eacute;action</a></FONT></h2>" + CtuluLibString.LINE_SEP;
    html += deplacement_.buildPartieHtml();

    html += "</ul>" + CtuluLibString.LINE_SEP;
    html += "<br><br><i>g&eacute;n&eacute;r&eacute; par le logiciel <b>" + AlbeImplementation.SOFTWARE_TITLE
        + "</b></i>" + CtuluLibString.LINE_SEP;
    html += "</body>" + CtuluLibString.LINE_SEP + "";
    html += "</html>" + CtuluLibString.LINE_SEP + "";

    try {
      final FileWriter fw = new FileWriter(f_);
      fw.write(html, 0, html.length());
      fw.flush();
      fw.close();
      final File file = new File(f_);
      getApplication().getImplementation().displayURL(file.getAbsolutePath());
    }
    catch (final IOException _e1) {
      System.err.println("Impossible d'�crire le fichier sur le disque.");
    }
  }

  /**
   * retourne le maximum de la colonne situ�e � l'indice _num du tableau _table.
   */
  public double getMaxTableau(BuTable _table, int _num) {
    double max = ((Double) (_table.getValueAt(0, _num))).doubleValue();
    for (int i = 1; i < _table.getRowCount(); i++) {

      double d = ((Double) (_table.getValueAt(i, _num))).doubleValue();
      if (d > max)
        max = d;
    }
    return max;
  }

  /**
   * retourne le minimum de la colonne situ�e � l'indice _num du tableau _table.
   */
  public double getMinTableau(BuTable _table, int _num) {
    double min = ((Double) (_table.getValueAt(0, _num))).doubleValue();
    for (int i = 1; i < _table.getRowCount(); i++) {
      double d = ((Double) (_table.getValueAt(i, _num))).doubleValue();
      if (d < min)
        min = d;
    }
    return min;
  }

  /**
   * 
   */
 /* protected String addImgFromScript(final String _script, final String _imgname, final String _alt) {
    String html = "";
    final String _img = f_ + "files" + AlbeLib.FILE_SEPARATOR + _imgname;
    AlbeLib.exportGraphScriptToGif(_script, _img);
    html += "<a href=\"" + _img + "\" target=\"_blank\"><img src=\"" + _img + "\" alt =\"" + _alt
        + "\"></a>" + CtuluLibString.LINE_SEP;
    return html;
  }*/
  
  protected String addImgFromScript(final String _script, final String _imgname, final String _alt) {
	  String html = "";
	  final String _img =f_+  "files" + "/" + _imgname;
	  final String _imgRel=fRel_ +  "files" + "/" + _imgname;
	  AlbeLib.exportGraphScriptToGif(_script, _img);
	  html += "<a href=\"" + _imgRel + "\" target=\"_blank\"><img src=\"" + _imgRel + "\" alt =\"" + _alt
	  + "\"></a>" + CtuluLibString.LINE_SEP;
	  return html;
  }


  /**
   * 
   */

  public void actionPerformed(ActionEvent _evt) {
    if (_evt.getSource() == fermerButton_)
      dispose();
    else if (_evt.getSource() == exporterButton_)
      sauvegarder();
  }
}
