/*
 * @creation 17 oct. 06
 * @modification $Date: 2007-05-11 09:00:04 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.fudaa.ctulu.gif.AcmeGifEncoder;
import org.fudaa.ebli.graphe.BGraphe;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialog;
import com.memoire.bu.BuPanel;

/**
 * boite de dialogue affichant un graphique.
 * 
 * @author Sabrina Delattre
 */
public class AlbeGraphiqueDialog extends BuDialog{

  JComponent graphic_;

  BuButton fermerButton_, exporterButton_;
  
  Container conteneur_;

  /**
   * constructeur.
   * 
   * @param _titre titre de la fen�tre
   * @param _graphics composant graphique � ins�rer dans la fen�tre
   */

  public AlbeGraphiqueDialog(final BuCommonInterface _app, final String _titre,
      final JComponent _graphic) {

    super(_app, _app.getInformationsSoftware(), _titre);
    setSize(700, 500);

    graphic_ = _graphic;

    conteneur_ = getContentPane();
    conteneur_.setLayout(new BorderLayout());
    conteneur_.add(_graphic, BorderLayout.CENTER);

    // Panneau contenant les boutons 'Exporter' et 'Fermer'
    BuPanel pButtons = new BuPanel();
    pButtons.setLayout(new FlowLayout());

    fermerButton_ = AlbeLib.buttonId("Fermer", "ANNULER");
    exporterButton_ = AlbeLib.buttonId("Exporter", "EXPORTER");
    fermerButton_.setToolTipText("Fermer la fen�tre");
    exporterButton_.setToolTipText("Exporter le graphique");
    fermerButton_.addActionListener(this);
    exporterButton_.addActionListener(this);
    pButtons.add(exporterButton_);
    pButtons.add(fermerButton_);

    conteneur_.add(pButtons, BorderLayout.SOUTH);
  }

  /**
   *
   */
  public JComponent getComponent() {
    return null;
  }

  /**
   * 
   */
  public void actionPerformed(final ActionEvent _evt) {

    if (_evt.getSource() == fermerButton_)
      dispose();
    else if (_evt.getSource() == exporterButton_)
      exporter();

  }

  /**
   *  exporte le graphique.
   */
  protected void exporter() {
    if (graphic_ instanceof BGraphe) {
      final String _f = AlbeLib.getFileChoosenByUser(graphic_, "Exporter l'image", "Exporter",
          AlbeLib.USER_HOME + "image.gif");
      if (_f == null) {
        return;
      }
      if ((new File(_f)).exists()) {
        final int _i = AlbeLib.dialogConfirmation((BuCommonInterface) null, null,
            "Le fichier cible existe d�j�. Voulez-vous l'�craser ?");
        if (_i != JOptionPane.YES_OPTION) {
          return;
        }
      }
      try {
        final AcmeGifEncoder _age = new AcmeGifEncoder(((BGraphe) graphic_).getImageCache(),
            new FileOutputStream(_f));
        _age.encode();
      }
      catch (final Exception _e1) {}
    }
  }
}
