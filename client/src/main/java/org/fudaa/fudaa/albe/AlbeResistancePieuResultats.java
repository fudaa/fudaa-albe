/*
 * @creation 30 nov. 06
 * @modification $Date: 2007-06-29 09:26:11 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.fudaa.ctulu.CtuluLibString;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuVerticalLayout;

/**
 * classe de l'onglet 'R�sistance du pieu' des r�sultats combinaison par
 * combinaison.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeResistancePieuResultats extends AlbeAbstractResultatsOnglet {

  BuButton btCourbeDeformee_, btCourbeMmt_, btCourbeEffort_, btCourbeContrainte_;

  BuTable tbMoments_;

  DefaultTableModel dmMoments_;

  /** r�f�rence vers la fen�tre parente. */
  AlbeFilleResultatsCombinaison fp_;

  /**
   * constructeur.
   */
  public AlbeResistancePieuResultats(final AlbeFilleResultatsCombinaison _fp) {

    super(_fp.getApplication(), _fp);

    fp_ = _fp;

    this.setLayout(new BuVerticalLayout());

    /** Bloc informations. */
    final BuPanel infoPanel = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    infoPanel.setLayout(new BorderLayout());
    BuLabelMultiLine infoLabel = AlbeLib
        .labelMultiLine("Vous trouverez ici les r�sultats concernant la r�sistance du tube ou caisson.\n"
            + "Vous pouvez exporter le tableau et visualiser les diff�rentes courbes de ce tableau.");
    infoPanel.add(infoLabel);

    /** Bloc 'R�sistance du pieu' */
    final BuPanel p = AlbeLib.titledPanel("R�sistance du pieu", AlbeLib.BEVEL_BORDER);
    p.setLayout(new BorderLayout());

    // Bloc nord
    final BuPanel bNord = new BuPanel(new FlowLayout());
    imprimerButton_ = AlbeLib.buttonId("Imprimer", "IMPRIMER");
    exporterButton_ = AlbeLib.buttonId("Exporter", "EXPORTER");
    imprimerButton_.setToolTipText("Imprimer la fen�tre");
    exporterButton_.setToolTipText("Exporter le tableau");
    imprimerButton_.addActionListener(this);
    exporterButton_.addActionListener(this);
    bNord.add(imprimerButton_);
    bNord.add(exporterButton_);

    // Bloc pour le tableau des moments
    final BuPanel bTable = new BuPanel(new FlowLayout());
    dmMoments_ = new AlbeResistancePieuResultatsTableModel(new Object[] { "Cote (m)",
        "Fl�che (cm)", "Moment (kN.m)", "Effort Tranchant (kN)", "Contrainte (MPa)" });
    double[][] contraintes = AlbeRes.calculContraintes(fp_.params_, fp_.resCombi_, fp_.typeCombi_,
        fp_.cor_);

    for (int i = 0; i < fp_.params_.pieu.nombreTroncons; i++) {
      for (int j = 0; j < fp_.resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
        if (!(fp_.resCombi_.resistanceTroncons[i].resTroncon[j].fleche == 0 && fp_.resCombi_.resistanceTroncons[i].resTroncon[j].moment == 0))
          dmMoments_
              .addRow(new Object[] {
                  new Double(AlbeLib
                      .arronditDouble(fp_.resCombi_.resistanceTroncons[i].resTroncon[j].cote)),
                  new Double(AlbeLib
                      .arronditDouble(fp_.resCombi_.resistanceTroncons[i].resTroncon[j].fleche)),
                  new Double(
                      AlbeLib
                          .arronditDouble(fp_.resCombi_.resistanceTroncons[i].resTroncon[j].moment * 10)),
                  new Double(
                      AlbeLib
                          .arronditDouble(fp_.resCombi_.resistanceTroncons[i].resTroncon[j].effortTranchant * 10)),
                  new Double(AlbeLib.arronditDouble(contraintes[i][j])) });
      }
    }

    tbMoments_ = new BuTable(dmMoments_);
    tbMoments_.setRowHeight(AlbeLib.TABLE_ROWHEIGHT);

    /** Alignement du texte au centre des cellules */
    DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
    renderer.setHorizontalAlignment(SwingConstants.CENTER);
    int nbColonnes = tbMoments_.getColumnCount();
    for (int i = 0; i < nbColonnes; i++) {
      tbMoments_.getColumnModel().getColumn(i).setCellRenderer(renderer);
    }

    /** Largeur de la derni�re colonne. */
    tbMoments_.getColumnModel().getColumn(3).setMinWidth(100);

    final JScrollPane sp = new JScrollPane(tbMoments_);
    sp.setPreferredSize(new Dimension(600, 300));
    bTable.add(sp);

    // Bloc pour les boutons courbes
    final BuPanel bCourbes = new BuPanel(new BuHorizontalLayout());
    // Initialisation des boutons
    btCourbeDeformee_ = AlbeLib.buttonId("D�form�e", "graphe");
    btCourbeDeformee_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        AlbeLib.showGraphViewer(getApplication(), getImplementation(),
            "Repr�sentation graphique de la d�form�e de l'ouvrage", getScriptDeformee());
      }
    });
    btCourbeMmt_ = AlbeLib.buttonId("Moments", "graphe");
    btCourbeMmt_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        AlbeLib.showGraphViewer(getApplication(), getImplementation(),
            "Repr�sentation graphique de la courbe des moments", getScriptCourbeMoments());
      }
    });
    btCourbeEffort_ = AlbeLib.buttonId("Efforts tranchants", "graphe");
    btCourbeEffort_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        AlbeLib
            .showGraphViewer(getApplication(), getImplementation(),
                "Repr�sentation graphique de la courbe de l'effort tranchant",
                getScriptCourbeEfforts());
      }
    });
    btCourbeContrainte_ = AlbeLib.buttonId("Contraintes", "graphe");
    btCourbeContrainte_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        AlbeLib.showGraphViewer(getApplication(), getImplementation(),
            "Repr�sentation graphique de la courbe des contraintes", getScriptCourbeContraintes());
      }
    });
    // //Bloc 'D�form�e'
    final BuPanel pDeform = AlbeLib.borderPanel(AlbeLib.BEVEL_BORDER);
    pDeform.setLayout(new BuVerticalLayout());
    JLabel lab1 = new JLabel("Courbe de la d�form�e de l'ouvrage ");
    lab1.setForeground(Color.red);
    final BuPanel p1 = new BuPanel(new FlowLayout());
    pDeform.add(lab1);
    p1.add(btCourbeDeformee_);
    pDeform.add(p1);

    // //Bloc 'Moments'
    final BuPanel pMmt = AlbeLib.borderPanel(AlbeLib.BEVEL_BORDER);
    pMmt.setLayout(new BuVerticalLayout());
    JLabel lab2 = new JLabel("Courbe des moments ");
    lab2.setForeground(Color.blue);
    final BuPanel p2 = new BuPanel(new FlowLayout());
    p2.add(btCourbeMmt_);
    pMmt.add(lab2);
    pMmt.add(p2);

    // //Bloc 'Effort tranchant'
    final BuPanel pEffort = AlbeLib.borderPanel(AlbeLib.BEVEL_BORDER);
    pEffort.setLayout(new BuVerticalLayout());
    JLabel lab3 = new JLabel("Courbe des efforts tranchants ");
    lab3.setForeground(Color.magenta);
    final BuPanel p3 = new BuPanel(new FlowLayout());
    p3.add(btCourbeEffort_);
    pEffort.add(lab3);
    pEffort.add(p3);

    // Bloc 'Contraintes'
    final BuPanel pContrainte = AlbeLib.borderPanel(AlbeLib.BEVEL_BORDER);
    pContrainte.setLayout(new BuVerticalLayout());
    JLabel lab4 = new JLabel("Courbe des contraintes ");
    lab4.setForeground(Color.ORANGE);
    final BuPanel p4 = new BuPanel(new FlowLayout());
    p4.add(btCourbeContrainte_);
    pContrainte.add(lab4);
    pContrainte.add(p4);

    bCourbes.add(pDeform);
    bCourbes.add(pMmt);
    bCourbes.add(pEffort);
    bCourbes.add(pContrainte);

    BuPanel tmpPanel = new BuPanel(new FlowLayout());
    tmpPanel.add(bCourbes);

    p.add(bNord, BorderLayout.NORTH);
    p.add(bTable, BorderLayout.CENTER);
    p.add(tmpPanel, BorderLayout.SOUTH);

    this.add(infoPanel);
    this.add(p);
  }

  /**
   * 
   */
  public String getScript(BuTable _table, String _title, String _titleAxeX, String _uniteAxeX,
      String _couleur, double _minx, double _maxx, double _pasx, int _numCol) {
    final StringBuffer s = new StringBuffer(4096);
    double coordMin = fp_.getMinTableau(_table, 0);
    double coordMax = fp_.getMaxTableau(_table, 0);
    double pasY = AlbeLib.getPas(coordMax, coordMin);
    double minY = AlbeLib.getMin(pasY, coordMin);
    double maxY = AlbeLib.getMax(pasY, coordMax);

    s.append("graphe \n{\n");
    s.append("  titre \"" + _title + "\"\n");
    s.append("  legende oui\n");
    s.append("  animation non\n");
    s.append("  marges\n{\n");
    s.append("    gauche 80\n");
    s.append("    droite 120\n");
    s.append("    haut 50\n");
    s.append("    bas 30\n");
    s.append("  }\n");
    s.append("  \n");
    s.append("  axe\n{\n");
    s.append("    titre \"" + _titleAxeX + "\"\n");
    s.append("    unite \"" + _uniteAxeX + "\"\n");
    s.append("    orientation horizontal\n");
    s.append("    graduations oui\n");
    s.append("    minimum " + _minx + "\n");
    s.append("    maximum " + _maxx + "\n");
    s.append("    pas " + _pasx + "\n");
    s.append("  }\n");
    s.append("  axe\n{\n");
    s.append("    titre \"cote\"\n");
    s.append("    unite \"m\"\n");
    s.append("    orientation vertical\n");
    s.append("    graduations oui\n");
    s.append("    minimum " + minY + "\n");
    s.append("    maximum " + maxY + "\n");
    s.append("    pas " + pasY + "\n");
    s.append("  }\n");
    s.append("  courbe\n{\n");
    s.append("    aspect\n{\n");
    s.append("    contour.couleur " + _couleur + "\n");
    s.append("    }\n");
    s.append("    valeurs\n{\n");
    for (int i = 0; i < _table.getRowCount(); i++) {
      s.append("      " + _table.getValueAt(i, _numCol) + " " + _table.getValueAt(i, 0) + "\n");
    }
    s.append("    }\n");
    s.append("  }\n");
    return s.toString();
  }

  /**
   * 
   */
  public String getScriptDeformee() {
    double coordMin = fp_.getMinTableau(tbMoments_, 1);
    double coordMax = fp_.getMaxTableau(tbMoments_, 1);
    double pasX = AlbeLib.getPas(coordMax, coordMin);
    double minX = AlbeLib.getMin(pasX, coordMin);
    double maxX = AlbeLib.getMax(pasX, coordMax);

    return getScript(tbMoments_, "Courbe de la d�form�e de l'ouvrage", "fl�che", "cm", "FF0000",
        minX, maxX, pasX, 1);
  }

  /**
   * 
   */
  public String getScriptCourbeMoments() {
    double coordMin = fp_.getMinTableau(tbMoments_, 2);
    double coordMax = fp_.getMaxTableau(tbMoments_, 2);
    double pasX = AlbeLib.getPas(coordMax, coordMin);
    double minX = AlbeLib.getMin(pasX, coordMin);
    double maxX = AlbeLib.getMax(pasX, coordMax);

    return getScript(tbMoments_, "Courbe des moments", "moment", "kN.m", "0000FF", minX, maxX,
        pasX, 2);
  }

  /**
   * 
   */
  public String getScriptCourbeEfforts() {
    double coordMin = fp_.getMinTableau(tbMoments_, 3);
    double coordMax = fp_.getMaxTableau(tbMoments_, 3);
    double pasX = AlbeLib.getPas(coordMax, coordMin);
    double minX = AlbeLib.getMin(pasX, coordMin);
    double maxX = AlbeLib.getMax(pasX, coordMax);

    return getScript(tbMoments_, "Courbe des efforts tranchants", "effort tranchant", "kN",
        "FF00FF", minX, maxX, pasX, 3);
  }

  /**
   * 
   */
  public String getScriptCourbeContraintes() {
    double coordMin = fp_.getMinTableau(tbMoments_, 4);
    double coordMax = fp_.getMaxTableau(tbMoments_, 4);
    double pasX = AlbeLib.getPas(coordMax, coordMin);
    double minX = AlbeLib.getMin(pasX, coordMin);
    double maxX = AlbeLib.getMax(pasX, coordMax);

    return getScript(tbMoments_, "Courbe des contraintes", "contrainte", "MPa", "FFA500", minX,
        maxX, pasX, 4);
  }

  /**
   * exporte le tableau dans un fichier .csv.
   */
  public void exporter() {
    final String f = AlbeLib.getFileChoosenByUser(this, "Exporter les donn�es", "Exporter",
        AlbeLib.USER_HOME
            + "Albe_resultatspieu_combinaison"
            + (fp_.typeCombi_ == AlbeLib.COMBINAISON_MANUELLE ? "manuelle"
                : (fp_.numCombi_ + "_" + AlbeRes.nomCombinaison(fp_.typeCombi_))) + ".csv");
    if (f == null) {
      return;
    }
    String txt = "";
    txt += "Cote (m);Fl�che (cm);Moment (kN.m);Effort tranchant (kN);Contrainte (MPa)"
        + CtuluLibString.LINE_SEP;
    for (int i = 0; i < dmMoments_.getRowCount(); i++) {
      txt += dmMoments_.getValueAt(i, 0) + ";" + dmMoments_.getValueAt(i, 1) + ";"
          + dmMoments_.getValueAt(i, 2) + ";" + dmMoments_.getValueAt(i, 3) + ";"
          + dmMoments_.getValueAt(i, 4) + CtuluLibString.LINE_SEP;
    }
    AlbeLib.writeFile(f, txt);
  }

  /**
   * construit la partie HTML des r�sultats sur le pieu.
   */
  public String buildPartieHtml() {
    String html = "";

    html += "<table border=\"2\" cellspacing=\"0\" cellpadding=\"5\">" + CtuluLibString.LINE_SEP;
    html += "<th bgcolor=\"#DDDDDD\">Cote (m)</th><th bgcolor=\"#DDDDDD\">Fl&egrave;che (cm)</th><th bgcolor=\"#DDDDDD\">Moment (kN.m)</th><th bgcolor=\"#DDDDDD\">Effort tranchant (kN)</th><th bgcolor=\"#DDDDDD\">Contrainte (MPa)</th>";
    for (int i = 0; i < tbMoments_.getRowCount(); i++) {
      html += "<tr align=center>" + CtuluLibString.LINE_SEP;
      for (int j = 0; j < tbMoments_.getColumnCount(); j++) {
        html += "<td>" + tbMoments_.getValueAt(i, j) + "</td>" + CtuluLibString.LINE_SEP;
      }
      html += "</tr>" + CtuluLibString.LINE_SEP;
    }
    html += "</table><br><br>" + CtuluLibString.LINE_SEP;
    html += fp_.addImgFromScript(getScriptDeformee(), "courbe_deformee_ouvrage.gif",
        "Courbe de la d&eacute;form&eacute;e de l'ouvrage");
    html += "<br><br>";
    html += fp_.addImgFromScript(getScriptCourbeMoments(), "courbe_moments.gif",
        "Courbe des moments");
    html += "<br><br>";
    html += fp_.addImgFromScript(getScriptCourbeEfforts(), "courbe_efforts_tranchants.gif",
        "Courbe des efforts tranchants");
    html += "<br><br>";

    return html;
  }

  /**
   *
   */
  public class AlbeResistancePieuResultatsTableModel extends DefaultTableModel {
    /**
     *
     */
    public AlbeResistancePieuResultatsTableModel(final Object[] _columnNames) {
      super(_columnNames, 0);
    }

    /**
     *
     */
    public boolean isCellEditable(final int _row, final int _column) {
      return false;
    }
  }
}
