package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuEmptyList;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuLib;

/**
 * Message view. sert � afficher dans le panneau message, la liste des messages
 * d'erreurs lors d'un changement d'onglet.
 * 
 * @author Axel von Arnim
 */
public class AlbeParamEventView extends BuEmptyList {
  /**
   *
   */
  DefaultListModel model_;

  /**
   *
   */
  Vector messages_;

  /**
   *
   */
  public AlbeParamEventView() {
    super();
    setName("albePARAMEVENTVIEW");
    model_ = new DefaultListModel();
    setModel(model_);
    setEmptyText("");
    setCellRenderer(new CellRenderer());
    setFont(BuLib.deriveFont("List", Font.PLAIN, -2));
    messages_ = new Vector();
  }

  /**
   *
   */
  public void addMessage(final VMessage _m) {
    boolean found = false;
    final Object[] _ma = messages_.toArray();
    for (int i = 0; i < _ma.length; i++) {
      if (((VMessage) _ma[i]).getId() == _m.getId()) {
        found = true;
        break;
      }
    }
    if (found) {
      return;
    }
    messages_.add(_m);
    model_.addElement(_m);
    revalidate();
    repaint(200);
  }

  /**
   *
   */
  public void addMessages(final VMessage[] _ms) {
    for (int i = 0; i < _ms.length; i++) {
      addMessage(_ms[i]);
    }
  }

  /**
   *
   */
  public void removeMessage(final VMessage _m) {
    boolean found = false;
    int index = -1;
    final Object[] _ma = messages_.toArray();
    for (int i = 0; i < _ma.length; i++) {
      if (((VMessage) _ma[i]).getId() == _m.getId()) {
        found = true;
        index = i;
        break;
      }
    }
    if (!found) {
      return;
    }
    final VMessage _dm = (VMessage) messages_.remove(index);
    model_.removeElement(_dm);
    revalidate();
    repaint(200);
  }

  /**
   *
   */
  public void removeAllMessagesInCategory(final String _cat) {
    final Object[] _ma = messages_.toArray();
    for (int i = 0; i < _ma.length; i++) {
      if (((VMessage) _ma[i]).getOnglet().equals(_cat)
          || ((VMessage) _ma[i]).getOnglet().indexOf(_cat)>=0) {
        removeMessage((VMessage) _ma[i]);
      }
    }
    revalidate();
    repaint(200);
  }

  /**
   *
   */
  public void removeAllMessages() {
    final Object[] _ma = messages_.toArray();
    for (int i = 0; i < _ma.length; i++) {
      removeMessage((VMessage) _ma[i]);
    }
    revalidate();
    repaint(200);
  }

  /**
   *
   */
  class Cell extends JPanel {
    /**
     *
     */
    BuLabelMultiLine level_;

    BuLabelMultiLine message_;

    BuLabelMultiLine category_;

    Color color1_ = new Color(255, 216, 216);

    Color color2_ = new Color(216, 255, 216);

    JPanel p_;

    JButton help_;

    String fullm_;

    /**
     * Constructor Cell.
     */
    public Cell() {
      this.setLayout(new BorderLayout());
      this.setOpaque(false);
      this.setBorder(new EmptyBorder(3, 3, 3, 3));
      help_ = new BuButton("?");
      p_ = new JPanel();
      p_.setLayout(new BorderLayout());
      p_.setBorder(new EmptyBorder(2, 2, 2, 2));
      p_.setOpaque(true);
      this.add(p_, BorderLayout.CENTER);
      final JPanel _p0 = new JPanel();
      _p0.setLayout(new BorderLayout());
      _p0.setOpaque(false);
      _p0.setBorder(new EmptyBorder(2, 2, 2, 2));
      final JPanel _p1 = new JPanel();
      _p1.setLayout(new BorderLayout());
      _p1.setOpaque(false);
      final JPanel _p2 = new JPanel();
      _p2.setLayout(new BorderLayout());
      _p2.setBorder(new EmptyBorder(2, 2, 2, 2));
      _p2.setOpaque(false);
      final JPanel _p3 = new JPanel();
      _p3.setLayout(new BorderLayout());
      _p3.setBorder(new EmptyBorder(2, 2, 2, 2));
      // _p1.add(_p2,BorderLayout.NORTH);
      _p1.add(_p3, BorderLayout.CENTER);
      p_.add(_p0, BorderLayout.NORTH);
      p_.add(_p1, BorderLayout.CENTER);
      level_ = new BuLabelMultiLine();
      level_.setWrapMode(BuLabelMultiLine.WORD);
      category_ = new BuLabelMultiLine();
      category_.setWrapMode(BuLabelMultiLine.WORD);
      message_ = new BuLabelMultiLine();
      message_.setWrapMode(BuLabelMultiLine.WORD);
      message_.setFont(BuLib.deriveFont("List", Font.PLAIN, -2));
      _p0.add(category_, BorderLayout.CENTER);
      // _p0.add(help_,BorderLayout.EAST);
      _p2.add(level_);
      _p3.add(message_);
      _p3.setOpaque(true);
      _p3.setBackground(new Color(255, 255, 255));
      fullm_ = "aucun message disponible.";
    }

    protected void showHelp() {
      JOptionPane.showMessageDialog(this, fullm_);
    }

    public void setLevel(final int _l) {
      String txt;
      Color color;
      switch (_l) {
      case 2: {
        txt = "Erreur non bloquante";
        color = color2_;
        break;
      }
      default: {
        txt = "Erreur bloquante";
        color = color1_;
        break;
      }
      }
      level_.setText(txt);
      level_.setPreferredSize(level_.computeHeight(AlbeLib.COLUMN_SCROLLPANE_WIDTH - 30));
      p_.setBackground(color);
    }

    public void setMessage(final String _m) {
      message_.setText(_m);
      message_.setPreferredSize(message_.computeHeight(AlbeLib.COLUMN_SCROLLPANE_WIDTH - 30));
    }

    public void setCategory(final String _c) {
      category_.setText(_c);
      category_.setPreferredSize(category_.computeHeight(AlbeLib.COLUMN_SCROLLPANE_WIDTH - 30));
    }

    public void setFullMessage(final String _m) {
      fullm_ = _m;
    }
  }

  /**
   *
   */
  class CellRenderer implements ListCellRenderer {
    /**
     *
     */
    private final Cell r_ = new Cell();

    /**
     *
     */
    public Component getListCellRendererComponent(final JList _list, final Object _value,
        final int _index, final boolean _isSelected, final boolean _cellHasFocus) {
      final VMessage _v = (VMessage) _value;
      r_.setCategory(_v.getOnglet());
      r_.setLevel(_v.getType().equals(VMessage.FATAL_ERROR) ? 1 : 2);
      r_.setMessage(_v.getDonnees() + " " + _v.getEtat());
      r_.setFullMessage(_v.getMessage());
      return r_;
    }
  }
}
