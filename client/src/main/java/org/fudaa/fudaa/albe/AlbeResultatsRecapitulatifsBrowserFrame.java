/*
 * @creation 19 d�c. 06
 * @modification $Date: 2007-06-01 10:17:46 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.SResultatsAlbe;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

import com.memoire.bu.BuBrowserFrame;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

/**
 * Fen�tre HTML contenant le tableau r�capitulatif des r�sultats pour un type de
 * combinaison.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeResultatsRecapitulatifsBrowserFrame extends BuBrowserFrame {

  BuCommonInterface appli_;

  FudaaProjet project_;

  SParametresAlbe params_;

  SResultatsAlbe results_;

  JComboBox combiBox_;

  BuButton exporterButton_, resButton_;

  String html_;

  int nbCombinaisons_;

  int typeCombi_;

  boolean accostage_;

  boolean defense_;

  /**
   * constructeur.
   * 
   * @param _appli
   * @param _project
   * @param _typeCombi type de la combinaison
   */
  public AlbeResultatsRecapitulatifsBrowserFrame(final BuCommonInterface _appli,
      final FudaaProjet _project, int _typeCombi) {

    super();

    setSize(810, 640);

    appli_ = _appli;
    project_ = _project;
    typeCombi_ = _typeCombi;

    defense_ = false;
    accostage_ = false;

    html_ = "";

    this.getContentPane().setLayout(new BuVerticalLayout());

    if (_project != null) {
      params_ = (SParametresAlbe) project_.getParam(AlbeResource.PARAMETRES);
      results_ = (SResultatsAlbe) project_.getResult(AlbeResource.RESULTATS);
    } else {
      //TODO FRED DAnS ce CAS ON DOIT FAIRE UN RETUrN
      //car tout le reste du constructeur suppose que params_ est non null.
      params_ = null;
      results_ = null;
      return;
    }

    if (params_.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)) {
      accostage_ = true;
      if (params_.defense.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI))
        defense_ = true;
    }

    if (accostage_) {
      if (defense_)
        nbCombinaisons_ = typeCombi_ == AlbeLib.ELU_FONDA ? 16 : 8;
      else
        nbCombinaisons_ = typeCombi_ == AlbeLib.ELU_FONDA ? 8 : 4;
    } else
      nbCombinaisons_ = typeCombi_ == AlbeLib.ELU_FONDA ? 4 : 2;

    buildHtmlPage();
    this.setHtmlSource(html_);

    /** Bloc bouton. */
    final BuPanel bButton = new BuPanel(new FlowLayout());
    JLabel lab = new JLabel("Cliquez ici pour exporter le tableau r�capitulatif des r�sultats - > ");
    lab.setForeground(Color.blue);
    bButton.add(lab);
    exporterButton_ = AlbeLib.buttonId("Exporter", "EXPORTER");
    exporterButton_.addActionListener(this);
    bButton.add(exporterButton_);

    /** Bloc 'Information'. */
    final BuPanel b1 = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    b1.setLayout(new BuVerticalLayout());
    final BuLabelMultiLine labMul = AlbeLib
        .labelMultiLine("Vous pouvez acc�der au d�tail des r�sultats combinaison par combinaison.\nPour cel�, cliquez sur le bouton 'R�sultats' ci dessous apr�s avoir s�lectionn� le num�ro de la combinaison.");
    b1.add(labMul);

    /** Bloc 'Choix de la combinaison'. */
    final BuPanel b2 = AlbeLib.titledPanel("Choix de la combinaison", AlbeLib.ETCHED_BORDER);
    b2.setLayout(new FlowLayout());
    b2.add(new JLabel("Num�ro de combinaison : "));
    combiBox_ = new JComboBox();
    // Initialisation de la boite de liste
    for (int i = 1; i <= nbCombinaisons_; i++) {
      combiBox_.addItem(Integer.toString(i));
    }
    b2.add(combiBox_);
    JLabel lab2 = new JLabel(" Voir les r�sultats de la combinaison s�lectionn�e - > ");
    lab2.setForeground(Color.red);
    b2.add(lab2);
    resButton_ = AlbeLib.buttonId("R�sultats", "analyser");
    resButton_.setToolTipText("Visualiser les r�sultats pour la combinaison s�lectionn�e");
    b2.add(resButton_);
    resButton_.addActionListener(this);

    /** Bloc 'R�sultats par combinaison'. */
    BuPanel p = AlbeLib.titledPanel("R�sultats par combinaison", AlbeLib.COMPOUND_BORDER);
    p.setLayout(new BuVerticalLayout());
    p.add(b1);
    p.add(b2);

    /**
     * Panneau regroupant le bloc boutons ainsi que le bloc 'R�sultats par
     * combinaison'.
     */
    BuPanel panelSud = new BuPanel(new BuVerticalLayout());
    panelSud.add(bButton);
    panelSud.add(p);

    container_.remove(urlBar_);
    sbStatus_.setVisible(false);
    container_.add(panelSud, BorderLayout.SOUTH);

  }// Fin constructeur

  /**
   * �crit dans la variable html_ le code HTML de la page.
   */
  public void buildHtmlPage() {
    // titre
    String titre = "Tableau r�capitulatif des r�sultats : " + AlbeRes.getCombinaison(typeCombi_);

    html_ += "<html>" + CtuluLibString.LINE_SEP + "";
    html_ += "<head><title>" + titre + "</title></head>" + CtuluLibString.LINE_SEP + "";
    html_ += "<body>" + CtuluLibString.LINE_SEP + "";
    html_ += "<center><h2><FONT COLOR = \"#3333CC\">" + titre + "</FONT></h2></center><br>"
        + CtuluLibString.LINE_SEP;
    html_ += AlbeRes.buildTabRecapResultats(params_, results_, accostage_, defense_, typeCombi_,true);
    html_ += "</body>" + CtuluLibString.LINE_SEP + "";
    html_ += "</html>" + CtuluLibString.LINE_SEP + "";
  }

  /**
   * exporte le tableau r�capitulatif dans une page HTML.
   */
  protected void exporter() {
    final String f = AlbeLib.getFileChoosenByUser(this, "Exporter le tableau r�capitulatif",
        "Enregistrer", AlbeLib.USER_HOME + "tableau_recapitulatif_"
            + AlbeRes.getCombinaison(typeCombi_) + ".html");
    if (f == null) {
      return;
    }
    String html = "";
    html += "<html>" + CtuluLibString.LINE_SEP + "";
    html += "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"><title>"
        + AlbeRes.getCombinaison(typeCombi_) + " : r&eacute;sultats r&eacute;capitulatifs" + "</title></head>"
        + CtuluLibString.LINE_SEP + "";
    html += "<body>" + CtuluLibString.LINE_SEP + "";
    html += "<hr>" + CtuluLibString.LINE_SEP;
    html += "<CENTER><h2><FONT COLOR = \"#FF0000\">R&eacute;sultats r&eacute;capitulatifs pour l'"
        + AlbeRes.getCombinaison(typeCombi_) + "</FONT></h2></CENTER>" + CtuluLibString.LINE_SEP
        + "";
    html += "<hr><br><br>" + CtuluLibString.LINE_SEP;
    html += AlbeRes.buildTabRecapResultats(params_, results_, accostage_, defense_, typeCombi_,true);
    html += "<br><br><i>g&eacute;n&eacute;r&eacute; par le logiciel <b>" + AlbeImplementation.SOFTWARE_TITLE
        + "</b></i>" + CtuluLibString.LINE_SEP;
    html += "</body>" + CtuluLibString.LINE_SEP + "";
    html += "</html>" + CtuluLibString.LINE_SEP + "";
//TODO FRed si erreur le flux fw ne sera jamais ferme
    FileWriter fw =null;
    try {
      fw = new FileWriter(f);
      fw.write(html, 0, html.length());
      fw.flush();
      //fait dans le finally
      //fw.close();
    }
    catch (final IOException _e1) {
      FuLog.warning("Impossible d'�crire le fichier sur le disque.");
    }
    finally{
      FuLib.safeClose(fw);
    }
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {
    if (_evt.getSource() == resButton_) {
      int numCombi = combiBox_.getSelectedIndex() + 1;
      ((AlbeImplementation) (appli_.getImplementation())).resultatsCombi(AlbeRes
          .getCombinaison(typeCombi_)
          + " : r�sultats d�taill�s de la combinaison " + numCombi, typeCombi_, numCombi,
          nbCombinaisons_);
    } else if (_evt.getSource() == exporterButton_)
      exporter();

  }
}
