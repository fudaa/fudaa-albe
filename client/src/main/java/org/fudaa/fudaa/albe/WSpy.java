/*
 * @file         WSpy.java
 * @creation     2000-10-18
 * @modification $Date: 2006-09-19 15:11:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2002 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.albe;


/**
 * Classes de rapports et de debugging sur console (Espion)
 * 
 * @version $Revision: 1.5 $ $Date: 2006-09-19 15:11:52 $ by $Author: deniger $
 * @author Olivier Hoareau
 */
public final class WSpy {
  /**
   * Flag permettant de fixer le mode de rapport d'erreur et de debugging. DEBUG doit avoir une des valeurs parmi les
   * constantes situ�es plus bas.
   */
  public final static int DEBUG = 0;
  /**
   * n'affiche que les messages d'actions sur la console.
   */
  public final static int ACTIONS_ONLY = -3;
  /**
   * n'affiche que les messages d'erreurs sur la console.
   */
  public final static int ERRORS_ONLY = -2;
  /**
   * n'affiche que les messages simples sur la console.
   */
  public final static int MESSAGES_ONLY = -1;
  /**
   * affiche tous les types de messages et traces sur la console. il faut utiliser ce flag en cas d'erreur fatale pour
   * tracer l'erreur.
   */
  public final static int ALL = 0;
  /**
   * n'affiche pas les entr�es-sorties dans les fonctions.
   */
  public final static int NO_FUNCTIONS = 1;
  /**
   * n'affiche pas les entr�es-sorties dans les fonctions. il faut utiliser ce flag pour les version Alpha du logiciel.
   */
  public final static int ALPHA_SOFTWARE = 1;
  /**
   * n'affiche ni les entr�es-sorties dans les fonctions, ni dans les proc�dures.
   */
  public final static int NO_PROCEDURES = 2;
  /**
   * n'affiche ni les entr�es-sorties dans les fonctions, ni dans les proc�dures. il faut utiliser ce flag pour les
   * version Beta du logiciel.
   */
  public final static int BETA_SOFTWARE = 2;
  /**
   * n'affiche aucune entr�es-sorties dans les fonctions et m�thodes et aucun messages simples.
   */
  public final static int NO_MESSAGES = 3;
  /**
   * n'affiche aucune entr�es-sorties dans les fonctions et m�thodes, aucun messages simples et aucun message d'action.
   */
  public final static int NO_ACTIONS = 4;
  /**
   * n'affiche aucune entr�es-sorties dans les fonctions et m�thodes, aucun messages simples et aucun message d'action.
   * il faut utiliser ce flag pour la version finale du logiciel.
   */
  public final static int FINAL_SOFTWARE = 4;
  /**
   * n'affiche aucune trace � l'�cran.
   */
  public final static int NO_ERRORS = 5;
  /**
   * n'affiche aucune trace � l'�cran.
   */
  public final static int NOTHING = 10;
  /**
   * Symbol d'indentation. sera r�p�t� autant de fois que le niveau d'imbrication dans lequel le message � afficher se
   * trouve.
   */
  private final static String INDENT_SYMBOL = "  ";
  /**
   * profondeur d'indentation initiale. Ne pas modifier ! (profondeur de d�part) sous risque de plantage.
   */
  private static int curdepth_ = -1;

  /**
   * affiche un message d'entr�e dans la proc�dure indent� sur la sortie standard.
   * 
   * @param _t texte du message d'entr�e � afficher
   */
  public static void Enter(final String _t) {
    if ((DEBUG >= NO_PROCEDURES) || (DEBUG == MESSAGES_ONLY) || (DEBUG == ERRORS_ONLY) || (DEBUG == ACTIONS_ONLY)) {
      return;
    }
    curdepth_++;
    String t = "";
    for (int i = 0; i < curdepth_; i++) {
      t += INDENT_SYMBOL;
    }
    System.out.println("[ --> ] " + t + _t);
  }

  /**
   * affiche un message de sortie de la proc�dure indent� sur la sortie standard.
   * 
   * @param _t texte du message de sortie � afficher
   */
  public static void Exit(final String _t) {
    if ((DEBUG >= NO_PROCEDURES) || (DEBUG == MESSAGES_ONLY) || (DEBUG == ERRORS_ONLY) || (DEBUG == ACTIONS_ONLY)) {
      return;
    }
    String t = "";
    for (int i = 0; i < curdepth_; i++) {
      t += INDENT_SYMBOL;
    }
    System.out.println("[ <-- ] " + t + _t);
    if (curdepth_ >= 0) {
      curdepth_--;
    }
  }

  /**
   * affiche un message d'entr�e dans la fonction indent� sur la sortie standard.
   * 
   * @param _t texte du message d'entr�e � afficher
   */
  public static void EnterFonction(final String _t) {
    if ((DEBUG >= NO_FUNCTIONS) || (DEBUG == MESSAGES_ONLY) || (DEBUG == ERRORS_ONLY) || (DEBUG == ACTIONS_ONLY)) {
      return;
    }
    curdepth_++;
    String t = "";
    for (int i = 0; i < curdepth_; i++) {
      t += INDENT_SYMBOL;
    }
    System.out.println("[ --> ] " + t + _t);
  }

  /**
   * affiche un message de sortie de la fonction indent� sur la sortie standard.
   * 
   * @param _t texte du message de sortie � afficher
   */
  public static void ExitFonction(final String _t) {
    if ((DEBUG >= NO_FUNCTIONS) || (DEBUG == MESSAGES_ONLY) || (DEBUG == ERRORS_ONLY) || (DEBUG == ACTIONS_ONLY)) {
      return;
    }
    String t = "";
    for (int i = 0; i < curdepth_; i++) {
      t += INDENT_SYMBOL;
    }
    System.out.println("[ <-- ] " + t + _t);
    if (curdepth_ >= 0) {
      curdepth_--;
    }
  }

  /**
   * affiche un message indent� sur la sortie standard.
   * 
   * @param _t texte du message de sortie � afficher
   */
  public static void Spy(final String _t) {
    if (DEBUG >= NO_MESSAGES) {
      return;
    }
    String t = "[ MSG ] ";
    if ((DEBUG < NO_PROCEDURES) && (DEBUG != MESSAGES_ONLY) && (DEBUG != ERRORS_ONLY) && (DEBUG != ACTIONS_ONLY)) {
      for (int i = 0; i < (curdepth_ + 1); i++) {
        t += INDENT_SYMBOL;
      }
    }
    System.out.println(t + _t);
  }

  /**
   * affiche un message d'action indent� sur la sortie standard.
   * 
   * @param _t texte du message d'action � afficher
   */
  public static void Action(final String _t) {
    if (DEBUG >= NO_ACTIONS) {
      return;
    }
    String t = "[ ACT ] ";
    if ((DEBUG < NO_PROCEDURES) && (DEBUG != MESSAGES_ONLY) && (DEBUG != ERRORS_ONLY) && (DEBUG != ACTIONS_ONLY)) {
      for (int i = 0; i < (curdepth_ + 1); i++) {
        t += INDENT_SYMBOL;
      }
    }
    System.out.println(t + _t);
  }

  /**
   * affiche un message d'erreur indent� sur la sortie erreur.
   * 
   * @param _t texte du message d'erreur � afficher
   */
  public static void Error(final String _t) {
    if (DEBUG >= NO_ERRORS) {
      return;
    }
    String t = "[ ERR ] ";
    if ((DEBUG < NO_PROCEDURES) && (DEBUG != MESSAGES_ONLY) && (DEBUG != ERRORS_ONLY) && (DEBUG != ACTIONS_ONLY)) {
      for (int i = 0; i < curdepth_; i++) {
        t += INDENT_SYMBOL;
      }
    }
    System.err.println(t + _t);
  }

  /**
   * affiche un message d'erreur indent� sur la sortie erreur.
   * 
   * @param _t exception throwable � afficher
   */
  public static void Error(final Throwable _t) {
    if (DEBUG >= NO_ERRORS) {
      return;
    }
    String t = "[ ERR ] ";
    if ((DEBUG < NO_PROCEDURES) && (DEBUG != MESSAGES_ONLY) && (DEBUG != ERRORS_ONLY) && (DEBUG != ACTIONS_ONLY)) {
      for (int i = 0; i < curdepth_; i++) {
        t += INDENT_SYMBOL;
      }
    }
    System.err.println(t + _t);
  }
}
