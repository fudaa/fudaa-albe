/*
 * @creation 17 nov. 06
 * @modification $Date: 2007-11-21 10:10:22 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gif.AcmeGifEncoder;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPicture;
import com.memoire.bu.BuVerticalLayout;

/**
 * affiche une fen�tre permettant de g�n�rer une note de calculs � partir des
 * r�sultats pr�c�demment calcul�s.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeFilleNoteDeCalculs extends AlbeInternalFrameAdapter {

  BuButton genererButton_, exporterButton_, fermerButton_;

  // -- Classeur qui contient l'onglet des choix de param�tres et l'onglet
  // d'affichage du rapport --//
  // BuTabbedPane classeur_=new BuTabbedPane();

  // -- Instance d'EkitCore qui permet de modifier du html dynamiquement et bien
  // d'autres fonctionnalit�s --//
  // CtuluHtmlEditorPanel editeur_=new CtuluHtmlEditorPanel();

  Settings settings_;

  WTree listconfig_;

  /**
   * constructeur.
   */

  public AlbeFilleNoteDeCalculs(final BuCommonInterface _appli, final FudaaProjet _project) {

    super("Note de calculs", "index", _appli, _project);
    settings_ = new Settings();
    JComponent content = (JComponent) getContentPane();
    // BuPanel content=new BuPanel(new GridLayout());

    content.setBorder(new EmptyBorder(5, 5, 5, 5));

    BuPanel p = new BuPanel(new BorderLayout());

    /** Bloc nord : informations */
    final BuPanel panelNord = AlbeLib.titledPanel("Informations", AlbeLib.ETCHED_BORDER);
    panelNord.setLayout(new BuVerticalLayout());

    BuLabelMultiLine infoLabel = AlbeLib
        .labelMultiLine("Veuillez choisir les options de personnalisation de votre note de calculs. Ajoutez ou supprimez une partie en cochant ou d�cochant 'inclure' ; "
            + "vous pouvez acc�der aux options de personnalisation de chacune des parties en cliquant sur la petite fl�che-bouton � gauche du titre de la partie. "
            + "Ce bouton vous permettra de d�plier/replier les options de personnalisation concernant la partie en question.\nLorsque vous aurez termin� de personnaliser votre note de calculs, "
            + "cliquez sur l'un des boutons 'G�n�rer' ou 'Exporter'. Le bouton 'Exporter' vous permettra de sauvegarder la note de calculs dans un fichier HTML.");
    panelNord.add(infoLabel);
    panelNord
        .add(new BuLabel(
            "<html><font color = \"red\">Pour la partie 'R�sultats des calculs', vous devez choisir la (les) "
                + "combinaison(s) pour la(les)quelle(s) vous souhaitez visualiser les r�sultats d�taill�s.</font></html>"));
    panelNord
        .add(new BuLabel(
            "Remarque : les op�rations de g�n�ration et d'exportation de la note de calculs peuvent prendre un certain temps."));

    /** Bloc centre : arbre */
    listconfig_ = new WTree(_project);
    JScrollPane sp = new JScrollPane(listconfig_);

    /** Bloc sud : boutons */
    BuPanel buttonsPanel = new BuPanel(new FlowLayout());
    genererButton_ = AlbeLib.buttonId("G�n�rer", "generer");
    exporterButton_ = AlbeLib.buttonId("Exporter", "exporter");
    fermerButton_ = AlbeLib.buttonId("Fermer", "fermer");
    genererButton_.setToolTipText("Visualiser la note de calculs");
    exporterButton_.setToolTipText("Sauvegarder la note de calculs dans un fichier HTML");
    fermerButton_.setToolTipText("Fermer la fen�tre");
    genererButton_.addActionListener(this);
    exporterButton_.addActionListener(this);
    fermerButton_.addActionListener(this);
    buttonsPanel.add(genererButton_);
    buttonsPanel.add(exporterButton_);
    buttonsPanel.add(fermerButton_);

    p.add(panelNord, BorderLayout.NORTH);
    p.add(sp, BorderLayout.CENTER);
    p.add(buttonsPanel, BorderLayout.SOUTH);
    content.add(p);

    // -- Creation du panel du rapport --//
    // BuPanel ContenuRapportHtml = new BuPanel(new BorderLayout());

    // -- Ajout des composants du EkitCore dans le panel --//
    // ContenuRapportHtml.add(editeur_, BorderLayout.CENTER);
    // ContenuRapportHtml.add(editeur_.getMenuBar(),BorderLayout.PAGE_START);
    // ContenuRapportHtml.add(editeur_.getToolBar(true), BorderLayout.NORTH);

    // -- ajout du contenu des parametres et de l'afficheur Html dans le
    // classeur --//
    // classeur_.add("Param�tres du rapport", content);
    // classeur_.add("Affichage du rapport", ContenuRapportHtml);

    // -- Onglet rapport non accessible tant qu'il n'y a pas eu de g�n�ration
    // --//
    // classeur_.setEnabledAt(1, false);

    // -- ajout du classeur dans l'interface --//
    // this.setLayout(new BorderLayout());
    // this.add(classeur_, BorderLayout.CENTER);
    // this.add(buttonsPanel, BorderLayout.SOUTH);

    pack();
  }

  /**
   *
   */
  protected synchronized void setFichier(final String _f, final String _name) {
    settings_.stocke("CHEMIN_DU_FICHIER_HTML", _f);
    settings_.stocke("NOM_DU_FICHIER_HTML", _name);
  }

  /**
   * renvoie une structure contenant toutes les pr�f�rences utilisateurs
   * concernant l'apparence de la note de calculs � g�n�rer. ces informations
   * sont destin�es � �tre enregistr�es dans le fichier de sauvegarde du projet.
   */
  public Settings getSettings() {
    final Settings _s = settings_;
    return _s;
  }

  /**
   * 
   */
  public boolean isChoosen(final String _setting) {

    final Boolean _b;
    try {
      _b = (Boolean) getSettings().lit(_setting);
      if (_b == null) {
        return false;
      }
      return _b.booleanValue();
    }
    catch (final ClassCastException _e) {
      if (getSettings().lit(_setting).equals(AlbeLib.VAL_NULLE))
        return false;
      return true;
    }

  }

  /**
   * g�n�re la note de calculs suivant les r�sultats et les pr�f�rences
   * utilisateurs.
   */
  boolean generer(final String _mode) {

    String html = "";
    String sommaire = "";
    String content = "";
    final Settings locSttings = getSettings();
    String f = (String) locSttings.lit("CHEMIN_DU_FICHIER_HTML");
    String name = (String) locSttings.lit("NOM_DU_FICHIER_HTML");

    final HtmlDoc doc = new HtmlDoc();
    HtmlPartie p = null;
    HtmlPartie sp = null;
    HtmlPartie ssp = null;

    if (_mode.equals("todisk")) {
      CtuluLibFile.deleteDir(new File(f + "files"));
      (new File(f + "files")).mkdirs();
    } else if (_mode.equals("toscreen")) {
      CtuluLibFile.deleteDir(new File(AlbeLib.NOTE_DE_CALCULS_HTML_FILE + "files"));
      f = "";
      settings_.stocke("CHEMIN_DU_FICHIER_HTML", null);
      settings_.stocke("NOM_DU_FICHIER_HTML", null);
      (new File(AlbeLib.NOTE_DE_CALCULS_HTML_FILE + "files")).mkdirs();
    }
    if (isChoosen("PRESENTATION")) {
      p = new HtmlPartie("PRESENTATION", "Pr&eacute;sentation", 1, WHtmlContent.createContentFor(
          "PRESENTATION", this, project_, locSttings));
      doc.addPartie(p);
    }
    if (isChoosen("RAPPELHYPOTHESES")) {
      p = new HtmlPartie("RAPPELHYPOTHESES", "Rappel des hypoth&egrave;ses", 1, WHtmlContent
          .createContentFor("RAPPELHYPOTHESES", this, project_, locSttings));
      if (isChoosen("RAPPELHYPOTHESES_OUVRAGE")) {
        sp = new HtmlPartie("RAPPELHYPOTHESES_OUVRAGE", "Ouvrage", 2, WHtmlContent
            .createContentFor("RAPPELHYPOTHESES_OUVRAGE", this, project_, locSttings));
        p.addSousPartie(sp);
      }
      if (isChoosen("RAPPELHYPOTHESES_SOL")) {
        sp = new HtmlPartie("RAPPELHYPOTHESES_SOL", "Couches de sol", 2, WHtmlContent
            .createContentFor("RAPPELHYPOTHESES_SOL", this, project_, locSttings));
        p.addSousPartie(sp);
      }
      if (isChoosen("RAPPELHYPOTHESES_ACTIONSOUVRAGE")) {
        sp = new HtmlPartie("RAPPELHYPOTHESES_ACTIONSOUVRAGE", "Actions sur l'ouvrage", 2,
            WHtmlContent.createContentFor("RAPPELHYPOTHESES_ACTIONSOUVRAGE", this, project_,
                locSttings));
        p.addSousPartie(sp);
      }
      if (isChoosen("RAPPELHYPOTHESES_COEFFPARTIELS")) {
        sp = new HtmlPartie("RAPPELHYPOTHESES_COEFFPARTIELS", "Coefficients partiels", 2,
            WHtmlContent.createContentFor("RAPPELHYPOTHESES_COEFFPARTIELS", this, project_,
                locSttings));
        if (isChoosen("RAPPELHYPOTHESES_COEFFPARTIELS_MATERIAUXETSOL")) {
          ssp = new HtmlPartie("RAPPELHYPOTHESES_COEFFPARTIELS_MATERIAUXETSOL", "Mat&eacute;riaux et sol",
              3, WHtmlContent.createContentFor("RAPPELHYPOTHESES_COEFFPARTIELS_MATERIAUXETSOL",
                  this, project_, locSttings));
          sp.addSousPartie(ssp);
        }
        if (isChoosen("RAPPELHYPOTHESES_COEFFPARTIELS_MODELE")) {
          ssp = new HtmlPartie("RAPPELHYPOTHESES_COEFFPARTIELS_MODELE", "Coefficients de mod&egrave;le",
              3, WHtmlContent.createContentFor("RAPPELHYPOTHESES_COEFFPARTIELS_MODELE", this,
                  project_, locSttings));
          sp.addSousPartie(ssp);
        }
        if (isChoosen("RAPPELHYPOTHESES_COEFFPARTIELS_CRITERESDIM")) {
          ssp = new HtmlPartie("RAPPELHYPOTHESES_COEFFPARTIELS_CRITERESDIM",
              "Crit&egrave;res de dimensionnement", 3, WHtmlContent.createContentFor(
                  "RAPPELHYPOTHESES_COEFFPARTIELS_CRITERESDIM", this, project_, locSttings));
          sp.addSousPartie(ssp);
        }
        p.addSousPartie(sp);
      }
      if (isChoosen("RAPPELHYPOTHESES_LANCEMENTCALCULS")) {
        sp = new HtmlPartie("RAPPELHYPOTHESES_LANCEMENTCALCULS", "Lancement des calculs", 2,
            WHtmlContent.createContentFor("RAPPELHYPOTHESES_LANCEMENTCALCULS", this, project_,
                locSttings));
        p.addSousPartie(sp);
      }
      doc.addPartie(p);
    }
    if (isChoosen("RESULTATS")) {
      p = new HtmlPartie("RESULTATS", "R&eacute;sultats des calculs", 1, WHtmlContent.createContentFor(
          "RESULTATS", this, project_, locSttings));
      if (isChoosen("RESULTATS_TABLEAUXRECAPITULATIFS")) {
        sp = new HtmlPartie("RESULTATS_TABLEAUXRECAPITULATIFS", "Tableaux r&eacute;capitulatifs", 2,
            WHtmlContent.createContentFor("RESULTATS_TABLEAUXRECAPITULATIFS", this, project_,
                locSttings));
        p.addSousPartie(sp);
      }
      if (isChoosen("RESULTATS_DETAILLES")) {

        // Si lancement manuel
        if (isChoosen("COMBINAISONMANUELLE")) {

          sp = new HtmlPartie("RESULTATS_DETAILLES_COMBIMANUELLE", "R&eacute;sultats d&eacute;taill&eacute;s", 2,
              WHtmlContent.createContentResultatsDetailles("RESULTATS_DETAILLES_COMBIMANUELLE",
                  this, project_, locSttings));
        }
        // Si lancement automatique
        else {
          sp = new HtmlPartie("RESULTATS_DETAILLES", "R&eacute;sultats d&eacute;taill&eacute;s", 2, WHtmlContent
              .createContentFor("RESULTATS_DETAILLES", this, project_, locSttings));

          // R�sultats ELU fondamental
          for (int i = 1; i <= 16; i++) {
            if (isChoosen("RESULTATS_DETAILLES_ELUFONDA" + i)) {
              ssp = new HtmlPartie("RESULTATS_DETAILLES_ELUFONDA" + i,
                  "ELU fondamental : combinaison " + i, 3, WHtmlContent
                      .createContentResultatsDetailles("RESULTATS_DETAILLES_ELUFONDA" + i, this,
                          project_, locSttings));
              sp.addSousPartie(ssp);
            }
          }
          // R�sultats ELU accidentel
          for (int i = 1; i <= 8; i++) {
            if (isChoosen("RESULTATS_DETAILLES_ELUACC" + i)) {
              ssp = new HtmlPartie("RESULTATS_DETAILLES_ELUACC" + i,
                  "ELU accidentel : combinaison " + i, 3, WHtmlContent
                      .createContentResultatsDetailles("RESULTATS_DETAILLES_ELUACC" + i, this,
                          project_, locSttings));
              sp.addSousPartie(ssp);
            }
          }
          // R�sultats ELS rare
          for (int i = 1; i <= 8; i++) {
            if (isChoosen("RESULTATS_DETAILLES_ELSRARE" + i)) {
              ssp = new HtmlPartie("RESULTATS_DETAILLES_ELSRARE" + i,
                  "ELS rare : combinaison " + i, 3, WHtmlContent.createContentResultatsDetailles(
                      "RESULTATS_DETAILLES_ELSRARE" + i, this, project_, locSttings));
              sp.addSousPartie(ssp);
            }
          }
          // R�sultats ELS fr�quent
          for (int i = 1; i <= 8; i++) {
            if (isChoosen("RESULTATS_DETAILLES_ELSFREQ" + i)) {
              ssp = new HtmlPartie("RESULTATS_DETAILLES_ELSFREQ" + i, "ELS fr&eacute;quent : combinaison "
                  + i, 3, WHtmlContent.createContentResultatsDetailles(
                  "RESULTATS_DETAILLES_ELSFREQ" + i, this, project_, locSttings));
              sp.addSousPartie(ssp);
            }
          }
        }
        p.addSousPartie(sp);
      }
      doc.addPartie(p);
    }
    String header = "";
    header += "<html>" + CtuluLibString.LINE_SEP + "";
    header += "<head>" + "<meta http-equiv=\"Content-Type\" content=\"text/html;"
        + "charset=iso-8859-1\"><title>Note de calculs</title>" + "</head>"
        + CtuluLibString.LINE_SEP + "";
    header += "<body>" + CtuluLibString.LINE_SEP + "";
    if (isChoosen("PAGEDEGARDE")) {

      if (isChoosen("PAGEDEGARDE_LOGOCETMEF")) {
        String chemRel = "";
        String chemAbs = "";
        if (_mode.equals("todisk")) {
          chemRel = name + "files" + AlbeLib.FILE_SEPARATOR + "logo-perso.gif";
          chemAbs = f + "files" + AlbeLib.FILE_SEPARATOR + "logo-perso.gif";
        } else if (_mode.equals("toscreen")) {
          chemRel = AlbeLib.NOTE_DE_CALCULS_HTML_FILE + "files" + AlbeLib.FILE_SEPARATOR
              + "logo-perso.gif";
          chemAbs = chemRel;
        }
        BuPicture picture = new BuPicture(AlbeResource.ALBE.getIcon("logo-perso"));
        FileOutputStream out = null;
        try {
          out = new FileOutputStream(chemAbs);
          final AcmeGifEncoder age = new AcmeGifEncoder(picture.getImage(), out);
          age.encode();
        }
        catch (final Exception _e1) {} finally {
          if (out != null)
            try {
              out.close();
            }
            catch (IOException _io) {}
        }
        header += "<DIV align = right><img hspace = 25 src=\"" + chemRel + "\"></DIV>"
            + CtuluLibString.LINE_SEP;

      }

      header += "<table border=\"0\" cellspacing=\"10\" cellpadding=\"5\" width = \"80%\"><tr>";
      header += "<td bgcolor = \"#FFFFFF\" align=\"center\" width=\"250\">";
      if (isChoosen("PAGEDEGARDE_LOGOALBE")) {

        String chemRel = "";
        String chemAbs = "";
        if (_mode.equals("todisk")) {
          chemRel = name + "files" + AlbeLib.FILE_SEPARATOR + "albe-logo.gif";
          chemAbs = f + "files" + AlbeLib.FILE_SEPARATOR + "albe-logo.gif";
        } else if (_mode.equals("toscreen")) {
          chemRel = AlbeLib.NOTE_DE_CALCULS_HTML_FILE + "files" + AlbeLib.FILE_SEPARATOR
              + "albe-logo.gif";
          chemAbs = chemRel;
        }

        BuPicture picture = new BuPicture(AlbeResource.ALBE.getIcon("albe-logo"));
        FileOutputStream out = null;
        try {
          out = new FileOutputStream(chemAbs);
          final AcmeGifEncoder age = new AcmeGifEncoder(picture.getImage(), out);
          age.encode();
        }
        catch (final Exception _e) {} finally {
          if (out != null)
            try {
              out.close();
            }
            catch (IOException _io) {}
        }
        header += "<DIV><img hspace = 15 align = \"absbottom\" src=\"" + chemRel + "\"/>";

      }
      if (isChoosen("PAGEDEGARDE_LOGICIEL")) {
        String logiciel = settings_.lit("PAGEDEGARDE_LOGICIEL").toString();
        header += "<FONT SIZE = 6 COLOR = \"#3333CC\">" + logiciel + "</FONT></DIV>"
            + CtuluLibString.LINE_SEP;
      }
      header += "</td>";
      if (isChoosen("PAGEDEGARDE_TITRE")) {
        String titre = settings_.lit("PAGEDEGARDE_TITRE").toString();
        header += "<td align = \"center\" bgcolor = \"#6699FF\"><FONT SIZE = 6 COLOR = \"#FFFFFF\">"
            + titre + "</FONT></td>" + CtuluLibString.LINE_SEP + "";;
      }
      header += "</tr></table><br><br>";
      if (isChoosen("PAGEDEGARDE_DATE")) {
        String date = settings_.lit("PAGEDEGARDE_DATE").toString();
        header += "<br><DIV align = left><i>" + date + "</i></DIV><br>" + CtuluLibString.LINE_SEP;
      }

      if (isChoosen("PAGEDEGARDE_SOMMAIRE")) {
        sommaire = doc.summarize();
      }
    }
    String end = "";
    end += "<br><i>g&eacute;n&eacute;r&eacute;e par le logiciel <b>" + AlbeImplementation.SOFTWARE_TITLE + "</b></i>"
        + CtuluLibString.LINE_SEP;
    end += "</body>" + CtuluLibString.LINE_SEP + "";
    end += "</html>" + CtuluLibString.LINE_SEP + "";
    content = doc.getContent();
    html = header + sommaire + content + end;

    if (_mode.equals("toscreen")) {
      File file = null;
      try {
        file = new File(AlbeLib.NOTE_DE_CALCULS_HTML_FILE + ".html");
        final FileWriter _fw = new FileWriter(file);
        _fw.write(html, 0, html.length());
        _fw.flush();
        _fw.close();
      }
      catch (final IOException _e1) {
        System.err.println("Impossible d'�crire le fichier sur le disque.");
        return false;
      }

      if (file != null) {
        appli_.getImplementation().displayURL(file.getAbsolutePath());

        // -- L'onglet affichage devient accessible et est affich� --//
        // classeur_.setEnabledAt(1, true);
        // classeur_.setSelectedIndex(1);

        // -- affichage dans le EkitCore --//
        // try {
        // editeur_.loadDocument(file);
        // editeur_.setCaretPosition(0);
        // }
        // catch (IOException e) {
        // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        // catch (BadLocationException e) {
        // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        // editeur_.setDocumentText(html);

        // -- ajout de la menuBarre d'ekit pour manipuler le fichier html --//
        // this.setJMenuBar(editeur_.getMenuBar());
        // this.revalidate();

      }
    }

    else if (_mode.equals("todisk")) {
      if (f != null) {
        try {
          final FileWriter fw = new FileWriter(f);
          fw.write(html, 0, html.length());
          // -- On enregistre le contenu de l'editeur pour prendre en compte les
          // modifications de l'utilisateur
          // fw.write(editeur_.getDocumentText(), 0, html.length());

          fw.flush();
          fw.close();
        }
        catch (final IOException _e1) {
          System.err.println("Impossible d'�crire le fichier sur le disque.");
          return false;
        }
      } else {
        System.err.println("Impossible d'afficher la note de calculs");
        return false;
      }
    }

    else {
      System.err.println("mode de g�n�ration de la note de calcul incorrect");
      return false;
    }

    return true;
  }

  /**
   * classe permettant de stocker toutes les informations de pr�f�rences
   * concernant la note de calculs.
   */
  public static class Settings {
    Hashtable settings_;

    /**
     * construit un objet de stockage des pr�f�rences utilisateur pour la note
     * de calculs.
     */
    public Settings() {
      settings_ = new Hashtable(4);
    }

    private Enumeration keys() {
      return settings_.keys();
    }

    public void stocke(final String _cle, final Object _valeur) {
      try {
        if (settings_.get(_cle) != null) {
          settings_.remove(_cle);
        }
        settings_.put(_cle, _valeur);
      }
      catch (final NullPointerException _e1) {}
    }

    public Object lit(final String _cle) {
      return settings_.get(_cle);
    }

    public void add(final Settings _toAdd) {
      try {
        for (final Enumeration _o = _toAdd.keys(); _o.hasMoreElements();) {
          final String _tmp = (String) _o.nextElement();
          stocke(_tmp, _toAdd.lit(_tmp));
        }
      }
      catch (final UnsupportedOperationException _e1) {
        System.err.println("unsupported operation exception");
      }
      catch (final ClassCastException _e2) {
        System.err.println("class cast exception");
      }
      catch (final IllegalArgumentException _e3) {
        System.err.println("illegal argument exception");
      }
      catch (final NullPointerException _e4) {
        System.err.println("null pointer exception");
      }
    }

    public String toString() {
      String txt = "";
      String tmp;
      Object o;
      for (final Enumeration _ek = settings_.keys(); _ek.hasMoreElements();) {
        o = _ek.nextElement();
        tmp = o + " = " + settings_.get(o);
        txt += tmp + "\n";
      }
      return txt;
    }
  }

  class HtmlPartie {
    Settings htmlSettings_;

    String partie_;

    String titre_;

    Vector sousparties_;

    int niveau_;

    WHtmlContent contentgenerator_;

    public HtmlPartie(final String _partie, final String _titre, final int _niveau,
        final WHtmlContent _contentgenerator) {
      htmlSettings_ = getSettings();
      contentgenerator_ = _contentgenerator;
      partie_ = _partie;
      titre_ = _titre;
      niveau_ = _niveau;
      sousparties_ = new Vector();
    }

    public String getPartie() {
      return partie_;
    }

    public Vector getSousParties() {
      return sousparties_;
    }

    public String getTitre() {
      return titre_;
    }

    public String summarize() {
      String txt = "";
      HtmlPartie node = null;
      if (sousparties_.size() > 0) {
        if (niveau_ != 0) {
          txt += "<ul>" + CtuluLibString.LINE_SEP;
        }
        for (int i = 0; i < sousparties_.size(); i++) {
          node = (HtmlPartie) sousparties_.get(i);
          txt += "<a class=\"partie" + niveau_ + "\" href=\"#" + node.getPartie().toLowerCase()
              + "\">" + (i + 1) + ". " + node.getTitre() + "</a><br>" + CtuluLibString.LINE_SEP;
          txt += node.summarize();
        }
        if (niveau_ != 0) {
          txt += "</ul>" + CtuluLibString.LINE_SEP;
        }
      }
      return txt;
    }

    public void addSousPartie(final HtmlPartie _souspartie) {
      sousparties_.add(_souspartie);
    }

    public String getContent() {
      String txt = "";
      String color = "";
      HtmlPartie node = null;
      txt += contentgenerator_.getContent();
      if (sousparties_.size() > 0) {
        if (niveau_ == 1)
          color = "#0099FF";
        else if (niveau_ == 2)
          color = "#009999";
        for (int i = 0; i < sousparties_.size(); i++) {
          node = (HtmlPartie) sousparties_.get(i);
          txt += "<h" + (niveau_ + 2) + "><FONT COLOR = \"" + color + "\" <a class=\"partie"
              + niveau_ + "\" name=\"" + node.getPartie().toLowerCase() + "\">" + (i + 1) + ". "
              + node.getTitre() + "</a></FONT></h" + (niveau_ + 2) + ">" + CtuluLibString.LINE_SEP;
          txt += node.getContent();
        }
      }
      return txt;
    }
  }

  class HtmlDoc {
    Vector parties_;

    int tdmprofondeur_ = 3;

    public HtmlDoc() {
      parties_ = new Vector();
    }

    public void setTdmProfondeur(final int _n) {
      if ((_n > 0) && (_n <= 3)) {
        tdmprofondeur_ = _n;
      }
    }

    public int getTdmProfondeur() {
      return tdmprofondeur_;
    }

    public void addPartie(final HtmlPartie _p) {
      parties_.add(_p);
    }

    public String summarize() {
      String html = "";
      HtmlPartie node = null;
      html = "<h2><FONT COLOR = \"#3333CC\"> Sommaire</FONT></h2>" + CtuluLibString.LINE_SEP;
      if (parties_.size() > 0) {
        for (int i = 0; i < parties_.size(); i++) {
          node = (HtmlPartie) parties_.get(i);
          html += "<a class=\"partie1\" href=\"#" + node.getPartie().toLowerCase() + "\"><br>"
              + (i + 1) + ". " + node.getTitre() + "</a><br>" + CtuluLibString.LINE_SEP;
          html += node.summarize();
        }
      }
      return html;
    }

    public String getContent() {
      String txt = "";
      HtmlPartie node = null;
      if (parties_.size() > 0) {
        for (int i = 0; i < parties_.size(); i++) {
          node = (HtmlPartie) parties_.get(i);
          txt += "<br><h2><FONT COLOR = \"#3333CC\"> <a class=\"partie1\" name=\""
              + node.getPartie().toLowerCase() + "\">" + (i + 1) + ". " + node.getTitre()
              + "</a></FONT></h2>" + CtuluLibString.LINE_SEP;
          txt += node.getContent();
        }
      }
      return txt;
    }
  }

  /**
   * 
   */

  public static String getFileChoosenByUser(final Component _parent, final String _title,
      final String _approvebutton, final String _currentpath) {
    final JFileChooser chooser = new JFileChooser();
    if (_approvebutton != null) {
      chooser.setApproveButtonText(_approvebutton);
    }
    if (_title != null) {
      chooser.setDialogTitle(_title);
    }
    if (_currentpath != null) {
      chooser.setCurrentDirectory(new File(_currentpath));
    }
    if (_currentpath != null) {
      chooser.setSelectedFile(new File(_currentpath));
    }
    final int returnVal = chooser.showOpenDialog(_parent);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      return chooser.getSelectedFile().getPath();
    }
    return null;
  }

  /**
   * 
   */

  public void actionPerformed(ActionEvent _evt) {
    if (_evt.getSource() == fermerButton_)
      dispose();
    else if (_evt.getSource() == genererButton_) {

      // Si les param�tres ont �t� modifi�s : message
      if (getImplementation().paramsModified())
        AlbeLib.dialogWarning(appli_, AlbeMsg.WAR007);

      final Settings config = listconfig_.getConfig();
      if (config != null) {
        settings_.add(config);
        if (generer("toscreen")) {} else {
          AlbeLib.dialogError(getApplication(), AlbeMsg.ERR010);
        }
      }
    }

    else if (_evt.getSource() == exporterButton_) {
      // Si les param�tres ont �t� modifi�s : message
      if (getImplementation().paramsModified())
        AlbeLib.dialogWarning(appli_, AlbeMsg.WAR008);

      /** Cr�ation d'une boite de dialogue pour l'enregistrement de fichier/ */
      final JFileChooser chooser = new JFileChooser();
      chooser.setApproveButtonText("Exporter le rapport HTML");
      chooser.setDialogTitle("Exporter au format HTML");
      String currentPath = AlbeLib.USER_HOME + "note.html";
      chooser.setCurrentDirectory(new File(currentPath));
      chooser.setSelectedFile(new File(currentPath));

      final int returnVal = chooser.showOpenDialog(this);
      File file = null;
      String f = null;
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        file = chooser.getSelectedFile();
        f = file.getPath();
      }

      if (f != null) {
        setFichier(f, chooser.getName(file));
        final Settings _config = listconfig_.getConfig();
        if (_config != null) {
          settings_.add(_config);
          if (generer("todisk")) {
            AlbeLib.dialogMessage(getApplication(), AlbeLib.replaceKeyWordBy("imgdir", f + "files"
                + AlbeLib.FILE_SEPARATOR, AlbeLib.replaceKeyWordBy("file", f, AlbeMsg.DLG007)));
          } else {
            AlbeLib.dialogError(getApplication(), AlbeMsg.ERR011);
          }
        }
      }
    }
  }

}
