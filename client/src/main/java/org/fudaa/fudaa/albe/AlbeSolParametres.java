package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;

import org.fudaa.dodico.corba.albe.SPointSol;
import org.fudaa.dodico.corba.albe.SSol;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;

/**
 * Description de l'onglet des param�tres de sol.
 * 
 * @author Sabrina Delattre
 */

public class AlbeSolParametres extends AlbeAbstractOnglet {

  String[] courbesList_ = { "Courbe manuelle", "Courbe �lasto-plastique pure", "Courbe type SETRA",
      "Courbe du fascicule 62 titre V sollicitation de courte dur�e",
      "Courbe du fascicule 62 titre V sollicitation de tr�s courte dur�e",
      "Courbe du fascicule 62 titre V sollicitation de longue dur�e" };

  JComboBox modeleCombo_;

  BuPanel courbePanel_;

  BuButton aideButton_;

  AlbeSolParametresCourbeManuelle courbeManuelle_;

  AlbeSolParametresCourbeElastoPlastique courbeElastoPlatique_;

  AlbeSolParametresCourbeSetra courbeSetra_;

  AlbeSolParametresCourbeTVCourteDuree courbeCourteDuree_;

  AlbeSolParametresCourbeTVTresCourteDuree courbeTresCourteDuree_;

  AlbeSolParametresCourbeTVLongueDuree courbeLongueDuree_;

  AlbeFilleParametres fp_;

  /**
   * nombre de couches de sol.
   */
  int nbCouches_;

  /**
   * Constructeur.
   * 
   * @param _fp
   */

  public AlbeSolParametres(final AlbeFilleParametres _fp) {

    super(_fp.getApplication(), _fp, AlbeMsg.URL002);

    fp_ = _fp;
    nbCouches_ = 1;
    this.setLayout(new BuVerticalLayout());

    BuPanel p = new BuPanel(new BuVerticalLayout());

    /** Panneau contenant la remarque. */
    BuPanel remarquePanel = AlbeLib.titledPanel("Remarque", AlbeLib.ETCHED_BORDER);
    remarquePanel.setLayout(new BuVerticalLayout());

    BuLabelMultiLine remarqueLabel = AlbeLib.labelMultiLine("Les courbes �lasto-plastiques pures "
        + "et les courbes du SETRA ont �t� remplac�es par les courbes du fascicule 62 titre V .");
    remarquePanel.add(remarqueLabel);

    /** Panneau nord contenant le panneau 'Remarque' et le bouton 'aide. */
    BuPanel panelNord = new BuPanel(new BorderLayout());
    BuPanel aidePanel = new BuPanel(new FlowLayout());
    aideButton_ = AlbeLib.aideButton();
    aideButton_.addActionListener(this);
    aidePanel.add(aideButton_);
    panelNord.add(remarquePanel, BorderLayout.CENTER);
    panelNord.add(aidePanel, BorderLayout.EAST);

    /** Panneau contenant la boite combo. */
    BuPanel comboPanel = new BuPanel();
    comboPanel.setLayout(new FlowLayout());
    JLabel modeleLabel = new JLabel("Mod�le de comportement du sol");
    modeleCombo_ = new JComboBox(courbesList_);
    modeleCombo_.addActionListener(this);
    modeleCombo_.addFocusListener(this);

    comboPanel.add(modeleLabel);
    comboPanel.add(modeleCombo_);

    p.add(panelNord);
    p.add(comboPanel);
    this.add(p, BorderLayout.NORTH);

    courbeManuelle_ = new AlbeSolParametresCourbeManuelle(this);
    courbeElastoPlatique_ = new AlbeSolParametresCourbeElastoPlastique(this);
    courbeSetra_ = new AlbeSolParametresCourbeSetra(this);
    courbeCourteDuree_ = new AlbeSolParametresCourbeTVCourteDuree(this);
    courbeTresCourteDuree_ = new AlbeSolParametresCourbeTVTresCourteDuree(this);
    courbeLongueDuree_ = new AlbeSolParametresCourbeTVLongueDuree(this);

    // CardLayout
    CardLayout c = new CardLayout();
    courbePanel_ = new BuPanel(c);
    courbePanel_.add("panCourbeMan", courbeManuelle_);
    courbePanel_.add("panCourbeElasto", courbeElastoPlatique_);
    courbePanel_.add("panCourbeSetra", courbeSetra_);
    courbePanel_.add("panCourbeVCourteDuree", courbeCourteDuree_);
    courbePanel_.add("panCourbeVTresCourteDuree", courbeTresCourteDuree_);
    courbePanel_.add("panCourbeVLongueDuree", courbeLongueDuree_);

    BuPanel tempPanel = new BuPanel();
    tempPanel.setLayout(new BorderLayout());
    this.add(courbePanel_);

  }// Fin constructeur

  /**
   * retourne la somme des �paisseurs des couches de sol pour la courbe
   * affich�e.
   */
  public double getSommeEpaisseurs() {

    AlbeAbstractSolCourbe c = getCourbe(modeleCombo_.getSelectedIndex());

    return c.getSommeEpaisseurs();

  }

  /**
   * retourne la valeur de l'�paisseur de la couche 1 de la courbe affich�e.
   */

  public double getEpaisseurCouche1() {

    AlbeAbstractSolCourbe c = getCourbe(modeleCombo_.getSelectedIndex());

    return AlbeLib.textFieldDoubleValue(c.epaisseur_[0]);

  }

  /**
   * genere les tableaux pour chaque couche pour le mod�le de comportement du
   * sol s�lectionn� (si les champs ne sont pas tous nuls).
   */
  public void genereTableau() {

    if (modeleCombo_.getSelectedIndex() == 1) {
      for (int i = 0; i < AlbeLib.NBMAX_COUCHES; i++) {
        if (!courbeElastoPlatique_.champsTousNuls(i))
          courbeElastoPlatique_.genereTableauCourbeElastoPlastique(i);
      }
    } else if (modeleCombo_.getSelectedIndex() == 2) {
      for (int i = 0; i < AlbeLib.NBMAX_COUCHES; i++) {
        if (!courbeSetra_.champsTousNuls(i))
          courbeSetra_.genereTableauCourbeSetra(i);
      }
    } else if (modeleCombo_.getSelectedIndex() == 3) {
      for (int i = 0; i < AlbeLib.NBMAX_COUCHES; i++) {
        if (!courbeCourteDuree_.champsTousNuls(i))
          courbeCourteDuree_.genereTableauxCourteEtLongueDuree(AlbeLib.TABLEAU_COURTE_DUREE, i);
      }
    } else if (modeleCombo_.getSelectedIndex() == 4) {
      for (int i = 0; i < AlbeLib.NBMAX_COUCHES; i++) {
        if (!courbeTresCourteDuree_.champsTousNuls(i))
          courbeTresCourteDuree_.genereTableauTresCourteDuree(i);
      }
    } else if (modeleCombo_.getSelectedIndex() == 5) {
      for (int i = 0; i < AlbeLib.NBMAX_COUCHES; i++) {
        if (!courbeLongueDuree_.champsTousNuls(i))
          courbeLongueDuree_.genereTableauxCourteEtLongueDuree(AlbeLib.TABLEAU_LONGUE_DUREE, i);
      }
    }
  }

  /**
   * 
   */
  protected String getScriptCourbeSol(int _numCouche, double _minX, double _maxX, double _pasX,
      double _minY, double _maxY, double _pasY) {
    final StringBuffer s = new StringBuffer();

    s.append("graphe \n{\n");
    s.append("  titre \"Comportement du sol\"\n");
    s.append("  sous-titre \"Courbe d�placement - pression (couche n�" + _numCouche + ")\"\n");
    s.append("  legende oui\n");
    s.append("  animation non\n");
    s.append("  marges\n{\n");
    s.append("    gauche 80\n");
    s.append("    droite 120\n");
    s.append("    haut 50\n");
    s.append("    bas 30\n");
    s.append("  }\n");
    s.append("  \n");
    s.append("  axe\n{\n");
    s.append("    titre \"d�placement\"\n");
    s.append("    unite \"cm\"\n");
    s.append("    orientation horizontal\n");
    s.append("    graduations oui\n");
    s.append("    minimum " + _minX + "\n");
    s.append("    maximum " + _maxX + "\n");
    s.append("    pas " + _pasX + "\n");;
    s.append("  }\n");
    s.append("  axe\n{\n");
    s.append("    titre \"pression\"\n");
    s.append("    unite \"kPa\"\n");
    s.append("    orientation vertical\n");
    s.append("    graduations oui\n");
    s.append("    minimum " + _minY + "\n");
    s.append("    maximum " + _maxY + "\n");
    s.append("    pas " + _pasY + "\n");
    s.append("  }\n");
    s.append("  courbe\n{\n");
    s.append("    valeurs\n{\n");

    return s.toString();
  }

  /**
   * retourne la courbe associ�e � l'entier _i (0 = courbe manuelle, 1 = courbe
   * �lasto-platique pure...).
   */
  public AlbeAbstractSolCourbe getCourbe(int _i) {

    AlbeAbstractSolCourbe c = null;

    switch (_i) {
    case 0: {
      c = courbeManuelle_;
      break;
    }
    case 1: {
      c = courbeElastoPlatique_;
      break;
    }
    case 2: {
      c = courbeSetra_;
      break;
    }
    case 3: {
      c = courbeCourteDuree_;
      break;
    }
    case 4: {
      c = courbeTresCourteDuree_;
      break;
    }
    case 5: {
      c = courbeLongueDuree_;
      break;
    }
    }

    return c;
  }

  /**
   * r�cup�re la courbe qui est actuellement affich�e dans l'onglet Sol. (On
   * n'utilise pas modeleCombo_.getSelectedIndex() car cette m�thode est appel�e
   * lors d'un clic sur cette boite combo.)
   * 
   * @return i entier correspondant � cet courbe (0 : courbe manuelle, 1 :
   *         courbe �lasto-platique pure, 2 : Setra, 3 : fascicule courte dur�e,
   *         4 : fascicule tr�s courte dur�e, 5 : fascicule longue dur�e)
   */

  public int getIndexCourbeAffichee() {

    int i = -1;

    if (courbeManuelle_.isVisible())
      i = 0;
    else if (courbeElastoPlatique_.isVisible())
      i = 1;
    else if (courbeSetra_.isVisible())
      i = 2;
    else if (courbeCourteDuree_.isVisible())
      i = 3;
    else if (courbeTresCourteDuree_.isVisible())
      i = 4;
    else if (courbeLongueDuree_.isVisible())
      i = 5;

    return i;
  }

  /**
   * retourne une structure de donn�es contenant les informations du point _np
   * du tableau de la couche _nc. retourne null si le point n'existe pas dans le
   * tableau.
   * 
   * @param _np num�ro du point � retourner (commence � 1)
   * @param _nc num�ro de la couche concern�e (commence � 1)
   */

  public SPointSol getPoint(final int _np, final JTable _t) {
    SPointSol p = null;
    try {
      p = (SPointSol) AlbeLib.createIDLObject(SPointSol.class);
      try {
        p.deplacement = ((Double) _t.getValueAt(_np - 1, 1)).doubleValue();
      }
      catch (final Exception _e2) {
        p.deplacement = VALEUR_NULLE.value;
      }

      try {
        p.pression = ((Double) _t.getValueAt(_np - 1, 2)).doubleValue();
      }
      catch (final Exception _e3) {
        p.pression = VALEUR_NULLE.value;
      }

    }
    catch (final ArrayIndexOutOfBoundsException _e1) {
      System.err.println(AlbeMsg.CONS006);
    }

    return p;
  }

  /**
   * retourne une structure de donn�es contenant les informations de tous les
   * points de la couche _nc.
   */
  public SPointSol[] getPointsCouche(final JTable _t) {
    if (_t.isEditing())
      _t.getCellEditor().stopCellEditing();

    SPointSol[] p = null;
    final int nb = _t.getRowCount();
    try {
      p = new SPointSol[nb];
    }
    catch (final NegativeArraySizeException _e2) {
      System.err.println(AlbeMsg.CONS007);
    }
    for (int i = 0; i < nb; i++) {
      try {
        p[i] = getPoint(i + 1, _t);
      }
      catch (final NullPointerException _e1) {
        System.err.println(AlbeMsg.CONS008);
      }
      catch (final ArrayIndexOutOfBoundsException _e2) {
        System.err.println(AlbeMsg.CONS009);
        return null;
      }
    }

    return p;
  }

  /**
   * retourne une structure de donn�es contenant les informations des tableaux
   * de toutes les couches.
   */

  public SPointSol[][] getTableaux(int _nbPoints, final JTable[] _t) {
    SPointSol[][] p = null;
    try {
      p = new SPointSol[nbCouches_][_nbPoints];
    }
    catch (final NegativeArraySizeException _e2) {
      System.err.println(AlbeMsg.CONS015);
    }

    for (int i = 0; i < nbCouches_; i++) {
      try {
        p[i] = getPointsCouche(_t[i]);
      }
      catch (final NullPointerException _e1) {
        System.err.println(AlbeMsg.CONS016);
      }
      catch (final ArrayIndexOutOfBoundsException _e2) {
        System.err.println(AlbeMsg.CONS017);
        return null;
      }
    }

    return p;
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres (liste
   * de points pour une couche de sol).
   */
  public synchronized void setPointsCouche(final SPointSol[] _p, final int _nc, final JTable[] _t) {
    if (_t[_nc - 1].isEditing()) {
      _t[_nc - 1].getCellEditor().stopCellEditing();
    }
    int n = 0;
    try {
      n = _p.length;
      AlbeLib.majNbLignes(_t[_nc - 1], n);
      AlbeLib.initialiseColonne1JTable(_t[_nc - 1]);
      for (int i = 0; i < n; i++) {
        if (_p[i].deplacement != VALEUR_NULLE.value)
          _t[_nc - 1].setValueAt(new Double(_p[i].deplacement), i, 1);

        if (_p[i].pression != VALEUR_NULLE.value)
          _t[_nc - 1].setValueAt(new Double(_p[i].pression), i, 2);

      }
    }
    catch (final NullPointerException _e1) {
      System.err.println(AlbeMsg.CONS010);
    }
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres (liste
   * de tableaux de points).
   */
  public synchronized void setTableaux(final SPointSol[][] _p, final JTable[] _t) {
    int n = 0;
    try {
      n = _p.length;

      for (int i = 0; i < n; i++) {
        setPointsCouche(_p[i], i + 1, _t);
      }
    }
    catch (final NullPointerException _e1) {
      System.err.println(AlbeMsg.CONS018);
    }
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans
   * l'onglet 'Sol'.
   */

  public SSol getParametresSol() {
    final SSol p = (SSol) AlbeLib.createIDLObject(SSol.class);

    // Si courbe manuelle
    if (modeleCombo_.getSelectedIndex() == 0) {
      p.modeleComportementSol = AlbeLib.IDL_COURBE_MANUELLE;
      p.nombrePoints = courbeManuelle_.table_[0].getRowCount();
      p.points = getTableaux(p.nombrePoints, courbeManuelle_.table_);
      p.epaisseursCourbeManuelle = courbeManuelle_.getEpaisseursCourbeManuelle();
    }

    // Si courbe �lasto-platique
    else if (modeleCombo_.getSelectedIndex() == 1) {
      p.modeleComportementSol = AlbeLib.IDL_COURBE_ELASTO_PLASTIQUE_PURE;
      p.nombrePoints = 5;
      p.points = getTableaux(p.nombrePoints, courbeElastoPlatique_.table_);
      p.courbe = courbeElastoPlatique_.getParametresCourbe();
    }
    // Si courbe Setra
    else if (modeleCombo_.getSelectedIndex() == 2) {
      p.modeleComportementSol = AlbeLib.IDL_COURBE_SETRA;
      p.nombrePoints = 7;
      p.points = getTableaux(p.nombrePoints, courbeSetra_.table_);
      p.courbe = courbeSetra_.getParametresCourbe();
    }

    // Si courbe fascicule 62 titre V courte dur�e
    else if (modeleCombo_.getSelectedIndex() == 3) {
      p.modeleComportementSol = AlbeLib.IDL_COURBE_FASCICULE_COURTE_DUREE;
      p.nombrePoints = 5;
      p.points = getTableaux(p.nombrePoints, courbeCourteDuree_.table_);
      p.courbe = courbeCourteDuree_.getParametresCourbe();
    }

    // Si courbe fascicule 62 titre V tr�s courte dur�e
    else if (modeleCombo_.getSelectedIndex() == 4) {
      p.modeleComportementSol = AlbeLib.IDL_COURBE_FASCICULE_TRES_COURTE_DUREE;
      p.nombrePoints = 7;
      p.points = getTableaux(p.nombrePoints, courbeTresCourteDuree_.table_);
      p.courbe = courbeTresCourteDuree_.getParametresCourbe();
    }

    // Si courbe fascicule 62 titre V longue dur�e
    else if (modeleCombo_.getSelectedIndex() == 5) {
      p.modeleComportementSol = AlbeLib.IDL_COURBE_FASCICULE_LONGUE_DUREE;
      p.nombrePoints = 5;
      p.points = getTableaux(p.nombrePoints, courbeLongueDuree_.table_);
      p.courbe = courbeLongueDuree_.getParametresCourbe();
    }
    return p;
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres dans
   * l'onglet 'Sol'.
   * 
   * @param _p structure de donn�es contenant les param�tres (donn�es) �
   *          importer
   */
  public synchronized void setParametresSol(SSol _p) {

    if (_p == null) {
      _p = (SSol) AlbeLib.createIDLObject(SSol.class);
    }

    // Si courbe manuelle
    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_MANUELLE)) {
      modeleCombo_.setSelectedIndex(0);
      courbeManuelle_.nbPointsCombo_.setSelectedIndex(_p.nombrePoints - 2);
      setTableaux(_p.points, courbeManuelle_.table_);
      courbeManuelle_.setEpaisseursCourbeManuelle(_p.epaisseursCourbeManuelle);
    }

    // Si courbe �lasto-platique
    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_ELASTO_PLASTIQUE_PURE)) {
      modeleCombo_.setSelectedIndex(1);
      setTableaux(_p.points, courbeElastoPlatique_.table_);
      courbeElastoPlatique_.setParametresCourbe(_p.courbe, _p);
    }

    // Si courbe �lasto-platique
    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_SETRA)) {
      modeleCombo_.setSelectedIndex(2);
      setTableaux(_p.points, courbeSetra_.table_);
      courbeSetra_.setParametresCourbe(_p.courbe, _p);
    }

    // Si courbe fascicule 62 titre V courte dur�e
    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_COURTE_DUREE)) {
      modeleCombo_.setSelectedIndex(3);
      setTableaux(_p.points, courbeCourteDuree_.table_);
      courbeCourteDuree_.setParametresCourbe(_p.courbe, _p);
    }

    // Si courbe fascicule 62 titre V tr�s courte dur�e
    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_TRES_COURTE_DUREE)) {
      modeleCombo_.setSelectedIndex(4);
      setTableaux(_p.points, courbeTresCourteDuree_.table_);
      courbeTresCourteDuree_.setParametresCourbe(_p.courbe, _p);
    }

    // Si courbe fascicule 62 titre V longue dur�e
    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_LONGUE_DUREE)) {
      modeleCombo_.setSelectedIndex(5);
      setTableaux(_p.points, courbeLongueDuree_.table_);
      courbeLongueDuree_.setParametresCourbe(_p.courbe, _p);
    }
  }

  /**
   * 
   */
  public boolean pointModified(SPointSol _p, int _np, JTable _t) {
    SPointSol p2 = getPoint(_np, _t);
    if (_p.deplacement != p2.deplacement || _p.pression != p2.pression)
      return true;
    return false;
  }

  /**
   * 
   */
  public boolean pointsModified(SPointSol[] _p, JTable _t) {

    if (_t.isEditing())
      _t.getCellEditor().stopCellEditing();

    for (int i = 0; i < _t.getRowCount(); i++) {
      if (pointModified(_p[i], i + 1, _t))
        return true;
    }
    return false;
  }

  /**
   * 
   */
  public boolean tableauxModified(SPointSol[][] _p, final JTable[] _t) {

    for (int i = 0; i < nbCouches_; i++) {
      if (pointsModified(_p[i], _t[i]))
        return true;
    }
    return false;
  }

  /**
   * retourne vrai si la structure pass�e en param�tre est diff�rente de celle
   * actuelle.
   */
  public boolean paramsSolModified(SSol _p) {
    SSol p2 = getParametresSol();
    if (_p.modeleComportementSol != p2.modeleComportementSol)
      return true;

    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_MANUELLE))
      if (_p.nombrePoints != p2.nombrePoints || tableauxModified(_p.points, courbeManuelle_.table_)
          || courbeManuelle_.paramsCourbeManuelleModified(_p.epaisseursCourbeManuelle))
        return true;
    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_ELASTO_PLASTIQUE_PURE))
      if (courbeElastoPlatique_.paramsCourbeModified(_p.courbe, _p))
        return true;
    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_SETRA))
      if (courbeSetra_.paramsCourbeModified(_p.courbe, _p))
        return true;
    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_COURTE_DUREE))
      if (courbeCourteDuree_.paramsCourbeModified(_p.courbe, _p))
        return true;
    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_TRES_COURTE_DUREE))
      if (courbeTresCourteDuree_.paramsCourbeModified(_p.courbe, _p))
        return true;
    if (_p.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_LONGUE_DUREE))
      if (courbeLongueDuree_.paramsCourbeModified(_p.courbe, _p))
        return true;

    return false;
  }

  /**
   * 
   */

  public void actionPerformed(ActionEvent _evt) {

    if (_evt.getSource() == aideButton_)
      aide();

    else if (_evt.getSource() == modeleCombo_) {

      // indice de la courbe actuellement affich�e (avant d'appuyer sur la boite
      // combo)
      int indexAncienneCourbe = getIndexCourbeAffichee();

      // indice de la courbe � afficher
      int indexNouvelleCourbe = modeleCombo_.getSelectedIndex();

      // bool�en sp�cifiant si on peut afficher la nouvelle courbe s�lectionn�e
      boolean b = true;

      // courbe actuellement affich�e
      AlbeAbstractSolCourbeElastoSetraFascicule courbeAffichee = null;

      // nouvelle courbe � afficher
      String nouvelleCourbe = "";

      switch (indexNouvelleCourbe) {
      case 0: {
        nouvelleCourbe = "panCourbeMan";
        break;
      }
      case 1: {
        nouvelleCourbe = "panCourbeElasto";
        break;
      }
      case 2: {
        nouvelleCourbe = "panCourbeSetra";
        break;
      }
      case 3: {
        nouvelleCourbe = "panCourbeVCourteDuree";
        break;
      }
      case 4: {
        nouvelleCourbe = "panCourbeVTresCourteDuree";
        break;
      }
      case 5: {
        nouvelleCourbe = "panCourbeVLongueDuree";
        break;
      }
      }

      if (indexAncienneCourbe == 0 && indexNouvelleCourbe != 0) {

        if (courbeManuelle_.courbeCompletee()) {
          AlbeLib.dialogWarning(getApplication(), AlbeMsg.WAR005);
          modeleCombo_.setSelectedIndex(indexAncienneCourbe);
        } else {
          CardLayout c = (CardLayout) courbePanel_.getLayout();
          c.show(courbePanel_, nouvelleCourbe);
        }
      }

      else {

        switch (indexAncienneCourbe) {

        case 1: {
          courbeAffichee = courbeElastoPlatique_;
          break;
        }
        case 2: {
          courbeAffichee = courbeSetra_;
          break;
        }
        case 3: {
          courbeAffichee = courbeCourteDuree_;
          break;
        }
        case 4: {
          courbeAffichee = courbeTresCourteDuree_;
          break;
        }
        case 5: {
          courbeAffichee = courbeLongueDuree_;
          break;
        }

        }// Fin switch

        if (indexAncienneCourbe != indexNouvelleCourbe) {
          try {
            if (courbeAffichee.courbeCompletee()) {
              b = false;
              AlbeLib.dialogWarning(getApplication(), AlbeMsg.WAR005);
              modeleCombo_.setSelectedIndex(indexAncienneCourbe);
            }
          }
          catch (NullPointerException _e) {
            System.err.println("Pas de v�rification possible, aucune courbe actuellement affich�e");
          }

          if (b) {
            CardLayout c = (CardLayout) courbePanel_.getLayout();
            c.show(courbePanel_, nouvelleCourbe);
          }
        }

      }// Fin else
    }
  }

  /**
   * 
   */
  public void changeOnglet(final ChangeEvent _evt) {

    // On actualise le tableau (utile si le diam�tre du pieu a �t� modifi� entre
    // temps)
    genereTableau();

    /**
     * D'apr�s le nombre de couches de sol (onglet 'Param�tres g�om�triques'),
     * on rend les onglets concern�s non visibles.
     */
    AlbeLib.setTabbedPane(courbeManuelle_.tp_, nbCouches_ - 1);
    AlbeLib.setTabbedPane(courbeElastoPlatique_.tp_, nbCouches_ - 1);
    AlbeLib.setTabbedPane(courbeSetra_.tp_, nbCouches_ - 1);
    AlbeLib.setTabbedPane(courbeCourteDuree_.tp_, nbCouches_ - 1);
    AlbeLib.setTabbedPane(courbeTresCourteDuree_.tp_, nbCouches_ - 1);
    AlbeLib.setTabbedPane(courbeLongueDuree_.tp_, nbCouches_ - 1);

    /**
     * si l'onglet courant n'est pas actif (suite � une diminution de couches de
     * sol), on "s�lectionne" le premier onglet (couche n�1).
     */
    if (!courbeManuelle_.tp_.isEnabledAt(courbeManuelle_.tp_.getSelectedIndex()))
      courbeManuelle_.tp_.setSelectedIndex(0);
    if (!courbeElastoPlatique_.tp_.isEnabledAt(courbeElastoPlatique_.tp_.getSelectedIndex()))
      courbeElastoPlatique_.tp_.setSelectedIndex(0);
    if (!courbeSetra_.tp_.isEnabledAt(courbeSetra_.tp_.getSelectedIndex()))
      courbeSetra_.tp_.setSelectedIndex(0);
    if (!courbeCourteDuree_.tp_.isEnabledAt(courbeCourteDuree_.tp_.getSelectedIndex()))
      courbeCourteDuree_.tp_.setSelectedIndex(0);
    if (!courbeTresCourteDuree_.tp_.isEnabledAt(courbeTresCourteDuree_.tp_.getSelectedIndex()))
      courbeTresCourteDuree_.tp_.setSelectedIndex(0);
    if (!courbeLongueDuree_.tp_.isEnabledAt(courbeLongueDuree_.tp_.getSelectedIndex()))
      courbeLongueDuree_.tp_.setSelectedIndex(0);

    majMessages();

  }

  /**
   * 
   */
  public ValidationMessages validation() {
    final ValidationMessages vm = new ValidationMessages();

    if (modeleCombo_.getSelectedIndex() == 0)
      vm.add(courbeManuelle_.validation());

    else if (modeleCombo_.getSelectedIndex() == 1)
      vm.add(courbeElastoPlatique_.validation());

    else if (modeleCombo_.getSelectedIndex() == 2)
      vm.add(courbeSetra_.validation());

    else if (modeleCombo_.getSelectedIndex() == 3)
      vm.add(courbeCourteDuree_.validation());

    else if (modeleCombo_.getSelectedIndex() == 4)
      vm.add(courbeTresCourteDuree_.validation());

    else if (modeleCombo_.getSelectedIndex() == 5)
      vm.add(courbeLongueDuree_.validation());

    if (AlbeLib.textFieldDoubleValue(fp_.geo_.coteTete_) != VALEUR_NULLE.value
        && fp_.pieu_.getLongueurPieu() != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(fp_.geo_.coteDragage_) != VALEUR_NULLE.value
        && getSommeEpaisseurs() != VALEUR_NULLE.value
        && (AlbeLib.textFieldDoubleValue(fp_.geo_.coteTete_) - fp_.pieu_.getLongueurPieu()) < (AlbeLib
            .textFieldDoubleValue(fp_.geo_.coteDragage_) - getSommeEpaisseurs()))
      vm.add(AlbeMsg.VMSG011);

    return vm;
  }
}
