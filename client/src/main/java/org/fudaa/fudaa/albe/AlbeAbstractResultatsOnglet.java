/*
 * @creation 7 f�vr. 07
 * @modification $Date: 2007-05-11 09:00:04 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.event.ActionEvent;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;

/**
 * classe de base pour les onglets de r�sultats.
 * 
 * @version $Id: AlbeAbstractResultatsOnglet.java,v 1.3 2007/05/04 13:57:29
 *          deniger Exp $
 * @author Sabrina Delattre
 */
public class AlbeAbstractResultatsOnglet extends AlbeAbstractOnglet {

  BuButton imprimerButton_, exporterButton_;

  /**
   * constructeur.
   */
  public AlbeAbstractResultatsOnglet(final BuCommonInterface _appli,
      final AlbeFilleResultatsCombinaison _parent) {

    super(_appli, _parent, null);

  }

  /**
   * imprime la fen�tre.
   */
  protected void imprimer() {
    getImplementation().actionPerformed(new ActionEvent(this, 4, "IMPRIMER"));
  }

  /**
   * exporte les donn�es.
   */
  protected void exporter() {}

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {
    if (_evt.getSource() == imprimerButton_)
      imprimer();
    else if (_evt.getSource() == exporterButton_)
      exporter();
  }
}
