/*
 * @creation 7 f�vr. 07
 * @modification $Date: 2007-10-26 09:49:33 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import javax.swing.SwingConstants;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.SResultatsAlbe;
import org.fudaa.dodico.corba.albe.SResultatsCombinaison;

import com.memoire.bu.BuTextField;
import com.memoire.fu.FuLog;

/**
 * Librairie de m�thodes utiles pour l'exploitation des r�sultats de Albe.
 * 
 * @author Sabrina Delattre
 */
public final class AlbeRes {

  static int limiteFlexion = 21000000;

  /**
   * remplit un champ texte avec la valeur du double plac� en param�tre suivi de
   * l'unit�.
   */
  public static void setField(BuTextField _t, double _d, String _unite) {
    _t.setText(String.valueOf(_d) + " " + _unite);
  }

  /**
   * rend un champ texte non �ditable et d�finit l'alignement du texte au
   * centre.
   */
  public static void setTextField(BuTextField _t) {
    _t.setEditable(false);
    _t.setHorizontalAlignment(SwingConstants.CENTER);
  }

  /**
   * retourne le coefficient partiel sur la courbe de r�action du sol.
   * 
   * @param _params param�tres de Albe
   * @param _typeCombi type de la combinaison
   * @param _numCombi num�ro de la combinaison
   * @param _accostage (oui : accostage / non : amarrage)
   * @param _defense presence ou non d'une defense
   */
  public static double getCoefSol(final SParametresAlbe _params, final int _typeCombi,
      final int _numCombi, final boolean _accostage, final boolean _defense) {
    double d = 0;

    switch (_typeCombi) {
    case AlbeLib.COMBINAISON_MANUELLE: {
      d = _params.lancementCalculs.gammaMSol;
      break;
    }
    case AlbeLib.ELU_FONDA: {
      // Cas favorable
      if (_defense && ((1 <= _numCombi && _numCombi <= 4) || (9 <= _numCombi && _numCombi <= 12)))
        d = _params.coefficientsPartiels.coeffSol[0].favorable;
      else if (!_defense && _accostage
          && (_numCombi == 1 || _numCombi == 2 || _numCombi == 5 || _numCombi == 6))
        d = _params.coefficientsPartiels.coeffSol[0].favorable;
      else if (!_accostage && (_numCombi == 1 || _numCombi == 2))
        d = _params.coefficientsPartiels.coeffSol[0].favorable;
      // Cas d�favorable
      else
        d = _params.coefficientsPartiels.coeffSol[0].defavorable;
      break;
    }
    default: {
      // Coef sol = 1
      d = 1;
    }
    }
    return d;
  }

  /**
   * retourne le coefficient partiel sur la r�sistance de l'acier sous forme
   * d'une chaine de caract�res.
   * 
   * @param _params param�tres de Albe
   * @param _typeCombi type de la combinaison
   */
  public static String getCoefAcier(final SParametresAlbe _params, final int _typeCombi) {
    String s = "";
    switch (_typeCombi) {
    case AlbeLib.COMBINAISON_MANUELLE: {
      s = "" + _params.lancementCalculs.gammaFyAcier;
      break;
    }
    case AlbeLib.ELU_FONDA: {
      s = "" + _params.coefficientsPartiels.coeffAcier[0];
      break;
    }
    case AlbeLib.ELU_ACC: {
      s = "" + _params.coefficientsPartiels.coeffAcier[1];
      break;
    }
    case AlbeLib.ELS_RARE: {
      s = "" + _params.coefficientsPartiels.coeffAcier[2];
      break;
    }
    case AlbeLib.ELS_FREQ: {
      s = "/";
      break;
    }
    }
    return s;
  }

  /**
   * retourne le coefficient partiel sur la courbe de la d�fense.
   * 
   * @param _params param�tres de Albe
   * @param _typeCombi type de la combinaison
   * @param _numCombi num�ro de la combinaison
   */

  public static double getCoefDefense(final SParametresAlbe _params, final int _typeCombi,
      final int _numCombi) {
    double d = 0;
    switch (_typeCombi) {
    case AlbeLib.COMBINAISON_MANUELLE: {
      d = _params.lancementCalculs.gammaMDefense;
      break;
    }
    case AlbeLib.ELU_FONDA: {
      if (_numCombi == 1 || _numCombi == 2 || _numCombi == 5 || _numCombi == 6 || _numCombi == 9
          || _numCombi == 10 || _numCombi == 13 || _numCombi == 14)
        d = _params.coefficientsPartiels.coeffDefense[0].defavorable;
      else
        d = _params.coefficientsPartiels.coeffDefense[0].favorable;
      break;
    }
    case AlbeLib.ELU_ACC: {
      if (_numCombi == 1 || _numCombi == 2 || _numCombi == 5 || _numCombi == 6)
        d = _params.coefficientsPartiels.coeffDefense[1].defavorable;
      else
        d = _params.coefficientsPartiels.coeffDefense[1].favorable;
      break;
    }
    case AlbeLib.ELS_RARE: {
      if (_numCombi == 1 || _numCombi == 2 || _numCombi == 5 || _numCombi == 6)
        d = _params.coefficientsPartiels.coeffDefense[2].defavorable;
      else
        d = _params.coefficientsPartiels.coeffDefense[2].favorable;
      break;
    }
    case AlbeLib.ELS_FREQ: {
      if (_numCombi == 1 || _numCombi == 2 || _numCombi == 5 || _numCombi == 6)
        d = _params.coefficientsPartiels.coeffDefense[3].defavorable;
      else
        d = _params.coefficientsPartiels.coeffDefense[3].favorable;
      break;
    }
    }
    return d;
  }

  /**
   * retourne dans un tableau de String les valeurs de la mobilisation de la
   * but�e du sol (valeur calcul�e, coefficient saisi dans l'interface et
   * gamma). retourne "/" si il n'y a pas de r�sultats.
   */
  public static String[] getMobilisation(final SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, final int _typeCombi, final int _numCombi) {
    String[] s = new String[3];
    double coef = getCoefMobilisation(_params, _typeCombi);
    double g = calculFacteurDimButee(_params, _resCombi, _typeCombi, _numCombi);
    double val = AlbeLib.arronditDouble(g * coef);
    double gamma = AlbeLib.arronditDouble(g);

    if (_typeCombi == AlbeLib.ELS_RARE) {
      for (int i = 0; i < 3; i++) {
        s[i] = "/";
      }
    } else {
      s[0] = "" + val;
      s[1] = "" + coef;
      s[2] = "" + gamma;
    }

    return s;
  }

  /**
   * retourne le coefficient de mobilisation de la but�e du sol sous forme d'un
   * double.
   */
  public static double getCoefMobilisation(final SParametresAlbe _params, final int _typeCombi) {
    double d = 0;
    switch (_typeCombi) {
    case AlbeLib.COMBINAISON_MANUELLE: {
      d = _params.lancementCalculs.gammaDButee;
      break;
    }
    case AlbeLib.ELU_FONDA: {
      d = _params.coefficientsPartiels.coeffModele[0].mobilisationButeeSol;
      break;
    }
    case AlbeLib.ELU_ACC: {
      d = _params.coefficientsPartiels.coeffModele[1].mobilisationButeeSol;
      break;
    }
    case AlbeLib.ELS_FREQ: {
      d = _params.coefficientsPartiels.coeffModele[3].mobilisationButeeSol;
      break;
    }
    }
    return d;
  }

  /**
   * retourne dans un tableau de String les valeurs de r�sistance en flexion du
   * pieu (valeur calcul�e, coefficient saisi dans l'interface et gamma).
   * retourne "/" si il n'y a pas de r�sultats.
   */

  public static String[] getResistancePieu(final SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, final int _typeCombi, boolean _corrode) {
    String[] s = new String[3];

    if (_typeCombi == AlbeLib.ELS_FREQ) {
      for (int i = 0; i < 3; i++) {
        s[i] = "/";
      }
    } else {
      double coeff = 0;

      switch (_typeCombi) {
      case AlbeLib.COMBINAISON_MANUELLE: {
        coeff = _params.lancementCalculs.gammaDPieu;
        break;
      }
      case AlbeLib.ELU_FONDA: {
        coeff = _params.coefficientsPartiels.coeffModele[0].resistanceStructuralePieux;
        break;
      }
      case AlbeLib.ELU_ACC: {
        coeff = _params.coefficientsPartiels.coeffModele[1].resistanceStructuralePieux;
        break;
      }
      case AlbeLib.ELS_RARE: {
        coeff = _params.coefficientsPartiels.coeffModele[2].resistanceStructuralePieux;
        break;
      }
      }
      double[] contraintes = getContraintes(_params, _resCombi, _typeCombi, _corrode);
      double valeur = CtuluLibArray.getMax(contraintes);
      double gamma = CtuluLibArray
          .getMin(getFlexionsPieu(_params, _resCombi, _typeCombi, _corrode));
      s[0] = "" + AlbeLib.arronditDouble(valeur);
      s[1] = "" + coeff;
      s[2] = "" + AlbeLib.arronditDouble(gamma);
    }

    return s;
  }

  /**
   * retourne dans un tableau de String les valeurs des d�placements en t�te
   * (valeur calcul�e, valeur maximale saisie dans l'interface et gamma).
   * retourne "/" si il n'y a pas de r�sultats.
   */
  public static String[] getDeplacements(final SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, final int _typeCombi) {
    String[] s = new String[3];
    double deplacement = _typeCombi == AlbeLib.COMBINAISON_MANUELLE ? _params.lancementCalculs.deplacementMax
        : _params.criteresDimensionnement.deplacementTeteOuvrage;

    s[0] = _resCombi.resultatsGeneraux.deplacementTete + " m";

    if (_typeCombi == AlbeLib.COMBINAISON_MANUELLE || _typeCombi == AlbeLib.ELS_RARE) {
      s[1] = deplacement + " m";
      s[2] = "" + AlbeLib.arronditDouble(deplacement / _resCombi.resultatsGeneraux.deplacementTete);
    } else {
      s[1] = "/";
      s[2] = "/";
    }
    return s;
  }

  /**
   * retourne dans un tableau de String les valeurs des r�actions (valeur
   * calcul�e, valeur maximale saisie dans l'interface et gamma). retourne "/"
   * si il n'y a pas de r�sultats.
   */
  public static String[] getReactions(final SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, final int _typeCombi) {
    String[] s = new String[3];
    double reactionMax = 0;

    switch (_typeCombi) {
    case AlbeLib.COMBINAISON_MANUELLE: {
      reactionMax = _params.lancementCalculs.reactionMax;
      break;
    }
    case AlbeLib.ELU_FONDA: {
      reactionMax = _params.criteresDimensionnement.reactionTeteOuvrageEluFonda;
      break;
    }
    case AlbeLib.ELU_ACC: {
      reactionMax = _params.criteresDimensionnement.reactionTeteOuvrageEluAcc;
      break;
    }
    }

    double reactionCalculee = AlbeLib
        .arronditDouble(_resCombi.resultatsGeneraux.effortStatiqueChoc * 10);
    s[0] = reactionCalculee + " kN";

    if (_typeCombi == AlbeLib.ELS_RARE || _typeCombi == AlbeLib.ELS_FREQ) {
      for (int i = 1; i < 3; i++) {
        s[i] = "/";
      }
    } else {
      s[1] = reactionMax + " kN";
      s[2] = "" + AlbeLib.arronditDouble(reactionMax / reactionCalculee);
    }

    return s;
  }

  /**
   * retourne dans un tableau de String les valeurs des d�flexions de la d�fense
   * (valeur calcul�e, valeur maximale saisie dans l'interface et gamma).
   * retourne "/" si il n'y a pas de r�sultats.
   */
  public static String[] getDeflexions(final SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, final int _typeCombi) {
    String[] s = new String[3];
    double deflexion = 0;

    switch (_typeCombi) {
    case AlbeLib.COMBINAISON_MANUELLE: {
      deflexion = _params.lancementCalculs.deflexionMax;
      break;
    }
    case AlbeLib.ELU_FONDA: {
      deflexion = _params.criteresDimensionnement.deflexionDefenseEluFonda;
      break;
    }
    case AlbeLib.ELU_ACC: {
      deflexion = _params.criteresDimensionnement.deflexionDefenseEluAcc;
      break;
    }
    case AlbeLib.ELS_RARE: {
      deflexion = _params.criteresDimensionnement.deflexionDefenseElsRare;
      break;
    }
    }

    double deflexionCalculee = _resCombi.resultatsGeneraux.deflexionDefenses;
    s[0] = deflexionCalculee + " %";

    if (_typeCombi == AlbeLib.ELS_FREQ) {
      for (int i = 1; i < 3; i++) {
        s[i] = "/";
      }
    } else {
      s[1] = deflexion + " %";
      s[2] = "" + AlbeLib.arronditDouble(deflexion / deflexionCalculee);
    }
    return s;
  }

  /**
   * retourne le type de combinaison sous forme de string � partir du num�ro
   * (utilis� pour le nom des fichiers enregistr�s).
   */
  public static String nomCombinaison(int _typeCombi) {
    String s = "";
    switch (_typeCombi) {
    case AlbeLib.ELU_FONDA: {
      s = "ELUfond";
      break;
    }
    case AlbeLib.ELU_ACC: {
      s = "ELUacc";
      break;
    }
    case AlbeLib.ELS_RARE: {
      s = "ELSrare";
      break;
    }
    case AlbeLib.ELS_FREQ: {
      s = "ELSfreq";
      break;
    }
    }

    return s;
  }

  /**
   * retourne le type de combinaison sous forme de string � partir du num�ro.
   */
  public static String getCombinaison(int _typeCombi) {
    String s = "";
    switch (_typeCombi) {
    case AlbeLib.ELU_FONDA: {
      s = "ELU fondamental";
      break;
    }
    case AlbeLib.ELU_ACC: {
      s = "ELU accidentel";
      break;
    }
    case AlbeLib.ELS_RARE: {
      s = "ELS rare";
      break;
    }
    case AlbeLib.ELS_FREQ: {
      s = "ELS fr&eacute;quent";
      break;
    }
    }

    return s;
  }

  /**
   * retourne la structure de r�sultats correspondant � la combinaison (� partir
   * de son type et de son num�ro).
   */
  public static SResultatsCombinaison getResCombi(final SResultatsAlbe _results,
      final int _typeCombi, final int _numCombi) {
    SResultatsCombinaison sRes = new SResultatsCombinaison();
    switch (_typeCombi) {
    case AlbeLib.ELU_FONDA: {
      sRes = _results.resultatsEluFonda[_numCombi - 1];
      break;
    }
    case AlbeLib.ELU_ACC: {
      sRes = _results.resultatsEluAcc[_numCombi - 1];
      break;
    }
    case AlbeLib.ELS_RARE: {
      sRes = _results.resultatsElsRare[_numCombi - 1];
      break;
    }
    case AlbeLib.ELS_FREQ: {
      sRes = _results.resultatsElsFreq[_numCombi - 1];
      break;
    }
    case AlbeLib.COMBINAISON_MANUELLE: {
      sRes = _results.resultatsCombiManuelle;
      break;
    }
    }
    return sRes;
  }

  /**
   * cosntruit l'en-t�te du tableau HTML r�capitulatif des r�sultats.
   */

  public static String buildHeaderTabRecapResultats(SParametresAlbe _params, boolean _accostage,
      boolean _defense, int _typeCombi, boolean _center) {
    String html = "";
    // nombre de tron�ons
    int nbTroncons = _params.pieu.nombreTroncons;

    // nombre de colonnes occup�es par 'Cas de charge'
    int n1 = 1;
    if (_accostage)
      n1++;
    if (_typeCombi == AlbeLib.ELU_FONDA)
      n1++;
    if (_defense)
      n1++;
    // nombre de colonnes occup�es par 'Facteurs de dimensionnement'
    int n2;
    switch (_typeCombi) {
    case AlbeLib.ELS_FREQ: {
      n2 = 1;
      break;
    }
    case AlbeLib.ELS_RARE: {
      n2 = nbTroncons + 2;
      if (_defense)
        n2 += 2;
      break;
    }
    default: {
      n2 = nbTroncons + 1;
      if (_defense)
        n2 += 4;
      else if (_accostage)
        n2 += 2;
    }
    }

    html += "<table border=\"2\" cellspacing=\"0\" cellpadding=\"2\""
        + (_center ? " align = \"center\"" : "") + ">" + CtuluLibString.LINE_SEP;
    html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
    html += "<th rowspan = \"3\">Combinaison</th>" + CtuluLibString.LINE_SEP;
    html += "<th colspan = \"" + n1 + "\">Cas de charge</th>" + CtuluLibString.LINE_SEP;
    html += "<th colspan = \"" + n2 + "\">Facteurs de dimensionnement</th>"
        + CtuluLibString.LINE_SEP;
    html += "</tr>" + CtuluLibString.LINE_SEP;
    html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
    if (_accostage)
      html += "<th rowspan = \"2\">Action</th>" + CtuluLibString.LINE_SEP;
    if (_typeCombi == AlbeLib.ELU_FONDA)
      html += "<th rowspan = \"2\">Action du<br>sol</th>" + CtuluLibString.LINE_SEP;
    if (_defense)
      html += "<th rowspan = \"2\">R&eacute;action<br>transmise<br>par la<br>d&eacute;fense</th>"
          + CtuluLibString.LINE_SEP;
    html += "<th rowspan = \"2\">Corrosion<br>du tube</th>" + CtuluLibString.LINE_SEP;
    if (_typeCombi != AlbeLib.ELS_FREQ)
      html += "<th colspan = \"" + nbTroncons + "\">Flexion des tron&ccedil;ons</th>"
          + CtuluLibString.LINE_SEP;
    if (_typeCombi != AlbeLib.ELS_RARE)
      html += "<th rowspan = \"2\">Mobilisation<br>de la but&eacute;e<br>du sol</th>"
          + CtuluLibString.LINE_SEP;
    if (_typeCombi == AlbeLib.ELS_RARE)
      html += "<th colspan = \"2\">D&eacute;placement<br>en t&ecirc;te</th>" + CtuluLibString.LINE_SEP;
    if (_defense && _typeCombi != AlbeLib.ELS_FREQ)
      html += "<th colspan = \"2\">D&eacute;flexion<br>d&eacute;fenses</th>" + CtuluLibString.LINE_SEP;
    if (_accostage && (_typeCombi == AlbeLib.ELU_FONDA || _typeCombi == AlbeLib.ELU_ACC))
      html += "<th colspan = \"2\">Efforts<br>(r&eacute;action)</th>" + CtuluLibString.LINE_SEP;
    html += "</tr>" + CtuluLibString.LINE_SEP;
    html += "<tr align = \"center\">";
    if (_typeCombi != AlbeLib.ELS_FREQ) {
      for (int i = 1; i < nbTroncons + 1; i++) {
        html += "<th>Tron&ccedil;on " + i + "</th>" + CtuluLibString.LINE_SEP;
      }
    }
    if (_typeCombi == AlbeLib.ELS_RARE) {
      html += "<th>Valeur<br>(m)</th>" + CtuluLibString.LINE_SEP;
      html += "<th>Fact<br>dim</th>"
          + CtuluLibString.LINE_SEP;
    }
    if (_defense && _typeCombi != AlbeLib.ELS_FREQ) {
      html += "<th>Valeur<br>(%)</th>" + CtuluLibString.LINE_SEP;
      html += "<th>Fact<br>dim</th>"
          + CtuluLibString.LINE_SEP;
    }
    if (_accostage && (_typeCombi == AlbeLib.ELU_FONDA || _typeCombi == AlbeLib.ELU_ACC)) {
      html += "<th>Valeur<br>(kN)</th>" + CtuluLibString.LINE_SEP;
      html += "<th>Fact<br>dim</th>"
          + CtuluLibString.LINE_SEP;
    }

    html += "</tr>" + CtuluLibString.LINE_SEP;
    return html;
  }

  /**
   * construit le corps du tableau HTML r�capitulatif des r�sultats pour l'ELU
   * fondamental.
   */

  public static String buildBodyTabRecapResultatsEluFonda(SParametresAlbe _params,
      SResultatsAlbe _results, boolean _accostage, boolean _defense) {
    String html = "";
    // Si accostage
    if (_accostage) {
      if (_defense) {
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>1</th>";
        html += "<td rowspan = \"8\">AC<br>bas</td>" + CtuluLibString.LINE_SEP;
        html += "<td rowspan = \"4\">favorable</td>" + CtuluLibString.LINE_SEP;
        html += "<td rowspan = \"2\">maximale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 0);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>2</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 1);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>3</th>";
        html += "<td rowspan = \"2\">minimale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 2);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>4</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 3);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>5</th>";
        html += "<td rowspan = \"4\">d&eacute;favorable</td>" + CtuluLibString.LINE_SEP;
        html += "<td rowspan = \"2\">maximale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 4);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>6</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 5);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>7</th>";
        html += "<td rowspan = \"2\">minimale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 6);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>8</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 7);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>9</th>";
        html += "<td rowspan = \"8\">AC<br>haut</td>" + CtuluLibString.LINE_SEP;
        html += "<td rowspan = \"4\">favorable</td>" + CtuluLibString.LINE_SEP;
        html += "<td rowspan = \"2\">maximale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 8);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>10</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 9);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>11</th>";
        html += "<td rowspan = \"2\">minimale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 10);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>12</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 11);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>13</th>";
        html += "<td rowspan = \"4\">d&eacute;favorable</td>" + CtuluLibString.LINE_SEP;
        html += "<td rowspan = \"2\">maximale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 12);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>14</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 13);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>15</th>";
        html += "<td rowspan = \"2\">minimale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 14);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>16</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, AlbeLib.ELU_FONDA, 15);
        html += "</tr>" + CtuluLibString.LINE_SEP;
      } else {
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>1</th>";
        html += "<td rowspan = \"4\">AC<br>bas</td>" + CtuluLibString.LINE_SEP;
        html += "<td rowspan = \"2\">favorable</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, AlbeLib.ELU_FONDA, 0);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>2</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, AlbeLib.ELU_FONDA, 1);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>3</th>";
        html += "<td rowspan = \"2\">d&eacute;favorable</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, AlbeLib.ELU_FONDA, 2);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>4</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, AlbeLib.ELU_FONDA, 3);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>5</th>";
        html += "<td rowspan = \"4\">AC<br>haut</td>" + CtuluLibString.LINE_SEP;
        html += "<td rowspan = \"2\">favorable</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, AlbeLib.ELU_FONDA, 4);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>6</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, AlbeLib.ELU_FONDA, 5);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>7</th>";
        html += "<td rowspan = \"2\">d&eacute;favorable</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, AlbeLib.ELU_FONDA, 6);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>8</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, AlbeLib.ELU_FONDA, 7);
        html += "</tr>" + CtuluLibString.LINE_SEP;
      }
    }
    // Si amarrage
    else {
      html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
      html += "<th>1</th>";
      html += "<td rowspan = \"2\">favorable</td>" + CtuluLibString.LINE_SEP;
      html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
      html += afficheLigneRes(_params, _results, false, false, AlbeLib.ELU_FONDA, 0);
      html += "</tr>" + CtuluLibString.LINE_SEP;
      html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
      html += "<th>2</th>";
      html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
      html += afficheLigneRes(_params, _results, false, false, AlbeLib.ELU_FONDA, 1);
      html += "</tr>" + CtuluLibString.LINE_SEP;
      html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
      html += "<th>3</th>";
      html += "<td rowspan = \"2\">d&eacute;favorable</td>" + CtuluLibString.LINE_SEP;
      html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
      html += afficheLigneRes(_params, _results, false, false, AlbeLib.ELU_FONDA, 2);
      html += "</tr>" + CtuluLibString.LINE_SEP;
      html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
      html += "<th>4</th>";
      html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
      html += afficheLigneRes(_params, _results, false, false, AlbeLib.ELU_FONDA, 3);
      html += "</tr>" + CtuluLibString.LINE_SEP;
    }
    html += "</table>" + CtuluLibString.LINE_SEP;

    return html;
  }

  /**
   * construit le corps du tableau HTML r�capitulatif des r�sultats pour l'ELU
   * acc, l'ELS rare et l'ELS fr�quent.
   */

  public static String buildBodyTabRecapResultatsEluAccEls(SParametresAlbe _params,
      SResultatsAlbe _results, boolean _accostage, boolean _defense, int _typeCombi) {

    String html = "";
    // Si accostage
    if (_accostage) {
      if (_defense) {
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>1</th>";
        html += "<td rowspan = \"4\">AC<br>bas</td>" + CtuluLibString.LINE_SEP;
        html += "<td rowspan = \"2\">maximale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, _typeCombi, 0);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>2</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, _typeCombi, 1);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>3</th>";
        html += "<td rowspan = \"2\">minimale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, _typeCombi, 2);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>4</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, _typeCombi, 3);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>5</th>";
        html += "<td rowspan = \"4\">AC<br>haut</td>" + CtuluLibString.LINE_SEP;
        html += "<td rowspan = \"2\">maximale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, _typeCombi, 4);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>6</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, _typeCombi, 5);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>7</th>";
        html += "<td rowspan = \"2\">minimale</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, _typeCombi, 6);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>8</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, true, _typeCombi, 7);
        html += "</tr>" + CtuluLibString.LINE_SEP;
      } else {
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>1</th>";
        html += "<td rowspan = \"2\">AC<br>bas</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, _typeCombi, 0);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>2</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, _typeCombi, 1);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>3</th>";
        html += "<td rowspan = \"2\">AC<br>haut</td>" + CtuluLibString.LINE_SEP;
        html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, _typeCombi, 2);
        html += "</tr>" + CtuluLibString.LINE_SEP;
        html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
        html += "<th>4</th>";
        html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
        html += afficheLigneRes(_params, _results, true, false, _typeCombi, 3);
        html += "</tr>" + CtuluLibString.LINE_SEP;
      }
    } else {
      html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
      html += "<th>1</th>";
      html += "<td>corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
      html += afficheLigneRes(_params, _results, false, false, _typeCombi, 0);
      html += "</tr>" + CtuluLibString.LINE_SEP;
      html += "<tr align = \"center\">" + CtuluLibString.LINE_SEP;
      html += "<th>2</th>";
      html += "<td>non corrod&eacute;</td>" + CtuluLibString.LINE_SEP;
      html += afficheLigneRes(_params, _results, false, false, _typeCombi, 1);
      html += "</tr>" + CtuluLibString.LINE_SEP;
    }
    html += "</table>" + CtuluLibString.LINE_SEP;

    return html;
  }

  /**
   * construit le tableau HTML de r�capitulatif des r�sultats.
   */

  public static String buildTabRecapResultats(SParametresAlbe _params, SResultatsAlbe _results,
      boolean _accostage, boolean _defense, int _typeCombi, boolean _center) {

    String html = "";

    // en-t�te du tableau
    html += buildHeaderTabRecapResultats(_params, _accostage, _defense, _typeCombi, _center);
    // corps du tableau
    html += _typeCombi == AlbeLib.ELU_FONDA ? buildBodyTabRecapResultatsEluFonda(_params, _results,
        _accostage, _defense) : buildBodyTabRecapResultatsEluAccEls(_params, _results, _accostage,
        _defense, _typeCombi);

    return html;

  }

  /**
   * affiche une ligne de r�sultats.
   */
  public static String afficheLigneRes(SParametresAlbe _params, SResultatsAlbe _results,
      boolean _accostage, boolean _defense, int _typeCombi, int _numLigne) {
    String html = "";
    boolean corrosion;
    if (_numLigne % 2 == 0)
      corrosion = true;
    else
      corrosion = false;

    switch (_typeCombi) {
    /** ELU fondamental. */
    case AlbeLib.ELU_FONDA: {
      SResultatsCombinaison res = _results.resultatsEluFonda[_numLigne];
      // Si le calcul n'a pas converg�, on affiche des "-"
      if (!res.convergence) {
        for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
          html += "<td>-</td>";
        }
        html += "<td>-</td>"
            + (_defense ? "<td>-</td><td>-</td><td>-</td><td>-</td>"
                : (_accostage ? "<td>-</td><td>-</td>" : ""));
      } else {
        // flexions des tubes
        double[] flexions = getFlexionsPieu(_params, res, _typeCombi, corrosion);
        for (int i = 0; i < flexions.length; i++) {
          if (flexions[i] > limiteFlexion)
            html += "<td>non sollicit&eacute;</td>";
          else
            html += "<td><font color = " + (flexions[i] < 1 ? "red>" : "green>") + flexions[i]
                + "</font></td>";
        }
        // mobilisation de la but�e du sol
        double mob = AlbeLib.arronditDouble(calculFacteurDimButee(_params, res, _typeCombi,
            _numLigne + 1));
        html += "<td><font color = " + (mob < 1 ? "red>" : "green>") + mob + "</font></td>";
        // d�flexion
        if (_defense) {
          double deflexionValeur = _results.resultatsEluFonda[_numLigne].resultatsGeneraux.deflexionDefenses;
          double deflexionGamma = AlbeLib
              .arronditDouble(_params.criteresDimensionnement.deflexionDefenseEluFonda
                  / deflexionValeur);
          html += "<td>" + deflexionValeur + "</td>";
          html += "<td><font color = " + (deflexionGamma < 1 ? "red>" : "green>") + deflexionGamma
              + "</font></td>";
        }
        // efforts
        if (_accostage) {
          double effortValeur = AlbeLib
              .arronditDouble(_results.resultatsEluFonda[_numLigne].resultatsGeneraux.effortStatiqueChoc * 10);
          double effortGamma = AlbeLib
              .arronditDouble(_params.criteresDimensionnement.reactionTeteOuvrageEluFonda
                  / effortValeur);
          html += "<td>" + effortValeur + "</td>";
          html += "<td><font color = " + (effortGamma < 1 ? "red>" : "green>") + effortGamma
              + "</font></td>";
        }
      }
      break;
    }
      /** ELU accidentel. */
    case AlbeLib.ELU_ACC: {
      SResultatsCombinaison res = _results.resultatsEluAcc[_numLigne];
      // Si le calcul n'a pas converg�, on affiche des "-"
      if (!res.convergence) {
        for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
          html += "<td>-</td>";
        }
        html += "<td>-</td>"
            + (_defense ? "<td>-</td><td>-</td><td>-</td><td>-</td>"
                : (_accostage ? "<td>-</td><td>-</td>" : ""));
      } else {
        // flexions des tubes
        double[] flexions = getFlexionsPieu(_params, res, _typeCombi, corrosion);
        for (int i = 0; i < flexions.length; i++) {
          if (flexions[i] > limiteFlexion)
            html += "<td>non sollicit&eacute;</td>";
          else
            html += "<td><font color = " + (flexions[i] < 1 ? "red>" : "green>") + flexions[i]
                + "</font></td>";
        }
        // mobilisation de la but�e du sol
        double mob = AlbeLib.arronditDouble(calculFacteurDimButee(_params, res, _typeCombi,
            _numLigne + 1));
        html += "<td><font color = " + (mob < 1 ? "red>" : "green>") + mob + "</font></td>";
        // d�flexion
        if (_defense) {
          double deflexionValeur = _results.resultatsEluAcc[_numLigne].resultatsGeneraux.deflexionDefenses;
          double deflexionGamma = AlbeLib
              .arronditDouble(_params.criteresDimensionnement.deflexionDefenseEluAcc
                  / deflexionValeur);
          html += "<td>" + deflexionValeur + "</td>";
          html += "<td><font color = " + (deflexionGamma < 1 ? "red>" : "green>") + deflexionGamma
              + "</font></td>";
        }
        // efforts
        if (_accostage) {
          double effortValeur = AlbeLib
              .arronditDouble(_results.resultatsEluAcc[_numLigne].resultatsGeneraux.effortStatiqueChoc * 10);
          double effortGamma = AlbeLib
              .arronditDouble(_params.criteresDimensionnement.reactionTeteOuvrageEluAcc
                  / effortValeur);
          html += "<td>" + effortValeur + "</td>";
          html += "<td><font color = " + (effortGamma < 1 ? "red>" : "green>") + effortGamma
              + "</font></td>";
        }
      }
      break;
    }
      /** ELS rare. */
    case AlbeLib.ELS_RARE: {
      SResultatsCombinaison res = _results.resultatsElsRare[_numLigne];
      // Si le calcul n'a pas converg�, on affiche des "-"
      if (!res.convergence) {
        for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
          html += "<td>-</td>";
        }
        html += "<td>-</td><td>-</td>" + (_defense ? "<td>-</td><td>-</td>" : "");
      } else {
        // flexions des tubes
        double[] flexions = getFlexionsPieu(_params, res, _typeCombi, corrosion);

        for (int i = 0; i < flexions.length; i++) {
          if (flexions[i] > limiteFlexion)
            html += "<td>non sollicit&eacute;</td>";
          else
            html += "<td><font color = " + (flexions[i] < 1 ? "red>" : "green>") + flexions[i]
                + "</font></td>";
        }
        // d�placement en t�te
        double deplacementValeur = _results.resultatsElsRare[_numLigne].resultatsGeneraux.deplacementTete;
        double deplacementGamma = AlbeLib
            .arronditDouble(_params.criteresDimensionnement.deplacementTeteOuvrage
                / deplacementValeur);
        html += "<td>" + deplacementValeur + "</td>";
        html += "<td><font color = " + (deplacementGamma < 1 ? "red>" : "green>")
            + deplacementGamma + "</font></td>";
        // d�flexion
        if (_defense) {
          double deflexionValeur = _results.resultatsElsRare[_numLigne].resultatsGeneraux.deflexionDefenses;
          double deflexionGamma = AlbeLib
              .arronditDouble(_params.criteresDimensionnement.deflexionDefenseElsRare
                  / deflexionValeur);
          html += "<td>" + deflexionValeur + "</td>";
          html += "<td><font color = " + (deflexionGamma < 1 ? "red>" : "green>") + deflexionGamma
              + "</font></td>";
        }
      }
      break;
    }
      /** ELS fr�quent. */
    case AlbeLib.ELS_FREQ: {
      SResultatsCombinaison res = _results.resultatsElsFreq[_numLigne];
      // Si le calcul n'a pas converg�, on affiche des "-"
      if (!res.convergence) {
        html += "<td>-</td>";
      } else {
        double mob = AlbeLib.arronditDouble(calculFacteurDimButee(_params, res, _typeCombi,
            _numLigne + 1));
        html += "<td><font color = " + (mob < 1 ? "red>" : "green>") + mob + "</font></td>";
      }
      break;
    }
    }

    return html;
  }

  /**
   * retourne le coefficient sur l'acier selon le type de combinaison.
   */
  public static double getCoefAcierDouble(SParametresAlbe _params, int _typeCombi) {
    double coef = 0;
    switch (_typeCombi) {
    case AlbeLib.ELU_FONDA: {
      coef = _params.coefficientsPartiels.coeffAcier[0];
      break;
    }
    case AlbeLib.ELU_ACC: {
      coef = _params.coefficientsPartiels.coeffAcier[1];
      break;
    }
    case AlbeLib.ELS_RARE: {
      coef = _params.coefficientsPartiels.coeffAcier[2];
      break;
    }
    case AlbeLib.ELS_FREQ: {
      FuLog.warning("Il n'existe pas de coefficient sur l'acier pour l'ELS fr&eacute;quent.");
      break;
    }
    case AlbeLib.COMBINAISON_MANUELLE: {
      coef = _params.lancementCalculs.gammaFyAcier;
      break;
    }
    }
    return coef;
  }

  /**
   * retourne le coefficient de resistance des pieux selon le type de
   * combinaison.
   */
  public static double getCoefPieu(SParametresAlbe _params, int _typeCombi) {
    double coef = 0;
    switch (_typeCombi) {
    case AlbeLib.ELU_FONDA: {
      coef = _params.coefficientsPartiels.coeffModele[0].resistanceStructuralePieux;
      break;
    }
    case AlbeLib.ELU_ACC: {
      coef = _params.coefficientsPartiels.coeffModele[1].resistanceStructuralePieux;
      break;
    }
    case AlbeLib.ELS_RARE: {
      coef = _params.coefficientsPartiels.coeffModele[2].resistanceStructuralePieux;
      break;
    }
    case AlbeLib.ELS_FREQ: {
      FuLog
          .warning("Il n'existe pas de coefficient sur la r&eacute;sistance structurale des pieux pour l'ELS fr&eacute;quent.");
      break;
    }
    case AlbeLib.COMBINAISON_MANUELLE: {
      coef = _params.lancementCalculs.gammaDPieu;
      break;
    }
    }
    return coef;
  }

  /**
   * retourne l'effort normal sur le pieu.
   */
  public static double getEffortNormal(SParametresAlbe _params, int _typeCombi) {
    double effort = 0;
    switch (_typeCombi) {
    case AlbeLib.ELU_FONDA: {
      effort = _params.action.effortVerticalAmarrageCalcul;
      break;
    }
    case AlbeLib.ELU_ACC: {
      effort = _params.action.effortVerticalAmarrageAccidentel;
      break;
    }
    case AlbeLib.ELS_RARE: {
      effort = _params.action.effortVerticalAmarrageCaracteristique;
      break;
    }
    case AlbeLib.ELS_FREQ: {
      effort = _params.action.effortVerticalAmarrageFrequent;
      break;
    }
    }
    return effort;
  }

  /**
   * calcul de l'inertie (en m4) d'un troncon tubulaire.
   */
  public static double calculInertieTronconTub(final double _dPrime, final double _ePrime) {
    double d1 = Math.pow(_dPrime, 4) / 64;
    double d2 = Math.pow(_dPrime - 2 * _ePrime, 4) / 64;

    return Math.PI * (d1 - d2);
  }

  /**
   * calcul de la section transversale d'acier (en m2) d'un tron�on tubulaire.
   */
  public static double calculSectionTronconTub(final double _dPrime, final double _ePrime) {
    double d1 = Math.pow(_dPrime, 2) / 4;
    double d2 = Math.pow(_dPrime - 2 * _ePrime, 2) / 4;

    return Math.PI * (d1 - d2);
  }

  /**
   * calcul de la contrainte de la ligne de r�sultat _numLigne dans le tron�on
   * _numTron�on dans le cas d'un pieu tubulaire.
   * 
   * @param _resCombi
   * @param _numTroncon indice du tron�on (commence � 0)
   */
  public static double calculContrainteTronconTub(final SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, final int _typeCombi, final int _numTroncon,
      int _numLigne, final boolean _corrosion) {
    double res;
    // moment M (en MN.m)
    double m = _resCombi.resistanceTroncons[_numTroncon].resTroncon[_numLigne].moment / 100;
    // diam�tre nominal D (en m)
    double d = _params.pieu.diametreTroncons / 1000;
    // �paisseur nominale e du tube (en m)
    double e = _params.pieu.tronconsTubulaire[_numTroncon].epaisseur / 1000;
    // diam�tre de calcul D' (en m)
    double dPrime = _corrosion ? (d - 2 * _params.pieu.tronconsTubulaire[_numTroncon].perteEpaisseurCorrosion / 1000)
        : d;
    // bras de levier v (en m)
    double v = dPrime / 2;
    // epaisseur de calcul ePrime (en m)
    double ePrime = _corrosion ? (e - _params.pieu.tronconsTubulaire[_numTroncon].perteEpaisseurCorrosion / 1000)
        : e;
    // calcul de l'inertie I (en m4)
    double inertie = calculInertieTronconTub(dPrime, ePrime);

    // effort normal (en MN)
    double n;
    if (_params.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE))
      n = _params.action.effortVerticalAccostage / 1000;
    else
      n = getEffortNormal(_params, _typeCombi) / 1000;

    // section transversale d'acier du pieu
    double s = calculSectionTronconTub(dPrime, ePrime);
    double d1 = Math.abs((m * v / inertie) + n / s);
    double d2 = Math.abs((-m * v / inertie) + n / s);
    res = Math.max(d1, d2);

    return res;
  }

  /**
   * calcul de la contrainte de la ligne de r�sultat _numLigne dans le tron�on
   * _numTron�on pour un pieu en caisson.
   */
  public static double calculContrainteTronconCais(final SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, final int _typeCombi, final int _numTroncon,
      int _numLigne, final boolean _corrosion) {
    double res;
    // moment maximal M (en MN.m)
    double m = _resCombi.resistanceTroncons[_numTroncon].resTroncon[_numLigne].moment / 100;
    // bras de levier v (en m)
    double v = _params.pieu.excentricite / 100;
    // inertie (en m4)
    double i = _corrosion ? _params.pieu.tronconsCaisson[_numTroncon].inertieCorrodee / 100000000
        : _params.pieu.tronconsCaisson[_numTroncon].inertieNonCorrodee / 100000000;

    // effort normal (en MN)
    double n;
    // TODO FRED A MODIFIER CAR JAMAIS VRAI
    // ne serait-ce pas
    // if (_params.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE))
    if (_params.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE))
      n = _params.action.effortVerticalAccostage / 1000;
    else
      n = getEffortNormal(_params, _typeCombi) / 1000;

    // section (en m2)
    double s = _corrosion ? _params.pieu.tronconsCaisson[_numTroncon].sectionCorrodee / 10000
        : _params.pieu.tronconsCaisson[_numTroncon].sectionNonCorrodee / 10000;
    double d1 = Math.abs((m * v / i) + n / s);
    double d2 = Math.abs((-m * v / i) + n / s);
    res = Math.max(d1, d2);
    return res;
  }

  /**
   * calcul la contrainte de la ligne de r�sultat _numLigne dans le tron�on
   * _numTron�on.
   */
  public static double calculContrainteTroncon(final SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, final int _typeCombi, final int _numTroncon,
      int _numLigne, final boolean _corrosion) {

    double d;
    // Pieu tubulaire
    if (_params.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE))
      d = calculContrainteTronconTub(_params, _resCombi, _typeCombi, _numTroncon, _numLigne,
          _corrosion);

    // Pieu en caisson
    else
      d = calculContrainteTronconCais(_params, _resCombi, _typeCombi, _numTroncon, _numLigne,
          _corrosion);

    return d;
  }

  /**
   * calcule les contraintes et retourne les r�sultats dans un tableau de
   * tableaux de double.
   */
  public static double[][] calculContraintes(SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, int _typeCombi, boolean _corrosion) {

    int nbTroncons = _params.pieu.nombreTroncons;
    int[] tabNb = new int[nbTroncons];

    for (int i = 0; i < nbTroncons; i++) {
      tabNb[i] = _resCombi.resistanceTroncons[i].nombreLignesRes;
    }

    int max = AlbeLib.max(tabNb);

    double[][] c = new double[nbTroncons][max];

    // Pieu tubulaire
    if (_params.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE)) {
      for (int i = 0; i < nbTroncons; i++) {
        for (int j = 0; j < _resCombi.resistanceTroncons[i].nombreLignesRes; j++) {
          c[i][j] = calculContrainteTronconTub(_params, _resCombi, _typeCombi, i, j, _corrosion);
        }
      }
    }
    // Pieu en caisson
    else {
      for (int i = 0; i < nbTroncons; i++) {
        for (int j = 0; j < _resCombi.resistanceTroncons[i].nombreLignesRes; j++) {
          c[i][j] = calculContrainteTronconCais(_params, _resCombi, _typeCombi, i, j, _corrosion);
        }
      }
    }

    return c;
  }

  /**
   * retourne la contrainte maximale dans un tron�on.
   */
  public static double getContrainteMaxTroncon(SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, int _typeCombi, boolean _corrosion, int _numTroncon) {

    double[] contraintes = new double[_resCombi.resistanceTroncons[_numTroncon].nombreLignesRes];
    for (int i = 0; i < _resCombi.resistanceTroncons[_numTroncon].nombreLignesRes; i++) {
      contraintes[i] = calculContrainteTroncon(_params, _resCombi, _typeCombi, _numTroncon, i,
          _corrosion);
    }

    return CtuluLibArray.getMax(contraintes);
  }

  /**
   * retourne dans un tableau les diff�rentes contraintes max (une pour chaque
   * tron�on).
   */
  public static double[] getContraintes(SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, int _typeCombi, boolean _corrosion) {
    double[] d = new double[_params.pieu.nombreTroncons];
    for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
      d[i] = getContrainteMaxTroncon(_params, _resCombi, _typeCombi, _corrosion, i);
    }
    return d;
  }

  /**
   * calcul du facteur de dimensionnement d'un troncon tubulaire.
   */
  public static double calculFacteurDimTronconTub(SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, int _typeCombi, boolean _corrosion, int _numTroncon) {

    double contrainteMax = getContrainteMaxTroncon(_params, _resCombi, _typeCombi, _corrosion,
        _numTroncon);

    double fyd = _params.pieu.tronconsTubulaire[_numTroncon].valeurCaracteristiqueLimiteElastiqueAcier
        / getCoefAcierDouble(_params, _typeCombi);

    return fyd / (getCoefPieu(_params, _typeCombi) * contrainteMax);
  }

  /**
   * calcul du facteur de dimensionnement d'un troncon en caisson.
   */
  public static double calculFacteurDimTronconCais(SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, int _typeCombi, boolean _corrosion, int _numTroncon) {

    double contrainteMax = getContrainteMaxTroncon(_params, _resCombi, _typeCombi, _corrosion,
        _numTroncon);

    double fyd = _params.pieu.tronconsCaisson[_numTroncon].valeurCaracteristiqueLimiteElastiqueAcier
        / getCoefAcierDouble(_params, _typeCombi);

    return fyd / (getCoefPieu(_params, _typeCombi) * contrainteMax);
  }

  /**
   * calcule les flexions.
   */
  public static double[] getFlexionsPieu(SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, int _typeCombi, boolean _corrosion) {
    int nbTroncons = _params.pieu.nombreTroncons;
    double[] flexions = new double[nbTroncons];

    // Pieu tubulaire
    if (_params.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE)) {
      for (int i = 0; i < nbTroncons; i++) {
        flexions[i] = AlbeLib.arronditDouble(calculFacteurDimTronconTub(_params, _resCombi,
            _typeCombi, _corrosion, i));
      }
    }// Pieu en caisson
    else {
      for (int i = 0; i < nbTroncons; i++) {
        flexions[i] = AlbeLib.arronditDouble(calculFacteurDimTronconCais(_params, _resCombi,
            _typeCombi, _corrosion, i));
      }
    }
    return flexions;
  }

  /**
   * retourne la largeur du pieu en m�tres.
   */
  public static double getLargeurPieu(final SParametresAlbe _params) {
    if (_params.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE))
      return _params.pieu.diametreTroncons / 1000;
    return _params.pieu.largeurEffectivePieu / 100;
  }

  /**
   * calcul du facteur de dimensionnement vis � vis de la mobilisation de la
   * but�e du sol.
   */
  public static double calculFacteurDimButee(final SParametresAlbe _params,
      final SResultatsCombinaison _resCombi, final int _typeCombi, final int _numCombi) {

    double buteeMobilisee = 0;
    double buteeMobilisable = 0;

    int indiceDepart = 0;
    int indiceFin = 0;

    int ind = 0;
    boolean trouveDepart = false;
    boolean trouveFin = false;

    int tailleTab = 0;
    for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
      tailleTab += _resCombi.resistanceTroncons[i].nombreLignesRes;
    }

    double[] cote = new double[tailleTab];
    double[] pression = new double[tailleTab];
    int[] couche = new int[tailleTab];

    // On r�cup�re l'indice de d�part � prendre en compte pour le calcul du
    // facteur de dimensionnement (premier indice o� la couche est 1)
    for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
      for (int j = 0; j < _resCombi.resistanceTroncons[i].nombreLignesRes; j++) {
        if (_resCombi.resistanceTroncons[i].resTroncon[j].numCouche == 1) {
          trouveDepart = true;
          indiceDepart += j;
          break;
        }
      }
      if (!trouveDepart)
        indiceDepart += _resCombi.resistanceTroncons[i].nombreLignesRes;
      else
        break;
    }

    // On r�cup�re l'indice de fin (on s'arr�te lorsque la pression est
    // strictement n�gative)
    for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
      for (int j = 0; j < _resCombi.resistanceTroncons[i].nombreLignesRes; j++) {
        if (_resCombi.resistanceTroncons[i].resTroncon[j].pression < 0) {
          trouveFin = true;
          indiceFin += j - 1;
          break;
        }
      }
      if (!trouveFin)
        indiceFin += _resCombi.resistanceTroncons[i].nombreLignesRes;
      else
        break;
    }

    for (int i = 0; i < _params.pieu.nombreTroncons; i++) {
      for (int j = 0; j < _resCombi.resistanceTroncons[i].nombreLignesRes; j++) {
        couche[ind] = _resCombi.resistanceTroncons[i].resTroncon[j].numCouche;
        cote[ind] = _resCombi.resistanceTroncons[i].resTroncon[j].cote;
        // pression en kPa
        pression[ind++] = _resCombi.resistanceTroncons[i].resTroncon[j].pression * 10
            / getLargeurPieu(_params);
      }
    }

    // Si toutes les pressions sont positives
    if (indiceFin == tailleTab) {
      indiceFin -= 1;
    }

    for (int i = indiceDepart; i < indiceFin; i++) {
      buteeMobilisee += (pression[i] + pression[i + 1]) * (cote[i] - cote[i + 1]) / 2;
    }

    for (int i = indiceDepart; i < indiceFin; i++) {
      buteeMobilisable += (cote[i] - cote[i + 1])
          * getPressionPalier(_params, _typeCombi, _numCombi, couche[i]);
    }

    return buteeMobilisable / (getCoefMobilisation(_params, _typeCombi) * buteeMobilisee);
  }

  /**
   * retourne la pression palier (en kPa).
   * 
   * @param _numCouche num�ro de la couche de sol concern�e (>= 1)
   */
  public static double getPressionPalier(final SParametresAlbe _params, final int _typeCombi,
      final int _numCombi, final int _numCouche) {

    double res = 0;
    boolean accostage = false;
    boolean defense = false;

    if (_params.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)) {
      accostage = true;
      if (_params.defense.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI))
        defense = true;
    }

    if (_params.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_ELASTO_PLASTIQUE_PURE)
        || _params.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_SETRA)
        || _params.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_TRES_COURTE_DUREE))
      res = _params.sol.courbe[_numCouche - 1].pressionLimite;
    else if (_params.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_COURTE_DUREE)
        || _params.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_LONGUE_DUREE))
      res = _params.sol.courbe[_numCouche - 1].pressionFluage;
    else if (_params.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_MANUELLE))
      res = _params.sol.points[_numCouche - 1][_params.sol.nombrePoints - 1].pression;

    if (_typeCombi == AlbeLib.COMBINAISON_MANUELLE)
      res *= _params.lancementCalculs.gammaMSol;
    else if (_typeCombi == AlbeLib.ELU_FONDA)
      res *= getCoefSol(_params, _typeCombi, _numCombi, accostage, defense);

    return res;
  }

}
