/*
 * @creation 17 nov. 06
 * @modification $Date: 2007-05-04 13:57:29 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicArrowButton;

/**
 * @version $Version$
 */
public class WTreeItem extends JPanel {
  protected boolean expanded_ = true;

  protected boolean enabled_ = true;

  public final static Dimension ExpandedSize = new Dimension(Integer.MAX_VALUE, 100);

  public final static Dimension CollapsedSize = new Dimension(Integer.MAX_VALUE, 30);

  public final static Color BorderColor = new Color(215, 215, 215);

  public final static Color BorderColorSelected = new Color(239, 239, 239);

  protected WTree.TreeItemGUI content_;

  protected JPanel barre_;

  protected BasicArrowButton b1_;

  protected JCheckBox c1_;

  protected AlbeFilleNoteDeCalculs.Settings s_;

  public WTreeItem(final String _txt, final WTree.TreeItemGUI _content) {
    content_ = _content;
    barre_ = new JPanel();
    barre_.setOpaque(false);
    setLayout(new BorderLayout());
    barre_.setLayout(new BorderLayout());
    b1_ = new BasicArrowButton(SwingConstants.SOUTH);
    b1_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        toggleItemView();
      }
    });
    b1_.setPreferredSize(new Dimension(30, 30));
    final JPanel _b1panel = new JPanel();
    _b1panel.setOpaque(false);
    _b1panel.add(b1_);
    _b1panel.setBorder(new EmptyBorder(0, 0, 0, 5));
    barre_.add(_b1panel, BorderLayout.WEST);
    c1_ = new JCheckBox("inclure");
    c1_.setHorizontalTextPosition(SwingConstants.LEFT);
    c1_.setOpaque(false);
    c1_.setSelected(true);
    c1_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        toggleItemEnable();
      }
    });
    barre_.add(c1_, BorderLayout.EAST);
    final JLabel _l1 = new JLabel(_txt);
    _l1.setVerticalAlignment(SwingConstants.CENTER);
    barre_.add(_l1, BorderLayout.CENTER);
    add(barre_, BorderLayout.NORTH);
    add(content_, BorderLayout.CENTER);
    toggleItemView();
    toggleItemEnable();
  }

  protected void toggleItemView(boolean _v) {
    if (!_v) {
      setMinimumSize(null);
      setMaximumSize(CollapsedSize);
      content_.setVisible(false);
      expanded_ = false;
    } else {
      setMaximumSize(null);
      setMinimumSize(ExpandedSize);
      content_.setVisible(true);
      expanded_ = true;
    }
  }

  public void toggleExpand(final boolean _v) {
    toggleItemView(_v);
    toggleItemEnable(_v);
    c1_.setSelected(_v);
  }

  protected void toggleItemView() {
    toggleItemView(!expanded_);
  }

  public void toggleItemEnable(boolean _v) {
    if (!_v) {
      enabled_ = false;
      b1_.setEnabled(false);
      setBorder(new CompoundBorder(new LineBorder(BorderColor, 2, false), new EmptyBorder(2, 2, 2,
          2)));
      setBackground(BorderColor);
      toggleItemView(false);
    } else {
      enabled_ = true;
      b1_.setEnabled(true);
      setBorder(new CompoundBorder(new LineBorder(BorderColorSelected, 2, false), new EmptyBorder(
          2, 2, 2, 2)));
      setBackground(BorderColorSelected);
    }
  }

  protected void toggleItemEnable() {
    toggleItemEnable(!enabled_);
  }

  public AlbeFilleNoteDeCalculs.Settings getConfig() {
    s_ = content_.getConfig();
    s_.stocke(content_.base_, Boolean.valueOf(c1_.isSelected()));
    return s_;
  }
}
