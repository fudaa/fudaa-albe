package org.fudaa.fudaa.albe;

import java.awt.event.KeyEvent;

/**
 * Panneau s'affichant lorsque l'utilisateur choisit la courbe type SETRA dans
 * l'onglet 'Param�tres de sol'.
 * 
 * @author Sabrina Delattre
 */

public class AlbeSolParametresCourbeSetra extends AlbeAbstractSolCourbeElastoSetraFascicule {

  /**
   * Constructeur.
   * 
   * @param _sol
   */

  public AlbeSolParametresCourbeSetra(final AlbeSolParametres _sol) {

    super(_sol, 7, false, true);

  }

  /**
   * calcule et retourne la valeur de x1 (= p/Kh).
   */
  public double calculx1(int _indexCouche, double _rayonPieu) {
    return AlbeLib.textFieldDoubleValue(pressionLimite_[_indexCouche])
        / (2 * calculKh(_indexCouche, _rayonPieu));
  }

  /**
   * 
   */
  public void afficheGraphique(int _indexCouche) {
    afficheGraphiqueCourbe7Pts(_indexCouche);
  }

  /**
   * remplit le tableau de l'onglet _index si toutes les valeurs utiles sont
   * renseign�es.
   */
  public void genereTableauCourbeSetra(int _index) {
    try {
      double pf = AlbeLib.textFieldDoubleValue(pressionLimite_[_index]);
      double module = AlbeLib.textFieldDoubleValue(modulePressiometrique_[_index]);
      double alpha = AlbeLib.textFieldDoubleValue(coeffRheo_[_index]);
      // Coefficient permettant de convertir les m�tres en centim�tres
      int coefConv = 100;
      // Rayon du pieu en m�tre
      double rayonPieu = sol_.fp_.pieu_.getDiametrePieu() / 2;
      double x1 = calculx1(_index, rayonPieu) * coefConv;

      if (pf <= 0 || module <= 0 || alpha < 0 || alpha > 1 || rayonPieu <= 0) {
        graphiqueButton_[_index].setEnabled(false);
        AlbeLib.initialiseTableVide(table_[_index]);
      } else {
        // G�n�ration des valeurs des d�placements (premi�re colonne)
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(-9 * x1)), 0, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(-3 * x1)), 1, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(-x1)), 2, 1);
        table_[_index].setValueAt("0", 3, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(x1)), 4, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(3 * x1)), 5, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(9 * x1)), 6, 1);
        // G�n�ration des valeurs des pressions (deuxi�me colonne)
        table_[_index].setValueAt(Double.toString(-pf), 0, 2);
        table_[_index].setValueAt(Double.toString(-pf), 1, 2);
        table_[_index].setValueAt(Double.toString(-pf / 2), 2, 2);
        table_[_index].setValueAt("0", 3, 2);
        table_[_index].setValueAt(Double.toString(pf / 2), 4, 2);
        table_[_index].setValueAt(Double.toString(pf), 5, 2);
        table_[_index].setValueAt(Double.toString(pf), 6, 2);
        graphiqueButton_[_index].setEnabled(true);
      }
    }
    catch (final NullPointerException _e1) {
      graphiqueButton_[_index].setEnabled(false);
      AlbeLib.initialiseTableVide(table_[_index]);
    }
  }

  /**
   * appell�e automatiquement lorsqu'une touche est relach�e. A chaque fois que
   * cette m�thode est appell�e, on v�rifie si le tableau peut �tre rempli.
   * 
   * @param _evt �v�nement qui a engendr� l'action
   */
  public void keyReleased(KeyEvent _evt) {
    genereTableauCourbeSetra(getCurrentTab());
  }

}
