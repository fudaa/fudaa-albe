/*
 * @creation 26 oct. 06
 * @modification $Date: 2007-11-05 15:28:55 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.dodico.corba.albe.SCoucheCourbe;
import org.fudaa.dodico.corba.albe.SSol;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * classe de base pour les panneaux concernant la courbe �lasto-platique pure,
 * la courbe type Setra et celles du fascicule 62 titre V (courte, tr�s courte
 * et longue dur�e) de l'onglet 'Sol'.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeAbstractSolCourbeElastoSetraFascicule extends AlbeAbstractSolCourbe {

  BuTextField[] pressionLimite_;

  BuTextField[] pressionFluage_;

  BuTextField[] modulePressiometrique_;

  BuTextField[] coeffRheo_;

  JTable[] table_;

  JScrollPane[] scrollPane_;

  /**
   * nombre de lignes du tableau.
   */
  int nbRow_;

  /**
   * bool�en sp�cifiant s'il doit y avoir un champ 'Pression de fluage'.
   */
  boolean presencePressionFluage_;

  /**
   * bool�en sp�cifiant s'il doit y avoir un champ 'Pression limite'.
   */
  boolean presencePressionLimite_;

  /**
   * valeur de x1 (utilis�e lors de la g�n�ration des tableaux des courbes du
   * fascicule courte et longue dur�e).
   */
  double x1CourteLongueDuree_;

  /**
   * Constructeur.
   * 
   * @param _sol
   * @param _nbRow nombre de lignes que doit comporter le tableau
   * @param _pressionFluage pr�cise si il doit y avoir un champ 'Pression de
   *          fluage'
   */

  protected AlbeAbstractSolCourbeElastoSetraFascicule(final AlbeSolParametres _sol, int _nbRow,
      boolean _pressionFluage, boolean _pressionLimite) {

    super(_sol);

    sol_ = _sol;
    nbRow_ = _nbRow;
    presencePressionFluage_ = _pressionFluage;
    presencePressionLimite_ = _pressionLimite;

    initTabbedPane();

    // Ajout des onglets et du bouton au panneau
    this.add(panelSud_);
    this.add(tp_);
  }

  /**
   * initialise les composants.
   */
  protected void initComposants() {

    pressionLimite_ = new BuTextField[AlbeLib.NBMAX_COUCHES];
    pressionFluage_ = new BuTextField[AlbeLib.NBMAX_COUCHES];
    modulePressiometrique_ = new BuTextField[AlbeLib.NBMAX_COUCHES];
    coeffRheo_ = new BuTextField[AlbeLib.NBMAX_COUCHES];
    table_ = new JTable[AlbeLib.NBMAX_COUCHES];
    scrollPane_ = new JScrollPane[AlbeLib.NBMAX_COUCHES];
    graphiqueButton_ = new BuButton[AlbeLib.NBMAX_COUCHES];
    p_ = new BuPanel[AlbeLib.NBMAX_COUCHES];
  }

  /**
   * initialise le panneau d'une couche de sol.
   * 
   * @param _pf
   * @param _num num�ro de la couche de sol
   */

  protected void initCouchePanel(BuPanel _pf, int _num) {

    _pf.setLayout(new BuVerticalLayout());
    initChamps(_num);

    /** Panneau nord. */
    panelNord_[_num] = AlbeLib.borderPanel(AlbeLib.ETCHED_BORDER);
    panelNord_[_num].setLayout(new BorderLayout());

    // //Panneau nord-ouest contenant les champs texte
    final BuPanel panelNordWest = new BuPanel(new SpringLayout());
    panelNordWest.add(new JLabel("Epaisseur"));
    panelNordWest.add(epaisseur_[_num]);
    panelNordWest.add(new JLabel("m"));
    if (presencePressionLimite_) {
      panelNordWest.add(new JLabel("Pression limite"));
      panelNordWest.add(pressionLimite_[_num]);
      panelNordWest.add(new JLabel("kPa"));
    }
    if (presencePressionFluage_) {
      panelNordWest.add(new JLabel("Pression de fluage"));
      panelNordWest.add(pressionFluage_[_num]);
      panelNordWest.add(new JLabel("kPa"));
    }
    panelNordWest.add(new JLabel("Module pressiom�trique"));
    panelNordWest.add(modulePressiometrique_[_num]);
    panelNordWest.add(new JLabel("kPa"));
    panelNordWest.add(new JLabel("Coefficient rh�ologique"));
    panelNordWest.add(coeffRheo_[_num]);
    panelNordWest.add(new JLabel("(-)"));

    if (presencePressionFluage_ && presencePressionLimite_)
      SpringUtilities.makeCompactGrid(panelNordWest, 5, 3, 5, 15, 10, 10);
    else if (presencePressionFluage_ || presencePressionLimite_)
      SpringUtilities.makeCompactGrid(panelNordWest, 4, 3, 5, 15, 10, 10);
    else
      SpringUtilities.makeCompactGrid(panelNordWest, 3, 3, 5, 15, 10, 10);

    // Panneau nord-est contenant les remarques
    panelNordEst_[_num] = AlbeLib.borderPanel(AlbeLib.ETCHED_BORDER);
    panelNordEst_[_num].setLayout(new BuVerticalLayout());
    panelNordEst_[_num] = new BuPanel(new BuVerticalLayout());
    final BuPanel labelPanel = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    labelPanel.setLayout(new BuVerticalLayout());
    final BuLabelMultiLine label = AlbeLib
        .labelMultiLine("Attention, le tableau ne se remplit pas tant que le diam�tre du pieu n'a pas �t� introduit.");
    labelPanel.add(label);
    panelNordEst_[_num].add(labelPanel);

    panelNord_[_num].add(panelNordWest, BorderLayout.WEST);
    panelNord_[_num].add(panelNordEst_[_num], BorderLayout.CENTER);

    /** Panneau centre contenant le tableau pour la saisie des points */
    DefaultTableModel model = new AlbeModeleTableNonEditable(new Object[] { "header-point",
        "header-deplacement", "header-pression" }, nbRow_);
    table_[_num] = new JTable(model);
    BuPanel pt = new BuPanel();
    scrollPane_[_num] = new JScrollPane(table_[_num]);
    scrollPane_[_num].setPreferredSize(new Dimension(370, 23 + 20 * nbRow_));
    table_[_num].getTableHeader().setDefaultRenderer(new AlbeTableCellRenderer());
    AlbeLib.initialiseColonne1JTable(table_[_num]);
    AlbeLib.initialiseTableVide(table_[_num]);
    AlbeLib.changeParametresTable(table_[_num]);
    table_[_num].getColumnModel().getColumn(0).setPreferredWidth(30);
    pt.add(scrollPane_[_num]);

    /** Panneau sud contenant le bouton 'Graphique' */
    BuPanel pb = new BuPanel();
    graphiqueButton_[_num] = AlbeLib.buttonId("Graphique", "graphe_32");
    graphiqueButton_[_num].setToolTipText("Visualiser la courbe d�placement-pression");
    graphiqueButton_[_num].setEnabled(false);
    graphiqueButton_[_num].addActionListener(this);
    pb.add(graphiqueButton_[_num]);

    /** Ajout des 3 panneaux au panneau final */
    _pf.add(panelNord_[_num]);
    _pf.add(pt);
    _pf.add(pb);
  }

  /**
   * initialise les champs pour une couche.
   * 
   * @param _num indice de la couche
   */

  public void initChamps(int _num) {
    pressionLimite_[_num] = AlbeLib.doubleField(AlbeLib.FORMAT001);
    pressionFluage_[_num] = AlbeLib.doubleField(AlbeLib.FORMAT001);
    modulePressiometrique_[_num] = AlbeLib.doubleField(AlbeLib.FORMAT001);
    coeffRheo_[_num] = AlbeLib.doubleField(AlbeLib.FORMAT001);

    pressionLimite_[_num].setHorizontalAlignment(SwingConstants.CENTER);
    pressionFluage_[_num].setHorizontalAlignment(SwingConstants.CENTER);
    modulePressiometrique_[_num].setHorizontalAlignment(SwingConstants.CENTER);
    coeffRheo_[_num].setHorizontalAlignment(SwingConstants.CENTER);

    pressionLimite_[_num].addKeyListener(this);
    pressionFluage_[_num].addKeyListener(this);
    modulePressiometrique_[_num].addKeyListener(this);
    coeffRheo_[_num].addKeyListener(this);
    pressionLimite_[_num].addFocusListener(this);
    pressionFluage_[_num].addFocusListener(this);
    modulePressiometrique_[_num].addFocusListener(this);
    coeffRheo_[_num].addFocusListener(this);

    epaisseur_[_num].setColumns(6);
  }

  /**
   * efface les donn�es saisies pour chaque couche de sol � partir de la couche
   * _n1 jusqu'� la couche _n2 (si _n1 = 1 et _n2 = 3, on efface les donn�es des
   * couches n�1, n�2 et n�3) .
   */

  protected void eraseDataCouches(int _n1, int _n2) {
    for (int i = _n1 - 1; i < _n2; i++) {

      epaisseur_[i].setText("0");

      if (presencePressionLimite_)
        pressionLimite_[i].setText("0");

      if (presencePressionFluage_)
        pressionFluage_[i].setText("0");

      modulePressiometrique_[i].setText("0");
      coeffRheo_[i].setText("0");

      AlbeLib.initialiseTableVide(table_[i]);

      graphiqueButton_[i].setEnabled(false);

    }
  }

  /**
   * retourne vrai si tous les champs texte de la couche i (on ne prend pas en
   * compte l'�paisseur) ont une valeur nulle.
   */

  public boolean champsTousNuls(int _indexCouche) {
    boolean b = false;

    if (presencePressionFluage_ && presencePressionLimite_
        && AlbeLib.textFieldDoubleValue(pressionLimite_[_indexCouche]) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(pressionLimite_[_indexCouche]) == 0
        && AlbeLib.textFieldDoubleValue(pressionFluage_[_indexCouche]) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(pressionFluage_[_indexCouche]) == 0
        && AlbeLib.textFieldDoubleValue(modulePressiometrique_[_indexCouche]) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(modulePressiometrique_[_indexCouche]) == 0
        && AlbeLib.textFieldDoubleValue(coeffRheo_[_indexCouche]) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(coeffRheo_[_indexCouche]) == 0)
      b = true;
    else if (presencePressionLimite_
        && AlbeLib.textFieldDoubleValue(pressionLimite_[_indexCouche]) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(pressionLimite_[_indexCouche]) == 0
        && AlbeLib.textFieldDoubleValue(modulePressiometrique_[_indexCouche]) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(modulePressiometrique_[_indexCouche]) == 0
        && AlbeLib.textFieldDoubleValue(coeffRheo_[_indexCouche]) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(coeffRheo_[_indexCouche]) == 0)
      b = true;
    else if (presencePressionFluage_
        && AlbeLib.textFieldDoubleValue(pressionFluage_[_indexCouche]) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(pressionFluage_[_indexCouche]) == 0
        && AlbeLib.textFieldDoubleValue(modulePressiometrique_[_indexCouche]) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(modulePressiometrique_[_indexCouche]) == 0
        && AlbeLib.textFieldDoubleValue(coeffRheo_[_indexCouche]) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(coeffRheo_[_indexCouche]) == 0)
      b = true;

    return b;
  }

  /**
   * retourne vrai si les donn�es de la courbe ont commenc� � �tre remplies par
   * l'utilisateur.
   */

  protected boolean courbeCompletee() {

    for (int i = 0; i < sol_.nbCouches_; i++) {

      if (AlbeLib.textFieldDoubleValue(epaisseur_[i]) == VALEUR_NULLE.value
          || (!AlbeLib.emptyField(epaisseur_[i]) && AlbeLib.textFieldDoubleValue(epaisseur_[i]) != 0)
          || AlbeLib.textFieldDoubleValue(modulePressiometrique_[i]) == VALEUR_NULLE.value
          || (!AlbeLib.emptyField(modulePressiometrique_[i]) && AlbeLib
              .textFieldDoubleValue(modulePressiometrique_[i]) != 0)
          || AlbeLib.textFieldDoubleValue(coeffRheo_[i]) == VALEUR_NULLE.value
          || (!AlbeLib.emptyField(coeffRheo_[i]) && AlbeLib.textFieldDoubleValue(coeffRheo_[i]) != 0))
        return true;

      if (presencePressionFluage_) {
        if (AlbeLib.textFieldDoubleValue(pressionFluage_[i]) == VALEUR_NULLE.value
            || (!AlbeLib.emptyField(pressionFluage_[i]) && AlbeLib
                .textFieldDoubleValue(pressionFluage_[i]) != 0))
          return true;
      }

      if (presencePressionLimite_) {
        if (AlbeLib.textFieldDoubleValue(pressionLimite_[i]) == VALEUR_NULLE.value
            || (!AlbeLib.emptyField(pressionLimite_[i]) && AlbeLib
                .textFieldDoubleValue(pressionLimite_[i]) != 0))
          return true;
      }
    }
    return false;
  }

  /**
   * calcule et retourne la valeur de Kh (utile pour la courbe �lasto-plastique
   * pure et Setra).
   */

  protected double calculKh(int _indexCouche, double _rayonPieu) {

    double alpha = AlbeLib.textFieldDoubleValue(coeffRheo_[_indexCouche]);
    double d1 = Math.pow(2.65 * _rayonPieu / 0.3, alpha);
    double denom = 0.4 * d1 + alpha * _rayonPieu;

    return (3 * AlbeLib.textFieldDoubleValue(modulePressiometrique_[_indexCouche])) / denom;
  }

  /**
   * calcule et retourne la valeur de Kf pour les courbes du fascicule courte et
   * tr�s courte dur�e.
   */

  protected double calculKfCourteTresCourteDuree(int _indexCouche, double _dimPieu) {

    double kf;

    // Num�rateur
    double num = 12 * AlbeLib.textFieldDoubleValue(modulePressiometrique_[_indexCouche]);

    // D�nominateur
    double alpha = AlbeLib.textFieldDoubleValue(coeffRheo_[_indexCouche]);
    double d1 = 0.6 / _dimPieu;
    double d2 = Math.pow(2.65 / d1, alpha);

    double denom;

    if (_dimPieu >= 0.6)
      denom = (double) 4 / 3 * d1 * d2 + alpha;

    else
      denom = (double) 4 / 3 * Math.pow(2.65, alpha) + alpha;

    kf = num / denom;

    return kf;
  }

  /**
   * calcule et retourne la valeur de x1 pour les courbes du fascicule courte et
   * tr�s courte dur�e.
   */

  protected double calculx1CourteTresCourteDuree(int _indexCouche, double _dimPieu) {

    return AlbeLib.textFieldDoubleValue(pressionFluage_[_indexCouche])
        / calculKfCourteTresCourteDuree(_indexCouche, _dimPieu);

  }

  /**
   * remplit le tableau de l'onglet _index des courbes du fascicule courte et
   * longue dur�e si toutes les valeurs utiles sont renseign�es.
   */
  public void genereTableauxCourteEtLongueDuree(String _s, int _index) {
    try {

      double pf = AlbeLib.textFieldDoubleValue(pressionFluage_[_index]);
      double module = AlbeLib.textFieldDoubleValue(modulePressiometrique_[_index]);
      double alpha = AlbeLib.textFieldDoubleValue(coeffRheo_[_index]);
      double dimPieu = sol_.fp_.pieu_.getDiametrePieu();
      // Coefficient permettant de convertir les m�tres en centim�tres
      int coefConv = 100;

      if (pf <= 0 || module <= 0 || alpha < 0  || alpha > 1 || dimPieu <= 0) {
        graphiqueButton_[_index].setEnabled(false);
        AlbeLib.initialiseTableVide(table_[_index]);
      } else {
        if (_s.equals(AlbeLib.TABLEAU_COURTE_DUREE))
          x1CourteLongueDuree_ = calculx1CourteTresCourteDuree(_index, dimPieu) * coefConv;
        else if (_s.equals(AlbeLib.TABLEAU_LONGUE_DUREE))
          x1CourteLongueDuree_ = 2 * calculx1CourteTresCourteDuree(_index, dimPieu) * coefConv;

        // G�n�ration des valeurs des d�placements (premi�re colonne)
        table_[_index].setValueAt(Double
            .toString(AlbeLib.arronditDouble(-3 * x1CourteLongueDuree_)), 0, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(-x1CourteLongueDuree_)),
            1, 1);
        table_[_index].setValueAt("0", 2, 1);
        table_[_index].setValueAt(Double.toString(AlbeLib.arronditDouble(x1CourteLongueDuree_)), 3,
            1);
        table_[_index].setValueAt(
            Double.toString(AlbeLib.arronditDouble(3 * x1CourteLongueDuree_)), 4, 1);
        // G�n�ration des valeurs des pressions (deuxi�me colonne)
        table_[_index].setValueAt(Double.toString(-pf), 0, 2);
        table_[_index].setValueAt(Double.toString(-pf), 1, 2);
        table_[_index].setValueAt("0", 2, 2);
        table_[_index].setValueAt(Double.toString(pf), 3, 2);
        table_[_index].setValueAt(Double.toString(pf), 4, 2);
        graphiqueButton_[_index].setEnabled(true);
      }
    }
    catch (final NullPointerException _e1) {
      graphiqueButton_[_index].setEnabled(false);
      AlbeLib.initialiseTableVide(table_[_index]);
    }
  }

  /**
   * retourne l'abscisse maximale � afficher pour les courbes contenant 5 points
   * (courbes �lasto-platique pure et du fascicule courte et longue dur�e).
   */
  protected double getMaxXCourbe5Pts(JTable _t) {
    return Double.parseDouble(_t.getValueAt(4, 1).toString()) + getPasXCourbe5Pts(_t);
  }

  /**
   * retourne le pas � afficher pour l'axe horizontal des courbes contenant 5
   * points.
   */
  protected double getPasXCourbe5Pts(JTable _t) {

    return Double.parseDouble(_t.getValueAt(4, 1).toString()) / 2;

  }

  /**
   * retourne l'ordonn�e maximale � afficher pour les courbes contenant 5
   * points.
   */
  protected double getMaxYCourbe5Pts(JTable _t) {

    return (int) (1.2 * Double.parseDouble(_t.getValueAt(3, 2).toString()));
  }

  /**
   * retourne le pas � afficher pour l'axe vertical pour les courbes contenant 5
   * points.
   */
  protected double getPasYCourbe5Pts(JTable _t) {
    return (int) (getMaxYCourbe5Pts(_t) / 5);
  }

  /**
   * retourne l'abscisse maximale � afficher pour les courbes contenant 7 points
   * (courbe Setra et du fascicule tr�s courte dur�e).
   */
  public double getMaxXCourbe7Pts(JTable _t) {

    double max = 0;
    while (max <= Double.parseDouble(_t.getValueAt(6, 1).toString())) {
      max += Double.parseDouble(_t.getValueAt(4, 1).toString());
    }
    return max;

  }

  /**
   * retourne le pas � afficher pour l'axe horizontal pour les courbes contenant
   * 7 points.
   */
  public double getPasXCourbe7Pts(JTable _t) {

    return getMaxXCourbe7Pts(_t) / 2;
  }

  /**
   * retourne l'ordonn�e maximale � afficher pour les courbes contenant 7
   * points.
   */
  public double getMaxYCourbe7Pts(JTable _t) {

    return (int) (1.2 * Double.parseDouble(_t.getValueAt(5, 2).toString()));
  }

  /**
   * retourne le pas � afficher pour l'axe vertical pour les courbes contenant 7
   * points.
   */
  public double getPasYCourbe7Pts(JTable _t) {

    return (int) (getMaxYCourbe7Pts(_t) / 6);
  }

  /**
   * 
   */
  protected String getScript(JTable _t, int _numCouche, double _maxX, double _pasX, double _maxY,
      double _pasY) {
    final StringBuffer s = new StringBuffer();

    s.append(sol_.getScriptCourbeSol(_numCouche, -_maxX, _maxX, _pasX, -_maxY, _maxY, _pasY));
    for (int i = 0; i < _t.getRowCount(); i++) {
      s.append("      " + _t.getValueAt(i, 1) + " " + _t.getValueAt(i, 2) + "\n");
    }
    s.append("    }\n");
    s.append("  }\n");
    return s.toString();
  }

  /**
   * 
   */
  protected String getScriptCourbe5pts(int _indexCouche) {
    return getScript(table_[_indexCouche], _indexCouche + 1,
        getMaxXCourbe5Pts(table_[_indexCouche]), getPasXCourbe5Pts(table_[_indexCouche]),
        getMaxYCourbe5Pts(table_[_indexCouche]), getPasYCourbe5Pts(table_[_indexCouche]));
  }

  /**
   * 
   */
  protected String getScriptCourbe7pts(int _indexCouche) {
    return getScript(table_[_indexCouche], _indexCouche + 1,
        getMaxXCourbe7Pts(table_[_indexCouche]), getPasXCourbe7Pts(table_[_indexCouche]),
        getMaxYCourbe7Pts(table_[_indexCouche]), getPasYCourbe7Pts(table_[_indexCouche]));
  }

  /**
   * affiche le graphique concernant la couche dont l'indice est plac� en
   * param�tre.
   */
  protected void afficheGraphique(int _indexCouche) {}

  /**
   * affiche le graphique pour les courbes contenant 5 points.
   */
  protected void afficheGraphiqueCourbe5Pts(int _indexCouche) {
    AlbeLib.showGraphViewer(getApplication(), getImplementation(),
        "Mod�lisation du comportement du sol", getScriptCourbe5pts(_indexCouche));
  }

  /**
   * affiche le graphique pour les courbes contenant 7 points.
   */
  public void afficheGraphiqueCourbe7Pts(int _indexCouche) {
    AlbeLib.showGraphViewer(getApplication(), getImplementation(),
        "Mod�lisation du comportement du sol", getScriptCourbe7pts(_indexCouche));
  }

  /**
   * renvoie une structure contenant les donn�es pour la couche de sol _n.
   */
  protected SCoucheCourbe getParametresCouche(final int _n) {
    final SCoucheCourbe p = (SCoucheCourbe) AlbeLib.createIDLObject(SCoucheCourbe.class);

    p.epaisseur = AlbeLib.textFieldDoubleValue(epaisseur_[_n - 1]);

    if (presencePressionLimite_) {

      p.pressionLimite = AlbeLib.textFieldDoubleValue(pressionLimite_[_n - 1]);

    }

    if (presencePressionFluage_) {

      p.pressionFluage = AlbeLib.textFieldDoubleValue(pressionFluage_[_n - 1]);

    }

    p.modulePressiometrique = AlbeLib.textFieldDoubleValue(modulePressiometrique_[_n - 1]);

    p.coefficientRheologique = AlbeLib.textFieldDoubleValue(coeffRheo_[_n - 1]);

    if (graphiqueButton_[_n - 1].isEnabled())
      p.graphiqueDisponible = true;
    else
      p.graphiqueDisponible = false;

    return p;
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es pour la
   * courbe.
   */
  protected SCoucheCourbe[] getParametresCourbe() {

    SCoucheCourbe[] c = null;

    try {
      c = new SCoucheCourbe[sol_.nbCouches_];
    }
    catch (final NegativeArraySizeException _e2) {
      System.err.println(AlbeMsg.CONS015);
    }
    for (int i = 0; i < sol_.nbCouches_; i++) {
      try {
        c[i] = getParametresCouche(i + 1);
      }
      catch (final NullPointerException _e1) {
        System.err.println(AlbeMsg.CONS016);
      }
      catch (final ArrayIndexOutOfBoundsException _e2) {
        System.err.println(AlbeMsg.CONS017);
        return null;
      }
    }
    return c;
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres dans
   * l'onglet de la couche de sol _n.
   * 
   * @param _c structure de donn�es contenant les param�tres (donn�es) �
   *          importer
   * @param _n num�ro de la couche de sol
   */
  protected synchronized void setParametresCouche(SCoucheCourbe _c, SSol _paramsSol, final int _n) {
    if (_c == null)
      _c = (SCoucheCourbe) AlbeLib.createIDLObject(SCoucheCourbe.class);

    if (_c.epaisseur != VALEUR_NULLE.value) {
      epaisseur_[_n - 1].setValue(CtuluLib.getDouble(_c.epaisseur));
    }

    if (presencePressionLimite_) {
      if (_c.pressionLimite != VALEUR_NULLE.value)
        pressionLimite_[_n - 1].setValue(CtuluLib.getDouble(_c.pressionLimite));
    }

    if (presencePressionFluage_) {
      if (_c.pressionFluage != VALEUR_NULLE.value)
        pressionFluage_[_n - 1].setValue(CtuluLib.getDouble(_c.pressionFluage));
    }

    if (_c.modulePressiometrique != VALEUR_NULLE.value)
      modulePressiometrique_[_n - 1].setValue(CtuluLib.getDouble(_c.modulePressiometrique));

    if (_c.coefficientRheologique != VALEUR_NULLE.value)
      coeffRheo_[_n - 1].setValue(CtuluLib.getDouble(_c.coefficientRheologique));

    if (_c.graphiqueDisponible)
      graphiqueButton_[_n - 1].setEnabled(true);
    else
      graphiqueButton_[_n - 1].setEnabled(false);
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres pour
   * la courbe.
   */
  protected synchronized void setParametresCourbe(SCoucheCourbe[] _c, SSol _paramsSol) {

    int n = 0;
    try {
      n = _c.length;

      for (int i = 0; i < n; i++) {
        setParametresCouche(_c[i], _paramsSol, i + 1);
      }
    }
    catch (final NullPointerException _e1) {
      System.err.println(AlbeMsg.CONS018);
    }

    // Si l'�paisseur de la couche 1 est remplie, on affiche le label (sinon il
    // apparaitra lorsque la zone de texte obtiendra le focus)
    if (_c[0].epaisseur != VALEUR_NULLE.value)
      infoEpaisseurLabel_.setVisible(true);

  }

  /**
   * 
   */
  public boolean paramsCoucheCourbeModified(SCoucheCourbe _p, SSol _paramsSol, int _n) {
    SCoucheCourbe p2 = getParametresCouche(_n);
    if (_p.epaisseur != p2.epaisseur || _p.pressionLimite != p2.pressionLimite
        || _p.modulePressiometrique != p2.modulePressiometrique
        || _p.coefficientRheologique != p2.coefficientRheologique)
      return true;
    if (_paramsSol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_COURTE_DUREE)
        || _paramsSol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_TRES_COURTE_DUREE)
        || _paramsSol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_LONGUE_DUREE)) {
      if (_p.pressionFluage != p2.pressionFluage)
        return true;
    }
    return false;
  }

  /**
   * 
   */
  public boolean paramsCourbeModified(SCoucheCourbe[] _p, SSol _paramsSol) {

    for (int i = 0; i < sol_.nbCouches_; i++) {
      if (paramsCoucheCourbeModified(_p[i], _paramsSol, i + 1))
        return true;
    }
    return false;
  }

  /** 
   * 
   */
  protected ValidationMessages validation() {
    final ValidationMessages vm = new ValidationMessages();

    for (int j = 0; j < sol_.nbCouches_; j++) {
      // On v�rifie qu'aucun champ texte n'est vide
      if (AlbeLib.emptyField(epaisseur_[j]) || AlbeLib.emptyField(modulePressiometrique_[j])
          || AlbeLib.emptyField(coeffRheo_[j])
          || (presencePressionLimite_ && AlbeLib.emptyField(pressionLimite_[j]))
          || presencePressionFluage_ && AlbeLib.emptyField(pressionFluage_[j]))
        vm.add(AlbeMsg.getMsgDonneesManquantesSol(j + 1));

      else
      // On v�rifie que les champs texte comprennent des nombres
      if (AlbeLib.textFieldDoubleValue(epaisseur_[j]) == VALEUR_NULLE.value
          || AlbeLib.textFieldDoubleValue(modulePressiometrique_[j]) == VALEUR_NULLE.value
          || AlbeLib.textFieldDoubleValue(coeffRheo_[j]) == VALEUR_NULLE.value
          || (presencePressionLimite_ && AlbeLib.textFieldDoubleValue(pressionLimite_[j]) == VALEUR_NULLE.value)
          || (presencePressionFluage_ && AlbeLib.textFieldDoubleValue(pressionFluage_[j]) == VALEUR_NULLE.value))
        vm.add(AlbeMsg.getMsgDonneesNonValidesSol(j + 1));

      // On v�rifie que pression de fluage < pression limite

      if (AlbeLib.textFieldDoubleValue(pressionFluage_[j]) != VALEUR_NULLE.value
          && AlbeLib.textFieldDoubleValue(pressionLimite_[j]) != VALEUR_NULLE.value
          && AlbeLib.textFieldDoubleValue(pressionFluage_[j]) >= AlbeLib
              .textFieldDoubleValue(pressionLimite_[j]))
        vm.add(AlbeMsg.getMsgPfSupPl(j + 1));

      // On v�rifie que les valeurs sont positives
      // // Epaisseur
      if (AlbeLib.textFieldDoubleValue(epaisseur_[j]) != VALEUR_NULLE.value
          && AlbeLib.textFieldDoubleValue(epaisseur_[j]) <= 0)
        vm.add(AlbeMsg.getMsgEpaisseurNeg(j + 1));

      // //Pression limite
      if (presencePressionLimite_
          && AlbeLib.textFieldDoubleValue(pressionLimite_[j]) != VALEUR_NULLE.value
          && AlbeLib.textFieldDoubleValue(pressionLimite_[j]) <= 0)
        vm.add(AlbeMsg.getMsgPressionLimiteNeg(j + 1));

      // //Pression limite
      if (presencePressionFluage_
          && AlbeLib.textFieldDoubleValue(pressionFluage_[j]) != VALEUR_NULLE.value
          && AlbeLib.textFieldDoubleValue(pressionFluage_[j]) <= 0)
        vm.add(AlbeMsg.getMsgPressionFluageNeg(j + 1));

      // //Module pressiom�trique
      if (AlbeLib.textFieldDoubleValue(modulePressiometrique_[j]) != VALEUR_NULLE.value
          && AlbeLib.textFieldDoubleValue(modulePressiometrique_[j]) <= 0)
        vm.add(AlbeMsg.getMsgModuleNeg(j + 1));

      // //Coefficient rh�ologique
      if (AlbeLib.textFieldDoubleValue(coeffRheo_[j]) != VALEUR_NULLE.value
          && (AlbeLib.textFieldDoubleValue(coeffRheo_[j]) < 0 || AlbeLib
              .textFieldDoubleValue(coeffRheo_[j]) > 1))
        vm.add(AlbeMsg.getMsgAlphaNeg(j + 1));
    }
    return vm;
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {
    int currentTab = tp_.getSelectedIndex();

    if (_evt.getSource() == initButton_) {
      eraseDataCouches(1, sol_.nbCouches_);
      majMessages();
    } else if (_evt.getSource() == graphiqueButton_[currentTab]) {
      // Affichage du graphique
      afficheGraphique(currentTab);
    }
  }
}
