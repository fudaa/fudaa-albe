package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import org.fudaa.dodico.corba.albe.SGeometrique;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * Description de l'onglet des param�tres g�om�triques.
 * 
 * @author Sabrina Delattre
 */

public class AlbeGeometriquesParametres extends AlbeAbstractOnglet {

  BuTextField coteTete_, coteDragage_, coteCalcul_;

  protected JComboBox nbCouchesCombo_;

  BuButton aideButton_;

  /**
   * r�f�rence vers le parent.
   */
  AlbeFilleParametres fp_;

  /**
   * Constructeur.
   * 
   * @param _fp
   */
  public AlbeGeometriquesParametres(final AlbeFilleParametres _fp) {

    super(_fp.getImplementation(), _fp, AlbeMsg.URL001);

    fp_ = _fp;

    initFields();

    /** Bloc 1 : information et bouton 'Aide'. */
    BuPanel b1 = new BuPanel(new BorderLayout());
    BuPanel infoPanel = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    infoPanel.add(new JLabel("Les cotes sont d�croissantes vers le bas."));
    BuPanel aidePanel = new BuPanel(new FlowLayout());
    aideButton_ = AlbeLib.aideButton();
    aideButton_.addActionListener(this);
    aidePanel.add(aideButton_);
    b1.add(infoPanel, BorderLayout.CENTER);
    b1.add(aidePanel, BorderLayout.EAST);

    /** Bloc 2 : t�te de l'ouvrage */
    BuPanel b2 = AlbeLib.titledPanel("T�te de l'ouvrage", AlbeLib.ETCHED_BORDER);
    b2.setLayout(new SpringLayout());
    b2.add(new JLabel("Cote de la t�te de l'ouvrage"));
    b2.add(coteTete_);
    b2.add(new JLabel("m"));

    SpringUtilities.makeCompactGrid(b2, 1, 3, 15, 20, 10, 20);

    /** Bloc 3 : cotes de calcul */
    BuPanel b3 = AlbeLib.titledPanel("Cotes de dragage", AlbeLib.ETCHED_BORDER);
    b3.setLayout(new SpringLayout());
    b3.add(new JLabel("Cote de dragage du bassin"));
    b3.add(coteDragage_);
    b3.add(new JLabel("m"));
    b3.add(new JLabel("Cote de calcul"));
    b3.add(coteCalcul_);
    b3.add(new JLabel("m"));

    SpringUtilities.makeCompactGrid(b3, 2, 3, 15, 20, 10, 20);

    /** Bloc 4 : couches de sol */
    BuPanel b4 = AlbeLib.titledPanel("Couche(s) de sol", AlbeLib.ETCHED_BORDER);
    JLabel nbCouchesLabel = new JLabel("Nombre de couche(s) de sol (de 1 � "
        + AlbeLib.NBMAX_COUCHES + ")");

    // Initialisation de la boite combo
    nbCouchesCombo_ = new JComboBox();
    for (int i = 0; i < AlbeLib.NBMAX_COUCHES; i++) {
      nbCouchesCombo_.addItem(Integer.toString(i + 1));
    }
    nbCouchesCombo_.addActionListener(this);
    b4.add(nbCouchesLabel);
    b4.add(nbCouchesCombo_);

    /** Panneau 'Param�tres g�om�triques */
    BuPanel p = AlbeLib.titledPanel("Param�tres g�om�triques", AlbeLib.COMPOUND_BORDER);
    p.setLayout(new BuVerticalLayout());
    p.add(b1);
    p.add(b2);
    p.add(b3);
    p.add(b4);

    this.setLayout(new FlowLayout());
    this.add(p);

  }// Fin constructeur

  /**
   * initialise les champs texte.
   */

  public void initFields() {

    coteTete_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    coteDragage_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    coteCalcul_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    coteTete_.setHorizontalAlignment(SwingConstants.CENTER);
    coteDragage_.setHorizontalAlignment(SwingConstants.CENTER);
    coteCalcul_.setHorizontalAlignment(SwingConstants.CENTER);
    coteTete_.addKeyListener(this);
    coteDragage_.addKeyListener(this);
    coteCalcul_.addKeyListener(this);
    coteTete_.addFocusListener(this);
    coteDragage_.addFocusListener(this);
    coteCalcul_.addFocusListener(this);
    coteTete_.setColumns(8);
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans
   * l'onglet 'Param�tres g�om�triques'.
   */

  public SGeometrique getParametresGeo() {
    final SGeometrique p = (SGeometrique) AlbeLib.createIDLObject(SGeometrique.class);

    p.coteTeteOuvrage = AlbeLib.textFieldDoubleValue(coteTete_);
    p.coteDragageBassin = AlbeLib.textFieldDoubleValue(coteDragage_);
    p.coteCalcul = AlbeLib.textFieldDoubleValue(coteCalcul_);
    p.nombreCouchesSol = nbCouchesCombo_.getSelectedIndex() + 1;
    return p;
  }

  /**
   * importe dans l'onglet 'Param�tres g�om�triques' les donn�es contenues dans
   * la structure pass�e en param�tre.
   */
  public synchronized void setParametresGeo(SGeometrique _p) {
    if (_p == null)
      _p = (SGeometrique) AlbeLib.createIDLObject(SGeometrique.class);

    if (_p.coteTeteOuvrage != VALEUR_NULLE.value)
      coteTete_.setValue(new Double(_p.coteTeteOuvrage));

    if (_p.coteDragageBassin != VALEUR_NULLE.value)
      coteDragage_.setValue(new Double(_p.coteDragageBassin));

    if (_p.coteCalcul != VALEUR_NULLE.value)
      coteCalcul_.setValue(new Double(_p.coteCalcul));

    nbCouchesCombo_.setSelectedIndex(_p.nombreCouchesSol - 1);
  }

  /**
   * retourne vrai si la structure pass�e en param�tre est diff�rente de celle
   * actuelle.
   */
  public boolean paramsGeoModified(SGeometrique _p) {
    SGeometrique p2 = getParametresGeo();
    if (_p.coteTeteOuvrage != p2.coteTeteOuvrage || _p.coteDragageBassin != p2.coteDragageBassin
        || _p.coteCalcul != p2.coteCalcul || _p.nombreCouchesSol != p2.nombreCouchesSol)
      return true;
    return false;
  }

  /**
   * 
   */
  public ValidationMessages validation() {
    final ValidationMessages vm = new ValidationMessages();

    boolean cT = false;
    boolean cD = false;
    boolean cC = false;

    if (AlbeLib.emptyField(coteTete_))
      vm.add(AlbeMsg.VMSG001);
    else if (AlbeLib.textFieldDoubleValue(coteTete_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG004);
    else
      cT = true;

    if (AlbeLib.emptyField(coteDragage_))
      vm.add(AlbeMsg.VMSG002);
    else if (AlbeLib.textFieldDoubleValue(coteDragage_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG005);
    else
      cD = true;

    if (AlbeLib.emptyField(coteCalcul_))
      vm.add(AlbeMsg.VMSG003);
    else if (AlbeLib.textFieldDoubleValue(coteCalcul_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG006);
    else
      cC = true;

    if (cT && cD
        && AlbeLib.textFieldDoubleValue(coteTete_) <= AlbeLib.textFieldDoubleValue(coteDragage_))
      vm.add(AlbeMsg.VMSG007);

    if (cT && cC
        && AlbeLib.textFieldDoubleValue(coteTete_) <= AlbeLib.textFieldDoubleValue(coteCalcul_))
      vm.add(AlbeMsg.VMSG008);

    if (cC && cD
        && AlbeLib.textFieldDoubleValue(coteCalcul_) >= AlbeLib.textFieldDoubleValue(coteDragage_))
      vm.add(AlbeMsg.VMSG009);

    double epCouche1 = fp_.sol_.getEpaisseurCouche1();
    if (cD
        && cC
        && epCouche1 != VALEUR_NULLE.value
        && Math.abs(AlbeLib.textFieldDoubleValue(coteDragage_)
            - AlbeLib.textFieldDoubleValue(coteCalcul_)) >= epCouche1)
      vm.add(AlbeMsg.VMSG010);

    double longPieu = fp_.pieu_.getLongueurPieu();
    if (cT
        && cD
        && cC
        && longPieu != VALEUR_NULLE.value
        && (AlbeLib.textFieldDoubleValue(coteTete_) - fp_.pieu_.getLongueurPieu()) > Math.max(
            AlbeLib.textFieldDoubleValue(coteDragage_), AlbeLib.textFieldDoubleValue(coteCalcul_)))
      vm.add(AlbeMsg.VMSG012);

    return vm;
  }

  /**
   *
   */
  public void keyPressed(KeyEvent _evt) {
    if (_evt.getKeyCode() == KeyEvent.VK_ENTER) {
      _evt.getComponent().transferFocus();
    }
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {
    if (_evt.getSource() == nbCouchesCombo_)
      fp_.sol_.nbCouches_ = nbCouchesCombo_.getSelectedIndex() + 1;
    else if (_evt.getSource() == aideButton_)
      aide();
  }

}
