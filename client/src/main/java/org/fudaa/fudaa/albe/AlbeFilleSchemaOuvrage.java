/*
 * @creation 22 janv. 07
 * @modification $Date: 2007-06-08 14:08:57 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;

/**
 * Classe permettant de visualiser un sch�ma de l'ouvrage.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeFilleSchemaOuvrage extends AlbeInternalFrameAdapter {

  BuButton fermerButton_, exporterButton_, actualiserButton_;

  AlbeSchemaOuvragePanel schema_;

  SParametresAlbe params_;

  BuPanel panelImage_;

  public AlbeFilleSchemaOuvrage(final BuCommonInterface _appli, final FudaaProjet _project,
      final SParametresAlbe _params) {
    super("Sch�ma de l'ouvrage", "avantplan", _appli, _project);

    params_ = _params;
    Container conteneur = getContentPane();
    conteneur.setLayout(new BuVerticalLayout());

    // Panneau information
    final BuPanel infoPanel = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    infoPanel.setLayout(new BuVerticalLayout());
    BuLabelMultiLine label = AlbeLib
        .labelMultiLine("Veuillez trouver ci dessous un sch�ma � l'echelle de l'ouvrage.\n"
            + "Vous avez la possibilit� d'exporter cette image.");
    infoPanel.add(label);

    // Panneau contenant l'image
    panelImage_ = AlbeLib.titledPanel("Sch�ma de l'ouvrage", AlbeLib.BEVEL_BORDER);
    schema_ = new AlbeSchemaOuvragePanel(_appli, params_);
    panelImage_.add(schema_);

    // Panneau contenant les boutons 'Actualiser', 'Exporter' et 'Fermer'
    final BuPanel pButtons = new BuPanel();
    pButtons.setLayout(new FlowLayout());

    actualiserButton_ = AlbeLib.buttonId("Rafra�chir", "rafraichir");
    fermerButton_ = AlbeLib.buttonId("Fermer", "ANNULER");
    exporterButton_ = AlbeLib.buttonId("Exporter", "EXPORTER");
    actualiserButton_.setToolTipText("Mettre � jour le sch�ma");
    fermerButton_.setToolTipText("Fermer la fen�tre");
    exporterButton_.setToolTipText("Exporter le sch�ma");
    actualiserButton_.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        actualiserSchema();
      }
    });
    fermerButton_.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        dispose();
      }
    });
    exporterButton_.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent _evt) {
        schema_.exporter();
      }
    });
    pButtons.add(actualiserButton_);
    pButtons.add(exporterButton_);
    pButtons.add(fermerButton_);

    conteneur.add(infoPanel);
    conteneur.add(pButtons);
    conteneur.add(panelImage_);

    pack();
  }

  /**
   * actualise le sch�ma de l'ouvrage.
   */
  public void actualiserSchema() {
    SParametresAlbe params = ((AlbeImplementation) getApplication().getImplementation()).fp_
        .getParametres();
    panelImage_.removeAll();
    if (params.geo.coteTeteOuvrage == VALEUR_NULLE.value
        || params.geo.coteDragageBassin == VALEUR_NULLE.value
        || (params.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE) ? params.pieu.tronconsTubulaire[0].longueur
            : params.pieu.tronconsCaisson[0].longueur) == VALEUR_NULLE.value){
      AlbeLib.dialogMessage(appli_, AlbeMsg.DLG009);
      dispose();
    }
     
    else {
      schema_ = new AlbeSchemaOuvragePanel(appli_, params);
      panelImage_.add(schema_);
    }
    pack();
  }

}
