/*
 * @creation 9 nov. 06
 * @modification $Date: 2007-06-08 14:08:57 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.FlowLayout;
import java.awt.event.FocusEvent;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * classe de base pour les diff�rents panneaux � afficher dans l'onglet 'Sol'
 * selon le mod�le de comportement du sol.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeAbstractSolCourbe extends AlbeAbstractOnglet {

  /**
   * set d'onglets.
   */
  BuTabbedPane tp_;

  /**
   * tableau de champs texte pour les �paisseurs.
   */
  BuTextField[] epaisseur_;

  /**
   * tableau de boutons pour l'affichage des graphiques.
   */
  BuButton[] graphiqueButton_;

  /**
   * tableau de panneaux pour chaque couche de sol.
   */
  BuPanel[] p_;

  /**
   * 
   */
  BuPanel[] panelNord_;

  /**
   *
   */
  BuPanel[] panelNordEst_;

  /**
   * bouton 'Initialiser'.
   */
  BuButton initButton_;

  /**
   * panneau contenant le bouton 'Initialiser'.
   */
  BuPanel panelSud_;

  /**
   * label � afficher lorsque la zone de texte 'Epaisseur' de la couche 1 re�oit
   * le focus.
   */
  BuLabel infoEpaisseurLabel_;

  /**
   * r�f�rence vers le parent (onglet 'Sol').
   */
  AlbeSolParametres sol_;

  /**
   * constructeur.
   */

  public AlbeAbstractSolCourbe(final AlbeSolParametres _sol) {

    super(_sol.getApplication(), _sol, null);
    sol_ = _sol;

    this.setLayout(new BuVerticalLayout());

    tp_ = new BuTabbedPane();
    p_ = new BuPanel[AlbeLib.NBMAX_COUCHES];
    panelNord_ = new BuPanel[AlbeLib.NBMAX_COUCHES];
    panelNordEst_ = new BuPanel[AlbeLib.NBMAX_COUCHES];
    epaisseur_ = new BuTextField[AlbeLib.NBMAX_COUCHES];
    graphiqueButton_ = new BuButton[AlbeLib.NBMAX_COUCHES];

    initComposants();

    // Initialisation des champs texte 'Epaisseur'
    for (int i = 0; i < AlbeLib.NBMAX_COUCHES; i++) {
      epaisseur_[i] = AlbeLib.doubleField(AlbeLib.FORMAT001);
      epaisseur_[i].setColumns(5);
      epaisseur_[i].setHorizontalAlignment(SwingConstants.CENTER);
      epaisseur_[i].addKeyListener(this);
      epaisseur_[i].addFocusListener(this);
    }

    // Panneau sud
    panelSud_ = new BuPanel(new FlowLayout());
    initButton_ = AlbeLib.button("Initialiser toutes les couches", "reinitialiser");
    initButton_.setToolTipText("Effacer les donn�es saisies pour toutes les couches");
    initButton_.addActionListener(this);
    panelSud_.add(initButton_);
  }

  /**
   * initialise le tabbedpane.
   */

  protected void initTabbedPane() {

    for (int i = 0; i < AlbeLib.NBMAX_COUCHES; i++) {
      int numCouche = i + 1;
      p_[i] = new BuPanel();
      initCouchePanel(p_[i], i);
      tp_.addTab("Couche " + numCouche, null, p_[i], AlbeLib.TIP_TEXT_COUCHES_SOL + numCouche);
    }
    infoEpaisseurLabel_ = new BuLabel(
        "<html><font color = \"blue\">L'�paisseur de la couche 1 est fix�e dans la situation cote de dragage.</font></html>");
    panelNordEst_[0].add(new JLabel(" "));
    panelNordEst_[0].add(infoEpaisseurLabel_);
    infoEpaisseurLabel_.setVisible(false);
  }

  /**
   * initialise les composants.
   */
  protected void initComposants() {}

  /**
   * retourne l'indice de l'onglet courant.
   */
  protected int getCurrentTab() {

    return tp_.getSelectedIndex();
  }

  /**
   * retourne la somme des �paisseurs des couches de sol.
   */
  protected double getSommeEpaisseurs() {
    double res = 0;

    for (int i = 0; i < sol_.nbCouches_; i++) {
      double d = AlbeLib.textFieldDoubleValue(epaisseur_[i]);
      if (d == VALEUR_NULLE.value)
        return VALEUR_NULLE.value;
      res += d;
    }
    return res;
  }

  /**
   * initialise le panneau d'une couche de sol.
   * 
   * @param _pf
   * @param _i num�ro de la couche de sol
   */
  protected void initCouchePanel(BuPanel _pf, int _i) {}

  /**
   * efface les donn�es saisies pour chaque couche de sol � partir de la couche
   * _n1 jusqu'� la couche _n2 (si _n1 = 1 et _n2 = 3, on efface les donn�es des
   * couches n�1, n�2 et n�3) .
   */
  protected void eraseDataCouches(int _n1, int _n2) {}

  /**
   * 
   */
  public void focusGained(FocusEvent _evt) {
    if (_evt.getSource() == epaisseur_[0])
      infoEpaisseurLabel_.setVisible(true);
  }

}
