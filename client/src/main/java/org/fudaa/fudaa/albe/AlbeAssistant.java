/*
 * @creation 20 oct. 06
 * @modification $Date: 2006-10-25 16:43:03 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import com.memoire.bu.BuAssistant;

/**
 * Classe de l'assistant du logiciel Fudaa-Albe (Albe).
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeAssistant extends BuAssistant {}
