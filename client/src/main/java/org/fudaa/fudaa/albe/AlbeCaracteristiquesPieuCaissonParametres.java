package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

import org.fudaa.dodico.corba.albe.STronconCaisson;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * Panneau � ins�rer dans l'onglet 'Caract�ristiques pieu' si le pieu est en
 * caisson.
 * 
 * @author Sabrina Delattre
 */
public class AlbeCaracteristiquesPieuCaissonParametres extends BuPanel implements ActionListener {

  BuTextField largeurPieu_, excentricite_;

  JComboBox nbTronconsCaissonCombo_;

  DefaultTableModel modelCaisson_;

  BuPanel tablePanelCaisson_;

  BuTable tableCaisson_;

  JScrollPane scrollPaneCaisson_;

  BuButton initButton_;

  /**
   * r�f�rence vers le parent.
   */

  AlbeCaracteristiquesPieuParametres pieu_;

  /**
   * Constructeur.
   */

  public AlbeCaracteristiquesPieuCaissonParametres(AlbeCaracteristiquesPieuParametres _pieu) {

    pieu_ = _pieu;

    this.setLayout(new BuVerticalLayout());

    Border b = BorderFactory.createTitledBorder(
        BorderFactory.createBevelBorder(BevelBorder.RAISED), "Caract�ristiques du pieu en caisson");
    this.setBorder(b);

    /**
     * Panneau contenant les deux champs texte.
     */
    BuPanel caissonPanelNord = new BuPanel();
    caissonPanelNord.setLayout(new SpringLayout());

    // Initialisation de la boite combo
    nbTronconsCaissonCombo_ = new JComboBox();
    for (int i = 0; i < AlbeLib.NBMAX_TRONCONS_PIEU_CAISSON; i++) {
      nbTronconsCaissonCombo_.addItem(Integer.toString(i + 1));
    }

    nbTronconsCaissonCombo_.addActionListener(this);
    largeurPieu_ = AlbeLib.doubleField(AlbeLib.FORMAT002);
    excentricite_ = AlbeLib.doubleField(AlbeLib.FORMAT002);
    largeurPieu_.setHorizontalAlignment(SwingConstants.CENTER);
    excentricite_.setHorizontalAlignment(SwingConstants.CENTER);
    largeurPieu_.addKeyListener(pieu_);
    excentricite_.addKeyListener(pieu_);
    largeurPieu_.addFocusListener(pieu_);
    excentricite_.addFocusListener(pieu_);

    caissonPanelNord.add(new JLabel("Nombre de tron�on(s) (de 1 � "
        + AlbeLib.NBMAX_TRONCONS_PIEU_CAISSON + ")"));
    caissonPanelNord.add(nbTronconsCaissonCombo_);
    caissonPanelNord.add(new JLabel("Largeur effective du pieu (en cm)"));
    caissonPanelNord.add(largeurPieu_);
    caissonPanelNord.add(new JLabel("Excentricit� du caisson (en cm)"));
    caissonPanelNord.add(excentricite_);

    SpringUtilities.makeCompactGrid(caissonPanelNord, 3, 2, 5, 5, 10, 10);

    BuPanel tempPanelCais = AlbeLib.borderPanel(AlbeLib.ETCHED_BORDER);
    tempPanelCais.setLayout(new BorderLayout());
    tempPanelCais.add(caissonPanelNord, BorderLayout.WEST);

    /**
     * Panneau contenant le tableau des tron�ons.
     */

    tablePanelCaisson_ = AlbeLib.titledPanel("Propri�t�s de chacun des tron�ons",
        AlbeLib.ETCHED_BORDER);
    modelCaisson_ = new AlbeModeleTablePieuEtCoeffMateriaux(new Object[] { "header-numero",
        "header-longueur", "header-section-non-cor", "header-inertie-non-cor",
        "header-section-cor", "header-inertie-cor", "header-fyk" }, 1, pieu_);
    tableCaisson_ = new BuTable(modelCaisson_);
    tableCaisson_.getTableHeader().setDefaultRenderer(new AlbePieuTableCellRenderer());
    scrollPaneCaisson_ = new JScrollPane(tableCaisson_);
    scrollPaneCaisson_.setPreferredSize(new Dimension(680, 77));

    // Largeur de la premi�re colonne
    tableCaisson_.getColumnModel().getColumn(0).setPreferredWidth(30);

    AlbeLib.initialiseColonne1JTable(tableCaisson_);
    AlbeLib.initialiseTableVide(tableCaisson_);
    AlbeLib.changeParametresTable(tableCaisson_);

    tablePanelCaisson_.add(scrollPaneCaisson_);

    /**
     * Panneau contenant le bouton 'Initialiser'.
     */

    BuPanel bp = new BuPanel(new FlowLayout());
    initButton_ = AlbeLib.button("Initialiser", "reinitialiser");
    initButton_.setToolTipText("Effacer les donn�es saisies");
    initButton_.addActionListener(this);
    bp.add(initButton_);

    this.add(tempPanelCais);
    this.add(tablePanelCaisson_);
    this.add(bp);

  }// Fin constructeur

  /**
   * efface toutes les donn�es entr�es pour un pieu en caisson.
   */
  public void eraseData() {
    largeurPieu_.setText("");
    largeurPieu_.requestFocus();
    excentricite_.setText("");
    excentricite_.requestFocus();
    excentricite_.transferFocus();
    // if (tableCaisson_.isEditing())
    // tableCaisson_.getCellEditor().stopCellEditing();
    AlbeLib.initialiseTableVide(tableCaisson_);
  }

  /**
   * retourne le nombre de tron�ons contenus dans le tableau.
   */
  public int getNbTroncons() {
    return tableCaisson_.getRowCount();
  }

  /**
   * retourne une structure de donn�es contenant les informations du tron�on _n
   * du tableau. retourne null si le tron�on n'existe pas dans le tableau.
   * 
   * @param _n num�ro du tron�on � retourner (commence � 1)
   */
  public STronconCaisson getTroncon(final int _n) {
    STronconCaisson t = null;
    try {
      t = (STronconCaisson) AlbeLib.createIDLObject(STronconCaisson.class);
      try {
        t.longueur = ((Double) modelCaisson_.getValueAt(_n - 1, 1)).doubleValue();
      }
      catch (final Exception _e2) {
        t.longueur = VALEUR_NULLE.value;
      }
      try {
        t.sectionNonCorrodee = ((Double) modelCaisson_.getValueAt(_n - 1, 2)).doubleValue();
      }
      catch (final Exception _e3) {
        t.sectionNonCorrodee = VALEUR_NULLE.value;
      }
      try {
        t.inertieNonCorrodee = ((Double) modelCaisson_.getValueAt(_n - 1, 3)).doubleValue();
      }
      catch (final Exception _e4) {
        t.inertieNonCorrodee = VALEUR_NULLE.value;
      }
      try {
        t.sectionCorrodee = ((Double) modelCaisson_.getValueAt(_n - 1, 4)).doubleValue();
      }
      catch (final Exception _e5) {
        t.sectionCorrodee = VALEUR_NULLE.value;
      }
      try {
        t.inertieCorrodee = ((Double) modelCaisson_.getValueAt(_n - 1, 5)).doubleValue();
      }
      catch (final Exception _e6) {
        t.inertieCorrodee = VALEUR_NULLE.value;
      }
      try {
        t.valeurCaracteristiqueLimiteElastiqueAcier = ((Double) modelCaisson_.getValueAt(_n - 1, 6))
            .doubleValue();
      }
      catch (final Exception _e7) {
        t.valeurCaracteristiqueLimiteElastiqueAcier = VALEUR_NULLE.value;
      }
    }
    catch (final ArrayIndexOutOfBoundsException _e1) {
      System.err.println(AlbeMsg.CONS001);
    }

    return t;
  }

  /**
   * retourne une structure de donn�es contenant les informations de tous les
   * tron�ons.
   */
  public STronconCaisson[] getTroncons() {
    if (tableCaisson_.isEditing()) {
      tableCaisson_.getCellEditor().stopCellEditing();
    }
    STronconCaisson[] t = null;
    final int nb = getNbTroncons();
    try {
      t = new STronconCaisson[nb];
    }
    catch (final NegativeArraySizeException _e1) {
      System.err.println(AlbeMsg.CONS002);
    }
    for (int i = 0; i < nb; i++) {
      try {
        t[i] = getTroncon(i + 1);
      }
      catch (final NullPointerException _e2) {
        System.err.println(AlbeMsg.CONS003);
      }
      catch (final ArrayIndexOutOfBoundsException _e3) {
        System.err.println(AlbeMsg.CONS004);
        return null;
      }
    }

    return t;
  }

  /**
   * charge les tron�ons sp�cifi�s dans le tableau des tron�ons.
   * 
   * @param _t tableau contenant les tron�ons � importer
   */
  public synchronized void setTroncons(final STronconCaisson[] _t) {
    if (tableCaisson_.isEditing()) {
      tableCaisson_.getCellEditor().stopCellEditing();
    }
    int n = 0;
    try {
      n = _t.length;
      AlbeLib.majNbLignes(tableCaisson_, n);
      AlbeLib.initialiseColonne1JTable(tableCaisson_);
      for (int i = 0; i < n; i++) {
        if (_t[i].longueur != VALEUR_NULLE.value)
          tableCaisson_.setValueAt(new Double(_t[i].longueur), i, 1);

        if (_t[i].sectionNonCorrodee != VALEUR_NULLE.value)
          tableCaisson_.setValueAt(new Double(_t[i].sectionNonCorrodee), i, 2);

        if (_t[i].inertieNonCorrodee != VALEUR_NULLE.value)
          tableCaisson_.setValueAt(new Double(_t[i].inertieNonCorrodee), i, 3);

        if (_t[i].sectionCorrodee != VALEUR_NULLE.value)
          tableCaisson_.setValueAt(new Double(_t[i].sectionCorrodee), i, 4);

        if (_t[i].inertieCorrodee != VALEUR_NULLE.value)
          tableCaisson_.setValueAt(new Double(_t[i].inertieCorrodee), i, 5);

        if (_t[i].valeurCaracteristiqueLimiteElastiqueAcier != VALEUR_NULLE.value)
          tableCaisson_.setValueAt(new Double(_t[i].valeurCaracteristiqueLimiteElastiqueAcier), i,
              6);

      }
    }
    catch (final NullPointerException _e1) {
      System.err.println(AlbeMsg.CONS005);
    }
  }

  /**
   * 
   */

  public boolean tronconCaissonModified(STronconCaisson _p, int _n) {
    STronconCaisson p2 = getTroncon(_n);
    if (_p.longueur != p2.longueur
        || _p.sectionNonCorrodee != p2.sectionNonCorrodee
        || _p.inertieNonCorrodee != p2.inertieNonCorrodee
        || _p.sectionCorrodee != p2.sectionCorrodee
        || _p.inertieCorrodee != p2.inertieCorrodee
        || _p.valeurCaracteristiqueLimiteElastiqueAcier != p2.valeurCaracteristiqueLimiteElastiqueAcier)
      return true;
    return false;
  }

  /**
   * 
   */

  public boolean tronconsCaissonModified(STronconCaisson[] _p) {
    if (tableCaisson_.isEditing()) {
      tableCaisson_.getCellEditor().stopCellEditing();
    }

    for (int i = 0; i < _p.length; i++) {
      if (tronconCaissonModified(_p[i], i + 1))
        return true;
    }
    return false;
  }

  /**
   * 
   */

  public void actionPerformed(ActionEvent _evt) {

    /**
     * Gestion de la s�lection du nombre de troncons (et donc du nombre de
     * lignes du tableau.
     */
    if (_evt.getSource() == nbTronconsCaissonCombo_) {
      if (tableCaisson_.isEditing())
        tableCaisson_.getCellEditor().stopCellEditing();

      // Pendant la mise � jour du nb de lignes du tableau, on n'effectue pas
      // les v�rifications
      AlbeLib.verificationPieu_ = false;
      int nbRow = nbTronconsCaissonCombo_.getSelectedIndex() + 1;
      AlbeLib.majNbLignes(tableCaisson_, nbRow);
      AlbeLib.initialiseColonne1JTable(tableCaisson_);
      pieu_.majMessages();
      AlbeLib.verificationPieu_ = true;

      /**
       * Si le nombre de tron�ons est inf�rieur ou �gal � 14, adaptation de la
       * taille du tableau pour ne pas avoir de barre de d�filement.
       */
      if (nbRow <= 14)
        scrollPaneCaisson_.setPreferredSize(new Dimension(680, 57 + 20 * nbRow));
      /**
       * Si le nombre de tron�ons est sup�rieur � 16, il y aura une barre de
       * d�filement.
       */
      else
        scrollPaneCaisson_.setPreferredSize(new Dimension(680, 337));

      // Rafra�chissement du panneau
      revalidate();
    }

    /** Clic sur le bouton 'Initialiser'. */
    else if (_evt.getSource() == initButton_) {
      // Pendant l'effacement des donn�es, on n'effectue pas les v�rifications
      AlbeLib.verificationPieu_ = false;
      eraseData();
      AlbeLib.verificationPieu_ = true;
    }

  }

  /**
   * 
   */
  public ValidationMessages validation() {
    final ValidationMessages vm = new ValidationMessages();

    // On stoppe l'�dition du tableau
    /*
     * if (tableCaisson_.isEditing())
     * tableCaisson_.getCellEditor().stopCellEditing();
     */

    // V�rification largeur
    if (AlbeLib.emptyField(largeurPieu_))
      vm.add(AlbeMsg.VMSG109);
    else if (AlbeLib.textFieldDoubleValue(largeurPieu_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG110);
    else if (AlbeLib.textFieldDoubleValue(largeurPieu_) <= 0)
      vm.add(AlbeMsg.VMSG115);
    
    // V�rification excentricit�
    if (AlbeLib.emptyField(excentricite_))
      vm.add(AlbeMsg.VMSG112);
    else if (AlbeLib.textFieldDoubleValue(excentricite_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG113);
    else if (AlbeLib.textFieldDoubleValue(excentricite_) <= 0)
      vm.add(AlbeMsg.VMSG116);

    // On v�rifie que toutes les cases du tableau sont remplies
    if (!AlbeLib.fullTable(tableCaisson_, modelCaisson_))
      vm.add(AlbeMsg.VMSG107);

    // On v�rifie que les longueurs sont strictement positives
    if (!AlbeLib.valeursColonneStrictPositives(tableCaisson_, 1))
      vm.add(AlbeMsg.VMSG117);

    // On v�rifie que les sections non corrod�es sont strictement positives
    if (!AlbeLib.valeursColonneStrictPositives(tableCaisson_, 2))
      vm.add(AlbeMsg.VMSG121);

    // On v�rifie que les inerties non corrod�es sont strictement positives
    if (!AlbeLib.valeursColonneStrictPositives(tableCaisson_, 3))
      vm.add(AlbeMsg.VMSG122);

    // On v�rifie que les sections corrod�es sont strictement positives
    if (!AlbeLib.valeursColonneStrictPositives(tableCaisson_, 4))
      vm.add(AlbeMsg.VMSG123);

    // On v�rifie que les inerties corrod�es sont strictement positives
    if (!AlbeLib.valeursColonneStrictPositives(tableCaisson_, 5))
      vm.add(AlbeMsg.VMSG124);

    // On v�rifie que les fyk sont strictement positives
    if (!AlbeLib.valeursColonneStrictPositives(tableCaisson_, 6))
      vm.add(AlbeMsg.VMSG120);

    // On v�rifie que la somme des longueurs des tron�ons est strictement
    // sup�rieure � la cote de la t�te de l'ouvrage moins la cote de calcul
    if (pieu_.getLongueurPieu() != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(pieu_.fp_.geo_.coteTete_) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(pieu_.fp_.geo_.coteCalcul_) != VALEUR_NULLE.value
        && pieu_.getLongueurPieu() <= (AlbeLib.textFieldDoubleValue(pieu_.fp_.geo_.coteTete_) - AlbeLib
            .textFieldDoubleValue(pieu_.fp_.geo_.coteCalcul_)))
      vm.add(AlbeMsg.VMSG111);

    return vm;
  }

}
