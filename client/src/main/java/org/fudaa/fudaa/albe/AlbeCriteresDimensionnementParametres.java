package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import org.fudaa.dodico.corba.albe.SCriteresDimensionnement;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * Description de l'onglet 'Crit�res de dimensionnement'.
 * 
 * @author Sabrina Delattre
 */

public class AlbeCriteresDimensionnementParametres extends AlbeAbstractOnglet {

  BuTextField deplacementElsRare_, deflexionElsRare_, deflexionEluFonda_, deflexionEluAcc_,
      reactionEluFond_, reactionEluAcc_;

  JLabel elsRareDeflexionLabel_, eluFondaDeflexionLabel_, eluAccDeflexionLabel_, perCentLabel1_,
      perCentLabel2_, perCentLabel3_, eluFondaReactionLabel_, eluAccReactionLabel_, kNLabel1_,
      kNLabel2_;

  BuButton aideButton_;

  /**
   * r�f�rence vers la fen�tre parente.
   */

  AlbeFilleParametres fp_;

  /**
   * Constructeur.
   * 
   * @param _fp
   */

  public AlbeCriteresDimensionnementParametres(final AlbeFilleParametres _fp) {

    super(_fp.getImplementation(), _fp, AlbeMsg.URL006);

    fp_ = _fp;

    // Initialisation des zones de texte
    deplacementElsRare_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    deflexionElsRare_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    deflexionEluFonda_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    deflexionEluAcc_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    reactionEluFond_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    reactionEluAcc_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    deplacementElsRare_.setHorizontalAlignment(SwingConstants.CENTER);
    deflexionElsRare_.setHorizontalAlignment(SwingConstants.CENTER);
    deflexionEluFonda_.setHorizontalAlignment(SwingConstants.CENTER);
    deflexionEluAcc_.setHorizontalAlignment(SwingConstants.CENTER);
    reactionEluFond_.setHorizontalAlignment(SwingConstants.CENTER);
    reactionEluAcc_.setHorizontalAlignment(SwingConstants.CENTER);
    deplacementElsRare_.setColumns(8);
    deflexionElsRare_.setColumns(8);
    reactionEluFond_.setColumns(8);
    deplacementElsRare_.addKeyListener(this);
    deflexionElsRare_.addKeyListener(this);
    deflexionEluFonda_.addKeyListener(this);
    deflexionEluAcc_.addKeyListener(this);
    reactionEluFond_.addKeyListener(this);
    reactionEluAcc_.addKeyListener(this);
    deplacementElsRare_.addFocusListener(this);
    deflexionElsRare_.addFocusListener(this);
    deflexionEluFonda_.addFocusListener(this);
    deflexionEluAcc_.addFocusListener(this);
    reactionEluFond_.addFocusListener(this);
    reactionEluAcc_.addFocusListener(this);

    /**
     * Panneau contenant le bouton 'aide'.
     */
    BuPanel aidePanel = new BuPanel(new BorderLayout());
    aideButton_ = AlbeLib.aideButton();
    aideButton_.addActionListener(this);
    aidePanel.add(aideButton_, BorderLayout.EAST);

    /**
     * Panel Nord.
     */

    BuPanel panelNord = AlbeLib.titledPanel("D�placement en t�te d'ouvrage", AlbeLib.ETCHED_BORDER);
    panelNord.setLayout(new SpringLayout());
    panelNord.add(new JLabel("ELS rare                "));
    panelNord.add(deplacementElsRare_);
    panelNord.add(new JLabel("m"));

    SpringUtilities.makeCompactGrid(panelNord, 1, 3, 25, 10, 10, 10);

    /**
     * Panel Centre.
     */

    BuPanel deflexionPanel = AlbeLib.titledPanel("D�flexion de la d�fense", AlbeLib.ETCHED_BORDER);
    deflexionPanel.setLayout(new SpringLayout());
    elsRareDeflexionLabel_ = new JLabel("ELS rare");
    eluFondaDeflexionLabel_ = new JLabel("ELU fondamental");
    eluAccDeflexionLabel_ = new JLabel("ELU accidentel");
    perCentLabel1_ = new JLabel("%");
    perCentLabel2_ = new JLabel("%");
    perCentLabel3_ = new JLabel("%");
    deflexionPanel.add(elsRareDeflexionLabel_);
    deflexionPanel.add(deflexionElsRare_);
    deflexionPanel.add(perCentLabel1_);
    deflexionPanel.add(eluFondaDeflexionLabel_);
    deflexionPanel.add(deflexionEluFonda_);
    deflexionPanel.add(perCentLabel2_);
    deflexionPanel.add(eluAccDeflexionLabel_);
    deflexionPanel.add(deflexionEluAcc_);
    deflexionPanel.add(perCentLabel3_);

    SpringUtilities.makeCompactGrid(deflexionPanel, 3, 3, 25, 10, 10, 10);

    /**
     * Panel Sud.
     */

    BuPanel reactionPanel = AlbeLib
        .titledPanel("R�action au choc", AlbeLib.ETCHED_BORDER);
    reactionPanel.setLayout(new SpringLayout());
    eluFondaReactionLabel_ = new JLabel("ELU fondamental");
    eluAccReactionLabel_ = new JLabel("ELU accidentel");
    kNLabel1_ = new JLabel("kN");
    kNLabel2_ = new JLabel("kN");
    reactionPanel.add(eluFondaReactionLabel_);
    reactionPanel.add(reactionEluFond_);
    reactionPanel.add(kNLabel1_);
    reactionPanel.add(eluAccReactionLabel_);
    reactionPanel.add(reactionEluAcc_);
    reactionPanel.add(kNLabel2_);

    SpringUtilities.makeCompactGrid(reactionPanel, 2, 3, 25, 10, 10, 10);

    BuPanel panelSud = new BuPanel();
    panelSud.setLayout(new BorderLayout());
    panelSud.add(deflexionPanel, BorderLayout.NORTH);
    BuPanel tempPanelSud = new BuPanel(new BorderLayout());
    tempPanelSud.add(reactionPanel, BorderLayout.NORTH);
    panelSud.add(tempPanelSud, BorderLayout.CENTER);

    BuPanel tempPanel2 = AlbeLib
        .titledPanel("Crit�res de dimensionnement", AlbeLib.COMPOUND_BORDER);
    tempPanel2.setLayout(new BuVerticalLayout());
    tempPanel2.add(aidePanel);
    tempPanel2.add(panelNord);
    tempPanel2.add(panelSud);

    this.setLayout(new FlowLayout());
    this.add(tempPanel2);

  }// Fin constructeur

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans
   * l'onglet 'Crit�res dimensionnement'.
   */

  public SCriteresDimensionnement getParametresCriteresDim() {
    final SCriteresDimensionnement p = (SCriteresDimensionnement) AlbeLib
        .createIDLObject(SCriteresDimensionnement.class);
    p.deplacementTeteOuvrage = AlbeLib.textFieldDoubleValue(deplacementElsRare_);

    if (deflexionElsRare_.isEditable())
      p.deflexionDefenseElsRare = AlbeLib.textFieldDoubleValue(deflexionElsRare_);
    else
      p.deflexionDefenseElsRare = VALEUR_NULLE.value;

    if (deflexionEluFonda_.isEditable())
      p.deflexionDefenseEluFonda = AlbeLib.textFieldDoubleValue(deflexionEluFonda_);
    else
      p.deflexionDefenseEluFonda = VALEUR_NULLE.value;

    if (deflexionEluAcc_.isEditable())
      p.deflexionDefenseEluAcc = AlbeLib.textFieldDoubleValue(deflexionEluAcc_);
    else
      p.deflexionDefenseEluAcc = VALEUR_NULLE.value;

    if (reactionEluFond_.isEditable())
      p.reactionTeteOuvrageEluFonda = AlbeLib.textFieldDoubleValue(reactionEluFond_);
    else
      p.reactionTeteOuvrageEluFonda = VALEUR_NULLE.value;

    if (reactionEluAcc_.isEditable())
      p.reactionTeteOuvrageEluAcc = AlbeLib.textFieldDoubleValue(reactionEluAcc_);
    else
      p.reactionTeteOuvrageEluAcc = VALEUR_NULLE.value;

    return p;
  }

  /**
   * importe dans l'onglet 'Crit�res dimensionnement' les donn�es contenues dans
   * la structure pass�e en param�tre.
   */
  public synchronized void setParametresCriteresDim(SCriteresDimensionnement _p) {
    if (_p == null)
      _p = (SCriteresDimensionnement) AlbeLib.createIDLObject(SCriteresDimensionnement.class);

    if (_p.deplacementTeteOuvrage != VALEUR_NULLE.value)
      deplacementElsRare_.setValue(new Double(_p.deplacementTeteOuvrage));

    if (_p.deflexionDefenseElsRare != VALEUR_NULLE.value)
      deflexionElsRare_.setValue(new Double(_p.deflexionDefenseElsRare));

    if (_p.deflexionDefenseEluFonda != VALEUR_NULLE.value)
      deflexionEluFonda_.setValue(new Double(_p.deflexionDefenseEluFonda));

    if (_p.deflexionDefenseEluAcc != VALEUR_NULLE.value)
      deflexionEluAcc_.setValue(new Double(_p.deflexionDefenseEluAcc));

    if (_p.reactionTeteOuvrageEluFonda != VALEUR_NULLE.value)
      reactionEluFond_.setValue(new Double(_p.reactionTeteOuvrageEluFonda));

    if (_p.reactionTeteOuvrageEluAcc != VALEUR_NULLE.value)
      reactionEluAcc_.setValue(new Double(_p.reactionTeteOuvrageEluAcc));

  }

  /**
   * 
   */

  public boolean paramsDimModified(SCriteresDimensionnement _p) {
    SCriteresDimensionnement p2 = getParametresCriteresDim();
    if (_p.deplacementTeteOuvrage != p2.deplacementTeteOuvrage)
      return true;
    if (deflexionElsRare_.isEditable()) {
      if (_p.deflexionDefenseElsRare != p2.deflexionDefenseElsRare)
        return true;
    }
    if (deflexionEluFonda_.isEditable()) {
      if (_p.deflexionDefenseEluFonda != p2.deflexionDefenseEluFonda)
        return true;
    }
    if (deflexionEluAcc_.isEditable()) {
      if (_p.deflexionDefenseEluAcc != p2.deflexionDefenseEluAcc)
        return true;
    }
    if (reactionEluFond_.isEditable()) {
      if (_p.reactionTeteOuvrageEluFonda != p2.reactionTeteOuvrageEluFonda)
        return true;
    }
    if (reactionEluAcc_.isEditable()) {
      if (_p.reactionTeteOuvrageEluAcc != p2.reactionTeteOuvrageEluAcc)
        return true;
    }
    return false;
  }

  /**
   * 
   */
  public ValidationMessages validationDeflexion(ValidationMessages _vm) {

    boolean defRare = false;
    boolean defFond = false;
    boolean defAcc = false;

    /** D�flexion ELS rare. */
    if (AlbeLib.emptyField(deflexionElsRare_))
      _vm.add(AlbeMsg.VMSG072);
    else if (AlbeLib.textFieldDoubleValue(deflexionElsRare_) == VALEUR_NULLE.value)
      _vm.add(AlbeMsg.VMSG073);
    else {
      defRare = true;
      if (AlbeLib.textFieldDoubleValue(deflexionElsRare_) < 0)
        _vm.add(AlbeMsg.VMSG083);
    }

    // On v�rifie que la d�flexion est inf�rieure ou �gale � la d�flexion
    // maximale introduite dans l'onglet 'D�fense' (dernier point deform).
    if (fp_.defense_.deformMaxRecuperable_ && defRare
        && AlbeLib.textFieldDoubleValue(deflexionElsRare_) > fp_.defense_.deformMax_)
      _vm.add(AlbeMsg.VMSG088);

    /** D�flexion ELU fondamental. */
    if (AlbeLib.emptyField(deflexionEluFonda_))
      _vm.add(AlbeMsg.VMSG074);
    else if (AlbeLib.textFieldDoubleValue(deflexionEluFonda_) == VALEUR_NULLE.value)
      _vm.add(AlbeMsg.VMSG075);
    else {
      defFond = true;
      if (AlbeLib.textFieldDoubleValue(deflexionEluFonda_) < 0)
        _vm.add(AlbeMsg.VMSG084);
    }

    // On v�rifie que la d�flexion est inf�rieure ou �gale � la d�flexion
    // maximale introduite dans l'onglet 'D�fense' (dernier point deform).
    if (fp_.defense_.deformMaxRecuperable_ && defFond
        && AlbeLib.textFieldDoubleValue(deflexionEluFonda_) > fp_.defense_.deformMax_)
      _vm.add(AlbeMsg.VMSG089);

    /** D�flexion ELU accidentelle. */
    if (AlbeLib.emptyField(deflexionEluAcc_))
      _vm.add(AlbeMsg.VMSG076);
    else if (AlbeLib.textFieldDoubleValue(deflexionEluAcc_) == VALEUR_NULLE.value)
      _vm.add(AlbeMsg.VMSG077);
    else {
      defAcc = true;
      if (AlbeLib.textFieldDoubleValue(deflexionEluAcc_) < 0)
        _vm.add(AlbeMsg.VMSG085);
    }

    // On v�rifie que la d�flexion est inf�rieure ou �gale � la d�flexion
    // maximale introduite dans l'onglet 'D�fense' (dernier point deform).
    if (fp_.defense_.deformMaxRecuperable_ && defAcc
        && AlbeLib.textFieldDoubleValue(deflexionEluAcc_) > fp_.defense_.deformMax_)
      _vm.add(AlbeMsg.VMSG090);

    /** V�rification d�flexions croissantes */

    if (defRare
        && defFond
        && defAcc
        && AlbeLib.textFieldDoubleValue(deflexionEluAcc_) < AlbeLib
            .textFieldDoubleValue(deflexionEluFonda_)
        || AlbeLib.textFieldDoubleValue(deflexionEluFonda_) < AlbeLib
            .textFieldDoubleValue(deflexionElsRare_))
      _vm.add(AlbeMsg.VMSG091);

    return _vm;
  }

  /**
   * 
   */
  public ValidationMessages validation() {
    ValidationMessages vm = new ValidationMessages();

    // V�rification 'D�placement en t�te d'ouvrage'
    if (AlbeLib.emptyField(deplacementElsRare_))
      vm.add(AlbeMsg.VMSG070);
    else if (AlbeLib.textFieldDoubleValue(deplacementElsRare_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG071);
    else if (AlbeLib.textFieldDoubleValue(deplacementElsRare_) < 0)
      vm.add(AlbeMsg.VMSG082);

    // V�rification 'D�flexion de la d�fense' (si accostage et d�fense)
    if (fp_.action_.actionCombo_.getSelectedIndex() == 0
        && fp_.defense_.presenceCombo_.getSelectedIndex() == 0)
      vm = validationDeflexion(vm);

    // V�rification 'R�action en t�te d'ouvrage' (si accostage)
    if (fp_.action_.actionCombo_.getSelectedIndex() == 0) {
      if (AlbeLib.emptyField(reactionEluFond_))
        vm.add(AlbeMsg.VMSG078);
      else if (AlbeLib.textFieldDoubleValue(reactionEluFond_) == VALEUR_NULLE.value)
        vm.add(AlbeMsg.VMSG079);
      else if (AlbeLib.textFieldDoubleValue(reactionEluFond_) < 0)
        vm.add(AlbeMsg.VMSG086);

      if (AlbeLib.emptyField(reactionEluAcc_))
        vm.add(AlbeMsg.VMSG080);
      else if (AlbeLib.textFieldDoubleValue(reactionEluAcc_) == VALEUR_NULLE.value)
        vm.add(AlbeMsg.VMSG081);
      else if (AlbeLib.textFieldDoubleValue(reactionEluAcc_) < 0)
        vm.add(AlbeMsg.VMSG087);
    }

    return vm;
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {
    if (_evt.getSource() == aideButton_)
      aide();
  }

}
