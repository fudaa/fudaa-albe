/*
 * @creation 18 oct. 06
 * @modification $Date: 2006-10-25 16:38:39 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialog;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;

/**
 * une bo�te de dialogue de confirmation comportant les boutons 'Oui', 'Non',
 * 'Annuler'.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */

public class AlbeDialogConfirmation extends BuDialog {

  BuCommonInterface parent_;

  /**
   * bouton 'Oui'.
   */
  protected BuButton btOui_;

  /**
   * bouton 'Non'.
   */
  protected BuButton btNon_;

  /**
   * bouton 'Annuler'.
   */
  protected BuButton btAnnuler_;

  /**
   * constructeur de la boite de dialogue.
   */
  public AlbeDialogConfirmation(final BuCommonInterface _parent,
      final BuInformationsSoftware _isoft, final Object _title, final Object _message) {
    super(_parent, _isoft, BuResource.BU.getString((String) _title), _message);
    parent_ = _parent;
    final BuPanel pnb = new BuPanel();
    pnb.setLayout(new FlowLayout(FlowLayout.RIGHT));
    // Initialisation des Boutons
    btOui_ = new BuButton(BuResource.BU.loadToolCommandIcon("OUI"), BuResource.BU.getString("Oui"));
    btNon_ = new BuButton(BuResource.BU.loadToolCommandIcon("NON"), BuResource.BU.getString("Non"));
    btAnnuler_ = new BuButton(BuResource.BU.loadToolCommandIcon("ANNULER"), BuResource.BU
        .getString("Annuler"));

    btOui_.setActionCommand("OUI");
    btOui_.addActionListener(this);
    pnb.add(btOui_);
    btNon_.setActionCommand("NON");
    btNon_.addActionListener(this);
    pnb.add(btNon_);
    btAnnuler_.setActionCommand("ANNULER");
    btAnnuler_.addActionListener(this);
    pnb.add(btAnnuler_);
    getRootPane().setDefaultButton(btAnnuler_);
    content_.add(pnb, BuBorderLayout.SOUTH);
    setSize(500, 300);
  }

  /**
   * retourne le composant.
   */
  public JComponent getComponent() {
    return null;
  }

  /**
   * 
   */
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();

    if (action.equals("OUI")) {
      reponse_ = JOptionPane.YES_OPTION;
      setVisible(false);
    } else if (action.equals("NON")) {
      reponse_ = JOptionPane.NO_OPTION;
      setVisible(false);
    } else if (action.equals("ANNULER")) {
      reponse_ = JOptionPane.CANCEL_OPTION;
      setVisible(false);
    }
  }

}
