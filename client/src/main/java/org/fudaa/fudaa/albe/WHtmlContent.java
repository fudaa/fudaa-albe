/*
 * @creation 21 nov. 06
 * @modification $Date: 2007-11-21 10:10:22 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.io.File;

import javax.swing.JComponent;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.SResultatsAlbe;
import org.fudaa.dodico.corba.albe.SResultatsCombinaison;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

/**
 * @version $Version$
 * @author Sabrina Delattre
 */
public abstract class WHtmlContent {

  FudaaProjet project_;

  AlbeFilleNoteDeCalculs.Settings settings_;

  String html_;

  SParametresAlbe params_;

  SResultatsAlbe results_;

  String fichier_;
  
  String name_;

  JComponent parent_;

  boolean accostage_, defense_;

  /**
   * constructeur.
   */
  protected WHtmlContent(final JComponent _parent, final FudaaProjet _project,
      final AlbeFilleNoteDeCalculs.Settings _settings) {
    project_ = _project;
    settings_ = _settings;
    html_ = "";
    parent_ = _parent;
    if (_project != null) {
      params_ = (SParametresAlbe) project_.getParam(AlbeResource.PARAMETRES);
      results_ = (SResultatsAlbe) project_.getResult(AlbeResource.RESULTATS);
    } else {
      params_ = null;
      results_ = null;
    }
    if (settings_ != null) {
      fichier_ = (String) settings_.lit("CHEMIN_DU_FICHIER_HTML");
      name_ = (String) settings_.lit("NOM_DU_FICHIER_HTML");
      if (fichier_ == null) {
        fichier_ = AlbeLib.NOTE_DE_CALCULS_HTML_FILE;
      }
      if (name_==null){
        name_ = AlbeLib.NOTE_DE_CALCULS_HTML_FILE;
      }
    } else {
      fichier_ = AlbeLib.NOTE_DE_CALCULS_HTML_FILE;
      name_ = AlbeLib.NOTE_DE_CALCULS_HTML_FILE;
    }
    accostage_ = false;
    defense_ = false;
    if (params_.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)) {
      accostage_ = true;
      if (params_.defense.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI))
        defense_ = true;
    }

  }

  /**
   * 
   */
  protected abstract void buildContent();

  /**
   * 
   */
  protected FudaaProjet getProject() {
    return project_;
  }

  /**
   * 
   */
  protected AlbeFilleNoteDeCalculs.Settings getSettings() {
    return settings_;
  }

  /**
   * 
   */
  public String getContent() {
    buildContent();
    return html_;
  }

  /**
   * cr�e un tableau HTML et le remplit avec les en t�tes de colonne _colnames
   * (si diff�rent de null) et les donn�es _data.
   */
  protected void addTable(final Object[] _colnames, final Object[][] _data) {
    html_ += "<br><table border=\"2\" cellspacing=\"0\" cellpadding=\"5\">"
        + CtuluLibString.LINE_SEP;
    if (_colnames != null) {
      for (int i = 0; i < _colnames.length; i++) {
        html_ += "<th bgcolor=\"#DDDDDD\">" + _colnames[i] + "</th>";
      }
      html_ += CtuluLibString.LINE_SEP;
    }
    for (int j = 0; j < _data[0].length; j++) {
      html_ += "<tr>" + CtuluLibString.LINE_SEP;
      for (int k = 0; k < _data.length; k++) {
        html_ += "<td align=center>" + _data[k][j] + "</td>";
      }
      html_ += "</tr>" + CtuluLibString.LINE_SEP;
    }
    html_ += "</table><br>" + CtuluLibString.LINE_SEP;
  }

  /**
   * 
   */
  protected void addImgFromScript(final String _script, final String _imgname, final String _alt) {
    final String _img = fichier_ + "files" + AlbeLib.FILE_SEPARATOR + _imgname;
    AlbeLib.exportGraphScriptToGif(_script, _img);
    String cheminRel = name_+ "files" + AlbeLib.FILE_SEPARATOR + _imgname;
    html_ += "<a href=\"" + cheminRel + "\" target=\"_blank\"><img src=\"" + cheminRel + "\" alt =\"" + _alt
        + "\"></a>" + CtuluLibString.LINE_SEP;
  }

  /**
   * 
   */
  public static WHtmlContent createContentFor(final String _type, final JComponent _parent,
      final FudaaProjet _project, final AlbeFilleNoteDeCalculs.Settings _settings) {
    WHtmlContent content = null;
    if (_type.equals("PRESENTATION"))
      content = new Presentation(_parent, _project, _settings);
    else if (_type.equals("RAPPELHYPOTHESES"))
      content = new RH(_parent, _project, _settings);
    else if (_type.equals("RAPPELHYPOTHESES_OUVRAGE"))
      content = new RHOuvrage(_parent, _project, _settings);
    else if (_type.equals("RAPPELHYPOTHESES_SOL"))
      content = new RHCouchesSol(_parent, _project, _settings);
    else if (_type.equals("RAPPELHYPOTHESES_ACTIONSOUVRAGE"))
      content = new RHActionsOuvrage(_parent, _project, _settings);
    else if (_type.equals("RAPPELHYPOTHESES_COEFFPARTIELS"))
      content = new RHCoeffPartiels(_parent, _project, _settings);
    else if (_type.equals("RAPPELHYPOTHESES_COEFFPARTIELS_MATERIAUXETSOL"))
      content = new RHCoeffPartielsMateriauxSol(_parent, _project, _settings);
    else if (_type.equals("RAPPELHYPOTHESES_COEFFPARTIELS_MODELE"))
      content = new RHCoeffPartielsModele(_parent, _project, _settings);
    else if (_type.equals("RAPPELHYPOTHESES_COEFFPARTIELS_CRITERESDIM"))
      content = new RHCoeffPartielsCriteresDim(_parent, _project, _settings);
    else if (_type.equals("RAPPELHYPOTHESES_LANCEMENTCALCULS"))
      content = new RHCalculs(_parent, _project, _settings);
    else if (_type.equals("RESULTATS"))
      content = new RE(_parent, _project, _settings);
    else if (_type.equals("RESULTATS_TABLEAUXRECAPITULATIFS"))
      content = new RETableauxRecap(_parent, _project, _settings);
    else if (_type.equals("RESULTATS_DETAILLES"))
      content = new REDetailles(_parent, _project, _settings);

    return content;
  }

  /**
   * 
   */
  public static WHtmlContent createContentResultatsDetailles(final String _type,
      final JComponent _parent, final FudaaProjet _project,
      final AlbeFilleNoteDeCalculs.Settings _settings) {
    WHtmlContent content = null;

    if (_type.equals("RESULTATS_DETAILLES_COMBIMANUELLE"))
      content = new REDetaillesCombinaison(_parent, _project, _settings,
          AlbeLib.COMBINAISON_MANUELLE, 0);
    else {
      for (int i = 1; i <= 16; i++) {
        if (_type.equals("RESULTATS_DETAILLES_ELUFONDA" + i))
          return new REDetaillesCombinaison(_parent, _project, _settings, AlbeLib.ELU_FONDA, i);
      }
      for (int i = 1; i <= 8; i++) {
        if (_type.equals("RESULTATS_DETAILLES_ELUACC" + i))
          return new REDetaillesCombinaison(_parent, _project, _settings, AlbeLib.ELU_ACC, i);
      }
      for (int i = 1; i <= 8; i++) {
        if (_type.equals("RESULTATS_DETAILLES_ELSRARE" + i))
          return new REDetaillesCombinaison(_parent, _project, _settings, AlbeLib.ELS_RARE, i);
      }
      for (int i = 1; i <= 8; i++) {
        if (_type.equals("RESULTATS_DETAILLES_ELSFREQ" + i))
          return new REDetaillesCombinaison(_parent, _project, _settings, AlbeLib.ELS_FREQ, i);
      }
    }
    return content;
  }

  /**
   *
   */
  protected static class Presentation extends WHtmlContent {
    public Presentation(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      html_ += "Cette note d&eacute;crit les r&eacute;sultats des calculs effectu&eacute;s par le logiciel <i>Fudaa-"
          + AlbeImplementation.SOFTWARE_TITLE + "</i> (version "
          + AlbeImplementation.informationsSoftware().version + ").<br></p>"
          + CtuluLibString.LINE_SEP;
      Object fichier = getSettings().lit("PRESENTATION_FICHIER");
      Object titre = getSettings().lit("PRESENTATION_TITRE");
      Object auteur = getSettings().lit("PRESENTATION_AUTEUR");
      Object dateCalculs = getSettings().lit("PRESENTATION_DATECALCULS");
      Object adresse = getSettings().lit("PRESENTATION_ADRESSE");
      Object organisation = getSettings().lit("PRESENTATION_ORGANISATION");
      Object departement = getSettings().lit("PRESENTATION_DEPARTEMENT");
      Object version = getSettings().lit("PRESENTATION_VERSION");
      Object dateCreation = getSettings().lit("PRESENTATION_DATECREATION");
      Object dateModification = getSettings().lit("PRESENTATION_DATEMODIFICATION");
      Object commentaires = getSettings().lit("PRESENTATION_COMMENTAIRES");

      html_ += "<ul type = square>" + CtuluLibString.LINE_SEP;
      if (fichier != AlbeLib.VAL_NULLE)
        html_ += "<li><b>Nom du fichier : </b>" + fichier + "<br>" + CtuluLibString.LINE_SEP;
      if (titre != AlbeLib.VAL_NULLE)
        html_ += "<li><b>Titre du projet : </b>" + titre + "<br>" + CtuluLibString.LINE_SEP;
      if (auteur != AlbeLib.VAL_NULLE)
        html_ += "<li><b>Auteur : </b>" + auteur + "<br>" + CtuluLibString.LINE_SEP;
      if (dateCalculs != AlbeLib.VAL_NULLE)
        html_ += "<li><b>Date de r&eacute;alisation des calculs : </b>" + dateCalculs + "<br>"
            + CtuluLibString.LINE_SEP;
      if (adresse != AlbeLib.VAL_NULLE)
        html_ += "<li><b>Adresse courriel : </b>" + adresse + "<br>" + CtuluLibString.LINE_SEP;
      if (organisation != AlbeLib.VAL_NULLE)
        html_ += "<li><b>Organisation : </b>" + organisation + "<br>" + CtuluLibString.LINE_SEP;
      if (departement != AlbeLib.VAL_NULLE)
        html_ += "<li><b>D&eacute;partement : </b>" + departement + "<br>" + CtuluLibString.LINE_SEP;
      if (version != AlbeLib.VAL_NULLE)
        html_ += "<li><b>Version : </b>" + version + "<br>" + CtuluLibString.LINE_SEP;
      if (dateCreation != AlbeLib.VAL_NULLE)
        html_ += "<li><b>Date de cr&eacute;ation : </b>" + dateCreation + "<br>" + CtuluLibString.LINE_SEP;
      if (dateModification != AlbeLib.VAL_NULLE)
        html_ += "<li><b>Date de modification : </b>" + dateModification + "<br>"
            + CtuluLibString.LINE_SEP;
      if (commentaires != AlbeLib.VAL_NULLE)
        html_ += "<li><b>Commentaires : </b>" + commentaires + "<br>" + CtuluLibString.LINE_SEP;
      html_ += "</ul>" + CtuluLibString.LINE_SEP;
    }
  }// Fin classe Presentation

  /**
   * 
   */
  protected static class RH extends WHtmlContent {
    public RH(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {}
  }

  /**
   *
   */
  protected static class RHOuvrage extends WHtmlContent {
    public RHOuvrage(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {

      // Donn�es de l'ouvrage
      if (((AlbeFilleNoteDeCalculs) parent_).isChoosen("RAPPELHYPOTHESES_OUVRAGE_DONNEES")) {
        int nbTroncons = params_.pieu.nombreTroncons;
        // Calcul de la longueur du pieu
        double longueur = 0;
        if (params_.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE)) {
          for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
            longueur += params_.pieu.tronconsTubulaire[i].longueur;
          }
        } else {
          for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
            longueur += params_.pieu.tronconsCaisson[i].longueur;
          }
        }
        // Calcul de la cote du pied de l'ouvrage
        double cotePied = params_.geo.coteTeteOuvrage - longueur;
        html_ += "<ul type = square>" + CtuluLibString.LINE_SEP;
        html_ += "<li>Cote de la t&ecirc;te de l'ouvrage : " + params_.geo.coteTeteOuvrage + " m<br>"
            + CtuluLibString.LINE_SEP;
        html_ += "<li>Cote du pied de l'ouvrage : " + cotePied + " m<br>" + CtuluLibString.LINE_SEP;
        html_ += "<li>Cote de dragage : " + params_.geo.coteDragageBassin + " m<br>"
            + CtuluLibString.LINE_SEP;
        html_ += "<li>Cote de calcul : " + params_.geo.coteCalcul + " m<br>"
            + CtuluLibString.LINE_SEP;

        // Si ouvrage tubulaire
        if (params_.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE)) {
          Object[] colNamesTub = new Object[5];
          Object[] tronconTub = new Object[nbTroncons];
          Object[] longueurTub = new Object[nbTroncons];
          Object[] epaisseur = new Object[nbTroncons];
          Object[] fykTub = new Object[nbTroncons];
          Object[] corrosion = new Object[nbTroncons];
          colNamesTub[0] = "Tron&ccedil;on";
          colNamesTub[1] = "Longueur (m)";
          colNamesTub[2] = "Epaisseur (mm)";
          colNamesTub[3] = "f<SUB>yk</SUB> (MPa)";
          colNamesTub[4] = "Corrosion (mm)";
          for (int i = 1; i < nbTroncons + 1; i++) {
            tronconTub[i - 1] = "" + i;
            longueurTub[i - 1] = "" + params_.pieu.tronconsTubulaire[i - 1].longueur;
            epaisseur[i - 1] = "" + params_.pieu.tronconsTubulaire[i - 1].epaisseur;
            fykTub[i - 1] = ""
                + params_.pieu.tronconsTubulaire[i - 1].valeurCaracteristiqueLimiteElastiqueAcier;
            corrosion[i - 1] = "" + params_.pieu.tronconsTubulaire[i - 1].perteEpaisseurCorrosion;
          }
          html_ += "<br><li>Diam&egrave;tre du tube : " + params_.pieu.diametreTroncons + " mm<br>"
              + CtuluLibString.LINE_SEP;
          addTable(colNamesTub, new Object[][] { tronconTub, longueurTub, epaisseur, fykTub,
              corrosion });
        }
        // Si ouvrage en caissons
        else if (params_.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_CAISSON)) {
          Object[] colNamesCais = new Object[7];
          Object[] tronconCais = new Object[nbTroncons];
          Object[] longueurCais = new Object[nbTroncons];
          Object[] inertie = new Object[nbTroncons];
          Object[] section = new Object[nbTroncons];
          Object[] fykCais = new Object[nbTroncons];
          Object[] inertieCor = new Object[nbTroncons];
          Object[] sectionCor = new Object[nbTroncons];
          colNamesCais[0] = "Tron&ccedil;on";
          colNamesCais[1] = "Longueur (m)";
          colNamesCais[2] = "Inertie (cm<SUP>4</SUP>)";
          colNamesCais[3] = "Section (cm<SUP>2</SUP>)";
          colNamesCais[4] = "f<SUB>yk</SUB> (MPa)";
          colNamesCais[5] = "Inertie corrod�e (cm<SUP>4</SUP>)";
          colNamesCais[6] = "Section corrod�e (cm<SUP>2</SUP>)";
          for (int i = 1; i < nbTroncons + 1; i++) {
            tronconCais[i - 1] = "" + i;
            longueurCais[i - 1] = "" + params_.pieu.tronconsCaisson[i - 1].longueur;
            inertie[i - 1] = "" + params_.pieu.tronconsCaisson[i - 1].inertieNonCorrodee;
            section[i - 1] = "" + params_.pieu.tronconsCaisson[i - 1].sectionNonCorrodee;
            fykCais[i - 1] = ""
                + params_.pieu.tronconsCaisson[i - 1].valeurCaracteristiqueLimiteElastiqueAcier;
            inertieCor[i - 1] = "" + params_.pieu.tronconsCaisson[i - 1].inertieCorrodee;
            sectionCor[i - 1] = "" + params_.pieu.tronconsCaisson[i - 1].sectionCorrodee;
          }
          html_ += "<br><li><b>Largeur effective du pieu : </b>"
              + params_.pieu.largeurEffectivePieu + " cm" + CtuluLibString.LINE_SEP;
          html_ += "<br><li><b>Excentricit� du caisson : </b>" + params_.pieu.excentricite
              + " cm<br>" + CtuluLibString.LINE_SEP;
          addTable(colNamesCais, new Object[][] { tronconCais, longueurCais, inertie, section,
              fykCais, inertieCor, sectionCor });
        }
        // Si une d�fense est pr�sente
        if (defense_) {
          int nbPoints = params_.defense.nombrePoints;
          Object[] colNamesDef = new Object[3];
          Object[] deflexion = new Object[nbPoints];
          Object[] reaction = new Object[nbPoints];
          Object[] energies = new Object[nbPoints];
          colNamesDef[0] = "D&eacute;flexion (%)";
          colNamesDef[1] = "R&eacute;action (kN)";
          colNamesDef[2] = "Energie (kJ)";
          double[] en = calculeEnergies();
          for (int i = 0; i < nbPoints; i++) {
            deflexion[i] = "" + params_.defense.points[i].deformation;
            reaction[i] = "" + params_.defense.points[i].effort;
            energies[i] = "" + AlbeLib.arronditDouble(en[i]);
          }
          html_ += "<br><li>Hauteur de la d&eacute;fense : " + params_.defense.hauteur + " mm<br>"
              + CtuluLibString.LINE_SEP;
          addTable(colNamesDef, new Object[][] { deflexion, reaction, energies });
          addImgFromScript(getScriptReaction(), "courbe_deflexion_reaction.gif",
              "Courbe d�flexion r�action");
          html_ += "<br><br>";
          addImgFromScript(getScriptEnergie(), "courbe_deflexion_energie.gif",
              "Courbe d�flexion-absorption d'�nergie");
          html_ += "<br>";
        }
        html_ += "</ul><br>" + CtuluLibString.LINE_SEP;
      }

      // Sch�ma de l'ouvrage
      if (((AlbeFilleNoteDeCalculs) parent_).isChoosen("RAPPELHYPOTHESES_OUVRAGE_SCHEMA")) {
        final String file = fichier_ + "files" + AlbeLib.FILE_SEPARATOR + "schema_ouvrage.png";
        AlbeSchemaOuvragePanel imagePanel = new AlbeSchemaOuvragePanel(
            ((AlbeFilleNoteDeCalculs) parent_).appli_, params_);
        final String cheminRel = name_ + "files" + AlbeLib.FILE_SEPARATOR + "schema_ouvrage.png";

        imagePanel.setSize(630, 600);
        imagePanel.setDoubleBuffered(false);

        CtuluImageExport.export(imagePanel.produceImage(null), new File(file), "png",
            ((AlbeImplementation) ((AlbeFilleNoteDeCalculs) parent_).appli_.getImplementation()));
        html_ += "<b>Sch&eacute;ma &agrave; l'&eacute;chelle de l'ouvrage :</b><br><br>" + CtuluLibString.LINE_SEP;
        html_ += "<a href=\"" + cheminRel + "\" target=\"_blank\"><img src=\"" + cheminRel
            + "\" alt =\"Sch�ma de l'ouvrage\"></a><br><br>" + CtuluLibString.LINE_SEP;
      }
    }

    /**
     * calcule les valeurs des �nergies.
     */
    protected double[] calculeEnergies() {
      double[] energies = new double[params_.defense.nombrePoints];

      // R�cup�ration de la valeur de la hauteur (saisie en mm) et conversion en
      // m�tres
      double hauteur = params_.defense.hauteur / 1000;

      energies[0] = params_.defense.points[0].deformation / 100 * hauteur
          * params_.defense.points[0].effort / 2;

      for (int i = 1; i < params_.defense.nombrePoints; i++) {
        double num = ((params_.defense.points[i].deformation - params_.defense.points[i - 1].deformation) / 100)
            * hauteur * (params_.defense.points[i].effort + params_.defense.points[i - 1].effort);
        energies[i] = num / 2 + energies[i - 1];
      }
      return energies;
    }

    protected String getScriptReaction() {
      final StringBuffer s = new StringBuffer(4096);
      double pas = AlbeLib.getPas(params_.defense.points[params_.defense.nombrePoints - 1].effort,
          0);
      s.append("graphe \n{\n");
      s.append("  titre \"D�fenses d'accostage\"\n");
      s.append("  sous-titre \"Courbe d�flexion-r�action\"\n");
      s.append(((AlbeFilleParametres) ((AlbeFilleNoteDeCalculs) parent_)
          .getInternalFrame(AlbeLib.INTERNAL_FRAME_PARAMETRES)).defense_.getScriptCommun());
      s.append("    titre \"effort\"\n");
      s.append("    unite \"kN\"\n");
      s.append("    orientation vertical\n");
      s.append("    graduations oui\n");
      s.append("    minimum 0\n");
      s.append("    maximum " + 10 * pas + "\n");
      s.append("    pas " + pas + "\n");
      s.append("  }\n");
      s.append("  courbe\n{\n");
      s.append("    aspect\n{\n");
      s.append("    contour.couleur 009999\n");
      s.append("    }\n");
      s.append("    valeurs\n{\n");
      s.append("      " + 0 + " " + 0 + "\n");
      for (int i = 0; i < params_.defense.nombrePoints; i++) {
        s.append("      " + params_.defense.points[i].deformation + " "
            + params_.defense.points[i].effort + "\n");
      }
      s.append("    }\n");
      s.append("  }\n");
      return s.toString();
    }

    /**
     * retourne le pas de l'axe des efforts de la courbe def-absorption.
     */
    public double getPasA() {

      return AlbeLib.getPas(calculeEnergies()[params_.defense.nombrePoints - 1], 0);
    }

    /**
     * retourne l'ordonn�e maximale � afficher sur l'axe vertical de la courbe
     * def-absorption.
     */
    public double getMaxA() {
      return 10 * getPasA();
    }

    protected String getScriptEnergie() {
      final StringBuffer s = new StringBuffer(4096);
      double[] energies = calculeEnergies();

      s.append("graphe \n{\n");
      s.append("  titre \"D�fenses d'accostage\"\n");
      s.append("  sous-titre \"Courbe d�flexion-absorption d'�nergie\"\n");
      s.append(((AlbeFilleParametres) ((AlbeFilleNoteDeCalculs) parent_)
          .getInternalFrame(AlbeLib.INTERNAL_FRAME_PARAMETRES)).defense_.getScriptCommun());
      s.append("    titre \"�nergie\"\n");
      s.append("    unite \"kJ\"\n");
      s.append("    orientation vertical\n");
      s.append("    graduations oui\n");
      s.append("    minimum 0\n");
      s.append("    maximum  " + getMaxA() + "\n");
      s.append("    pas " + getPasA() + "\n");
      s.append("  }\n");
      s.append("  courbe\n{\n");
      s.append("    type nuage\n");
      s.append("    valeurs\n{\n");
      s.append("      " + 0 + " " + 0 + "\n");
      for (int i = 0; i < params_.defense.nombrePoints; i++) {
        s.append("      " + params_.defense.points[i].deformation + " " + energies[i] + "\n");
      }
      s.append("    }\n");
      s.append("  }\n");
      return s.toString();
    }

  }// Fin classe RHOuvrage

  /**
   *
   */
  protected static class RHCouchesSol extends WHtmlContent {
    public RHCouchesSol(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      int nbCouches = params_.geo.nombreCouchesSol;
      if (!params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_MANUELLE)) {
        Object[] couche = new Object[nbCouches];
        Object[] epaisseur = new Object[nbCouches];
        Object[] pl = new Object[nbCouches];
        Object[] pf = new Object[nbCouches];
        Object[] em = new Object[nbCouches];
        Object[] alpha = new Object[nbCouches];
        int nbCol = params_.sol.modeleComportementSol
            .equals(AlbeLib.IDL_COURBE_FASCICULE_TRES_COURTE_DUREE) ? 6 : 5;
        Object[] colNames = new Object[nbCol];
        colNames[0] = "Couche";
        colNames[1] = "Epaisseur (m)";
        colNames[nbCol - 2] = "Em (kPa)";
        colNames[nbCol - 1] = "Alpha";

        for (int i = 1; i < nbCouches + 1; i++) {
          couche[i - 1] = "" + i;
          epaisseur[i - 1] = "" + params_.sol.courbe[i - 1].epaisseur;
          em[i - 1] = "" + params_.sol.courbe[i - 1].modulePressiometrique;
          alpha[i - 1] = "" + params_.sol.courbe[i - 1].coefficientRheologique;
        }
        html_ += "<ul type = disc>" + CtuluLibString.LINE_SEP;
        html_ += "<li><b>Caract&eacute;ristiques introduites :</b><br>";
        if (params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_ELASTO_PLASTIQUE_PURE)
            || params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_SETRA)) {
          colNames[2] = "pl* (kPa)";
          for (int i = 1; i < nbCouches + 1; i++) {
            pl[i - 1] = "" + params_.sol.courbe[i - 1].pressionLimite;
          }
          addTable(colNames, new Object[][] { couche, epaisseur, pl, em, alpha });
        } else if (params_.sol.modeleComportementSol
            .equals(AlbeLib.IDL_COURBE_FASCICULE_COURTE_DUREE)
            || params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_LONGUE_DUREE)) {
          colNames[2] = "pf (kPa)";
          for (int i = 1; i < nbCouches + 1; i++) {
            pf[i - 1] = "" + params_.sol.courbe[i - 1].pressionFluage;
          }
          addTable(colNames, new Object[][] { couche, epaisseur, pf, em, alpha });
        } else if (params_.sol.modeleComportementSol
            .equals(AlbeLib.IDL_COURBE_FASCICULE_TRES_COURTE_DUREE)) {
          colNames[2] = "pl* (kPa)";
          colNames[3] = "pf (kPa)";
          for (int i = 1; i < nbCouches + 1; i++) {
            pl[i - 1] = "" + params_.sol.courbe[i - 1].pressionLimite;
            pf[i - 1] = "" + params_.sol.courbe[i - 1].pressionFluage;
          }
          addTable(colNames, new Object[][] { couche, epaisseur, pl, pf, em, alpha });
        }

      }

      html_ += "<li><b>Mod&egrave;le de comportement adopt&eacute; : </b>";
      if (params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_MANUELLE))
        html_ += "courbe introduite manuellement point � point<br><br>";
      else if (params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_ELASTO_PLASTIQUE_PURE))
        html_ += "courbe de r�action &eacute;lasto-plastique<br><br>";
      else if (params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_SETRA))
        html_ += "courbe type du SETRA<br><br>";
      else if (params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_COURTE_DUREE))
        html_ += "courbe du fascicule 62 titre V sollicitation de courte dur&eacute;e<br><br>";
      else if (params_.sol.modeleComportementSol
          .equals(AlbeLib.IDL_COURBE_FASCICULE_TRES_COURTE_DUREE))
        html_ += "courbe du fascicule 62 titre V sollicitation de tr&egrave;s courte dur&eacute;e<br><br>";
      else if (params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_LONGUE_DUREE))
        html_ += "courbe du fascicule 62 titre V sollicitation de longue dur&eacute;e<br><br>";
      html_ += "</ul>" + CtuluLibString.LINE_SEP;
      html_ += "<ul type = circle>" + CtuluLibString.LINE_SEP;
      for (int i = 0; i < nbCouches; i++) {
        buildContentCouche(i + 1);
      }
      html_ += "</ul>" + CtuluLibString.LINE_SEP;
    }

    /**
     * construit la partie HTML r�capitulant les donn�es pour la couche de sol
     * _n.
     * 
     * @param _n numero de la couche (commence � 1)
     */
    protected void buildContentCouche(int _n) {
      int nbPoints = params_.sol.nombrePoints;
      Object[] colNames = new Object[2];
      Object[] deplacement = new Object[nbPoints];
      Object[] pression = new Object[nbPoints];
      colNames[0] = "D&eacute;placement (m)";
      colNames[1] = "Pression (kPa)";
      for (int i = 0; i < nbPoints; i++) {
        deplacement[i] = "" + params_.sol.points[_n - 1][i].deplacement;
        pression[i] = "" + params_.sol.points[_n - 1][i].pression;
      }
      html_ += "<li><b>Couche " + _n + "</b><br>";
      addTable(colNames, new Object[][] { deplacement, pression });
      String script = "";
      if (params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_MANUELLE))
        script = getScriptCourbeManuelle(_n);
      else if (params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_ELASTO_PLASTIQUE_PURE)
          || params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_COURTE_DUREE)
          || params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_FASCICULE_LONGUE_DUREE))
        script = getScriptCourbe5pts(_n);

      else if (params_.sol.modeleComportementSol.equals(AlbeLib.IDL_COURBE_SETRA)
          || params_.sol.modeleComportementSol
              .equals(AlbeLib.IDL_COURBE_FASCICULE_TRES_COURTE_DUREE))
        script = getScriptCourbe7pts(_n);

      addImgFromScript(script, "courbe_deplacement_pression_couche" + _n + ".gif",
          "Courbe d�placement pression couche " + _n);
      html_ += "<br><br>";
    }

    protected double getPasXCourbeManuelle(int _n) {
      return AlbeLib.getPas(params_.sol.points[_n - 1][params_.sol.nombrePoints - 1].deplacement,
          params_.sol.points[_n - 1][0].deplacement);
    }

    protected double getMinXCourbeManuelle(int _n) {
      double min = getPasXCourbeManuelle(_n);

      if (params_.sol.points[_n - 1][0].deplacement <= 0) {
        while (min >= params_.sol.points[_n - 1][0].deplacement) {
          min -= getPasXCourbeManuelle(_n);
        }
      } else {
        if (min < params_.sol.points[_n - 1][0].deplacement) {
          while (min + getPasXCourbeManuelle(_n) < params_.sol.points[_n - 1][0].deplacement) {
            min += getPasXCourbeManuelle(_n);
          }
        } else {
          return 0;
        }
      }
      return min;
    }

    protected double getMaxXCourbeManuelle(int _n) {
      double max = getPasXCourbeManuelle(_n);
      if (params_.sol.points[_n - 1][params_.sol.nombrePoints - 1].deplacement >= 0) {
        while (max <= params_.sol.points[_n - 1][params_.sol.nombrePoints - 1].deplacement) {
          max += getPasXCourbeManuelle(_n);
        }
      } else {
        while (max - getPasXCourbeManuelle(_n) > params_.sol.points[_n][params_.sol.nombrePoints - 1].deplacement) {
          max -= getPasXCourbeManuelle(_n);
        }
      }
      return max;
    }

    protected double getPasYCourbeManuelle(int _n) {
      return AlbeLib.getPas(params_.sol.points[_n - 1][params_.sol.nombrePoints - 1].pression,
          params_.sol.points[_n - 1][0].pression);
    }

    protected double getMinYCourbeManuelle(int _n) {

      double min = getPasYCourbeManuelle(_n);
      if (params_.sol.points[_n - 1][0].pression <= 0) {
        while (min >= params_.sol.points[_n - 1][0].pression) {
          min -= getPasYCourbeManuelle(_n);
        }
      } else {
        if (min < params_.sol.points[_n - 1][0].pression) {
          while (min + getPasYCourbeManuelle(_n) < params_.sol.points[_n - 1][0].pression) {
            min += getPasYCourbeManuelle(_n);
          }
        } else {
          return 0;
        }
      }
      return min;
    }

    protected double getMaxYCourbeManuelle(int _n) {
      double max = getPasYCourbeManuelle(_n);
      if (params_.sol.points[_n - 1][params_.sol.nombrePoints - 1].pression >= 0) {
        while (max <= params_.sol.points[_n - 1][params_.sol.nombrePoints - 1].pression) {
          max += getPasYCourbeManuelle(_n);
        }
      } else {
        while (max - getPasYCourbeManuelle(_n) > params_.sol.points[_n - 1][params_.sol.nombrePoints - 1].pression) {
          max -= getPasYCourbeManuelle(_n);
        }
      }
      return max;
    }

    protected String getScriptCourbeManuelle(int _n) {
      final StringBuffer s = new StringBuffer();
      s.append(((AlbeFilleParametres) ((AlbeFilleNoteDeCalculs) parent_)
          .getInternalFrame(AlbeLib.INTERNAL_FRAME_PARAMETRES)).sol_.getScriptCourbeSol(_n,
          getMinXCourbeManuelle(_n), getMaxXCourbeManuelle(_n), getPasXCourbeManuelle(_n),
          getMinYCourbeManuelle(_n), getMaxYCourbeManuelle(_n), getPasYCourbeManuelle(_n)));
      for (int i = 0; i < params_.sol.nombrePoints; i++) {
        s.append("      " + params_.sol.points[_n - 1][i].deplacement + " "
            + params_.sol.points[_n - 1][i].pression + "\n");
      }
      s.append("    }\n");
      s.append("  }\n");
      return s.toString();
    }

    protected double getMaxXCourbe5Pts(int _n) {
      return params_.sol.points[_n - 1][4].deplacement + getPasXCourbe5Pts(_n);
    }

    protected double getPasXCourbe5Pts(int _n) {
      return params_.sol.points[_n - 1][4].deplacement / 2;
    }

    protected double getMaxYCourbe5Pts(int _n) {
      return (int) (1.2 * params_.sol.points[_n - 1][3].pression);
    }

    protected double getPasYCourbe5Pts(int _n) {
      return (int) (getMaxYCourbe5Pts(_n) / 5);
    }

    protected String getScript(int _n, double _maxX, double _pasX, double _maxY, double _pasY) {
      final StringBuffer s = new StringBuffer();

      s.append(((AlbeFilleParametres) ((AlbeFilleNoteDeCalculs) parent_)
          .getInternalFrame(AlbeLib.INTERNAL_FRAME_PARAMETRES)).sol_.getScriptCourbeSol(_n, -_maxX,
          _maxX, _pasX, -_maxY, _maxY, _pasY));
      for (int i = 0; i < params_.sol.nombrePoints; i++) {
        s.append("      " + params_.sol.points[_n - 1][i].deplacement + " "
            + params_.sol.points[_n - 1][i].pression + "\n");
      }
      s.append("    }\n");
      s.append("  }\n");
      return s.toString();
    }

    protected String getScriptCourbe5pts(int _n) {
      return getScript(_n, getMaxXCourbe5Pts(_n), getPasXCourbe5Pts(_n), getMaxYCourbe5Pts(_n),
          getPasYCourbe5Pts(_n));
    }

    protected double getMaxXCourbe7Pts(int _n) {
      double max = 0;
      while (max <= params_.sol.points[_n - 1][6].deplacement) {
        max += params_.sol.points[_n - 1][4].deplacement;
      }
      return max;
    }

    protected double getPasXCourbe7Pts(int _n) {

      return getMaxXCourbe7Pts(_n) / 2;
    }

    protected double getMaxYCourbe7Pts(int _n) {

      return (int) (1.2 * params_.sol.points[_n - 1][5].pression);
    }

    protected double getPasYCourbe7Pts(int _n) {

      return (int) (getMaxYCourbe7Pts(_n) / 6);
    }

    protected String getScriptCourbe7pts(int _n) {
      return getScript(_n, getMaxXCourbe7Pts(_n), getPasXCourbe7Pts(_n), getMaxYCourbe7Pts(_n),
          getPasYCourbe7Pts(_n));
    }

  }// fin classe RHCouchesSol

  /**
   *
   */
  protected static class RHActionsOuvrage extends WHtmlContent {
    public RHActionsOuvrage(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {

      if (accostage_) {
        html_ += "Cote d'accostage haut : " + params_.action.coteAccostageHaut + " m<br>"
            + CtuluLibString.LINE_SEP;
        html_ += "Cote d'accostage bas : " + params_.action.coteAccostageBas + " m<br>"
            + CtuluLibString.LINE_SEP;
        html_ += "<br>Effort vertical en t&ecirc;te de pieu : " + params_.action.effortVerticalAccostage
            + " kN<br>" + CtuluLibString.LINE_SEP;
        html_ += "<br><b>Energies d'accostage</b>" + CtuluLibString.LINE_SEP;
        html_ += "<ul type = square>" + CtuluLibString.LINE_SEP;
        html_ += "<li>Accostage fr&eacute;quent : " + params_.action.energieAccostageFrequente + " kJ<br>"
            + CtuluLibString.LINE_SEP;
        html_ += "<li>Accostage caract&eacute;ristique : "
            + params_.action.energieAccostageCaracteristique + " kJ<br>" + CtuluLibString.LINE_SEP;
        html_ += "<li>Accostage de calcul : " + params_.action.energieAccostageCalcul + " kJ<br>"
            + CtuluLibString.LINE_SEP;
        html_ += "<li>Accostage accidentel : " + params_.action.energieAccostageAccidentelle
            + " kJ<br>" + CtuluLibString.LINE_SEP;
        html_ += "</ul>" + CtuluLibString.LINE_SEP;
      }

      else {
        html_ += "Cote amarrage : " + params_.action.coteAmarrage + " m<br>"
            + CtuluLibString.LINE_SEP;
        html_ += "<b><br>Forces d'amarrage</b><br>" + CtuluLibString.LINE_SEP;
        html_ += "<ul type = square>" + CtuluLibString.LINE_SEP;
        html_ += "<li>Amarrage fr�quent : " + params_.action.effortAmarrageFrequent + " kN<br>"
            + CtuluLibString.LINE_SEP;
        html_ += "<li>Amarrage caract�ristique : " + params_.action.effortAmarrageCaracteristique
            + " kN<br>" + CtuluLibString.LINE_SEP;
        html_ += "<li>Amarrage de calcul : " + params_.action.effortAmarrageCalcul + " kN<br>"
            + CtuluLibString.LINE_SEP;
        html_ += "<li>Amarrage accidentel : " + params_.action.effortAmarrageAccidentel + " kN<br>"
            + CtuluLibString.LINE_SEP;
        html_ += "<li>Effort vertical fr�quent : " + params_.action.effortVerticalAmarrageFrequent
            + " kN<br>" + CtuluLibString.LINE_SEP;
        html_ += "<li>Effort vertical caract�ristique : "
            + params_.action.effortVerticalAmarrageCaracteristique + " kN<br>"
            + CtuluLibString.LINE_SEP;
        html_ += "<li>Effort vertical de calcul : " + params_.action.effortVerticalAmarrageCalcul
            + " kN<br>" + CtuluLibString.LINE_SEP;
        html_ += "<li>Effort vertical accidentel : "
            + params_.action.effortVerticalAmarrageAccidentel + " kN<br>" + CtuluLibString.LINE_SEP;
        html_ += "</ul>" + CtuluLibString.LINE_SEP;
      }
    }
  }// Fin classe RHActionsOuvrage

  /**
   *
   */
  protected static class RHCoeffPartiels extends WHtmlContent {
    public RHCoeffPartiels(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {}

  }

  /**
   * 
   */
  protected static class RHCoeffPartielsMateriauxSol extends WHtmlContent {
    public RHCoeffPartielsMateriauxSol(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      // Donn�es pour le tableau des coefficients de l'acier
      Object[] colNamesAcier = new Object[2];
      Object[] combiAcier = new Object[3];
      Object[] coefAcier = new Object[3];
      colNamesAcier[0] = "Combinaison";
      colNamesAcier[1] = "Valeur du coefficient";
      combiAcier[0] = "ELU fondamental";
      combiAcier[1] = "ELU accidentel";
      combiAcier[2] = "ELS rare";
      for (int i = 0; i < 3; i++) {
        coefAcier[i] = "" + params_.coefficientsPartiels.coeffAcier[i];
      }
      // Donn�es pour le tableau des coefficients de sol
      Object[] colNames = new Object[3];
      colNames[0] = "Combinaison";
      colNames[1] = "Favorable (>1)";
      colNames[2] = "D&eacute;favorable (<1)";
      html_ += "<ul type = disc>" + CtuluLibString.LINE_SEP;
      html_ += "<li><b>R&eacute;sistance &eacute;lastique de l'acier</b><br>" + CtuluLibString.LINE_SEP;
      addTable(colNamesAcier, new Object[][] { combiAcier, coefAcier });
      html_ += "<br><li><b>Courbe de r&eacute;action du sol pour une action du sol consid&eacute;r&eacute;e comme &eacute;tant</b><br> "
          + CtuluLibString.LINE_SEP;
      addTable(colNames, new Object[][] { new Object[] { "ELU fondamental" },
          new Object[] { "" + params_.coefficientsPartiels.coeffSol[0].favorable },
          new Object[] { "" + params_.coefficientsPartiels.coeffSol[0].defavorable } });
      if (defense_) {
        // Donn�es du tableau sur les coefficients de d�fense
        Object[] combiDefense = new Object[4];
        combiDefense[0] = "ELU fondamental";
        combiDefense[1] = "ELU accidentel";
        combiDefense[2] = "ELS rare";
        combiDefense[3] = "ELS fr&eacute;quent";
        Object[] coefDefenseFav = new Object[4];
        Object[] coefDefenseDef = new Object[4];
        for (int i = 0; i < 4; i++) {
          coefDefenseFav[i] = "" + params_.coefficientsPartiels.coeffDefense[i].favorable;
          coefDefenseDef[i] = "" + params_.coefficientsPartiels.coeffDefense[i].defavorable;
        }
        html_ += "<br><li><b>Courbe de r&eacute;action des d&eacute;fenses pour une action de la d&eacute;fense consid&eacute;r&eacute;e comme &eacute;tant</b><br>"
            + CtuluLibString.LINE_SEP;
        addTable(colNames, new Object[][] { combiDefense, coefDefenseFav, coefDefenseDef });
      }
      html_ += "</ul>" + CtuluLibString.LINE_SEP;
    }
  }

  /**
   * 
   */
  protected static class RHCoeffPartielsModele extends WHtmlContent {
    public RHCoeffPartielsModele(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      html_ += "<ul><table border=\"2\" cellspacing=\"0\" cellpadding=\"5\">"
          + CtuluLibString.LINE_SEP;
      html_ += "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<th bgcolor=\"#DDDDDD\">Etat-limite et combinaison<br>type d'actions associ&eacute;es</th>"
          + CtuluLibString.LINE_SEP;
      html_ += "<th  bgcolor=\"#DDDDDD\">Valeur du coef. de mod&egrave;le</th>" + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP + "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<th colspan=\"3\" align = center>INSTABILITE EXTERNE</th>"
          + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP + "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<th colspan=\"3\" align = left>Mobilisation du sol en but&eacute;e</th>"
          + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP + "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = right>fondamentale</td>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = center>"
          + params_.coefficientsPartiels.coeffModele[0].mobilisationButeeSol + "</td>"
          + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP + "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = right>accidentelle</td>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = center>"
          + params_.coefficientsPartiels.coeffModele[1].mobilisationButeeSol + "</td>"
          + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP + "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = right>rare</td>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = center>/</td>" + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP + "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = right>fr&eacute;quente</td>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = center>"
          + params_.coefficientsPartiels.coeffModele[3].mobilisationButeeSol + "</td>"
          + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP;
      html_ += "<th colspan=\"3\" align = center>INSTABILITE INTERNE</th>"
          + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP + "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<th colspan=\"3\" align = left>R&eacute;sistance structurale</th>"
          + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP + "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = right>fondamentale</td>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = center>"
          + params_.coefficientsPartiels.coeffModele[0].resistanceStructuralePieux + "</td>"
          + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP + "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = right>accidentelle</td>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = center>"
          + params_.coefficientsPartiels.coeffModele[1].resistanceStructuralePieux + "</td>"
          + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP + "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = right>rare</td>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = center>"
          + params_.coefficientsPartiels.coeffModele[2].resistanceStructuralePieux + "</td>"
          + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP + "<tr>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = right>fr&eacute;quente</td>" + CtuluLibString.LINE_SEP;
      html_ += "<td align = center>/</td>" + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP;
      html_ += "</tr>" + CtuluLibString.LINE_SEP;
      html_ += "</table></ul><br>" + CtuluLibString.LINE_SEP;
    }
  }

  /**
   * 
   */
  protected static class RHCoeffPartielsCriteresDim extends WHtmlContent {
    public RHCoeffPartielsCriteresDim(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      html_ += "<ul type = disc>" + CtuluLibString.LINE_SEP;
      // D�flexion de la d�fense (si une d�fense est pr�sente)
      if (defense_) {
        Object[] deflexionColNames = new Object[2];
        Object[] combiDeflexion = new Object[3];
        Object[] deflexion = new Object[3];
        deflexionColNames[0] = "Combinaison type";
        deflexionColNames[1] = "D&eacute;flexion admissible (%)";
        combiDeflexion[0] = "ELS rare";
        combiDeflexion[1] = "ELU fondamental";
        combiDeflexion[2] = "ELU accidentel";
        deflexion[0] = "" + params_.criteresDimensionnement.deflexionDefenseElsRare;
        deflexion[1] = "" + params_.criteresDimensionnement.deflexionDefenseEluFonda;
        deflexion[2] = "" + params_.criteresDimensionnement.deflexionDefenseEluAcc;
        html_ += "<li><b>D&eacute;flexion de la d&eacute;fense</b><br>" + CtuluLibString.LINE_SEP;
        addTable(deflexionColNames, new Object[][] { combiDeflexion, deflexion });
        html_ += "<br>";
      }
      // D�placement en t�te
      html_ += "<li><b>D&eacute;placement en t&ecirc;te</b><br>" + CtuluLibString.LINE_SEP;
      addTable(new Object[] { "Combinaison type", "D&eacute;placement admissible (m)" }, new Object[][] {
          { "ELS rare" }, { "" + params_.criteresDimensionnement.deplacementTeteOuvrage } });
      // R�action au choc (si une d�fense est pr�sente)
      if (defense_) {
        Object[] reaction = new Object[2];
        reaction[0] = "" + params_.criteresDimensionnement.reactionTeteOuvrageEluFonda;
        reaction[1] = "" + params_.criteresDimensionnement.reactionTeteOuvrageEluAcc;
        html_ += "<br><li><b>R&eacute;action au choc</b><br>" + CtuluLibString.LINE_SEP;
        addTable(new Object[] { "Combinaison type", "R&eacute;action admissible (kN)" }, new Object[][] {
            new Object[] { "ELU fondamental", "ELU accidentel" }, reaction });
      }
      html_ += "</ul>" + CtuluLibString.LINE_SEP;
    }
  }// Fin classe RHCoeffPartielsCriteresDim

  /**
   *
   */
  protected static class RHCalculs extends WHtmlContent {
    public RHCalculs(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      if (params_.lancementCalculs.modeLancementCalcul == AlbeLib.IDL_LANCEMENT_AUTOMATIQUE) {
        html_ += "Les calculs ont &eacute;t&eacute; lanc&eacute;s en automatique pour la (les) combinaison(s) suivante(s) : <br>"
            + CtuluLibString.LINE_SEP;
        html_ += "<ul type = disc>" + CtuluLibString.LINE_SEP;
        if (params_.lancementCalculs.eluFonda)
          html_ += "<li>ELU fondamental";
        if (params_.lancementCalculs.eluAcc)
          html_ += "<li>ELU accidentel";
        if (params_.lancementCalculs.elsRare)
          html_ += "<li>ELS rare";
        if (params_.lancementCalculs.elsFreq)
          html_ += "<li>ELS fr&eacute;quent";
        html_ += "</ul>" + CtuluLibString.LINE_SEP;
      } else if (params_.lancementCalculs.modeLancementCalcul == AlbeLib.IDL_LANCEMENT_MANUEL)
        html_ += "Les calculs ont &eacute;t&eacute; lanc&eacute;s manuellement pour une combinaison.<br>";
    }
  }

  /**
   * 
   */
  protected static class RE extends WHtmlContent {
    public RE(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {}
  }

  /**
   * 
   */
  protected static class RETableauxRecap extends WHtmlContent {
    public RETableauxRecap(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {
      if (params_.lancementCalculs.modeLancementCalcul == AlbeLib.IDL_LANCEMENT_MANUEL) {
        html_ += "<b>Coefficients partiels utilis&eacute;s pour le calcul</b>" + CtuluLibString.LINE_SEP;
        html_ += "<ul type = square>" + CtuluLibString.LINE_SEP;
        html_ += "<li><img src=\"" + Albe.class.getResource("gamma-m.gif")
            + " \"> sur le comportement du sol : " + params_.lancementCalculs.gammaMSol
            + CtuluLibString.LINE_SEP;
        html_ += "<br><li><img src=\"" + Albe.class.getResource("gamma-m.gif")
            + " \"> sur le comportement de la d&eacute;fense : " + params_.lancementCalculs.gammaMDefense
            + CtuluLibString.LINE_SEP;
        html_ += "<br><li><img src=\"" + Albe.class.getResource("gamma-fy.gif")
            + " \"> sur la r&eacute;sistance &eacute;lastique de l'acier : "
            + params_.lancementCalculs.gammaFyAcier + CtuluLibString.LINE_SEP;
        html_ += "<br><li><img src=\"" + Albe.class.getResource("gamma-d.gif")
            + " \"> mobilisation de la but&eacute;e du sol : " + params_.lancementCalculs.gammaDButee
            + CtuluLibString.LINE_SEP;
        html_ += "<br><li><img src=\"" + Albe.class.getResource("gamma-d.gif")
            + " \"> r&eacute;sistance struturale des pieux : " + params_.lancementCalculs.gammaDPieu
            + CtuluLibString.LINE_SEP;
        html_ += "</ul>" + CtuluLibString.LINE_SEP;
      }

      else if (params_.lancementCalculs.modeLancementCalcul == AlbeLib.IDL_LANCEMENT_AUTOMATIQUE) {
        html_ += "<ul type = disc>" + CtuluLibString.LINE_SEP;
        if (params_.lancementCalculs.eluFonda) {
          html_ += "<li><b>ELU fondamental</b><br><br>" + CtuluLibString.LINE_SEP;
          html_ += AlbeRes.buildTabRecapResultats(params_, results_, accostage_, defense_,
              AlbeLib.ELU_FONDA, false)
              + "<br><br>";
        }
        if (params_.lancementCalculs.eluAcc) {
          html_ += "<li><b>ELU accidentel</b><br><br>" + CtuluLibString.LINE_SEP;
          html_ += AlbeRes.buildTabRecapResultats(params_, results_, accostage_, defense_,
              AlbeLib.ELU_ACC, false)
              + "<br><br>";
        }
        if (params_.lancementCalculs.elsRare) {
          html_ += "<li><b>ELS rare</b><br><br>" + CtuluLibString.LINE_SEP;
          html_ += AlbeRes.buildTabRecapResultats(params_, results_, accostage_, defense_,
              AlbeLib.ELS_RARE, false)
              + "<br><br>";
        }
        if (params_.lancementCalculs.elsFreq) {
          html_ += "<li><b>ELS fr&eacute;quent</b><br><br>" + CtuluLibString.LINE_SEP;
          html_ += AlbeRes.buildTabRecapResultats(params_, results_, accostage_, defense_,
              AlbeLib.ELS_FREQ, false)
              + "<br><br>";
        }
        html_ += "</ul>" + CtuluLibString.LINE_SEP;
      }
    }
  }

  /**
   * 
   */
  protected static class REDetailles extends WHtmlContent {
    public REDetailles(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings) {
      super(_parent, _project, _settings);
    }

    protected void buildContent() {}
  }

  /**
   * 
   */
  protected static class REDetaillesCombinaison extends WHtmlContent {
    int typeCombi_;

    int nombreCombi_;

    int numCombi_;

    boolean cor_;

    SResultatsCombinaison resCombi_;

    int nbPtsCourbe_;

    boolean identification_, resume_, tableau_, graphique_;

    public REDetaillesCombinaison(final JComponent _parent, final FudaaProjet _project,
        final AlbeFilleNoteDeCalculs.Settings _settings, int _typeCombi, final int _numCombi) {
      super(_parent, _project, _settings);
      typeCombi_ = _typeCombi;
      numCombi_ = _numCombi;
      nombreCombi_ = getNbCombi();
      cor_ = getCorrosion();
      resCombi_ = AlbeRes.getResCombi(results_, _typeCombi, numCombi_);
      nbPtsCourbe_ = getNbPoint();
    }

    protected void buildContent() {
      if (typeCombi_ == AlbeLib.COMBINAISON_MANUELLE && !resCombi_.convergence) {
        html_ += "Les calculs n'ont pas converg&eacute;. Il n'y a pas de r&eacute;sultats pour la combinaison manuelle.<br><br>";
      } else {
        html_ += "<ul type = disc>" + CtuluLibString.LINE_SEP;

        // Partie 'Identification de la combinaison �tudi�e'
        if (((AlbeFilleNoteDeCalculs) parent_).isChoosen("RESULTATS_DETAILLES_COMBIETUDIEE"))
          buildContentIdentificationCombinaison();

        // Partie 'R�sum� des r�sultats'
        if (((AlbeFilleNoteDeCalculs) parent_).isChoosen("RESULTATS_DETAILLES_RESUME"))
          buildContentResumeResultats();

        // Partie 'tableau d�taill�'
        if (((AlbeFilleNoteDeCalculs) parent_).isChoosen("RESULTATS_DETAILLES_TABLEAU"))
          buildContentTableauDetaille();

        // Partie 'Graphiques'
        if (((AlbeFilleNoteDeCalculs) parent_).isChoosen("RESULTATS_DETAILLES_GRAPHIQUES"))
          buildContentGraphique();

        html_ += "</ul> " + CtuluLibString.LINE_SEP;
      }
    }

    /**
     * retourne le nombre de combinaisons possibles � partir du type.
     */
    protected int getNbCombi() {
      int nb = 1;
      if (typeCombi_ == AlbeLib.ELU_FONDA)
        nb = (accostage_ ? (defense_ ? 16 : 8) : 4);
      else if (typeCombi_ == AlbeLib.ELU_ACC || typeCombi_ == AlbeLib.ELS_RARE
          || typeCombi_ == AlbeLib.ELS_FREQ)
        nb = (accostage_ ? (defense_ ? 8 : 4) : 2);
      return nb;
    }

    /**
     * retourne la cote d'accostage.
     */
    protected double getCoteAccostage() {
      double cote = params_.action.coteAccostageHaut;

      if (numCombi_ <= nombreCombi_ / 2)
        return params_.action.coteAccostageBas;
      return cote;
    }

    /**
     * retourne un bool�en pr�cisant s'il y a ou non corrosion.
     */
    protected boolean getCorrosion() {

      // Si numCombi_ est pair, tube non corrod�
      if (numCombi_ % 2 == 0)
        return false;

      return true;
    }

    /**
     * retourne la ligne � afficher concernant l'action d'amarrage ou
     * d'accostage.
     */
    protected String getLigneAction() {
      String s = "";

      switch (typeCombi_) {
      case AlbeLib.ELU_FONDA: {
        s = (accostage_ ? ("<li>Valeur de l'action d'accostage : "
            + params_.action.energieAccostageCalcul + " kJ")
            : ("<li>Valeur de l'action d'amarrage : " + params_.action.effortAmarrageCalcul + " kN"));
        break;
      }
      case AlbeLib.ELU_ACC: {
        s = (accostage_ ? ("<li>Valeur de l'action d'accostage : "
            + params_.action.energieAccostageAccidentelle + " kJ")
            : ("<li>Valeur de l'action d'amarrage : " + params_.action.effortAmarrageAccidentel + " kN"));
        break;
      }
      case AlbeLib.ELS_RARE: {
        s = (accostage_ ? ("<li>Valeur de l'action d'accostage : "
            + params_.action.energieAccostageCaracteristique + " kJ")
            : ("<li>Valeur de l'action d'amarrage : "
                + params_.action.effortAmarrageCaracteristique + " kN"));
        break;
      }
      case AlbeLib.ELS_FREQ: {
        s = (accostage_ ? ("<li>Valeur de l'action d'accostage : "
            + params_.action.energieAccostageFrequente + " kJ")
            : ("<li>Valeur de l'action d'amarrage : " + params_.action.effortAmarrageFrequent + " kN"));
        break;
      }
      }
      return s;
    }

    /**
     * construit la partie 'Identification de la combinaison �tudi�e'.
     */
    protected void buildContentIdentificationCombinaison() {
      html_ += "<li><b>Identification de la combinaison &eacute;tudi&eacute;e</b><br><br>";
      // Si combinaison manuelle
      if (typeCombi_ == AlbeLib.COMBINAISON_MANUELLE) {
        html_ += "<ul type = square>" + CtuluLibString.LINE_SEP;
        html_ += "<li>Cote du fond utilis&eacute;e : " + params_.geo.coteDragageBassin + " m";
        html_ += "<li>" + (accostage_ ? "Cote d'accostage : " : "Cote d'amarrage : ")
            + params_.lancementCalculs.cote + " m";
        html_ += "<li>Corrosion du tube : " + (params_.lancementCalculs.corrosion ? "oui" : "non");
        if (accostage_)
          html_ += "<li>Valeur de l'action d'accostage : "
              + params_.lancementCalculs.energieAccostage + " kJ";
        else
          html_ += "<li>Valeur de l'action d'amarrage : " + params_.lancementCalculs.effortAmarrage
              + " kN";
        html_ += "</ul><br><br>" + CtuluLibString.LINE_SEP;
      }

      // Si combinaison automatique
      else {
        html_ += "<ul type = square>" + CtuluLibString.LINE_SEP;
        html_ += "<li>Cote du fond utilis&eacute;e : "
            + (typeCombi_ == AlbeLib.ELU_FONDA ? params_.geo.coteCalcul
                : params_.geo.coteDragageBassin) + " m";
        html_ += "<li>"
            + (accostage_ ? "Cote d'accostage : " + getCoteAccostage() : "Cote d'amarrage : "
                + params_.action.coteAmarrage) + " m";
        html_ += "<li>Corrosion du tube : " + (getCorrosion() ? "oui" : "non");
        html_ += getLigneAction();
        html_ += "<li>Coefficient partiel sur la r&eacute;sistance de l'acier : "
            + AlbeRes.getCoefAcier(params_, typeCombi_);
        html_ += "<li>Coefficient partiel sur la courbe de r&eacute;action du sol : "
            + AlbeRes.getCoefSol(params_, typeCombi_, numCombi_, accostage_, defense_);
        if (defense_)
          html_ += "<li>Coefficient partiel sur la courbe de la d&eacute;fense : "
              + AlbeRes.getCoefDefense(params_, typeCombi_, numCombi_);
        html_ += "</ul><br><br>" + CtuluLibString.LINE_SEP;
      }
    }

    /**
     * construit la partie 'R�sum� des r�sultats'.
     */
    protected void buildContentResumeResultats() {
      Object[] etats;
      Object[] criteres;
      Object[] valeurs;
      Object[] dim;
      String[] dep = AlbeRes.getDeplacements(params_, resCombi_, typeCombi_);
      String[] mob = AlbeRes.getMobilisation(params_, resCombi_, typeCombi_, numCombi_);
      String[] pieu = AlbeRes.getResistancePieu(params_, resCombi_, typeCombi_, cor_);

      if (accostage_) {
        String[] re = AlbeRes.getReactions(params_, resCombi_, typeCombi_);
        if (defense_) {
          String[] def = AlbeRes.getDeflexions(params_, resCombi_, typeCombi_);
          etats = new Object[] { "Mobilisation de la but&eacute;e du sol",
              "R&eacute;sistance en flexion du pieu", "D&eacute;flexion de la d&eacute;fense (%)",
              "D&eacute;placement en t&ecirc;te (m)", "R&eacute;action au choc (kN)" };
          criteres = new Object[] { mob[1], pieu[1], def[1], dep[1], re[1] };
          valeurs = new Object[] { mob[0], pieu[0], def[0], dep[0], re[0] };
          dim = new Object[] { mob[2], pieu[2], def[2], dep[2], re[2] };
        } else {
          etats = new Object[] { "Mobilisation de la but&eacute;e du sol",
              "R&eacute;sistance en flexion du pieu", "D&eacute;placement en t&ecirc;te (m)", "R&eacute;action au choc (kN)" };
          criteres = new Object[] { mob[1], pieu[1], dep[1], re[1] };
          valeurs = new Object[] { mob[0], pieu[0], dep[0], re[0] };
          dim = new Object[] { mob[2], pieu[2], dep[2], re[2] };
        }
      } else {
        etats = new Object[] { "Mobilisation de la but&eacute;e du sol", "R&eacute;sistance en flexion du pieu",
            "D�placement en t�te (m)" };
        criteres = new Object[] { mob[1], pieu[1], dep[1] };
        valeurs = new Object[] { mob[0], pieu[0], dep[0] };
        dim = new Object[] { mob[2], pieu[2], dep[2] };
      }
      html_ += "<li><b>R&eacute;sum&eacute; des r&eacute;sultats</b><br>";
      addTable(new Object[] { "Etat limite", "Crit&egrave;re ou coefficient<br>de mod&egrave;le",
          "Valeur calcul&eacute;e", "Valeur du facteur<br>de dimensionnement" }, new Object[][] { etats,
          criteres, valeurs, dim });
      if (typeCombi_ != AlbeLib.ELS_FREQ) {
        double[] flexions = AlbeRes.getFlexionsPieu(params_, resCombi_, typeCombi_, cor_);
        // On recherche le minimum des flexions
        double min = flexions[0];
        int num = 0;
        for (int i = 1; i < flexions.length; i++) {
          if (flexions[i] < min) {
            min = flexions[i];
            num = i;
          }
        }
        /** Calcul de la contrainte max de l'acier dans le tron�on num. */
        double valCar;
        if (params_.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE))
          valCar = params_.pieu.tronconsTubulaire[num].valeurCaracteristiqueLimiteElastiqueAcier;
        else
          valCar = params_.pieu.tronconsCaisson[num].valeurCaracteristiqueLimiteElastiqueAcier;
        double contrainte = AlbeLib.arronditDouble(valCar
            / (AlbeRes.getCoefAcierDouble(params_, typeCombi_)
                * AlbeRes.getCoefPieu(params_, typeCombi_) * flexions[num]));
        html_ += "Tron&ccedil;on ayant le plus faible facteur de dimensionnement : tron&ccedil;on " + (num + 1);
        html_ += "<br>Contrainte maximale de l'acier dans ce tron&ccedil;on : " + contrainte
            + " MPa<br><br><br>";
      }
    }

    /**
     * construit la partie 'Tableau d�taill�'.
     */
    protected void buildContentTableauDetaille() {
      html_ += "<li><b>Tableau d&eacute;taill&eacute;</b><br>";
      int tailleTab = 0;
      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          if (!(resCombi_.resistanceTroncons[i].resTroncon[j].fleche == 0 && resCombi_.resistanceTroncons[i].resTroncon[j].moment == 0))
            tailleTab++;
        }
      }

      // Calcul de la flexion du premier tron�on
      /*
       * double flexionPremTroncon = 0; if
       * (params_.pieu.typePieu.equals(AlbeLib.IDL_TYPE_PIEU_TUBULAIRE))
       * flexionPremTroncon = AlbeRes.calculFacteurDimTronconTub(params_,
       * resCombi_, typeCombi_, cor_, 0); else flexionPremTroncon =
       * AlbeRes.calculFacteurDimTronconCais(params_, resCombi_, typeCombi_,
       * cor_, 0); if (flexionPremTroncon > AlbeRes.limiteFlexion)
       * tronconNonSollicite = true; else tronconNonSollicite = false; if
       * (flexionPremTroncon > AlbeRes.limiteFlexion) tailleTab -= 1;
       */

      Object[] cote = new Object[tailleTab];
      Object[] pression = new Object[tailleTab];
      Object[] pressionMax = new Object[tailleTab];
      Object[] fleche = new Object[tailleTab];
      Object[] moment = new Object[tailleTab];
      Object[] effortTranch = new Object[tailleTab];
      Object[] contrainte = new Object[tailleTab];

      double[][] contraintes = AlbeRes.calculContraintes(params_, resCombi_, typeCombi_, cor_);

      int ind = 0;
      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          if (!(resCombi_.resistanceTroncons[i].resTroncon[j].fleche == 0 && resCombi_.resistanceTroncons[i].resTroncon[j].moment == 0)) {
            cote[ind] = "" + resCombi_.resistanceTroncons[i].resTroncon[j].cote;
            pression[ind] = ""
                + AlbeLib.arronditDouble(resCombi_.resistanceTroncons[i].resTroncon[j].pression
                    * 10 / AlbeRes.getLargeurPieu(params_));
            // calcul des pressions lim
            if (resCombi_.resistanceTroncons[i].resTroncon[j].numCouche == 0)
              pressionMax[ind] = "X";
            else
              pressionMax[ind] = ""
                  + AlbeLib.arronditDouble(AlbeRes.getPressionPalier(params_, typeCombi_,
                      numCombi_, resCombi_.resistanceTroncons[i].resTroncon[j].numCouche));
            fleche[ind] = ""
                + AlbeLib.arronditDouble(resCombi_.resistanceTroncons[i].resTroncon[j].fleche);
            moment[ind] = ""
                + AlbeLib.arronditDouble(resCombi_.resistanceTroncons[i].resTroncon[j].moment * 10);
            effortTranch[ind] = ""
                + AlbeLib
                    .arronditDouble(resCombi_.resistanceTroncons[i].resTroncon[j].effortTranchant * 10);
            contrainte[ind++] = "" + AlbeLib.arronditDouble(contraintes[i][j]);
          }
        }
      }
      addTable(new Object[] { "Cote<br>(m)", "Pression<br>(kPa)", "Pression<br>limite (kPa)",
          "Fl&egrave;che<br>(cm)", "Moment<br>(kN.m)", "Effort tranchant<br>(kN)",
          "Contrainte dans<br>l'acier (MPa)" }, new Object[][] { cote, pression, pressionMax,
          fleche, moment, effortTranch, contrainte });
      html_ += "<br>";
    }

    /**
     * construit la partie 'Graphiques'.
     */
    protected void buildContentGraphique() {
      html_ += "<li><b>Graphiques</b><br>";
      if (((AlbeFilleNoteDeCalculs) parent_).isChoosen("RESULTATS_DETAILLES_GRAPHIQUES_DEFORMEE")) {
        html_ += "<br>D&eacute;form&eacute;e de l'ouvrage : <br><br>";
        addImgFromScript(getScriptDeformee(), "courbe_deformee" + typeCombi_ + "_" + numCombi_
            + ".gif", "Courbe de la d&eacute;form&eacute;e de l'ouvrage");
        html_ += "<br>";
      }
      if (((AlbeFilleNoteDeCalculs) parent_).isChoosen("RESULTATS_DETAILLES_GRAPHIQUES_MOMENTS")) {
        html_ += "<br>Moments : <br><br>";
        addImgFromScript(getScriptMoments(), "courbe_moments" + typeCombi_ + "_" + numCombi_
            + ".gif", "Courbe des moments");
        html_ += "<br>";
      }
      if (((AlbeFilleNoteDeCalculs) parent_).isChoosen("RESULTATS_DETAILLES_GRAPHIQUES_EFFORTS")) {
        html_ += "<br>Efforts tranchants : <br><br>";
        addImgFromScript(getScriptEfforts(), "courbe_efforts_tranchants" + typeCombi_ + "_" + numCombi_
            + ".gif",
            "Courbe des efforts tranchants");
        html_ += "<br>";
      }
      if (((AlbeFilleNoteDeCalculs) parent_).isChoosen("RESULTATS_DETAILLES_GRAPHIQUES_BUTEE")) {
        html_ += "<br>Mobilisation de la but&eacute;e : <br><br>";
        html_ += "<font color = \"FF00FF\">_____  </font>Pression calcul&eacute;e<br>";
        html_ += "<font color = \"0000FF\">_____  </font>Pression limite<br><br>";
        addImgFromScript(getScriptPressions(), "courbe_pressions" + typeCombi_ + "_" + numCombi_
            + ".gif", "Courbe des pressions");
        html_ += "<br>";
      }
      if (((AlbeFilleNoteDeCalculs) parent_).isChoosen("RESULTATS_DETAILLES_GRAPHIQUES_DEFENSE")) {
        html_ += "<br>Mobilisation de la d&eacute;fense : <br><br>";
        addImgFromScript(getScriptDefense(), "courbe_defense" + typeCombi_ + "_" + numCombi_
            + ".gif",
            "Courbe de d�flexion-r�action de la d�fense");
        html_ += "<br>";
      }
    }

    /**
     * 
     */
    protected String getScript(String _title, String _titleAxeX, String _uniteAxeX,
        String _couleur, double _minx, double _maxx, double _pasx, double[] _abs) {
      final StringBuffer s = new StringBuffer(4096);

      double coordMin = resCombi_.resistanceTroncons[params_.pieu.nombreTroncons - 1].resTroncon[resCombi_.resistanceTroncons[params_.pieu.nombreTroncons - 1].nombreLignesRes - 1].cote;
      double coordMax = resCombi_.resistanceTroncons[0].resTroncon[0].cote;
      double pasY = AlbeLib.getPas(coordMax, coordMin);
      double minY = AlbeLib.getMin(pasY, coordMin);
      double maxY = AlbeLib.getMax(pasY, coordMax);

      s.append("graphe \n{\n");
      s.append("  titre \"" + _title + "\"\n");
      s.append("  legende oui\n");
      s.append("  animation non\n");
      s.append("  marges\n{\n");
      s.append("    gauche 80\n");
      s.append("    droite 120\n");
      s.append("    haut 50\n");
      s.append("    bas 30\n");
      s.append("  }\n");
      s.append("  \n");
      s.append("  axe\n{\n");
      s.append("    titre \"" + _titleAxeX + "\"\n");
      s.append("    unite \"" + _uniteAxeX + "\"\n");
      s.append("    orientation horizontal\n");
      s.append("    graduations oui\n");
      s.append("    minimum " + _minx + "\n");
      s.append("    maximum " + _maxx + "\n");
      s.append("    pas " + _pasx + "\n");
      s.append("  }\n");
      s.append("  axe\n{\n");
      s.append("    titre \"cote\"\n");
      s.append("    unite \"m\"\n");
      s.append("    orientation vertical\n");
      s.append("    graduations oui\n");
      s.append("    minimum " + minY + "\n");
      s.append("    maximum " + maxY + "\n");
      s.append("    pas " + pasY + "\n");
      s.append("  }\n");
      s.append("  courbe\n{\n");
      s.append("    aspect\n{\n");
      s.append("    contour.couleur " + _couleur + "\n");
      s.append("    }\n");
      s.append("    valeurs\n{\n");
      int n = 0;
      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          if (!(resCombi_.resistanceTroncons[i].resTroncon[j].fleche == 0 && resCombi_.resistanceTroncons[i].resTroncon[j].moment == 0))
            s.append("      " + _abs[j + n] + " "
                + resCombi_.resistanceTroncons[i].resTroncon[j].cote + "\n");
        }
        n += resCombi_.resistanceTroncons[i].nombreLignesRes;
      }
      s.append("    }\n");
      s.append("  }\n");
      return s.toString();
    }

    protected int getNbPoint() {
      int nbPts = 0;

      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          nbPts++;
        }
      }
      return nbPts;
    }

    protected String getScriptDeformee() {
      double[] fleche = new double[nbPtsCourbe_];
      int nbJ = 0;

      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          fleche[j + nbJ] = resCombi_.resistanceTroncons[i].resTroncon[j].fleche;
        }
        nbJ += resCombi_.resistanceTroncons[i].nombreLignesRes;
      }

      double absMin = CtuluLibArray.getMin(fleche);
      double absMax = CtuluLibArray.getMax(fleche);
      double pasX = AlbeLib.getPas(absMax, absMin);
      double minX = AlbeLib.getMin(pasX, absMin);
      double maxX = AlbeLib.getMax(pasX, absMax);

      return getScript("Courbe de la d�form�e de l'ouvrage", "fl�che", "cm", "FF0000", minX, maxX,
          pasX, fleche);
    }

    protected String getScriptMoments() {

      double[] mmt = new double[nbPtsCourbe_];
      int nbJ = 0;

      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          mmt[j + nbJ] = resCombi_.resistanceTroncons[i].resTroncon[j].moment * 10;
        }
        nbJ += resCombi_.resistanceTroncons[i].nombreLignesRes;
      }

      double absMin = CtuluLibArray.getMin(mmt);
      double absMax = CtuluLibArray.getMax(mmt);
      double pasX = AlbeLib.getPas(absMax, absMin);
      double minX = AlbeLib.getMin(pasX, absMin);
      double maxX = AlbeLib.getMax(pasX, absMax);

      return getScript("Courbe des moments", "moment", "kN.m", "0000FF", minX, maxX, pasX, mmt);
    }

    protected String getScriptEfforts() {

      double[] effort = new double[nbPtsCourbe_];
      int nbJ = 0;

      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          effort[j + nbJ] = resCombi_.resistanceTroncons[i].resTroncon[j].effortTranchant * 10;
        }
        nbJ += resCombi_.resistanceTroncons[i].nombreLignesRes;
      }

      double absMin = CtuluLibArray.getMin(effort);
      double absMax = CtuluLibArray.getMax(effort);
      double pasX = AlbeLib.getPas(absMax, absMin);
      double minX = AlbeLib.getMin(pasX, absMin);
      double maxX = AlbeLib.getMax(pasX, absMax);

      return getScript("Courbe des efforts tranchants", "effort tranchant", "kN", "FF00FF", minX,
          maxX, pasX, effort);
    }

    /**
     * 
     */
    public int[] getIndicePression() {
      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          if (resCombi_.resistanceTroncons[i].resTroncon[j].numCouche == 1)
            return new int[] { i, j };
        }
      }
      return new int[] { 0, 0 };
    }

    /**
     * retourne le nombre maximum de lignes de r�sultats.
     */
    public int getMaxLigneRes() {
      int max = resCombi_.resistanceTroncons[0].nombreLignesRes;
      for (int i = 1; i < params_.pieu.nombreTroncons; i++) {
        if (resCombi_.resistanceTroncons[i].nombreLignesRes > max)
          max = resCombi_.resistanceTroncons[i].nombreLignesRes;
      }
      return max;
    }

    /**
     * calcule les pressions.
     */
    public double[][] getPressions() {

      double[][] res = new double[params_.pieu.nombreTroncons][getMaxLigneRes()];
      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          res[i][j] = resCombi_.resistanceTroncons[i].resTroncon[j].pression * 10
              / AlbeRes.getLargeurPieu(params_);
        }
      }
      return res;
    }

    /**
     * calcule les pressions max.
     */
    public double[][] getPressionsMax() {

      double[][] res = new double[params_.pieu.nombreTroncons][getMaxLigneRes()];
      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          if (resCombi_.resistanceTroncons[i].resTroncon[j].numCouche == 0)
            res[i][j] = -1;
          else {
            res[i][j] = AlbeRes.getPressionPalier(params_, typeCombi_, numCombi_,
                resCombi_.resistanceTroncons[i].resTroncon[j].numCouche);
          }
        }
      }
      return res;
    }

    /**
     * retourne le minimum des pressions.
     */
    public double getMinPression() {

      double min = resCombi_.resistanceTroncons[0].resTroncon[0].pression;

      int[] ind = getIndicePression();

      double[][] pressions = getPressions();
      double[][] pressionsMax = getPressionsMax();

      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          if (pressions[i][j] < min)
            min = pressions[i][j];
        }
      }

      for (int i = ind[0]; i < params_.pieu.nombreTroncons; i++) {
        for (int j = (i == ind[0]) ? ind[1] : 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          if (pressionsMax[i][j] < min)
            min = pressionsMax[i][j];
        }
      }
      return min;
    }

    /**
     * retourne le maximum des pressions.
     */
    public double getMaxPression() {

      int[] ind = getIndicePression();

      double[][] pressions = getPressions();
      double[][] pressionsMax = getPressionsMax();

      double max = pressionsMax[0][0];

      for (int i = ind[0] + 1; i < params_.pieu.nombreTroncons; i++) {
        for (int j = (i == ind[0]) ? ind[1] : 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          if (pressionsMax[i][j] > max)
            max = pressionsMax[i][j];
        }
      }

      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          if (pressions[i][j] > max)
            max = pressions[i][j];
        }
      }
      return max;
    }

    /**
     * construit le graphique pour la courbe des pressions.
     */
    public String getScriptPressions() {
      final StringBuffer s = new StringBuffer(4096);

      int[] ind = getIndicePression();

      double coordMinY = resCombi_.resistanceTroncons[params_.pieu.nombreTroncons - 1].resTroncon[resCombi_.resistanceTroncons[params_.pieu.nombreTroncons - 1].nombreLignesRes - 1].cote;
      double coordMaxY = resCombi_.resistanceTroncons[0].resTroncon[0].cote;
      double pasY = AlbeLib.getPas(coordMaxY, coordMinY);
      double minY = AlbeLib.getMin(pasY, coordMinY);
      double maxY = AlbeLib.getMax(pasY, coordMaxY);

      double coordMaxX = getMaxPression();
      double coordMinX = getMinPression();
      double pasX = AlbeLib.getPas(coordMaxX, coordMinX);
      double minX = AlbeLib.getMin(pasX, coordMinX);
      double maxX = AlbeLib.getMax(pasX, coordMaxX);

      double[][] pressions = getPressions();
      double[][] pressionsMax = getPressionsMax();

      s.append("graphe \n{\n");
      s.append("  titre \"Courbe des pressions du sol\"\n");
      s.append("  legende oui\n");
      s.append("  animation non\n");
      s.append("  marges\n{\n");
      s.append("    gauche 80\n");
      s.append("    droite 120\n");
      s.append("    haut 50\n");
      s.append("    bas 30\n");
      s.append("  }\n");
      s.append("  \n");
      s.append("  axe\n{\n");
      s.append("    titre \"pression\"\n");
      s.append("    unite \"kPa\"\n");
      s.append("    orientation horizontal\n");
      s.append("    graduations oui\n");
      s.append("    minimum " + minX + "\n");
      s.append("    maximum " + maxX + "\n");
      s.append("    pas " + pasX + "\n");
      s.append("  }\n");
      s.append("  axe\n{\n");
      s.append("    titre \"cote\"\n");
      s.append("    unite \"m\"\n");
      s.append("    orientation vertical\n");
      s.append("    graduations oui\n");
      s.append("    minimum " + minY + "\n");
      s.append("    maximum " + maxY + "\n");
      s.append("    pas " + pasY + "\n");
      s.append("  }\n");
      s.append("  courbe\n{\n");
      s.append("    aspect\n{\n");
      s.append("    contour.couleur FF00FF\n");
      s.append("    }\n");
      s.append("    valeurs\n{\n");
      for (int i = 0; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          if (!(resCombi_.resistanceTroncons[i].resTroncon[j].fleche == 0 && resCombi_.resistanceTroncons[i].resTroncon[j].moment == 0))
            s.append("      " + pressions[i][j] + " "
                + resCombi_.resistanceTroncons[i].resTroncon[j].cote + "\n");
        }
      }
      s.append("    }\n");
      s.append("    }\n");
      s.append("  courbe\n{\n");
      s.append("    aspect\n{\n");
      s.append("    contour.couleur 0000FF\n");
      s.append("    }\n");
      s.append("    valeurs\n{\n");
      for (int i = ind[0]; i < params_.pieu.nombreTroncons; i++) {
        for (int j = 0; j < resCombi_.resistanceTroncons[i].nombreLignesRes; j++) {
          if (!(resCombi_.resistanceTroncons[i].resTroncon[j].fleche == 0 && resCombi_.resistanceTroncons[i].resTroncon[j].moment == 0))
            s.append("      " + pressionsMax[i][j] + " "
                + resCombi_.resistanceTroncons[i].resTroncon[j].cote + "\n");
        }
      }
      s.append("    }\n");
      s.append("    }\n");
      s.append("  }\n");
      return s.toString();
    }

    protected String getScriptDefense() {
      final StringBuffer s = new StringBuffer(4096);
      double coefDefense = AlbeRes.getCoefDefense(params_, typeCombi_, numCombi_);
      double pas = AlbeLib.getPas(coefDefense
          * params_.defense.points[params_.defense.nombrePoints - 1].effort, 0);
      s.append("graphe \n{\n");
      s.append("  titre \"D�fenses d'accostage\"\n");
      s.append("  sous-titre \"Courbe d�flexion-r�action\"\n");
      s.append("  legende oui\n");
      s.append("  animation non\n");
      s.append("  marges\n{\n");
      s.append("    gauche 80\n");
      s.append("    droite 120\n");
      s.append("    haut 50\n");
      s.append("    bas 30\n");
      s.append("  }\n");
      s.append("  \n");
      s.append("  axe\n{\n");
      s.append("    titre \"d�formation\"\n");
      s.append("    unite \"%\"\n");
      s.append("    orientation horizontal\n");
      s.append("    graduations oui\n");
      s.append("    minimum 0\n");
      s.append("    maximum 100 \n");
      s.append("    pas 10\n");
      s.append("  }\n");
      s.append("  axe\n{\n");
      s.append("    titre \"effort\"\n");
      s.append("    unite \"kN\"\n");
      s.append("    orientation vertical\n");
      s.append("    graduations oui\n");
      s.append("    minimum 0\n");
      s.append("    maximum " + 10 * pas + "\n");
      s.append("    pas " + pas + "\n");
      s.append("  }\n");
      s.append("  courbe\n{\n");
      s.append("    aspect\n{\n");
      s.append("    contour.couleur 0000FF\n");
      s.append("    }\n");
      s.append("    valeurs\n{\n");
      s.append("      " + 0 + " " + 0 + "\n");
      for (int i = 0; i < params_.defense.nombrePoints; i++) {
        s.append("      " + params_.defense.points[i].deformation + " "
            + AlbeLib.arronditDouble(coefDefense * params_.defense.points[i].effort) + "\n");
      }
      s.append("    }\n");
      s.append("  }\n");
      s.append("  courbe\n{\n");
      s.append("    aspect\n{\n");
      s.append("    contour.couleur 008000\n");
      s.append("    }\n");
      s.append("    valeurs\n{\n");
      s.append("      " + resCombi_.resultatsGeneraux.deflexionDefenses + " " + 0 + "\n");
      s.append("      " + resCombi_.resultatsGeneraux.deflexionDefenses + " " + 10 * pas + "\n");
      s.append("    }\n");
      s.append("  }\n");
      s.append("  }\n");
      return s.toString();
    }

  }

}
