package org.fudaa.fudaa.albe;

import java.applet.Applet;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.IndexColorModel;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gif.AcmeGifEncoder;
import org.fudaa.dodico.corba.albe.SAction;
import org.fudaa.dodico.corba.albe.SCoefficientsPartiels;
import org.fudaa.dodico.corba.albe.SCombinaisonMateriaux;
import org.fudaa.dodico.corba.albe.SCombinaisonModele;
import org.fudaa.dodico.corba.albe.SCoucheCourbe;
import org.fudaa.dodico.corba.albe.SCriteresDimensionnement;
import org.fudaa.dodico.corba.albe.SDefense;
import org.fudaa.dodico.corba.albe.SGeometrique;
import org.fudaa.dodico.corba.albe.SLancementCalculs;
import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.dodico.corba.albe.SPieu;
import org.fudaa.dodico.corba.albe.SPointDefense;
import org.fudaa.dodico.corba.albe.SPointSol;
import org.fudaa.dodico.corba.albe.SSol;
import org.fudaa.dodico.corba.albe.STronconCaisson;
import org.fudaa.dodico.corba.albe.STronconTubulaire;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Lecteur;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuDialogWarning;
import com.memoire.bu.BuIcon;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTextField;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

/**
 * Librairie de m�thodes et constantes utiles pour le d�veloppement d'Albe.
 * 
 * @author Sabrina Delattre
 */
public final class AlbeLib {

  // Constantes de fonctionnement

  /** nombre maximum de couches de sol. */
  public final static int NBMAX_COUCHES = 20;

  /**
   * nombre maximum de points que introduire l'utilisateur pour la courbe
   * manuelle.
   */
  public final static int NBMAX_POINTS_COURBEMANUELLE = 10;

  /** nombre maximum de tron�ons pour un pieu tubulaire. */
  public final static int NBMAX_TRONCONS_PIEU_TUBULAIRE = 100;

  /** nombre maximum de tron�ons pour un pieu tubulaire. */
  public final static int NBMAX_TRONCONS_PIEU_CAISSON = 100;

  /**
   * nombre maximum de points de la courbe d�flexion-r�action (onglet
   * 'D�fense').
   */
  public final static int NBMAX_POINTS_COURBE_DEF_REACTION = 10;

  public final static String FILE_SEPARATOR = System.getProperty("file.separator");

  public final static String USER_HOME = System.getProperty("user.home") + FILE_SEPARATOR;

  public final static int INTERNALFRAME_PREFERRED_WIDTH = 500;

  public final static int INTERNALFRAME_PREFERRED_HEIGHT = 700;

  public final static int INTERNAL_FRAME_PARAMETRES = 1;

  public final static int INTERNAL_FRAME_NOTEDECALCULS = 2;

  public final static int COLUMN_SCROLLPANE_WIDTH = 150;

  public final static int COLUMN_SCROLLPANE_HEIGHT = 120;

  public final static int TABLE_ROWHEIGHT = 20;

  public final static String FILTRE_FICHIER = "albe";

  // Valeur nulle associ�e aux cl�s des hashtables
  public final static Object VAL_NULLE = "null";

  public final static String NOTE_DE_CALCULS_HTML_FILE = "note";

  // Formats de champs num�rique
  public final static String FORMAT001 = "#0.00";

  public final static String FORMAT002 = "#0.0";

  // Tool tip text pour les onglets relatifs aux couches de sol
  public final static String TIP_TEXT_COUCHES_SOL = "Informations concernant la couche de sol n�";

  // String pour les tableaux de l'onglet 'Sol'
  public final static String TABLEAU_COURTE_DUREE = "tableau_courte_duree";

  public final static String TABLEAU_LONGUE_DUREE = "tableau_longue_duree";

  // Types de bordures de panneaux
  public final static int ETCHED_BORDER = 1;

  public final static int BEVEL_BORDER = 2;

  public final static int COMPOUND_BORDER = 3;

  // Valeurs possible pour les donn�es IDL
  public final static String IDL_ACTION_ACCOSTAGE = "C";

  public final static String IDL_ACTION_AMARRAGE = "M";

  public final static String IDL_TYPE_PIEU_TUBULAIRE = "T";

  public final static String IDL_TYPE_PIEU_CAISSON = "C";

  public final static String IDL_DEFENSE_ACCOSTAGE_OUI = "O";

  public final static String IDL_DEFENSE_ACCOSTAGE_NON = "N";

  public final static String IDL_COURBE_MANUELLE = "M";

  public final static String IDL_COURBE_ELASTO_PLASTIQUE_PURE = "E";

  public final static String IDL_COURBE_SETRA = "S";

  public final static String IDL_COURBE_FASCICULE_COURTE_DUREE = "C";

  public final static String IDL_COURBE_FASCICULE_TRES_COURTE_DUREE = "TC";

  public final static String IDL_COURBE_FASCICULE_LONGUE_DUREE = "L";

  // // Modes de lancement des calculs

  public final static int IDL_LANCEMENT_MANUEL = 0;

  public final static int IDL_LANCEMENT_AUTOMATIQUE = 1;

  // Types de combinaisons
  public final static int COMBINAISON_MANUELLE = 0;

  public final static int ELU_FONDA = 1;

  public final static int ELU_ACC = 2;

  public final static int ELS_RARE = 3;

  public final static int ELS_FREQ = 4;

  // Bool�ens permettant de savoir si les v�rifications sur les tableaux de
  // tron�ons et de la courbe manuelle doivent �tre effectu�es (si modification
  // du nombre de lignes du tableau ou effacement des donn�es, on ne fait pas la
  // v�rification -> prend trop de temps)
  public static boolean verificationPieu_;

  public static boolean verificationCourbeManuelle_;

  // M�thodes utiles

  /**
   * �crit un fichier.
   * 
   * @param _f nom du fichier
   * @param _txt texte � �crire dans le fichier
   */
  public static void writeFile(final String _f, final String _txt) {
    // flux peut etre non ferme
    FileWriter fw = null;
    try {
      fw = new FileWriter(_f);
      fw.write(_txt, 0, _txt.length());
      fw.flush();
      fw.close();
    }
    catch (IOException _e1) {} finally {
      FuLib.safeClose(fw);
    }
  }

  /**
   * renvoie une icone repr�sentant le fichier icone sp�cifi�.
   * 
   * @param _i nom du fichier icone a charger (sans l'extension .gif)
   */
  public static BuIcon getIcon(final String _i) {
    return AlbeResource.ALBE.getIcon(_i + ".gif");
  }

  /**
   * renvoie un composant ScrollPane (conteneur permettant le d�filement par
   * ascenseurs) aux dimensions standards pour �tre inscrit dans une des
   * colonnes (gauches ou droites). Ses propri�t�s sont positionn�es � celles
   * par d�faut (voir les constantes COLUMN_SCROLLPANE_*).
   */
  public static BuScrollPane columnScrollPane(final JComponent _c) {
    final BuScrollPane sp = new BuScrollPane(_c);
    sp.setPreferredSize(new Dimension(COLUMN_SCROLLPANE_WIDTH, COLUMN_SCROLLPANE_HEIGHT));
    return sp;
  }

  /**
   * renvoie un objet FudaaFiltreFichier en positionnant ses donn�es "Filtre"
   * par d�faut avec l'extension .albe (constante FILTRE_FICHIER).
   */
  public static FudaaFiltreFichier filtreFichier() {
    return new FudaaFiltreFichier(FILTRE_FICHIER);
  }

  /**
   * renvoie une chaine de caract�re correspondant au chemin syst�me par d�faut
   * pour les boites de dialogues ouvrir et nouveau.
   */
  public static String defaultDirectory() {
    return System.getProperty("user.dir") + File.separator + "exemples" + File.separator + "albe";
  }

  /**
   * renvoie une chaine de caract�res correspondant � la liste des arguments de
   * la commande pass�e en param�tres mais sans le mot cl� de l'action de cette
   * commande, ne renvoie null si la commande ne contenait aucun argument.
   */
  public static String actionArgumentStringOnly(final String _a) {
    final int i = _a.indexOf('(');
    String arg = null;
    if (i >= 0) {
      arg = _a.substring(i + 1, _a.length() - 1);
    }
    return arg;
  }

  /**
   * renvoie une chaine de caract�re correspondant au mot cl� de l'action pass�e
   * en param�tres sans sa liste �ventuelle de param�tres, si aucun param�tres
   * n'�tant pr�sent dans la commande initiale, renvoie le chaine tel quelle.
   */
  public static String actionStringWithoutArgument(final String _a) {
    final int i = _a.indexOf('(');
    return (i >= 0) ? _a.substring(0, i) : _a;
  }

  /**
   * remplace toutes les occurences de<code>_pattern</code> dans
   * <code>_source</code> par <code>_to</code>. retourne
   * <code>_source</code>, si aucune occurence n'a �t� trouv�e.
   */
  public static String replaceKeyWordBy(final String _pattern, final String _to,
      final String _source) {
    int i1;
    String target;
    String avant;
    String apres;
    target = _source;
    do {
      i1 = target.indexOf("<" + _pattern.toUpperCase() + ">");
      if (i1 >= 0) {
        avant = _source.substring(0, i1);
        apres = _source.substring(i1 + 2 + _pattern.length());
        target = avant + ((_to != null) ? _to : "") + apres;
      }
    } while (i1 >= 0);
    return target;
  }

  /**
   * retourne le maximum d'un tableau d'entiers.
   */
  public static int max(int[] _d) {
    int max = _d[0];
    for (int i = 1; i < _d.length; i++) {
      if (_d[i] > max)
        max = _d[i];
    }
    return max;
  }

  /**
   * renvoie un composant LabelMultiline avec ses propri�t�s positionn�es �
   * celles par d�faut (notamment sa taille en largeur : voir
   * INTERNALFRAME_PREFERRED_WIDTH).
   */
  public static BuLabelMultiLine labelMultiLine(final String _txt) {
    final BuLabelMultiLine lb = new BuLabelMultiLine(_txt);
    lb.setWrapMode(BuLabelMultiLine.WORD);
    lb.setPreferredSize(lb.computeHeight(INTERNALFRAME_PREFERRED_WIDTH));
    return lb;
  }

  /**
   * renvoie un composant champs de saisie permettant de saisir des chiffres de
   * type double.
   * 
   * @param _format mod�le de format d'affichage du nombre double
   */
  public static BuTextField doubleField(final String _format) {
    final BuTextField tf = BuTextField.createDoubleField();
    tf.setDisplayFormat(new DecimalFormat(_format));
    tf.setRequestFocusEnabled(true);
    return tf;
  }

  /**
   * retourne vrai si la zone de texte plac�e en param�tre n'est pas remplie par
   * l'utilisateur ou faux sinon.
   */
  public static boolean emptyField(BuTextField _text) {

    return CtuluLibString.isEmpty(_text.getText());
  }

  /**
   * renvoie null si le contenu du champ de texte n'est pas un nombre.
   */
  public static Double textFieldConversionDouble(BuTextField _tf) {

    Double v = null;
    try {
      v = (Double) _tf.getValue();
    }
    catch (NumberFormatException _nfe) {}
    return v;
  }

  /**
   * renvoie un double (nombre) correspondant � la valeur d'un champ de texte.
   */
  public static double textFieldDoubleValue(final BuTextField _tf) {

    final Double res = ((Double) _tf.getValue());

    if (res == null) {
      return VALEUR_NULLE.value;
    }

    return res.doubleValue();

  }

  /**
   * idem que textFieldDoubleValue mais retourne null si le champ est vide ou ne contient pas de double.
   */
  public static double textFieldDoubleValue2(final BuTextField _tf) {
    final double d = ((Double) _tf.getValue()).doubleValue();
    return d;
  }

  /**
   * modifie certains param�tres par d�faut d'une JTable.
   * 
   * @param _table
   */
  public static void changeParametresTable(JTable _table) {
    /** Hauteur des lignes */
    _table.setRowHeight(TABLE_ROWHEIGHT);

    /** S�lection par cellule et non pas par ligne */
    _table.setCellSelectionEnabled(true);

    /** Alignement du texte au centre des cellules */
    DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
    renderer.setHorizontalAlignment(SwingConstants.CENTER);
    int nbColonnes = _table.getColumnCount();
    for (int i = 0; i < nbColonnes; i++) {
      _table.getColumnModel().getColumn(i).setCellRenderer(renderer);
    }
  }

  /**
   * initialise les cellules de la premi�re colonne du tableau avec le num�ro
   * des lignes.
   */

  public static void initialiseColonne1JTable(JTable _t) {

    for (int i = 0; i < _t.getRowCount(); i++) {
      _t.setValueAt(Integer.toString(i + 1), i, 0);
    }
  }

  /**
   * initialise le tableau avec des valeurs vides partout sauf dans la premi�re
   * colonne.
   */
  public static void initialiseTableVide(JTable _t) {

    Object o = "";

    for (int i = 0; i < _t.getRowCount(); i++) {
      for (int j = 1; j < _t.getColumnCount(); j++) {
        _t.setValueAt(o, i, j);
      }
    }
  }

  /**
   * ajoute ou supprime une ou plusieurs lignes d'un tableau.
   * 
   * @param _t : le tableau
   * @param _nbRowf : le nombre de lignes finales que le tableau doit comporter
   */

  public static void majNbLignes(JTable _t, int _nbRowf) {

    // R�cup�ration du nombre de lignes initial du tableau
    int nbRowi = _t.getRowCount();
    // R�cup�ration du nombre de colonnes
    int nbColumn = _t.getColumnCount();

    Object o = "";

    DefaultTableModel model = (DefaultTableModel) _t.getModel();
    Object[] ob = new Object[3];

    if (nbRowi < _nbRowf) { // Ajout de lignes
      for (int i = 0; i < _nbRowf - nbRowi; i++) {
        model.addRow(ob);
      }
      _t.setModel(model);

      /**
       * On initialise chaque cellule que l'on a ajout�e � vide (sauf celles de
       * la premi�re colonne)
       */
      for (int i = nbRowi; i < _nbRowf; i++) {
        for (int j = 0; j < nbColumn; j++) {
          _t.setValueAt(o, i, j);
        }
      }
    }

    else if (nbRowi > _nbRowf) // Suppression de lignes
      for (int i = nbRowi - 1; i >= _nbRowf; i--) {
        model.removeRow(i);
      }
  }

  /**
   * retourne vrai si toutes les cellules de la JTable plac�e en param�tre sont
   * remplies.
   */
  public static boolean fullTable(JTable _t, DefaultTableModel _tableModel) {

    Double d;
    for (int i = 0; i < _t.getRowCount(); i++) {
      for (int j = 1; j < _t.getColumnCount(); j++) {

        try {
          d = (Double) _tableModel.getValueAt(i, j);
        }

        catch (final ClassCastException c) {
          d = null;
        }
        if (d == null)
          return false;
      }
    }
    return true;
  }

  /**
   * retourne vrai si toutes les cellules EDITABLES de la JTable plac�e en
   * param�tre sont remplies.
   */

  public static boolean fullTableCellsEditables(JTable _t, DefaultTableModel _tableModel) {

    Double d;
    for (int i = 0; i < _t.getRowCount(); i++) {
      for (int j = 1; j < _t.getColumnCount(); j++) {
        if (_tableModel.isCellEditable(i, j)) {
          try {
            d = (Double) _tableModel.getValueAt(i, j);
          }

          catch (final ClassCastException c) {
            d = null;
          }
          if (d == null)
            return false;
        }
      }
    }
    return true;
  }

  /**
   * retourne vrai si toutes les cellules de la JTable sont vides (ne prend pas
   * en compte la premi�re colonne) ou faux si au moins une cellule est remplie.
   */
  public static boolean emptyTable(JTable _t, DefaultTableModel _tableModel) {

    Double d;
    for (int i = 0; i < _t.getRowCount(); i++) {
      for (int j = 1; j < _t.getColumnCount(); j++) {

        try {
          d = (Double) _tableModel.getValueAt(i, j);
        }

        catch (final ClassCastException c) {
          d = null;
        }
        if (d != null)
          return false;
      }
    }
    return true;
  }

  /**
   * retourne vrai si toute les cellules de la colonne _i contiennent des
   * valeurs positives ou nulles.
   */

  public static boolean valeursColonnePositivesOuNulles(JTable _table, int _num) {
    for (int i = 0; i < _table.getRowCount(); i++) {
      try {
        if (((Double) _table.getValueAt(i, _num)).doubleValue() < 0)
          return false;
      }
      catch (final NullPointerException _e1) {}
    }
    return true;
  }

  /**
   * retourne vrai si toute les cellules de la colonne _i contiennent des
   * valeurs strictement positives.
   */
  public static boolean valeursColonneStrictPositives(JTable _table, int _num) {
    for (int i = 0; i < _table.getRowCount(); i++) {
      try {
        if (((Double) _table.getValueAt(i, _num)).doubleValue() <= 0)
          return false;
      }
      catch (final NullPointerException _e1) {}
    }
    return true;
  }

  /**
   * retourne vrai si toute les cellules du tableau contiennent des valeurs
   * positives ou nulles.
   */

  public static boolean valeursPositivesOuNulles(JTable _table) {
    for (int i = 0; i < _table.getRowCount(); i++) {
      for (int j = 1; j < _table.getColumnCount(); j++) {
        try {
          if (((Double) _table.getValueAt(i, j)).doubleValue() < 0)
            return false;
        }
        catch (final NullPointerException _e1) {}
      }
    }
    return true;
  }

  /**
   * retourne vrai si toute les cellules du tableau contiennent des valeurs
   * strictement positives.
   */

  public static boolean valeursStrictPositives(JTable _table) {
    for (int i = 0; i < _table.getRowCount(); i++) {
      for (int j = 1; j < _table.getColumnCount(); j++) {
        try {
          if (((Double) _table.getValueAt(i, j)).doubleValue() <= 0)
            return false;
        }
        catch (final NullPointerException _e1) {}
      }
    }
    return true;
  }

  /**
   * remplit une JTable avec les donn�es (ne prend pas en compte la premi�re
   * colonne).
   */

  public static void setValuesInTable(JTable _t, Object[][] _donnees) {

    int nbColumn = _t.getColumnCount();
    for (int i = 0; i < _t.getRowCount(); i++) {
      for (int j = 1; j < nbColumn; j++) {
        _t.setValueAt(_donnees[i][j - 1], i, j);
      }
    }
  }

  /**
   * modifie un tableau � partir du nombre de lignes qu'il doit d�sormais
   * comporter + initialisation de la premi�re colonne avec le num�ro des
   * lignes. Les dimensions du scrollPane sont �galement ajust�es.
   * 
   * @param _t le tableau
   * @param _sp le scrollpane
   * @param _nbRow nombre de lignes que contient le tableau
   */

  public static void majTableSol(JTable _t, JScrollPane _sp, int _nbRow) {

    _sp.setPreferredSize(new Dimension(400, 23 + 20 * _nbRow));
    AlbeLib.majNbLignes(_t, _nbRow);
    AlbeLib.initialiseColonne1JTable(_t);
  }

  /**
   * rend actifs les 'index + 1' premiers onglets d'un tabbedPane et inactifs
   * les autres.
   * 
   * @param _tp
   * @param index
   */

  public static void setTabbedPane(BuTabbedPane _tp, int _index) {

    for (int i = _index + 1; i < _tp.getTabCount(); i++) {
      _tp.setEnabledAt(i, false);
    }

    for (int i = 0; i < _index + 1; i++) {
      _tp.setEnabledAt(i, true);
    }
  }

  /**
   * renvoie un composant Button (bouton de commande) avec une ic�ne situ�e dans
   * le dossier bu.
   * 
   * @param _label titre du bouton
   * @param _icone nom de l'icone sans l'extension
   */

  public static BuButton button(final String _label, final String _icone) {

    final BuButton bt = new BuButton(BuResource.BU.getIcon(_icone), BuResource.BU.getString(_label));
    return bt;

  }

  /**
   * renvoie un composant Button (bouton de commande) avec son ic�ne.
   * 
   * @param _label titre du bouton
   * @param _icone identifiant du bouton � utiliser pour retrouver son ic�ne
   */
  public static BuButton buttonId(final String _label, final String _icone) {
    final BuButton bt = new BuButton(BuResource.BU.loadToolCommandIcon(_icone), BuResource.BU
        .getString(_label));
    return bt;
  }

  /**
   * renvoie un bouton 'aide' avec une icone et un tool tip text 'Afficher
   * l'aide'.
   */

  public static BuButton aideButton() {
    BuButton aideButton = new BuButton(BuResource.BU.loadCommandIcon("AIDE"));
    aideButton.setToolTipText("Afficher l'aide");
    return aideButton;
  }

  /**
   * affiche une boite de dialogue erreur.
   * 
   * @param _msg message � afficher
   */
  public static void dialogError(final BuCommonInterface _a, final String _msg) {
    BuDialogError de = new BuDialogError(_a, AlbeImplementation.informationsSoftware(), _msg);
    de.activate();
  }

  /**
   * affiche une boite de dialogue avertissement.
   * 
   * @param _msg message � afficher
   */
  public static void dialogWarning(final BuCommonInterface _a, final String _msg) {
    BuDialogWarning dw = new BuDialogWarning(_a, AlbeImplementation.informationsSoftware(), _msg);
    dw.activate();
  }

  /**
   * affiche une boite de dialogue de message � l'�cran avec le texte _t et
   * renvoie le code du bouton cliqu� par l'utilisateur (entier).
   */
  public static int dialogMessage(final BuCommonInterface _a, final String _t) {
    final BuDialogMessage dm = new BuDialogMessage(_a, AlbeImplementation.informationsSoftware(),
        _t);
    dm.setSize(500, 300);
    final int i = dm.activate();
    return i;
  }

  /**
   * affiche une boite de dialogue de confirmation � l'�cran avec le texte _t et
   * renvoie le code du bouton cliqu� par l'utilisateur (entier).
   */
  public static int dialogConfirmation(final BuCommonInterface _a, final String _title,
      final String _t) {
    final BuDialogConfirmation dc = new BuDialogConfirmation(_a, AlbeImplementation
        .informationsSoftware(), _t);
    if (_title != null)
      dc.setTitle(_title);
    dc.setSize(500, 300);
    final int i = dc.activate();
    return i;
  }

  /**
   * affiche une boite de dialogue de confirmation avec les boutons 'Oui', 'Non'
   * et 'Annuler' avec un titre et le texte _t et renvoie le code du bouton
   * cliqu� par l'utilisateur (entier).
   */
  public static int dialogAdvancedConfirmation(final BuCommonInterface _a, final String _title,
      final String _t) {
    final AlbeDialogConfirmation dm = new AlbeDialogConfirmation(_a, AlbeImplementation
        .informationsSoftware(), _title, _t);
    dm.setSize(500, 300);
    final int i = dm.activate();
    return i;
  }

  /**
   * boite de dialogue pour enregistrement de fichier.
   */
  public static String getFileChoosenByUser(final Component _parent, final String _title,
      final String _approvebutton, final String _currentpath) {
    final JFileChooser chooser = new JFileChooser();
    if (_approvebutton != null) {
      chooser.setApproveButtonText(_approvebutton);
    }
    if (_title != null) {
      chooser.setDialogTitle(_title);
    }
    if (_currentpath != null) {
      chooser.setCurrentDirectory(new File(_currentpath));
    }
    if (_currentpath != null) {
      chooser.setSelectedFile(new File(_currentpath));
    }
    final int returnVal = chooser.showOpenDialog(_parent);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      return chooser.getSelectedFile().getPath();
    }
    return null;
  }

  /**
   * renvoie un panneau avec une bordure sans titre.
   * 
   * @param _type type de la bordure
   */
  public static BuPanel borderPanel(final int _type) {
    final BuPanel p = new BuPanel();
    Border b = null;
    if (_type == ETCHED_BORDER)
      b = BorderFactory.createEtchedBorder();
    else if (_type == BEVEL_BORDER)
      b = BorderFactory.createBevelBorder(BevelBorder.RAISED);
    else if (_type == COMPOUND_BORDER)
      b = BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory
          .createLoweredBevelBorder());
    else
      FuLog.error("type de bordure inconnu");
    p.setBorder(b);
    return p;
  }

  /**
   * renvoie un panneau avec une bordure et un titre.
   * 
   * @param _titre texte � afficher en tant que titre du panel
   * @param _type type de la bordure
   */
  public static BuPanel titledPanel(final String _titre, final int _type) {
    final BuPanel p = new BuPanel();
    Border b1 = null;
    if (_type == ETCHED_BORDER)
      b1 = BorderFactory.createEtchedBorder();
    else if (_type == BEVEL_BORDER)
      b1 = BorderFactory.createBevelBorder(BevelBorder.RAISED);
    else if (_type == COMPOUND_BORDER)
      b1 = BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(),
          BorderFactory.createLoweredBevelBorder());
    else
      FuLog.error("type de bordure inconnu");
    Border b2 = BorderFactory.createTitledBorder(b1, _titre);
    p.setBorder(b2);
    return p;
  }

  /**
   * arrondit un double � 2 chiffres apr�s la virgule.
   */

  public static double arronditDouble(double _d) {

    _d *= 100;
    if (_d >= 0)
      _d = (int) (_d + .5);
    else
      _d = (int) (_d - .5);
    _d /= 100;

    return _d;
  }

  /**
   * calcule un pas id�al � afficher pour un axe de courbe.
   * 
   * @param _max abscisse ou ordonn�e maximale de la courbe
   * @param _min abscisse ou ordonn�e minimale de la courbe
   */

  public static double getPas(double _max, double _min) {

    int n = 0;
    double tmp = _max - _min;

    if (tmp == 0)
      return 10;

    while ((int) (tmp / 10) != 0 && (int) (tmp / 10) != 1) {
      tmp /= 10;
      n++;
    }
    int e = (int) (tmp + 1);

    for (int i = 0; i < n - 1; i++) {
      e *= 10;
    }

    if (e >= _max - _min) {
      double d = e;
      while (d >= _max - _min) {
        d = d / 5;
      }
      return d;
    }

    return e;
  }

  /**
   * retourne l'abscisse/l'ordonn�e minimale id�ale � afficher sur un axe.
   */
  public static double getMin(double _pas, double _coordMin) {
    double min = _pas;

    if (_coordMin <= 0) {
      while (min >= _coordMin) {
        min -= _pas;
      }
    } else {
      if (min < _coordMin) {
        while (min + _pas < _coordMin) {
          min += _pas;
        }
      } else {
        return 0;
      }
    }

    return min;
  }

  /**
   * retourne l'abscisse/l'ordonn�e maximale id�ale � afficher sur un axe.
   */
  public static double getMax(double _pas, double _coordMax) {

    double max = _pas;
    if (_coordMax >= 0) {
      while (max <= _coordMax) {
        max += _pas;
      }
    } else {
      while (max - _pas > _coordMax) {
        max -= _pas;
      }
    }
    return max;
  }

  /**
   * affiche un rapport d'erreur de validation des donn�es dans une fen�tre
   * HTML.
   */
  public static void showValidationMessagesWindow(final BuCommonInterface _a,
      final ValidationMessages _vm) {
    String html = "";
    int i = 0;
    html += "<html>" + CtuluLibString.LINE_SEP + "";
    html += "<head><title>Rapport d'erreurs de validation</title></head>" + CtuluLibString.LINE_SEP
        + "";
    html += "<body>" + CtuluLibString.LINE_SEP + "";
    html += "<h1><font color = \"#3333CC\">Rapport d'erreurs de validation</font></h1><br>"
        + CtuluLibString.LINE_SEP + "";
    final VMessage[] _fatalMsg = _vm.getFatalErrorMessages();
    final VMessage[] _warningMsg = _vm.getWarningMessages();
    if (_fatalMsg.length > 0) {
      html += "<h2><font color = red>Erreurs bloquantes</font></h2>" + CtuluLibString.LINE_SEP + "";
      html += "<table width=\"100%\" border=1>" + CtuluLibString.LINE_SEP + "";
      html += "<tr><td><b>Onglet</b></td><td><b>Donn&eacute;es</b></td><td><b>Etat</b></td><td><b>Message</b></td></tr>"
          + CtuluLibString.LINE_SEP + "";
      for (i = 0; i < _fatalMsg.length; i++) {
        html += "<tr bgcolor=\"#" + (((i % 2) == 1) ? "DDDDDD" : "FFFFFF") + "\">"
            + CtuluLibString.LINE_SEP + "";
        html += "<td><b>" + _fatalMsg[i].getOnglet() + "</b></td>";
        html += "<td>" + _fatalMsg[i].getDonnees() + "</td>";
        html += "<td><i>" + _fatalMsg[i].getEtat() + "</i></td>";
        html += "<td><font color=red>" + _fatalMsg[i].getMessage() + "</font></td>"
            + CtuluLibString.LINE_SEP + "";
        html += "</tr>" + CtuluLibString.LINE_SEP + "";
      }
      html += "</table>" + CtuluLibString.LINE_SEP + "";
    }
    if (_warningMsg.length > 0) {
      html += "<br><h2><font color = green>Avertissements</font></h2>" + CtuluLibString.LINE_SEP
          + "";
      html += "<table width=\"100%\" border=1>" + CtuluLibString.LINE_SEP + "";
      html += "<tr><td><b>Onglet</b></td><td><b>Donn&eacute;es</b></td><td><b>Etat</b></td><td><b>Message</b></td></tr>"
          + CtuluLibString.LINE_SEP + "";
      for (i = 0; i < _warningMsg.length; i++) {
        html += "<tr bgcolor=\"#" + (((i % 2) == 1) ? "DDDDDD" : "FFFFFF") + "\">"
            + CtuluLibString.LINE_SEP + "";
        html += "<td><b>" + _warningMsg[i].getOnglet() + "</b></td>";
        html += "<td>" + _warningMsg[i].getDonnees() + "</td>";
        html += "<td><i>" + _warningMsg[i].getEtat() + "</i></td>";
        html += "<td><font color=green>" + _warningMsg[i].getMessage() + "</font></td>"
            + CtuluLibString.LINE_SEP + "";
        html += "</tr>" + CtuluLibString.LINE_SEP + "";
      }
      html += "</table>" + CtuluLibString.LINE_SEP + "";
    }
    html += "<br><br><b>Remarques :</b><br>Les erreurs bloquantes vous emp&ecirc;chent de lancer les calculs, vous devez donc les corriger pour pouvoir continuer.<br>"
        + " Les avertissements ne vous emp�chent pas de lancer les calculs mais vous signalent d'une incoh&eacute;rence de certaines donn&eacute;es.";
    html += "</body>" + CtuluLibString.LINE_SEP + "";
    html += "</html>" + CtuluLibString.LINE_SEP + "";
    final AlbeImplementation _albe = (AlbeImplementation) _a.getImplementation();
    _albe.showHtml("Rapport d'erreurs", html);
  }

  /**
   * renvoie un composant Label contenant l'image d'un symbol sp�cifi�e avec ses
   * propri�t�s positionn�es � celles par d�faut (notamment l'alignement
   * vertical du texte. Le label dispose d'une icone.
   * 
   * @param _symb fichier symbole � afficher
   */
  public static BuLabel symbol(final BuIcon _symb) {
    final BuLabel lb = new BuLabel("");
    lb.setIcon(_symb);
    lb.setVerticalAlignment(SwingConstants.CENTER);
    return lb;
  }

  /**
   * 
   */
  public static void showGraphViewer(final BuCommonInterface _app, AlbeImplementation _imp,
      final String _title, final String _script) {
    final Lecteur lin = new Lecteur(new StringReader(_script));
    final Graphe g = Graphe.parse(lin, (Applet) null);
    final BGraphe bg = new BGraphe();
    bg.setGraphe(g);
    final AlbeGraphiqueDialog agf = new AlbeGraphiqueDialog(_app, _title, bg);
    agf.setVisible(true);
  }

  /**
   *
   */
  public static void exportGraphScriptToGif(final String _script, final String _file) {
    final Graphe _g = Graphe.parse(new Lecteur(new StringReader(_script)), (Applet) null);
    // Palette Web => 216 couleurs
    final byte[] _cr = new byte[256];
    final byte[] _cg = new byte[256];
    final byte[] _cb = new byte[256];
    int i = 0;
    int rc;
    int gc;
    int bc;
    rc = 0;
    while (rc <= 255) {
      gc = 0;
      while (gc <= 255) {
        bc = 0;
        while (bc <= 255) {
          _cr[i] = (byte) rc;
          _cg[i] = (byte) gc;
          _cb[i] = (byte) bc;
          bc += 51;
          i++;
        }
        gc += 51;
      }
      rc += 51;
    }
    final BufferedImage _img = new BufferedImage(500, 500, BufferedImage.TYPE_BYTE_INDEXED,
        new IndexColorModel(8, 216, _cr, _cg, _cb));
    _g.dessine(_img.createGraphics(), 0, 0, 500, 500, 0, (ImageObserver) null);
    try {
      final AcmeGifEncoder _age = new AcmeGifEncoder(_img, new FileOutputStream(_file));
      _age.encode();
    }
    catch (final Exception _e1) {
      System.err
          .println("Erreur d'entr�e/sortie. Impossible d'�crire le fichier image sur le disque dur");
    }
  }

  /**
   * retourne un objet IDL instance de la classe de type sp�cifi�e.
   */
  public static Object createIDLObject(final Class _class) {
    Object o = null;
    if (_class.equals(SParametresAlbe.class)) {
      final SParametresAlbe _idl = new SParametresAlbe();
      o = _idl;
    } else if (_class.equals(SGeometrique.class)) {
      final SGeometrique _idl = new SGeometrique();
      o = _idl;
    } else if (_class.equals(SSol.class)) {
      final SSol _idl = new SSol();
      o = _idl;
    } else if (_class.equals(SCoucheCourbe.class)) {
      final SCoucheCourbe _idl = new SCoucheCourbe();
      o = _idl;
    } else if (_class.equals(SPointSol.class)) {
      final SPointSol _idl = new SPointSol();
      o = _idl;
    } else if (_class.equals(SPieu.class)) {
      final SPieu _idl = new SPieu();
      o = _idl;
    } else if (_class.equals(STronconTubulaire.class)) {
      final STronconTubulaire _idl = new STronconTubulaire();
      o = _idl;
    } else if (_class.equals(STronconCaisson.class)) {
      final STronconCaisson _idl = new STronconCaisson();
      o = _idl;
    } else if (_class.equals(SAction.class)) {
      final SAction _idl = new SAction();
      o = _idl;
    } else if (_class.equals(SDefense.class)) {
      final SDefense _idl = new SDefense();
      o = _idl;
    } else if (_class.equals(SPointDefense.class)) {
      final SPointDefense _idl = new SPointDefense();
      o = _idl;
    } else if (_class.equals(SCriteresDimensionnement.class)) {
      final SCriteresDimensionnement _idl = new SCriteresDimensionnement();
      o = _idl;
    } else if (_class.equals(SCoefficientsPartiels.class)) {
      final SCoefficientsPartiels _idl = new SCoefficientsPartiels();
      o = _idl;
    } else if (_class.equals(SCombinaisonMateriaux.class)) {
      final SCombinaisonMateriaux _idl = new SCombinaisonMateriaux();
      o = _idl;
    } else if (_class.equals(SCombinaisonModele.class)) {
      final SCombinaisonModele _idl = new SCombinaisonModele();
      o = _idl;
    } else if (_class.equals(SLancementCalculs.class)) {
      final SLancementCalculs _idl = new SLancementCalculs();
      o = _idl;
    }

    return o;
  }
}
