package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;

import org.fudaa.dodico.corba.albe.SLancementCalculs;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;

/**
 * Description de la fen�tre de 'Lancement des calculs' (qui apparait lorsque
 * l'utilisateur a valid� ses donn�es et qu'elles ne contiennent aucune erreur).
 * 
 * @author Sabrina Delattre
 */

public class AlbeLancementCalculsFrame extends AlbeInternalFrameAdapter {

  JRadioButton toutCalculerRadio_, choisirCombinaisonRadio_, lancementManuelRadio_;

  JCheckBox eluFondaBox_, eluAccBox_, elsRareBox_, elsFreqBox_;

  BuButton lancerButton_, annulerButton_, aideButton_;

  int modeCalcul_;

  /**
   * fen�tre de lancement des calculs manuellement.
   */
  AlbeDialogLancementManuelCalculs fCalcul_;

  /**
   * fen�tre de saisie des param�tres.
   */
  AlbeFilleParametres fp_;

  /**
   * Constructeur.
   */

  public AlbeLancementCalculsFrame(final BuCommonInterface _appli, final FudaaProjet _project,
      final AlbeFilleParametres _fp) {

    super("Lancement des calculs", "executer", _appli, _project);
    setSize(620, 410);

    appli_ = _appli.getApp();
    fp_ = _fp;

    JComponent content = (JComponent) getContentPane();
    content.setBorder(new EmptyBorder(5, 5, 5, 5));

    SpringLayout layout = new SpringLayout();
    content.setLayout(new BuVerticalLayout());

    /**
     * Cr�ation d'un groupe de boutons radio.
     */
    ButtonGroup group = new ButtonGroup();

    toutCalculerRadio_ = new JRadioButton("Tout calculer");
    choisirCombinaisonRadio_ = new JRadioButton("Choisir combinaison(s)");
    lancementManuelRadio_ = new JRadioButton("Lancement manuel");

    toutCalculerRadio_.addActionListener(this);
    choisirCombinaisonRadio_.addActionListener(this);
    lancementManuelRadio_.addActionListener(this);

    group.add(toutCalculerRadio_);
    group.add(choisirCombinaisonRadio_);
    group.add(lancementManuelRadio_);

    /**
     * Cr�ation de cases � cocher.
     */
    eluFondaBox_ = new JCheckBox("ELU fondamental");
    eluAccBox_ = new JCheckBox("ELU accidentel");
    elsRareBox_ = new JCheckBox("ELS rare");
    elsFreqBox_ = new JCheckBox("ELS fr�quent");

    eluFondaBox_.addActionListener(this);
    eluAccBox_.addActionListener(this);
    elsRareBox_.addActionListener(this);
    elsFreqBox_.addActionListener(this);

    /**
     * Au d�part, on s�lectionne le bouton "Tout calculer" et donc on rend
     * transparentes les 4 cases � cocher.
     */
    toutCalculerRadio_.setSelected(true);
    eluFondaBox_.setEnabled(false);
    eluAccBox_.setEnabled(false);
    elsRareBox_.setEnabled(false);
    elsFreqBox_.setEnabled(false);

    // Par d�faut, toutes les cases sont coch�es
    eluFondaBox_.setSelected(true);
    eluAccBox_.setSelected(true);
    elsRareBox_.setSelected(true);
    elsFreqBox_.setSelected(true);

    /**
     * Panneau nord.
     */
    final BuPanel pNord = new BuPanel(new BorderLayout());

    // Panneau 'Informations'
    final BuPanel pInfo = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    pInfo.setLayout(new BuVerticalLayout());
    final BuLabelMultiLine infoLabel = AlbeLib
        .labelMultiLine("Vous allez choisir le mode de lancement des calculs.\nPour cel�, vous devez choisir le(s) type(s) de combinaison que vous souhaitez voir v�rifier.\n"
            + "Vous pouvez �galement lancer le calcul pour une seule combinaison en introduisant vous-m�me les coefficients partiels (pour cel�, s�lectionnez 'Lancement manuel').");
    pInfo.add(infoLabel);

    // Panneau aide
    final BuPanel pAide = new BuPanel(new FlowLayout());
    aideButton_ = AlbeLib.aideButton();
    aideButton_.addActionListener(this);
    pAide.add(aideButton_);

    pNord.add(pInfo, BorderLayout.CENTER);
    pNord.add(pAide, BorderLayout.EAST);

    /**
     * Panneau contenant les boutons radio.
     */
    BuPanel tempPanel2 = AlbeLib.titledPanel("Mode de calculs", AlbeLib.COMPOUND_BORDER);
    tempPanel2.setLayout(new BuVerticalLayout());

    // Panneau contenant les cases � cocher.

    final BuPanel tempPanel1 = new BuPanel();
    tempPanel1.setLayout(new BuVerticalLayout());
    tempPanel1.add(eluFondaBox_);
    tempPanel1.add(eluAccBox_);
    tempPanel1.add(elsRareBox_);
    tempPanel1.add(elsFreqBox_);

    final BuPanel p = new BuPanel();
    p.setLayout(layout);
    p.add(tempPanel1);

    layout.putConstraint(SpringLayout.NORTH, tempPanel1, 5, SpringLayout.NORTH, p);

    layout.putConstraint(SpringLayout.WEST, tempPanel1, 30, SpringLayout.WEST, p);

    layout.putConstraint(SpringLayout.SOUTH, p, 5, SpringLayout.SOUTH, tempPanel1);

    layout.putConstraint(SpringLayout.EAST, p, 20, SpringLayout.EAST, tempPanel1);

    tempPanel2.add(toutCalculerRadio_);
    tempPanel2.add(choisirCombinaisonRadio_);
    tempPanel2.add(p);
    tempPanel2.add(lancementManuelRadio_);

    BuPanel pCentre = new BuPanel();
    pCentre.setLayout(new FlowLayout());
    pCentre.add(tempPanel2);

    /**
     * Panneau contenant les deux boutons 'Lancer' et 'Annuler'.
     */
    final BuPanel pButton = new BuPanel(new FlowLayout());
    lancerButton_ = AlbeLib.buttonId("Lancer", "EXECUTER");
    lancerButton_.addActionListener(this);
    annulerButton_ = AlbeLib.buttonId("Annuler", "ANNULER");
    annulerButton_.addActionListener(this);
    pButton.add(lancerButton_);
    pButton.add(annulerButton_);

    /** Ajout dans le conteneur. */
    content.add(pNord);
    content.add(pCentre);
    content.add(pButton);

  }// Fin constructeur

  /**
   * retourne une structure contenant les param�tres de calculs.
   */
  public SLancementCalculs getParametresCalculs() {
    SLancementCalculs p = (SLancementCalculs) AlbeLib.createIDLObject(SLancementCalculs.class);
    try {
      p.modeLancementCalcul = modeCalcul_;
    }
    catch (final NullPointerException _e1) {}

    if (modeCalcul_ == AlbeLib.IDL_LANCEMENT_AUTOMATIQUE) {
      if (toutCalculerRadio_.isSelected()) {
        p.eluFonda = true;
        p.eluAcc = true;
        p.elsRare = true;
        p.elsFreq = true;
      } else {
        p.eluFonda = eluFondaBox_.isSelected();
        p.eluAcc = eluAccBox_.isSelected();
        p.elsRare = elsRareBox_.isSelected();
        p.elsFreq = elsFreqBox_.isSelected();
      }
    } else if (modeCalcul_ == AlbeLib.IDL_LANCEMENT_MANUEL)
      p = fCalcul_.addParametresLancementManuel(p);

    return p;
  }

  /**
   * affiche l'aide concernant le lancement des calculs.
   */
  protected void aide() {
    String path = /*"file://" +*/ AlbeImplementation.informationsSoftware().man
        + AlbeImplementation.informationsSoftware().name.toLowerCase() + "/" + AlbeMsg.URL009
        + ".html";
    // String path = "aide/albe/" + helpfile_ + ".html";
    //getImplementation().showHtml(path);
    getImplementation().displayURL(path);
  }

  /**
   * 
   */

  public void actionPerformed(ActionEvent _evt) {

    Object src = _evt.getSource();
    if (src == choisirCombinaisonRadio_) {
      eluFondaBox_.setEnabled(true);
      eluAccBox_.setEnabled(true);
      elsRareBox_.setEnabled(true);
      elsFreqBox_.setEnabled(true);
    } else if (src == toutCalculerRadio_ || src == lancementManuelRadio_) {
      eluFondaBox_.setEnabled(false);
      eluAccBox_.setEnabled(false);
      elsRareBox_.setEnabled(false);
      elsFreqBox_.setEnabled(false);
    }

    // Clic sur le bouton aide
    else if (src == aideButton_)
      aide();

    // Clic sur le bouton 'Annuler'
    else if (src == annulerButton_)
      getImplementation().closeModeLancementCalculsFrame();

    // Clic sur le bouton "Lancer"
    else if (src == lancerButton_) {

      final ValidationMessages _vm = new ValidationMessages();
      _vm.add(fp_.geo_.validation());
      _vm.add(fp_.sol_.validation());
      _vm.add(fp_.pieu_.validation());
      _vm.add(fp_.action_.validation());
      _vm.add(fp_.defense_.validation());
      _vm.add(fp_.dim_.validation());
      _vm.add(fp_.coef_.validation());

      if (_vm.hasFatalError()) {
        /**
         * afficher une boite de dialogue pour dire que les donn�es contiennent
         * des erreurs fatales et que les calculs ne peuvent �tre lanc�s.
         */
        AlbeLib.dialogError(appli_, AlbeMsg.ERR014);
        ((AlbeImplementation) appli_.getImplementation()).activateInternalFrame(fp_);
        AlbeLib.showValidationMessagesWindow(appli_, _vm);
        ((AlbeImplementation) appli_.getImplementation()).closeModeLancementCalculsFrame();
        // D�sactivation du menu 'Calculs'
        ((AlbeImplementation) appli_.getImplementation()).setEnabledForAction("CALCULS", false);
        ((AlbeImplementation) appli_.getImplementation()).setEnabledForAction(
            "CHOIX_MODE_LANCEMENT_CALCULS", false);
        ((AlbeImplementation) appli_.getImplementation()).setEnabledForAction(
            "RESULTATS_DISPONIBLE", false);

      } else {

        // Si 'Tout calculer' est s�lectionn�
        if (toutCalculerRadio_.isSelected()) {
          modeCalcul_ = AlbeLib.IDL_LANCEMENT_AUTOMATIQUE;
          ((AlbeImplementation) appli_.getImplementation()).actionPerformed(new ActionEvent(this,
              3, "LANCER_CALCULS"));
        }

        // Si 'Choisir combinaison' est s�lectionn�
        else if (choisirCombinaisonRadio_.isSelected()) {
          if (!eluFondaBox_.isSelected() && !eluAccBox_.isSelected() && !elsRareBox_.isSelected()
              && !elsFreqBox_.isSelected())
            AlbeLib.dialogError(appli_, "Vous devez choisir au moins un type de combinaison!");
          else {
            modeCalcul_ = AlbeLib.IDL_LANCEMENT_AUTOMATIQUE;
            ((AlbeImplementation) appli_.getImplementation()).actionPerformed(new ActionEvent(this,
                3, "LANCER_CALCULS"));
          }
        }

        // Si 'Lancement manuel' est s�lectionn�
        else if (lancementManuelRadio_.isSelected()) {
          modeCalcul_ = AlbeLib.IDL_LANCEMENT_MANUEL;
          // On affiche la fen�tre de lancement manuel des calculs
          fCalcul_ = new AlbeDialogLancementManuelCalculs(getApplication());
          fCalcul_.setVisible(true);
        }
      }
    }
  }
}
