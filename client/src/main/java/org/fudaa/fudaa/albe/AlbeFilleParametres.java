package org.fudaa.fudaa.albe;

import java.awt.event.ActionEvent;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;

import org.fudaa.dodico.corba.albe.SParametresAlbe;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

import com.memoire.bu.BuCommonInterface;

/**
 * affiche une fen�tre de saisie des param�tres (donn�es).
 * 
 * @author Sabrina Delattre
 */
public class AlbeFilleParametres extends AlbeInternalFrameAdapter {

  /** onglet 'Param�tres g�om�triques'. */
  protected AlbeGeometriquesParametres geo_;

  /** onglet 'Caract�ristiques du pieu'. */
  protected AlbeCaracteristiquesPieuParametres pieu_;

  /** onglet 'Param�tres de sol'. */
  AlbeSolParametres sol_;

  /** onglet 'Action'. */
  protected AlbeActionParametres action_;

  /** onglet 'D�fense'. */
  protected AlbeDefenseParametres defense_;

  /** onglet 'Crit�res de dimensionnement'. */
  protected AlbeCriteresDimensionnementParametres dim_;

  /** onglet 'Coefficients partiels'. */
  protected AlbeCoefficientsPartielsParametres coef_;

  /** onglet 'Validation'. */
  protected AlbeValidationParametres validation_;

  /** set d'onglets principaux de la fen�tre. */
  protected JTabbedPane tp_;

  /**
   * constructeur de la fenetre. construit une fen�tre de saisie des donn�es
   * avec plusieurs onglets.
   */
  public AlbeFilleParametres(final BuCommonInterface _appli, final FudaaProjet _project) {

    super("Introduction des donn�es", "parametre", _appli, _project);
    setSize(800, 670);

    JComponent content = (JComponent) getContentPane();
    content.setBorder(new EmptyBorder(5, 5, 5, 5));

    tp_ = new JTabbedPane();
    tp_.addChangeListener(this);

    geo_ = new AlbeGeometriquesParametres(this);
    sol_ = new AlbeSolParametres(this);
    pieu_ = new AlbeCaracteristiquesPieuParametres(this);
    dim_ = new AlbeCriteresDimensionnementParametres(this);
    action_ = new AlbeActionParametres(this);
    defense_ = new AlbeDefenseParametres(this);
    coef_ = new AlbeCoefficientsPartielsParametres(this);
    validation_ = new AlbeValidationParametres(this);

    tp_.addTab("Param�tres g�om�triques", null, geo_,
        "Informations concernant les param�tres g�om�triques");
    tp_.addTab("Sol", null, sol_, "Informations concernant le sol");
    tp_.addTab("Caract�ristiques du pieu", null, pieu_, "Informations concernant le pieu");
    tp_.addTab("Action", null, action_, "Informations concernant l'action (accostage ou amarrage)");
    tp_.addTab("D�fenses d'accostage", null, defense_,
        "Informations concernant les d�fenses d'accostage");
    tp_.addTab("Crit�res de dimensionnement", null, dim_,
        "Informations concernant les crit�res de dimensionnement");
    tp_.addTab("Coefficients partiels", null, coef_,
        "Informations concernant les coefficients partiels");
    tp_.addTab("Validation", null, validation_, "Validation des donn�es saisies");

    // Ajout des onglets � la fen�tre
    content.add(tp_);

    this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

  }

  /**
   * 
   */

  public void majMessages() {

    final AlbeParamEventView mv = getImplementation().getParamEventView();
    mv.removeAllMessages();
    mv.addMessages(validation().getMessages());

  }

  /**
   * 
   */

  public void changeOnglet(final ChangeEvent _evt) {

    majMessages();

    ((AlbeAbstractOnglet) tp_.getComponentAt(tp_.getSelectedIndex())).changeOnglet(_evt);

    setCurrentTab(tp_.getSelectedIndex());
  }

  /**
   * renvoie une structure contenant les param�tres actuellement affich�s dans
   * les onglets de la fen�tre.
   */
  public SParametresAlbe getParametres() {
    final SParametresAlbe _p = (SParametresAlbe) AlbeLib.createIDLObject(SParametresAlbe.class);
    _p.geo = geo_.getParametresGeo();
    _p.sol = sol_.getParametresSol();
    _p.pieu = pieu_.getParametresPieu();
    _p.action = action_.getParametresAction();
    if (_p.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE))
      _p.defense = defense_.getParametresDefense();
    _p.criteresDimensionnement = dim_.getParametresCriteresDim();
    _p.coefficientsPartiels = coef_.getParametresCoef();
    _p.projet = project_.getInformations().titre;

    return _p;
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tre dans
   * les onglets de la fen�tre.
   */
  private synchronized void setParametres(SParametresAlbe _p) {
    if (_p == null) {
      _p = (SParametresAlbe) AlbeLib.createIDLObject(SParametresAlbe.class);
    }
    geo_.setParametresGeo(_p.geo);
    sol_.setParametresSol(_p.sol);
    pieu_.setParametresPieu(_p.pieu);
    if (_p.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE))
      defense_.setParametresDefense(_p.defense);
    dim_.setParametresCriteresDim(_p.criteresDimensionnement);
    action_.setParametresAction(_p.action);
    // setParametresCoef � placer apr�s setParametresAction sinon les
    // coefficients partiels ne sont pas import�s dans les tableaux.
    coef_.setParametresCoef(_p.coefficientsPartiels);
  }

  /**
   * retourne vrai si la structure de param�tres _p est diff�rente de celle
   * actuelle.
   */
  public boolean parametresModifies(SParametresAlbe _p) {
    if (geo_.paramsGeoModified(_p.geo) || sol_.paramsSolModified(_p.sol)
        || pieu_.paramsPieuModified(_p.pieu) || action_.paramsActionModified(_p.action)
        || dim_.paramsDimModified(_p.criteresDimensionnement)
        || coef_.paramsCoeffPartielsModified(_p, _p.coefficientsPartiels))
      return true;

    if (_p.action.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE))
      if (defense_.paramsDefenseModified(_p.defense))
        return true;
    return false;
  }

  /**
   * mise � jour du contenu des onglets de la fen�tre.
   */
  protected void updatePanels() {
    setParametres((SParametresAlbe) getProjet().getParam(AlbeResource.PARAMETRES));
  }

  /**
   * 
   */
  public void loadValidationMsg() {
    final AlbeParamEventView mv = ((AlbeImplementation) getApplication().getImplementation())
        .getParamEventView();
    mv.removeAllMessages();
    final ValidationMessages vm = new ValidationMessages();

    vm.add(geo_.validation());
    vm.add(sol_.validation());
    vm.add(pieu_.validation());
    vm.add(action_.validation());
    vm.add(defense_.validation());
    vm.add(dim_.validation());
    vm.add(coef_.validation());

    mv.addMessages(vm.getMessages());
  }

  /**
   * lance la v�rification des donn�es saisies par l'utilisateur et retourne la
   * liste des messages d'erreurs.
   */
  public ValidationMessages validation() {
    final ValidationMessages vm = new ValidationMessages();

    vm.add(geo_.validation());
    vm.add(sol_.validation());
    vm.add(pieu_.validation());
    vm.add(action_.validation());
    vm.add(defense_.validation());
    vm.add(dim_.validation());
    vm.add(coef_.validation());
    return vm;
  }

  /**
   * lance la validation des donn�es saisies par l'utilisateur et affiche les
   * messages d'erreurs �ventuels dans une fen�tre HTML.
   */
  public void valider() {

    // Fermeture de la fen�tre de lancement des calculs
    if (((AlbeImplementation) getApplication().getImplementation()).fl_ != null)
      ((AlbeImplementation) getApplication().getImplementation()).closeModeLancementCalculsFrame();

    final ValidationMessages _vm = new ValidationMessages();
    _vm.add(geo_.validation());
    _vm.add(sol_.validation());
    _vm.add(pieu_.validation());
    _vm.add(action_.validation());
    _vm.add(defense_.validation());
    _vm.add(dim_.validation());
    _vm.add(coef_.validation());

    if (_vm.hasFatalError()) {
      /**
       * afficher une boite de dialogue pour dire que les donn�es contiennent
       * des erreurs fatales et que les calculs ne peuvent �tre lanc�s.
       */
      AlbeLib.dialogError(getApplication(), AlbeMsg.DLG001);
      AlbeLib.showValidationMessagesWindow(getApplication(), _vm);
      firePropertyChange("CALCUL_DISPONIBLE", true, false);
      firePropertyChange("RESULTATS_DISPONIBLE", true, false);

    } else if (_vm.hasWarning()) {
      /**
       * afficher une boite de dialogue pour dire que les donn�es contiennent
       * des avertissements mais que les calculs peuvent �tre lanc�s.
       */
      final int _i = AlbeLib.dialogConfirmation(getApplication(), "Avertissement", AlbeMsg.DLG002);
      firePropertyChange("CALCUL_DISPONIBLE", false, true);
      firePropertyChange("RESULTATS_DISPONIBLE", true, false);
      if (_i == JOptionPane.YES_OPTION) {
        getProjet().addParam(AlbeResource.PARAMETRES, getParametres());
        ((AlbeImplementation) getApplication().getImplementation())
            .actionPerformed(new ActionEvent(this, 2, "CHOIX_MODE_LANCEMENT_CALCULS"));
      } else if (_i == JOptionPane.NO_OPTION) {
        AlbeLib.showValidationMessagesWindow(getApplication(), _vm);
      }
    }

    else {
      /**
       * afficher une boite de dialogue pour dire que les donn�es ne contiennent
       * aucune erreur et que les calculs peuvent �tre lanc�s.
       */
      firePropertyChange("CALCUL_DISPONIBLE", false, true);
      firePropertyChange("RESULTATS_DISPONIBLE", true, false);
      AlbeLib.dialogMessage(getApplication(), AlbeMsg.DLG003);
      ((AlbeImplementation) getApplication().getImplementation()).actionPerformed(new ActionEvent(
          this, 2, "CHOIX_MODE_LANCEMENT_CALCULS"));
      getProjet().addParam(AlbeResource.PARAMETRES, getParametres());
    }

  }
}
