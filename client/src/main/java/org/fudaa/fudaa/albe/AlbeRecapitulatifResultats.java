/*
 * @creation 30 nov. 06
 * @modification $Date: 2007-04-25 15:18:06 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;

import org.fudaa.ctulu.CtuluLibString;

import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * classe de l'onglet 'R�capitulatif' des r�sultats combinaison par combinaison.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */
public class AlbeRecapitulatifResultats extends AlbeAbstractResultatsOnglet {

  BuTextField coteDragage_, coteTete_, coteChoc_, corrosion_, energieEffort_, coefSol_, coefAcier_,
      coefDefense_, coefModeleResistance_, coefModeleMobilisation_, deflexionMax_, deplacementMax_,
      reactionMax_, coefModeleResistanceGamma_, coefModeleMobilisationGamma_, deflexionMaxGamma_,
      deplacementMaxGamma_, reactionMaxGamma_, energieModules_, energieDefenses_;

  /** r�f�rence vers la fen�tre parente. */
  AlbeFilleResultatsCombinaison fp_;

  /**
   * constructeur.
   */
  public AlbeRecapitulatifResultats(AlbeFilleResultatsCombinaison _fp) {

    super(_fp.getApplication(), _fp);

    fp_ = _fp;

    /** Bloc informations */
    BuPanel infoPanel = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    infoPanel.setLayout(new BorderLayout());
    BuLabelMultiLine infoLabel = AlbeLib
        .labelMultiLine("Cette page r�capitule :\n   - les caract�ristiques g�om�triques mises en oeuvre dans le calcul\n   "
            + "- les valeurs des coefficients partiels mis en oeuvre dans le calcul ainsi que les valeurs de calculs (valeurs apr�s pond�ration par les coefficients partiels)\n   "
            + "- les facteurs de dimensionnement obtenus\n   - les valeurs des �nergies\nVous pouvez exporter les r�sultats r�capitulatifs dans un fichier texte.");
    infoPanel.add(infoLabel);

    initTextField();

    /** Bloc 1 : boutons. */
    BuPanel b1 = new BuPanel(new FlowLayout());
    imprimerButton_ = AlbeLib.buttonId("Imprimer", "IMPRIMER");
    exporterButton_ = AlbeLib.buttonId("Exporter", "EXPORTER");
    imprimerButton_.setToolTipText("Imprimer la fen�tre");
    exporterButton_.setToolTipText("Exporter les r�sultats r�capitulatifs");
    imprimerButton_.addActionListener(this);
    exporterButton_.addActionListener(this);
    b1.add(imprimerButton_);
    b1.add(exporterButton_);

    /** Bloc 2. */
    BuPanel b2 = AlbeLib.titledPanel("Caract�ristiques g�om�triques", AlbeLib.ETCHED_BORDER);
    b2.setLayout(new SpringLayout());
    b2.add(new JLabel("Cote de dragage"));
    b2.add(coteDragage_);
    b2.add(new JLabel("Cote de la t�te de l'ouvrage"));
    b2.add(coteTete_);
    b2.add(new JLabel("Cote du choc"));
    b2.add(coteChoc_);
    b2.add(new JLabel("Corrosion"));
    b2.add(corrosion_);
    if (fp_.accostage_)
      b2.add(new JLabel("Energie d'accostage"));
    else
      b2.add(new JLabel("Effort d'amarrage"));
    b2.add(energieEffort_);

    SpringUtilities.makeCompactGrid(b2, 5, 2, 5, 5, 5, 5);

    /** Bloc 3. */
    BuPanel tmp3 = AlbeLib.titledPanel("Coefficients partiels", AlbeLib.ETCHED_BORDER);
    BuPanel b3 = new BuPanel();
    b3.setLayout(new FlowLayout());
    tmp3.setLayout(new SpringLayout());
    tmp3.add(new JLabel("Coefficient sur sol"));
    tmp3.add(coefSol_);
    tmp3.add(new JLabel("Coefficient sur acier"));
    tmp3.add(coefAcier_);
    JLabel coefDefenseLabel = new JLabel("Coefficient sur d�fense");
    tmp3.add(coefDefenseLabel);
    tmp3.add(coefDefense_);

    SpringUtilities.makeCompactGrid(tmp3, 3, 2, 5, 5, 5, 5);

    b3.add(tmp3);

    /** Bloc 4. */
    BuPanel b4 = AlbeLib.titledPanel("Facteurs de dimensionnement", AlbeLib.ETCHED_BORDER);
    b4.setLayout(new SpringLayout());
    b4.add(new JLabel(""));
    b4.add(new JLabel(""));
    b4.add(AlbeLib.symbol(AlbeLib.getIcon("grand-gamma-2")));
    b4.add(new JLabel("Coefficient de mod�le r�sistance des tubes"));
    b4.add(coefModeleResistance_);
    b4.add(coefModeleResistanceGamma_);
    b4.add(new JLabel("Coefficient de mod�le mobilisation de la but�e du sol"));
    b4.add(coefModeleMobilisation_);
    b4.add(coefModeleMobilisationGamma_);
    JLabel deflexionLabel = new JLabel("D�flexion maximale de d�fense");
    b4.add(deflexionLabel);
    b4.add(deflexionMax_);
    b4.add(deflexionMaxGamma_);
    b4.add(new JLabel("D�placement maximal en t�te"));
    b4.add(deplacementMax_);
    b4.add(deplacementMaxGamma_);
    JLabel reactionLabel = new JLabel("R�action maximale au choc");
    b4.add(reactionLabel);
    b4.add(reactionMax_);
    b4.add(reactionMaxGamma_);
    if (!fp_.accostage_)
      reactionLabel.setEnabled(false);

    SpringUtilities.makeCompactGrid(b4, 6, 3, 5, 5, 5, 5);

    /** Bloc 5 : Energies. */
    BuPanel b5 = new BuPanel(new BorderLayout());
    BuPanel tmp5 = AlbeLib.titledPanel("Energies", AlbeLib.ETCHED_BORDER);
    tmp5.setLayout(new SpringLayout());
    tmp5.add(new JLabel("Energie des modules"));
    tmp5.add(energieModules_);
    JLabel energieDefensesLabel = new JLabel("Energie des d�fenses");
    tmp5.add(energieDefensesLabel);
    tmp5.add(energieDefenses_);
    b5.add(tmp5, BorderLayout.WEST);

    SpringUtilities.makeCompactGrid(tmp5, 2, 2, 5, 5, 5, 5);

    // Si pas de d�fense
    if (!fp_.presenceDefense_) {
      coefDefenseLabel.setEnabled(false);
      deflexionLabel.setEnabled(false);
      energieDefensesLabel.setEnabled(false);
    }

    /** Panneau regroupant les cinq blocs pr�c�dents */
    BuPanel p = AlbeLib.titledPanel("R�capitulatif des r�sultats", AlbeLib.BEVEL_BORDER);
    p.setLayout(new BuVerticalLayout());
    BuPanel p1 = new BuPanel(new BuHorizontalLayout());
    p1.add(b2);
    p1.add(b3);
    p.add(b1);
    p.add(p1);
    p.add(b4);
    p.add(b5);

    BuPanel tmpPanel = new BuPanel(new FlowLayout());
    tmpPanel.add(p);

    // Ecriture des r�sultats dans les champs texte
    setResults();

    this.setLayout(new BorderLayout());
    BuPanel content = new BuPanel(new BuVerticalLayout());
    content.add(infoPanel);
    content.add(tmpPanel);

    // Ajout d'un scrollBar dans le cas o� tous les composants ne tiennent pas
    // dans la fen�tre
    JScrollPane sp = new JScrollPane(content);
    JScrollBar sb = sp.getVerticalScrollBar();
    this.add(sp, BorderLayout.CENTER);
    this.add(sb, BorderLayout.EAST);

  }// Fin constructeur

  /**
   * initialise tous les champs texte.
   */
  public void initTextField() {
    coteDragage_ = new BuTextField(8);
    coteTete_ = new BuTextField(8);
    coteChoc_ = new BuTextField(8);
    corrosion_ = new BuTextField(8);
    energieEffort_ = new BuTextField(8);
    coefSol_ = new BuTextField(8);
    coefAcier_ = new BuTextField(8);
    coefDefense_ = new BuTextField(8);
    coefModeleResistance_ = new BuTextField(8);
    coefModeleMobilisation_ = new BuTextField(8);
    deflexionMax_ = new BuTextField(8);
    deplacementMax_ = new BuTextField(8);
    reactionMax_ = new BuTextField(8);
    coefModeleResistanceGamma_ = new BuTextField(8);
    coefModeleMobilisationGamma_ = new BuTextField(8);
    deflexionMaxGamma_ = new BuTextField(8);
    deplacementMaxGamma_ = new BuTextField(8);
    reactionMaxGamma_ = new BuTextField(8);
    energieModules_ = new BuTextField(8);
    energieDefenses_ = new BuTextField(8);
    AlbeRes.setTextField(coteDragage_);
    AlbeRes.setTextField(coteTete_);
    AlbeRes.setTextField(coteChoc_);
    AlbeRes.setTextField(corrosion_);
    AlbeRes.setTextField(energieEffort_);
    AlbeRes.setTextField(coefSol_);
    AlbeRes.setTextField(coefAcier_);
    AlbeRes.setTextField(coefDefense_);
    AlbeRes.setTextField(coefModeleResistance_);
    AlbeRes.setTextField(coefModeleMobilisation_);
    AlbeRes.setTextField(deflexionMax_);
    AlbeRes.setTextField(deplacementMax_);
    AlbeRes.setTextField(reactionMax_);
    AlbeRes.setTextField(coefModeleResistanceGamma_);
    AlbeRes.setTextField(coefModeleMobilisationGamma_);
    AlbeRes.setTextField(deflexionMaxGamma_);
    AlbeRes.setTextField(deplacementMaxGamma_);
    AlbeRes.setTextField(reactionMaxGamma_);
    AlbeRes.setTextField(energieModules_);
    AlbeRes.setTextField(energieDefenses_);
  }

  /**
   * retourne l'�nergie (ou l'effort si amarrage) sous forme d'une string suivi
   * de l'unit� correspondante.
   */
  public String getEnergie_Effort() {
    String s = "";

    switch (fp_.typeCombi_) {
    case AlbeLib.ELU_FONDA: {
      s = fp_.accostage_ ? fp_.params_.action.energieAccostageCalcul + " kJ"
          : fp_.params_.action.effortAmarrageCalcul + " kN";
      break;
    }
    case AlbeLib.ELU_ACC: {
      s = fp_.accostage_ ? fp_.params_.action.energieAccostageAccidentelle + " kJ"
          : fp_.params_.action.effortAmarrageAccidentel + " kN";
      break;
    }
    case AlbeLib.ELS_RARE: {
      s = fp_.accostage_ ? fp_.params_.action.energieAccostageCaracteristique + " kJ"
          : fp_.params_.action.effortAmarrageCaracteristique + " kN";
      break;
    }
    case AlbeLib.ELS_FREQ: {
      s = fp_.accostage_ ? fp_.params_.action.energieAccostageFrequente + " kJ"
          : fp_.params_.action.effortAmarrageFrequent + " kN";
      break;
    }
    }
    return s;
  }

  /**
   * remplit les champs texte avec les r�sultats dans le cas d'un lancement
   * automatique.
   */
  public void setResultsLancementAutomatique() {
    /** COTE DU CHOC */
    if (fp_.accostage_) {
      // bool�en sp�cifiant le type d'accostage (haut ou bas)
      boolean accostageHaut = true;
      if (fp_.numCombi_ <= fp_.nbCombi_ / 2)
        accostageHaut = false;
      AlbeRes.setField(coteChoc_, accostageHaut ? fp_.params_.action.coteAccostageHaut
          : fp_.params_.action.coteAccostageBas, "m");
    } else {
      AlbeRes.setField(coteChoc_, fp_.params_.action.coteAmarrage, "m");
    }

    /** CORROSION */
    corrosion_.setText(fp_.cor_ ? "Oui" : "Non");

    /** ENERGIE D'ACCOSTAGE (OU EFFORT D'AMARRAGE) */
    energieEffort_.setText(getEnergie_Effort());
  }

  /**
   * remplit les champs de texte avec les r�sultats.
   */
  public void setResults() {

    /** COTE DE DRAGAGE */
    AlbeRes.setField(coteDragage_, fp_.params_.geo.coteDragageBassin, "m");

    /** COTE TETE OUVRAGE */
    AlbeRes.setField(coteTete_, fp_.params_.geo.coteTeteOuvrage, "m");

    /** COEFFICIENT SUR SOL */
    AlbeRes.setField(coefSol_, AlbeRes.getCoefSol(fp_.params_, fp_.typeCombi_, fp_.numCombi_,
        fp_.accostage_, fp_.presenceDefense_), "");

    /** COEFFICIENT SUR ACIER */
    coefAcier_.setText(AlbeRes.getCoefAcier(fp_.params_, fp_.typeCombi_));

    /** COEFFICIENT DE MODELE RESISTANCE DES TUBES */
    String[] pieu = AlbeRes.getResistancePieu(fp_.params_, fp_.resCombi_, fp_.typeCombi_, fp_.cor_);
    // Valeur de l'interface
    coefModeleResistance_.setText(pieu[1]);
    // Facteur de dimensionnement
    coefModeleResistanceGamma_.setText(pieu[2]);

    /** COEFFICIENT DE MODELE MOBILISATION DE LA BUTEE DU SOL */
    String[] mobilisation = AlbeRes.getMobilisation(fp_.params_, fp_.resCombi_, fp_.typeCombi_, fp_.numCombi_);
    // Valeur de l'interface
    coefModeleMobilisation_.setText(mobilisation[1]);
    //Facteur de dimensionnement
    coefModeleMobilisationGamma_.setText(mobilisation[2]);

    /** DEPLACEMENT MAXIMAL EN T�TE */
    deplacementMax_.setText(fp_.deplacements_[1]);
    deplacementMaxGamma_.setText(fp_.deplacements_[2]);

    /** ENERGIE DES MODULES */
    AlbeRes.setField(energieModules_, AlbeLib
        .arronditDouble(fp_.resCombi_.resultatsGeneraux.energieModules * 10), "kJ");

    // Si accostage
    if (fp_.accostage_) {
      /** REACTION MAXIMALE AU CHOC */
      reactionMax_.setText(fp_.reactions_[1]);
      reactionMaxGamma_.setText(fp_.reactions_[2]);
    }

    // Si d�fense
    if (fp_.presenceDefense_) {
      /** COEFFICIENT SUR DEFENSE */
      AlbeRes.setField(coefDefense_, AlbeRes.getCoefDefense(fp_.params_, fp_.typeCombi_,
          fp_.numCombi_), "");
      /** ENERGIE DES DEFENSES */
      AlbeRes.setField(energieDefenses_, AlbeLib
          .arronditDouble(fp_.resCombi_.resultatsGeneraux.energieDefenses * 10), "kJ");
      /** DEFLEXION MAXIMALE DE DEFENSE */
      deflexionMax_.setText(fp_.deflexions_[1]);
      deflexionMaxGamma_.setText(fp_.deflexions_[2]);
    }

    // Si lancement manuel
    if (fp_.params_.lancementCalculs.modeLancementCalcul == AlbeLib.IDL_LANCEMENT_MANUEL) {
      AlbeRes.setField(coteChoc_, fp_.params_.lancementCalculs.cote, "m");
      corrosion_.setText(fp_.params_.lancementCalculs.corrosion ? "Oui" : "Non");
      if (fp_.accostage_) {
        AlbeRes.setField(energieEffort_, fp_.params_.lancementCalculs.energieAccostage, "kJ");
      } else
        AlbeRes.setField(energieEffort_, fp_.params_.lancementCalculs.effortAmarrage, "kN");
    }

    // Si lancement automatique
    else
      setResultsLancementAutomatique();
  }

  /**
   * exporte le r�capitulatif des r�sultats dans un fichier .txt.
   */
  public void exporter() {
    final String f = AlbeLib.getFileChoosenByUser(this, "Exporter les donn�es", "Exporter",
        AlbeLib.USER_HOME
            + "Albe_resultatsrecapitulatifs_combinaison"
            + (fp_.typeCombi_ == AlbeLib.COMBINAISON_MANUELLE ? "manuelle"
                : (fp_.numCombi_ + "_" + AlbeRes.nomCombinaison(fp_.typeCombi_))) + ".txt");
    if (f == null) {
      return;
    }
    String txt = "";
    txt += "ALBE : combinaison "
        + (fp_.typeCombi_ == AlbeLib.COMBINAISON_MANUELLE ? "manuelle"
            : (fp_.numCombi_ + " " + AlbeRes.getCombinaison(fp_.typeCombi_)))
        + CtuluLibString.LINE_SEP;
    txt += "R�capitulatif des r�sultats" + CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP;

    // Caract�ristiques g�om�triques
    txt += "Caract�ristiques g�om�triques" + CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP;
    txt += "Cote de dragage : " + coteDragage_.getText() + CtuluLibString.LINE_SEP;
    txt += "Cote de la t�te de l'ouvrage : " + coteTete_.getText() + CtuluLibString.LINE_SEP;
    txt += "Cote du choc : " + coteChoc_.getText() + CtuluLibString.LINE_SEP;
    txt += "Corrosion : " + corrosion_.getText() + CtuluLibString.LINE_SEP;
    txt += (fp_.accostage_ ? "Energie d'accostage : " : "Effort d'amarrage : ")
        + energieEffort_.getText() + CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP
        + CtuluLibString.LINE_SEP;

    // Coefficients partiels
    txt += "Coefficients partiels" + CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP;
    txt += "Coefficient sur sol : " + coefSol_.getText() + CtuluLibString.LINE_SEP;
    txt += "Coefficient sur acier : " + coefAcier_.getText() + CtuluLibString.LINE_SEP;
    if (fp_.presenceDefense_)
      txt += "Coefficient sur d�fense : " + coefDefense_.getText() + CtuluLibString.LINE_SEP;
    txt += CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP;

    //
    txt += "Coefficient de mod�le r�sistance des tubes : " + coefModeleResistance_.getText()
        + CtuluLibString.LINE_SEP;
    txt += "Coefficient de mod�le mobilisation de la but�e du sol : "
        + coefModeleMobilisation_.getText() + CtuluLibString.LINE_SEP;
    if (fp_.presenceDefense_)
      txt += "D�flexion maximale de la d�fense : " + deflexionMax_.getText()
          + CtuluLibString.LINE_SEP;
    txt += "D�placement maximal en t�te : " + deplacementMax_.getText() + CtuluLibString.LINE_SEP;
    if (fp_.accostage_)
      txt += "R�action maximale au choc : " + reactionMax_.getText() + CtuluLibString.LINE_SEP;
    txt += CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP;

    // Facteurs de dimensionnement
    txt += "Facteurs de dimensionnement" + CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP;
    txt += "R�sistance des tubes : " + coefModeleResistanceGamma_.getText()
        + CtuluLibString.LINE_SEP;
    txt += "Mobilisation de la but�e du sol : " + coefModeleMobilisationGamma_.getText()
        + CtuluLibString.LINE_SEP;
    if (fp_.presenceDefense_)
      txt += "D�flexion de la d�fense : " + deflexionMaxGamma_.getText() + CtuluLibString.LINE_SEP;
    txt += "D�placement en t�te : " + deplacementMaxGamma_.getText() + CtuluLibString.LINE_SEP;
    if (fp_.accostage_)
      txt += "R�action au choc : " + reactionMaxGamma_.getText() + CtuluLibString.LINE_SEP;
    txt += CtuluLibString.LINE_SEP + CtuluLibString.LINE_SEP;

    // Energies
    txt += "Energie des modules : " + energieModules_.getText();
    if (fp_.presenceDefense_)
      txt += CtuluLibString.LINE_SEP + "Energie des d�fenses : " + energieDefenses_.getText();

    AlbeLib.writeFile(f, txt);
  }

  /**
   * construit la partie HTML des r�sultats r�capitulatifs.
   */
  public String buildPartieHtml() {
    String html = "";

    html += "<ul>" + CtuluLibString.LINE_SEP;
    // Caract�ristiques g�om�triques
    html += "<li><b>Caract&eacute;ristiques g&eacute;om&eacute;triques</b>" + CtuluLibString.LINE_SEP;
    html += "<br><ul type = square>" + CtuluLibString.LINE_SEP;
    html += "<li>Cote de dragage : " + coteDragage_.getText() + CtuluLibString.LINE_SEP;
    html += "<li>Cote de la t&ecirc;te de l'ouvrage : " + coteTete_.getText() + CtuluLibString.LINE_SEP;
    html += "<li>Cote du choc : " + coteChoc_.getText() + CtuluLibString.LINE_SEP;
    html += "<li>Corrosion : " + corrosion_.getText() + CtuluLibString.LINE_SEP;
    html += "<li>" + (fp_.accostage_ ? "Energie d'accostage : " : "Effort d'amarrage : ")
        + energieEffort_.getText() + CtuluLibString.LINE_SEP;
    html += "</ul>" + CtuluLibString.LINE_SEP;
    // Coefficients partiels
    html += "<br><li><b>Coefficients partiels</b>" + CtuluLibString.LINE_SEP;
    html += "<br><ul type = square>" + CtuluLibString.LINE_SEP;
    html += "<li>Coefficient sur sol : " + coefSol_.getText() + CtuluLibString.LINE_SEP;
    html += "<li>Coefficient sur acier : " + coefAcier_.getText() + CtuluLibString.LINE_SEP;
    if (fp_.presenceDefense_)
      html += "<li>Coefficient sur d&eacute;fense : " + coefDefense_.getText() + CtuluLibString.LINE_SEP;
    html += "</ul>" + CtuluLibString.LINE_SEP;
    // Facteurs de dimensionnement
    html += "<br><li><b>Facteurs de dimensionnement</b>" + CtuluLibString.LINE_SEP;
    html += "<br><br><table border=\"2\" cellspacing=\"0\" cellpadding=\"5\">" + CtuluLibString.LINE_SEP;
    html += "<th></th><th bgcolor=\"#DDDDDD\">Valeur maximale</th><th bgcolor=\"#DDDDDD\">Facteur de dimensionnement</th>";
    html += "<tr align=center>" + CtuluLibString.LINE_SEP;
    html += "<th bgcolor=\"#DDDDDD\">R&eacute;sistance des tubes</th>" + CtuluLibString.LINE_SEP;
    html += "<td>" + coefModeleResistance_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "<td>" + coefModeleResistanceGamma_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "</tr>" + CtuluLibString.LINE_SEP;
    html += "<tr align=center>" + CtuluLibString.LINE_SEP;
    html += "<th bgcolor=\"#DDDDDD\">Mobilisation de la but&eacute;e du sol</th>" + CtuluLibString.LINE_SEP;
    html += "<td>" + coefModeleMobilisation_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "<td>" + coefModeleMobilisationGamma_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "</tr>" + CtuluLibString.LINE_SEP;
    if (fp_.presenceDefense_) {
      html += "<tr align=center>" + CtuluLibString.LINE_SEP;
      html += "<th bgcolor=\"#DDDDDD\">D&eacute;flexion de la d&eacute;fense</th>" + CtuluLibString.LINE_SEP;
      html += "<td>" + deflexionMax_.getText() + "</td>" + CtuluLibString.LINE_SEP;
      html += "<td>" + deflexionMaxGamma_.getText() + "</td>" + CtuluLibString.LINE_SEP;
      html += "</tr>" + CtuluLibString.LINE_SEP;
    }
    html += "<tr align=center>" + CtuluLibString.LINE_SEP;
    html += "<th bgcolor=\"#DDDDDD\">D&eacute;placement en t&ecirc;te</th>" + CtuluLibString.LINE_SEP;
    html += "<td>" + deplacementMax_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "<td>" + deplacementMaxGamma_.getText() + "</td>" + CtuluLibString.LINE_SEP;
    html += "</tr>" + CtuluLibString.LINE_SEP;
    if (fp_.accostage_) {
      html += "<tr align=center>" + CtuluLibString.LINE_SEP;
      html += "<th bgcolor=\"#DDDDDD\">R&eacute;action au choc</th>" + CtuluLibString.LINE_SEP;
      html += "<td>" + reactionMax_.getText() + "</td>" + CtuluLibString.LINE_SEP;
      html += "<td>" + reactionMaxGamma_.getText() + "</td>" + CtuluLibString.LINE_SEP;
      html += "</tr>" + CtuluLibString.LINE_SEP;
    }
    html += "</table><br>" + CtuluLibString.LINE_SEP;
    // Energies
    html += "<li><b>Energies</b>" + CtuluLibString.LINE_SEP;
    html += "<br><ul type = square>" + CtuluLibString.LINE_SEP;
    html += "<li>Energie des modules : " + energieModules_.getText() + CtuluLibString.LINE_SEP;
    if (fp_.presenceDefense_)
      html += "<li>Energie des d&eacute;fenses : " + energieDefenses_.getText() + CtuluLibString.LINE_SEP;
    html += "</ul></ul><br>" + CtuluLibString.LINE_SEP;

    return html;
  }
}
