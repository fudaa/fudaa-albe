package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import org.fudaa.dodico.corba.albe.SAction;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * Description de l'onglet 'Action'.
 * 
 * @author Sabrina Delattre
 */

public class AlbeActionParametres extends AlbeAbstractOnglet {

  JComboBox actionCombo_;

  String[] actionList_ = { "ACCOSTAGE", "AMARRAGE" };

  /**
   * champs texte pour l'accostage.
   */
  BuTextField accostageHaut_, accostageBas_, energieAccostFreq_, energieAccostCaract_,
      energieAccostCalcul_, energieAccostAcc_, effortVertAccostage_;

  /**
   * boutons pour l'accostage.
   */
  BuButton calcul1Button_, calcul2Button_, calcul3Button_, calcul4Button_;

  /**
   * bouton 'Aide'.
   */
  BuButton aideButton_;

  /**
   * boutons 'Initialiser'.
   */
  BuButton initButtonAccost_, initButtonAmar_;

  /**
   * champs texte pour l'amarrage.
   */
  BuTextField amarrage_, effortFreq_, effortCaract_, effortCalcul_, effortAcc_, effortVertFreq_,
      effortVertCaract_, effortVertCalcul_, effortVertAcc_;

  BuPanel panelCenter_;

  /**
   * r�f�rence vers la fen�tre parente.
   */
  AlbeFilleParametres fp_;

  /**
   * fen�tre de calcul de l'�nergie d'accostage.
   */
  AlbeDialogCalculEnergieAccostage fCalcul_;

  /**
   * Constructeur.
   * 
   * @param _fp fen�tre parente
   * @param _t set d'onglets de la fen�tre de saisie des param�tres
   */
  public AlbeActionParametres(final AlbeFilleParametres _fp) {

    super(_fp.getImplementation(), _fp, AlbeMsg.URL004);

    fp_ = _fp;

    this.setLayout(new BuVerticalLayout());

    BuPanel panelNord = new BuPanel(new BorderLayout());

    /**
     * Panneau contenant la boite combo 'Accostage'/'Amarrage'.
     */
    BuPanel actionPanel = new BuPanel(new FlowLayout());
    actionCombo_ = new JComboBox(actionList_);
    actionCombo_.addActionListener(this);
    actionPanel.add(actionCombo_);

    aideButton_ = AlbeLib.aideButton();
    aideButton_.addActionListener(this);

    panelNord.add(actionPanel, BorderLayout.CENTER);
    panelNord.add(aideButton_, BorderLayout.EAST);

    /**
     * Utilisation du gestionnaire CardLayout pour afficher les panneaux
     * relatifs � l'accostage et � l'amarrage.
     */
    CardLayout c = new CardLayout();
    panelCenter_ = new BuPanel(c);
    panelCenter_.add("panAccost", initAccostagePanel());
    panelCenter_.add("panAmar", initAmarragePanel());

    this.add(panelNord);
    this.add(panelCenter_);

  }// Fin constructeur

  /**
   * cr�� et retourne le panneau pour l'accostage.
   */

  public BuPanel initAccostagePanel() {
    BuPanel accostagePanel = new BuPanel();

    BuPanel pAccost1 = new BuPanel(new BorderLayout());
    BuPanel tmpPanelAccostageNord = AlbeLib.titledPanel("Cotes d'accostage", AlbeLib.ETCHED_BORDER);
    tmpPanelAccostageNord.setLayout(new SpringLayout());

    BuPanel pAccost2 = new BuPanel(new FlowLayout());

    BuPanel pAccost3 = AlbeLib.titledPanel("Energies d'accostage", AlbeLib.ETCHED_BORDER);
    pAccost3.setLayout(new SpringLayout());

    BuPanel pAccost4 = new BuPanel(new FlowLayout());

    // Initialisation des composants
    initTextFieldAccostage();
    initButtonAccostage();

    // Ajout des composants
    tmpPanelAccostageNord.add(new JLabel("Cote d'accostage haut"));
    tmpPanelAccostageNord.add(accostageHaut_);
    tmpPanelAccostageNord.add(new JLabel("m"));
    tmpPanelAccostageNord.add(new JLabel("Cote d'accostage bas"));
    tmpPanelAccostageNord.add(accostageBas_);
    tmpPanelAccostageNord.add(new JLabel("m"));
    pAccost2.add(new JLabel("Effort vertical en t�te de pieu"));
    pAccost2.add(effortVertAccostage_);
    pAccost2.add(new JLabel("kN"));
    pAccost3.add(new JLabel("Energie d'accostage fr�quente"));
    pAccost3.add(energieAccostFreq_);
    pAccost3.add(new JLabel("kJ"));
    pAccost3.add(calcul1Button_);
    pAccost3.add(new JLabel("Energie d'accostage caract�ristique"));
    pAccost3.add(energieAccostCaract_);
    pAccost3.add(new JLabel("kJ"));
    pAccost3.add(calcul2Button_);
    pAccost3.add(new JLabel("Energie d'accostage de calcul"));
    pAccost3.add(energieAccostCalcul_);
    pAccost3.add(new JLabel("kJ"));
    pAccost3.add(calcul3Button_);
    pAccost3.add(new JLabel("Energie d'accostage accidentelle"));
    pAccost3.add(energieAccostAcc_);
    pAccost3.add(new JLabel("kJ"));
    pAccost3.add(calcul4Button_);

    SpringUtilities.makeCompactGrid(tmpPanelAccostageNord, 2, 3, 10, 10, 10, 10);
    SpringUtilities.makeCompactGrid(pAccost3, 4, 4, 10, 10, 10, 10);

    pAccost4.add(initButtonAccost_);

    BuPanel tempPanelAccost = AlbeLib.titledPanel("Accostage", AlbeLib.COMPOUND_BORDER);
    tempPanelAccost.setLayout(new BuVerticalLayout());
    pAccost1.add(tmpPanelAccostageNord, BorderLayout.WEST);
    BuPanel tmpAccost2 = new BuPanel(new BorderLayout());
    tmpAccost2.add(pAccost2, BorderLayout.WEST);
    tempPanelAccost.add(pAccost1);
    tempPanelAccost.add(tmpAccost2);
    tempPanelAccost.add(pAccost3);
    tempPanelAccost.add(pAccost4);

    accostagePanel.add(tempPanelAccost);

    return accostagePanel;
  }

  /**
   * initialise les champs texte utilis�s pour l'accostage.
   */
  public void initTextFieldAccostage() {
    accostageHaut_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    accostageBas_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    effortVertAccostage_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    energieAccostFreq_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    energieAccostCaract_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    energieAccostCalcul_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    energieAccostAcc_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    accostageHaut_.setHorizontalAlignment(SwingConstants.CENTER);
    accostageBas_.setHorizontalAlignment(SwingConstants.CENTER);
    effortVertAccostage_.setHorizontalAlignment(SwingConstants.CENTER);
    energieAccostCaract_.setHorizontalAlignment(SwingConstants.CENTER);
    energieAccostCalcul_.setHorizontalAlignment(SwingConstants.CENTER);
    energieAccostFreq_.setHorizontalAlignment(SwingConstants.CENTER);
    energieAccostAcc_.setHorizontalAlignment(SwingConstants.CENTER);
    accostageHaut_.addKeyListener(this);
    accostageBas_.addKeyListener(this);
    effortVertAccostage_.addKeyListener(this);
    energieAccostCaract_.addKeyListener(this);
    energieAccostCalcul_.addKeyListener(this);
    energieAccostFreq_.addKeyListener(this);
    energieAccostAcc_.addKeyListener(this);
    accostageHaut_.addFocusListener(this);
    accostageBas_.addFocusListener(this);
    effortVertAccostage_.addFocusListener(this);
    energieAccostCaract_.addFocusListener(this);
    energieAccostCalcul_.addFocusListener(this);
    energieAccostFreq_.addFocusListener(this);
    energieAccostAcc_.addFocusListener(this);
    accostageHaut_.setColumns(7);
    effortVertAccostage_.setColumns(7);
    energieAccostFreq_.setColumns(7);
  }

  /**
   * initialise les boutons utilis�s pour l'accostage.
   */
  public void initButtonAccostage() {
    // Boutons 'Calculatrice'
    calcul1Button_ = AlbeLib.buttonId("Calculatrice", "EXECUTER");
    calcul2Button_ = AlbeLib.buttonId("Calculatrice", "EXECUTER");
    calcul3Button_ = AlbeLib.buttonId("Calculatrice", "EXECUTER");
    calcul4Button_ = AlbeLib.buttonId("Calculatrice", "EXECUTER");
    calcul1Button_.setToolTipText("Calculer l'�nergie d'accostage fr�quente");
    calcul2Button_.setToolTipText("Calculer l'�nergie d'accostage caract�ristique");
    calcul3Button_.setToolTipText("Calculer l'�nergie d'accostage de calcul");
    calcul4Button_.setToolTipText("Calculer l'�nergie d'accostage accidentelle");
    calcul1Button_.addActionListener(this);
    calcul2Button_.addActionListener(this);
    calcul3Button_.addActionListener(this);
    calcul4Button_.addActionListener(this);
    calcul1Button_.setFocusable(false);
    calcul2Button_.setFocusable(false);
    calcul3Button_.setFocusable(false);
    calcul4Button_.setFocusable(false);

    // Bouton 'Initialiser
    initButtonAccost_ = AlbeLib.button("Initialiser", "reinitialiser");
    initButtonAccost_.setToolTipText("Effacer les donn�es saisies");
    initButtonAccost_.addActionListener(this);
  }

  /**
   * initialise les composants utilis�s pour l'amarrage.
   */
  public void initComposantsAmarrage() {

    // Zones de texte
    amarrage_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    effortFreq_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    effortCaract_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    effortCalcul_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    effortAcc_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    effortVertFreq_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    effortVertCaract_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    effortVertCalcul_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    effortVertAcc_ = AlbeLib.doubleField(AlbeLib.FORMAT001);
    amarrage_.setHorizontalAlignment(SwingConstants.CENTER);
    effortFreq_.setHorizontalAlignment(SwingConstants.CENTER);
    effortCaract_.setHorizontalAlignment(SwingConstants.CENTER);
    effortCalcul_.setHorizontalAlignment(SwingConstants.CENTER);
    effortAcc_.setHorizontalAlignment(SwingConstants.CENTER);
    effortVertFreq_.setHorizontalAlignment(SwingConstants.CENTER);
    effortVertCaract_.setHorizontalAlignment(SwingConstants.CENTER);
    effortVertCalcul_.setHorizontalAlignment(SwingConstants.CENTER);
    effortVertAcc_.setHorizontalAlignment(SwingConstants.CENTER);
    amarrage_.addKeyListener(this);
    effortFreq_.addKeyListener(this);
    effortCaract_.addKeyListener(this);
    effortCalcul_.addKeyListener(this);
    effortAcc_.addKeyListener(this);
    effortVertFreq_.addKeyListener(this);
    effortVertCaract_.addKeyListener(this);
    effortVertCalcul_.addKeyListener(this);
    effortVertAcc_.addKeyListener(this);
    amarrage_.addFocusListener(this);
    effortFreq_.addFocusListener(this);
    effortCaract_.addFocusListener(this);
    effortCalcul_.addFocusListener(this);
    effortAcc_.addFocusListener(this);
    effortVertFreq_.addFocusListener(this);
    effortVertCaract_.addFocusListener(this);
    effortVertCalcul_.addFocusListener(this);
    effortVertAcc_.addFocusListener(this);
    amarrage_.setColumns(7);
    effortFreq_.setColumns(7);
    effortVertFreq_.setColumns(7);

    // Bouton 'Initialiser
    initButtonAmar_ = AlbeLib.button("Initialiser", "reinitialiser");
    initButtonAmar_.setToolTipText("Effacer les donn�es saisies");
    initButtonAmar_.addActionListener(this);
  }

  /**
   * cr�� et retourne le panneau pour l'amarrage.
   */
  public BuPanel initAmarragePanel() {

    BuPanel amarragePanel = new BuPanel();

    BuPanel tmpAmarragePanelNord = new BuPanel(new FlowLayout());
    BuPanel amarragePanelNord = new BuPanel(new BorderLayout());
    BuPanel amarragePanelCentre = new BuPanel();
    amarragePanelCentre.setLayout(new SpringLayout());
    BuPanel amarragePanelSud = new BuPanel(new FlowLayout());

    // Initialisation des composants
    initComposantsAmarrage();

    // Ajout des composants
    tmpAmarragePanelNord.add(new JLabel("Cote d'amarrage"));
    tmpAmarragePanelNord.add(amarrage_);
    tmpAmarragePanelNord.add(new JLabel("m"));
    amarragePanelCentre.add(new JLabel(""));
    amarragePanelCentre.add(new JLabel(""));
    amarragePanelCentre.add(new JLabel("Effort vertical sur le pieu"));
    amarragePanelCentre.add(new JLabel(""));
    amarragePanelCentre.add(new JLabel("Effort d'amarrage fr�quent"));
    amarragePanelCentre.add(effortFreq_);
    amarragePanelCentre.add(effortVertFreq_);
    amarragePanelCentre.add(new JLabel("kN"));
    amarragePanelCentre.add(new JLabel("Effort d'amarrage caract�ristique"));
    amarragePanelCentre.add(effortCaract_);
    amarragePanelCentre.add(effortVertCaract_);
    amarragePanelCentre.add(new JLabel("kN"));
    amarragePanelCentre.add(new JLabel("Effort d'amarrage de calcul"));
    amarragePanelCentre.add(effortCalcul_);
    amarragePanelCentre.add(effortVertCalcul_);
    amarragePanelCentre.add(new JLabel("kN"));
    amarragePanelCentre.add(new JLabel("Effort d'amarrage accidentel"));
    amarragePanelCentre.add(effortAcc_);
    amarragePanelCentre.add(effortVertAcc_);
    amarragePanelCentre.add(new JLabel("kN"));

    SpringUtilities.makeCompactGrid(amarragePanelCentre, 5, 4, 10, 10, 10, 10);

    amarragePanelSud.add(initButtonAmar_);

    BuPanel tempPanelAmar = AlbeLib.titledPanel("Amarrage", AlbeLib.COMPOUND_BORDER);
    tempPanelAmar.setLayout(new BuVerticalLayout());
    amarragePanelNord.add(tmpAmarragePanelNord, BorderLayout.WEST);
    tempPanelAmar.add(amarragePanelNord);
    tempPanelAmar.add(amarragePanelCentre);
    tempPanelAmar.add(amarragePanelSud);

    amarragePanel.add(tempPanelAmar);

    return amarragePanel;
  }

  /**
   * affiche la boite de dialogue de calcul des �nergies d'accostage.
   * 
   * @param _i num�ro du bouton calcul s�lectionn� (entre 1 et 4)
   */
  public void calculEnergie(int _i) {
    fCalcul_ = new AlbeDialogCalculEnergieAccostage(fp_.appli_, this, _i);
    fCalcul_.setVisible(true);
  }

  /**
   * appel�e lorsque l'utilisateur clique sur 'Accostage'.
   */
  public void accostage() {
    // V�rification qu'aucune donn�e pour l'amarrage n'a �t� saisie
    if (!AlbeLib.emptyField(amarrage_) || !AlbeLib.emptyField(effortAcc_)
        || !AlbeLib.emptyField(effortCalcul_) || !AlbeLib.emptyField(effortCaract_)
        || !AlbeLib.emptyField(effortFreq_) || !AlbeLib.emptyField(effortVertAcc_)
        || !AlbeLib.emptyField(effortVertCalcul_) || !AlbeLib.emptyField(effortVertCaract_)
        || !AlbeLib.emptyField(effortVertFreq_)) {

      AlbeLib.dialogWarning(getApplication(), AlbeMsg.WAR001);

      // On s�lectionne de nouveau 'Amarrage'
      actionCombo_.setSelectedIndex(1);
    } else {
      // Affichage du panneau 'Accostage'
      CardLayout c = (CardLayout) panelCenter_.getLayout();
      c.show(panelCenter_, "panAccost");

      // Activation de l'onglet 'D�fense'
      fp_.tp_.setEnabledAt(4, true);

      // Activation des lignes de r�action dans l'onglet
      // 'Crit�res de dimensionnement'
      fp_.dim_.eluFondaReactionLabel_.setEnabled(true);
      fp_.dim_.reactionEluFond_.setEditable(true);
      fp_.dim_.kNLabel1_.setEnabled(true);
      fp_.dim_.eluAccReactionLabel_.setEnabled(true);
      fp_.dim_.reactionEluAcc_.setEditable(true);
      fp_.dim_.kNLabel2_.setEnabled(true);

      // Activation des lignes de d�flexion (si d�fense) dans l'onglet
      // 'Crit�res de dimensionnement'
      if (fp_.defense_.presenceCombo_.getSelectedIndex() == 0) {
        fp_.dim_.deflexionElsRare_.setEditable(true);
        fp_.dim_.deflexionEluAcc_.setEditable(true);
        fp_.dim_.deflexionEluFonda_.setEditable(true);
        fp_.dim_.perCentLabel1_.setEnabled(true);
        fp_.dim_.perCentLabel2_.setEnabled(true);
        fp_.dim_.perCentLabel3_.setEnabled(true);
        fp_.dim_.elsRareDeflexionLabel_.setEnabled(true);
        fp_.dim_.eluFondaDeflexionLabel_.setEnabled(true);
        fp_.dim_.eluAccDeflexionLabel_.setEnabled(true);
      } else {
        fp_.dim_.deflexionElsRare_.setEditable(false);
        fp_.dim_.deflexionEluAcc_.setEditable(false);
        fp_.dim_.deflexionEluFonda_.setEditable(false);
        fp_.dim_.perCentLabel1_.setEnabled(false);
        fp_.dim_.perCentLabel2_.setEnabled(false);
        fp_.dim_.perCentLabel3_.setEnabled(false);
        fp_.dim_.elsRareDeflexionLabel_.setEnabled(false);
        fp_.dim_.eluFondaDeflexionLabel_.setEnabled(false);
        fp_.dim_.eluAccDeflexionLabel_.setEnabled(false);
      }

      // Mise � jour des valeurs des coefficients partiels (3
      // tableaux)
      AlbeLib.setValuesInTable(fp_.coef_.solTable_, fp_.coef_.donneesSolAccost_);
      AlbeLib.setValuesInTable(fp_.coef_.acierTable_, fp_.coef_.donneesAcierAccost_);
      AlbeLib.setValuesInTable(fp_.coef_.modeleTable_, fp_.coef_.donneesModeleAccost_);
      AlbeLib.setValuesInTable(fp_.coef_.defenseTable_, fp_.coef_.donneesDefense_);

      // On rend visible le tableau sur les coefficients de la d�fense
      fp_.coef_.defensePanel_.setVisible(true);

      // On modifie le renderer des tableaux sur les coefficients partiels
      for (int i = 1; i < fp_.coef_.solTable_.getColumnCount(); i++) {
        fp_.coef_.solTable_.getColumnModel().getColumn(i).setCellRenderer(
            new AlbeTableColorCellRenderer(fp_.coef_.donneesSolAccost_));
      }

      for (int i = 1; i < fp_.coef_.acierTable_.getColumnCount(); i++) {
        fp_.coef_.acierTable_.getColumnModel().getColumn(i).setCellRenderer(
            new AlbeTableColorCellRenderer(fp_.coef_.donneesAcierAccost_));
      }

      for (int i = 1; i < fp_.coef_.modeleTable_.getColumnCount(); i++) {
        fp_.coef_.modeleTable_.getColumnModel().getColumn(i).setCellRenderer(
            new AlbeTableColorCellRenderer(fp_.coef_.donneesModeleAccost_));
      }

    }
  }

  /**
   * appel�e lorsque l'utilisateur clique sur 'Amarrage'.
   */
  public void amarrage() {
    // V�rification qu'aucune donn�e pour l'accostage n'a �t� saisie
    if (!AlbeLib.emptyField(accostageHaut_) || !AlbeLib.emptyField(accostageBas_)
        || !AlbeLib.emptyField(effortVertAccostage_) || !AlbeLib.emptyField(energieAccostAcc_)
        || !AlbeLib.emptyField(energieAccostCalcul_) || !AlbeLib.emptyField(energieAccostCaract_)
        || !AlbeLib.emptyField(energieAccostFreq_)) {

      AlbeLib.dialogWarning(getApplication(), AlbeMsg.WAR002);

      // On s�lectionne de nouveau 'Accostage'
      actionCombo_.setSelectedIndex(0);
    } else {
      // Affichage du panneau 'Amarrage'
      CardLayout c = (CardLayout) panelCenter_.getLayout();
      c.show(panelCenter_, "panAmar");

      // D�sactivation de l'onglet 'D�fense'
      fp_.tp_.setEnabledAt(4, false);

      // D�sactivation des lignes de d�flexion et de r�action dans l'onglet
      // 'Crit�res
      // de dimensionnement'
      fp_.dim_.elsRareDeflexionLabel_.setEnabled(false);
      fp_.dim_.eluFondaDeflexionLabel_.setEnabled(false);
      fp_.dim_.eluAccDeflexionLabel_.setEnabled(false);
      fp_.dim_.deflexionElsRare_.setEditable(false);
      fp_.dim_.deflexionEluAcc_.setEditable(false);
      fp_.dim_.deflexionEluFonda_.setEditable(false);
      fp_.dim_.perCentLabel1_.setEnabled(false);
      fp_.dim_.perCentLabel2_.setEnabled(false);
      fp_.dim_.perCentLabel3_.setEnabled(false);
      fp_.dim_.eluFondaReactionLabel_.setEnabled(false);
      fp_.dim_.reactionEluFond_.setEditable(false);
      fp_.dim_.kNLabel1_.setEnabled(false);
      fp_.dim_.eluAccReactionLabel_.setEnabled(false);
      fp_.dim_.reactionEluAcc_.setEditable(false);
      fp_.dim_.kNLabel2_.setEnabled(false);

      // Mise � jour des valeurs des coefficients partiels (4
      // tableaux)
      AlbeLib.setValuesInTable(fp_.coef_.solTable_, fp_.coef_.donneesSolAmar_);
      AlbeLib.setValuesInTable(fp_.coef_.acierTable_, fp_.coef_.donneesAcierAmar_);
      AlbeLib.setValuesInTable(fp_.coef_.modeleTable_, fp_.coef_.donneesModeleAmar_);

      // On rend invisible le tableau sur les coefficients de la
      // d�fense
      fp_.coef_.defensePanel_.setVisible(false);

      // On modifie le renderer des tableaux sur les coefficients partiels
      for (int i = 1; i < fp_.coef_.solTable_.getColumnCount(); i++) {
        fp_.coef_.solTable_.getColumnModel().getColumn(i).setCellRenderer(
            new AlbeTableColorCellRenderer(fp_.coef_.donneesSolAmar_));
      }

      for (int i = 1; i < fp_.coef_.acierTable_.getColumnCount(); i++) {
        fp_.coef_.acierTable_.getColumnModel().getColumn(i).setCellRenderer(
            new AlbeTableColorCellRenderer(fp_.coef_.donneesAcierAmar_));
      }

      for (int i = 1; i < fp_.coef_.modeleTable_.getColumnCount(); i++) {
        fp_.coef_.modeleTable_.getColumnModel().getColumn(i).setCellRenderer(
            new AlbeTableColorCellRenderer(fp_.coef_.donneesModeleAmar_));
      }
    }
  }

  /**
   * efface les donn�es saisies pour l'accostage.
   */
  public void eraseDataAccost() {
    accostageHaut_.setText("");
    accostageHaut_.requestFocus();
    accostageBas_.setText("");
    accostageBas_.requestFocus();
    effortVertAccostage_.setText("");
    effortVertAccostage_.requestFocus();
    energieAccostFreq_.setText("");
    energieAccostFreq_.requestFocus();
    energieAccostCaract_.setText("");
    energieAccostCaract_.requestFocus();
    energieAccostCalcul_.setText("");
    energieAccostCalcul_.requestFocus();
    energieAccostAcc_.setText("");
    energieAccostAcc_.requestFocus();
    energieAccostAcc_.transferFocus();
  }

  /**
   * efface les donn�es saisies pour l'amarrage.
   */
  public void eraseDataAmar() {
    amarrage_.setText("");
    amarrage_.requestFocus();
    effortFreq_.setText("");
    effortFreq_.requestFocus();
    effortVertFreq_.setText("");
    effortVertFreq_.requestFocus();
    effortCaract_.setText("");
    effortCaract_.requestFocus();
    effortVertCaract_.setText("");
    effortVertCaract_.requestFocus();
    effortCalcul_.setText("");
    effortCalcul_.requestFocus();
    effortVertCalcul_.setText("");
    effortVertCalcul_.requestFocus();
    effortAcc_.setText("");
    effortAcc_.requestFocus();
    effortVertAcc_.setText("");
    effortVertAcc_.requestFocus();
    effortVertAcc_.transferFocus();
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans
   * l'onglet 'Action'.
   */
  public SAction getParametresAction() {
    final SAction p = (SAction) AlbeLib.createIDLObject(SAction.class);

    // Si Accostage
    if (actionCombo_.getSelectedIndex() == 0) {
      p.typeAction = AlbeLib.IDL_ACTION_ACCOSTAGE;
      p.coteAccostageHaut = AlbeLib.textFieldDoubleValue(accostageHaut_);
      p.coteAccostageBas = AlbeLib.textFieldDoubleValue(accostageBas_);
      p.energieAccostageCaracteristique = AlbeLib.textFieldDoubleValue(energieAccostCaract_);
      p.energieAccostageCalcul = AlbeLib.textFieldDoubleValue(energieAccostCalcul_);
      p.energieAccostageFrequente = AlbeLib.textFieldDoubleValue(energieAccostFreq_);
      p.energieAccostageAccidentelle = AlbeLib.textFieldDoubleValue(energieAccostAcc_);
      p.effortVerticalAccostage = AlbeLib.textFieldDoubleValue(effortVertAccostage_);
    }// Si Amarrage
    else if (actionCombo_.getSelectedIndex() == 1) {
      p.typeAction = AlbeLib.IDL_ACTION_AMARRAGE;
      p.coteAmarrage = AlbeLib.textFieldDoubleValue(amarrage_);
      p.effortAmarrageCaracteristique = AlbeLib.textFieldDoubleValue(effortCaract_);
      p.effortAmarrageCalcul = AlbeLib.textFieldDoubleValue(effortCalcul_);
      p.effortAmarrageFrequent = AlbeLib.textFieldDoubleValue(effortFreq_);
      p.effortAmarrageAccidentel = AlbeLib.textFieldDoubleValue(effortAcc_);
      p.effortVerticalAmarrageCaracteristique = AlbeLib.textFieldDoubleValue(effortVertCaract_);
      p.effortVerticalAmarrageCalcul = AlbeLib.textFieldDoubleValue(effortVertCalcul_);
      p.effortVerticalAmarrageFrequent = AlbeLib.textFieldDoubleValue(effortVertFreq_);
      p.effortVerticalAmarrageAccidentel = AlbeLib.textFieldDoubleValue(effortVertAcc_);
    }
    return p;
  }

  /**
   * importe dans l'onglet 'Action' les donn�es contenues dans la structure
   * pass�e en param�tre.
   */
  public synchronized void setParametresAction(SAction _p) {
    if (_p == null) {
      _p = (SAction) AlbeLib.createIDLObject(SAction.class);
    }
    // Si accostage
    if (_p.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)) {
      actionCombo_.setSelectedIndex(0);
      if (_p.coteAccostageHaut != VALEUR_NULLE.value)
        accostageHaut_.setValue(new Double(_p.coteAccostageHaut));

      if (_p.coteAccostageBas != VALEUR_NULLE.value)
        accostageBas_.setValue(new Double(_p.coteAccostageBas));

      if (_p.effortVerticalAccostage != VALEUR_NULLE.value)
        effortVertAccostage_.setValue(new Double(_p.effortVerticalAccostage));

      if (_p.energieAccostageCaracteristique != VALEUR_NULLE.value)
        energieAccostCaract_.setValue(new Double(_p.energieAccostageCaracteristique));

      if (_p.energieAccostageCalcul != VALEUR_NULLE.value)
        energieAccostCalcul_.setValue(new Double(_p.energieAccostageCalcul));

      if (_p.energieAccostageFrequente != VALEUR_NULLE.value)
        energieAccostFreq_.setValue(new Double(_p.energieAccostageFrequente));

      if (_p.energieAccostageAccidentelle != VALEUR_NULLE.value)
        energieAccostAcc_.setValue(new Double(_p.energieAccostageAccidentelle));
    }

    // Si amarrage
    else if (_p.typeAction.equals(AlbeLib.IDL_ACTION_AMARRAGE)) {
      actionCombo_.setSelectedIndex(1);
      if (_p.coteAmarrage != VALEUR_NULLE.value)
        amarrage_.setValue(new Double(_p.coteAmarrage));

      if (_p.effortAmarrageCaracteristique != VALEUR_NULLE.value)
        effortCaract_.setValue(new Double(_p.effortAmarrageCaracteristique));

      if (_p.effortAmarrageCalcul != VALEUR_NULLE.value)
        effortCalcul_.setValue(new Double(_p.effortAmarrageCalcul));

      if (_p.effortAmarrageFrequent != VALEUR_NULLE.value)
        effortFreq_.setValue(new Double(_p.effortAmarrageFrequent));

      if (_p.effortAmarrageAccidentel != VALEUR_NULLE.value)
        effortAcc_.setValue(new Double(_p.effortAmarrageAccidentel));

      if (_p.effortVerticalAmarrageCaracteristique != VALEUR_NULLE.value)
        effortVertCaract_.setValue(new Double(_p.effortVerticalAmarrageCaracteristique));

      if (_p.effortVerticalAmarrageCalcul != VALEUR_NULLE.value)
        effortVertCalcul_.setValue(new Double(_p.effortVerticalAmarrageCalcul));

      if (_p.effortVerticalAmarrageFrequent != VALEUR_NULLE.value)
        effortVertFreq_.setValue(new Double(_p.effortVerticalAmarrageFrequent));

      if (_p.effortVerticalAmarrageAccidentel != VALEUR_NULLE.value)
        effortVertAcc_.setValue(new Double(_p.effortVerticalAmarrageAccidentel));
    }
  }

  /**
   * 
   */

  public boolean paramsActionModified(SAction _p) {
    SAction p2 = getParametresAction();
    if (_p.typeAction != p2.typeAction)
      return true;
    if (_p.typeAction.equals(AlbeLib.IDL_ACTION_ACCOSTAGE)) {
      if (_p.coteAccostageHaut != p2.coteAccostageHaut
          || _p.coteAccostageBas != p2.coteAccostageBas
          || _p.energieAccostageAccidentelle != p2.energieAccostageAccidentelle
          || _p.energieAccostageCalcul != p2.energieAccostageCalcul
          || _p.energieAccostageCaracteristique != p2.energieAccostageCaracteristique
          || _p.energieAccostageFrequente != p2.energieAccostageFrequente)
        return true;
    }

    else if (_p.typeAction.equals(AlbeLib.IDL_ACTION_AMARRAGE)) {
      if (_p.coteAmarrage != p2.coteAmarrage
          || _p.effortAmarrageAccidentel != p2.effortAmarrageAccidentel
          || _p.effortAmarrageCalcul != p2.effortAmarrageCalcul
          || _p.effortAmarrageCaracteristique != p2.effortAmarrageCaracteristique
          || _p.effortAmarrageFrequent != p2.effortAmarrageFrequent)
        return true;
    }

    return false;
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {

    Object src = _evt.getSource();
    // Clic sur la boite combo Accostage/Amarrage
    if (src == actionCombo_) {

      // Si 'Accostage' s�lectionn�
      if (actionCombo_.getSelectedIndex() == 0)
        accostage();
      // Si 'Amarrage' s�lectionn�
      else if (actionCombo_.getSelectedIndex() == 1)
        amarrage();
    } else if (src == aideButton_)
      aide();

    // Clic sur les boutons 'Calculatrice' (accostage)
    else if (src == calcul1Button_)
      calculEnergie(1);
    else if (src == calcul2Button_)
      calculEnergie(2);
    else if (src == calcul3Button_)
      calculEnergie(3);
    else if (src == calcul4Button_)
      calculEnergie(4);

    // Clic sur le bouton 'Initialiser' pour l'accostage
    else if (src == initButtonAccost_)
      eraseDataAccost();

    // Clic sur le bouton 'Initialiser' pour l'amarrage
    else if (src == initButtonAmar_)
      eraseDataAmar();
  }

  /**
   * valide les donn�es pour un accostage.
   */

  public ValidationMessages validationAccostage() {
    final ValidationMessages vm = new ValidationMessages();
    boolean acH = false;
    boolean acB = false;

    // Accostage haut
    if (AlbeLib.emptyField(accostageHaut_))
      vm.add(AlbeMsg.VMSG020);
    else if (AlbeLib.textFieldDoubleValue(accostageHaut_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG021);
    else
      acH = true;

    // Accostage bas
    if (AlbeLib.emptyField(accostageBas_))
      vm.add(AlbeMsg.VMSG022);
    else if (AlbeLib.textFieldDoubleValue(accostageBas_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG023);
    else
      acB = true;

    // Energie fr�quente
    if (AlbeLib.emptyField(energieAccostFreq_))
      vm.add(AlbeMsg.VMSG024);
    else if (AlbeLib.textFieldDoubleValue(energieAccostFreq_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG025);
    else if (AlbeLib.textFieldDoubleValue(energieAccostFreq_) < 0)
      vm.add(AlbeMsg.VMSG042);

    // Energie caract�ristique
    if (AlbeLib.emptyField(energieAccostCaract_))
      vm.add(AlbeMsg.VMSG026);
    else if (AlbeLib.textFieldDoubleValue(energieAccostCaract_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG027);
    else if (AlbeLib.textFieldDoubleValue(energieAccostCaract_) < 0)
      vm.add(AlbeMsg.VMSG043);

    // Energie de calcul
    if (AlbeLib.emptyField(energieAccostCalcul_))
      vm.add(AlbeMsg.VMSG028);
    else if (AlbeLib.textFieldDoubleValue(energieAccostCalcul_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG029);
    else if (AlbeLib.textFieldDoubleValue(energieAccostCalcul_) < 0)
      vm.add(AlbeMsg.VMSG044);

    // Energie accidentelle
    if (AlbeLib.emptyField(energieAccostAcc_))
      vm.add(AlbeMsg.VMSG030);
    else if (AlbeLib.textFieldDoubleValue(energieAccostAcc_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG031);
    else if (AlbeLib.textFieldDoubleValue(energieAccostAcc_) < 0)
      vm.add(AlbeMsg.VMSG045);

    // Effort vertical en t�te de pieu
    if (AlbeLib.emptyField(effortVertAccostage_))
      vm.add(AlbeMsg.VMSG063);
    else if (AlbeLib.textFieldDoubleValue(effortVertAccostage_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG064);

    if (acH
        && acB
        && AlbeLib.textFieldDoubleValue(accostageBas_) >= AlbeLib
            .textFieldDoubleValue(accostageHaut_))
      vm.add(AlbeMsg.VMSG050);

    if (acH
        && acB
        && AlbeLib.textFieldDoubleValue(fp_.geo_.coteTete_) != VALEUR_NULLE.value
        && Math.max(AlbeLib.textFieldDoubleValue(accostageHaut_), AlbeLib
            .textFieldDoubleValue(accostageBas_)) > AlbeLib
            .textFieldDoubleValue(fp_.geo_.coteTete_))
      vm.add(AlbeMsg.VMSG051);

    if (acH
        && acB
        && AlbeLib.textFieldDoubleValue(fp_.geo_.coteDragage_) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(fp_.geo_.coteCalcul_) != VALEUR_NULLE.value
        && Math.min(AlbeLib.textFieldDoubleValue(accostageHaut_), AlbeLib
            .textFieldDoubleValue(accostageBas_)) < Math.max(AlbeLib
            .textFieldDoubleValue(fp_.geo_.coteDragage_), AlbeLib
            .textFieldDoubleValue(fp_.geo_.coteCalcul_)))
      vm.add(AlbeMsg.VMSG054);

    return vm;
  }

  /**
   * valide les donn�es pour un amarrage.
   */

  public ValidationMessages validationAmarrage() {
    final ValidationMessages vm = new ValidationMessages();

    boolean am = false;

    // Cote d'amarrage
    if (AlbeLib.emptyField(amarrage_))
      vm.add(AlbeMsg.VMSG032);
    else if (AlbeLib.textFieldDoubleValue(amarrage_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG033);
    else
      am = true;

    // Amarrage fr�quent
    if (AlbeLib.emptyField(effortFreq_))
      vm.add(AlbeMsg.VMSG034);
    else if (AlbeLib.textFieldDoubleValue(effortFreq_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG035);
    else if (AlbeLib.textFieldDoubleValue(effortFreq_) < 0)
      vm.add(AlbeMsg.VMSG046);

    if (AlbeLib.emptyField(effortVertFreq_))
      vm.add(AlbeMsg.VMSG055);
    else if (AlbeLib.textFieldDoubleValue(effortVertFreq_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG056);

    // Amarrage caract�ristique
    if (AlbeLib.emptyField(effortCaract_))
      vm.add(AlbeMsg.VMSG036);
    else if (AlbeLib.textFieldDoubleValue(effortCaract_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG037);
    else if (AlbeLib.textFieldDoubleValue(effortCaract_) < 0)
      vm.add(AlbeMsg.VMSG047);

    if (AlbeLib.emptyField(effortVertCaract_))
      vm.add(AlbeMsg.VMSG057);
    else if (AlbeLib.textFieldDoubleValue(effortVertCaract_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG058);

    // Amarrage de calcul
    if (AlbeLib.emptyField(effortCalcul_))
      vm.add(AlbeMsg.VMSG038);
    else if (AlbeLib.textFieldDoubleValue(effortCalcul_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG039);
    else if (AlbeLib.textFieldDoubleValue(effortCalcul_) < 0)
      vm.add(AlbeMsg.VMSG048);

    if (AlbeLib.emptyField(effortVertCalcul_))
      vm.add(AlbeMsg.VMSG059);
    else if (AlbeLib.textFieldDoubleValue(effortVertCalcul_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG060);

    // Amarrage accidentel
    if (AlbeLib.emptyField(effortAcc_))
      vm.add(AlbeMsg.VMSG040);
    else if (AlbeLib.textFieldDoubleValue(effortAcc_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG041);
    else if (AlbeLib.textFieldDoubleValue(effortAcc_) < 0)
      vm.add(AlbeMsg.VMSG049);

    if (AlbeLib.emptyField(effortVertAcc_))
      vm.add(AlbeMsg.VMSG061);
    else if (AlbeLib.textFieldDoubleValue(effortVertAcc_) == VALEUR_NULLE.value)
      vm.add(AlbeMsg.VMSG062);

    if (am
        && AlbeLib.textFieldDoubleValue(fp_.geo_.coteTete_) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(amarrage_) > AlbeLib
            .textFieldDoubleValue(fp_.geo_.coteTete_))
      vm.add(AlbeMsg.VMSG052);

    if (am
        && AlbeLib.textFieldDoubleValue(fp_.geo_.coteDragage_) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(fp_.geo_.coteCalcul_) != VALEUR_NULLE.value
        && AlbeLib.textFieldDoubleValue(amarrage_) < Math.max(AlbeLib
            .textFieldDoubleValue(fp_.geo_.coteDragage_), AlbeLib
            .textFieldDoubleValue(fp_.geo_.coteCalcul_)))
      vm.add(AlbeMsg.VMSG053);

    return vm;
  }

  /**
   * 
   */
  public ValidationMessages validation() {

    ValidationMessages vm = new ValidationMessages();

    // Cas o� la structure est soumise � un accostage
    if (actionCombo_.getSelectedIndex() == 0)
      vm = validationAccostage();

    // Cas o� la structure est soumise � un amarrage
    else if (actionCombo_.getSelectedIndex() == 1)
      vm = validationAmarrage();

    return vm;
  }
}
