package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;

/**
 * Description de l'onglet 'Validation'.
 * 
 * @author Sabrina Delattre
 */

public class AlbeValidationParametres extends AlbeAbstractOnglet {

  BuButton validationButton_, fermerButton_, aideButton_;

  /**
   * Constructeur.
   * 
   * @param _fp
   */
  public AlbeValidationParametres(final AlbeFilleParametres _fp) {

    super(_fp.getImplementation(), _fp, AlbeMsg.URL008);

    this.setLayout(new BuVerticalLayout());

    /**
     * Panneau nord contenant le message 'Informations'.
     */

    BuPanel panelNord = new BuPanel();
    panelNord.setLayout(new BorderLayout());

    BuPanel infoPanel = AlbeLib.titledPanel("Information", AlbeLib.ETCHED_BORDER);
    infoPanel.setLayout(new BuVerticalLayout());
    infoPanel
        .add(new BuLabel(
            "<html><font color = \"red\">Pour lancer les calculs, vous devez valider la pr�sente fen�tre d'entr�e des donn�es.</font></html>"));

    aideButton_ = AlbeLib.aideButton();
    aideButton_.addActionListener(this);

    panelNord.add(infoPanel, BorderLayout.CENTER);
    panelNord.add(aideButton_, BorderLayout.EAST);

    /**
     * Panneau sud.
     */
    BuPanel panelSud = AlbeLib.titledPanel("Validation", AlbeLib.ETCHED_BORDER);
    panelSud.setLayout(new BuVerticalLayout());

    /* Panneau contenant le message 'Validation' */

    BuPanel validPanel = new BuPanel();
    validPanel.setLayout(new BuVerticalLayout());

    BuLabelMultiLine validationLabel = AlbeLib
        .labelMultiLine("Pour valider les donn�es que vous avez d�j� saisies, cliquez sur le bouton \"Valider\"."
            + " Si aucune erreur n'est d�tect�e, les calculs seront automatiquement lanc�s et vous pourrez alors visualiser les r�sultats en allant dans le menu \"R�sultats\". "
            + "Si des erreurs sont d�tect�es, une liste vous en sera donn�e et vous devrez les corriger avant de lancer � nouveau les calculs. "
            + "Pour sauvegarder ces donn�es uniquement sans les valider et fermer le fichier projet, cliquez sur \"Fermer\".");
    validPanel.add(validationLabel);

    /* Panneau contenant les deux boutons */

    JPanel buttonPanel = new JPanel();;
    buttonPanel.setLayout(new FlowLayout());
    validationButton_ = AlbeLib.buttonId("Valider", "VALIDER");
    validationButton_.setToolTipText("Valider les donn�es saisies");
    fermerButton_ = AlbeLib.buttonId("Fermer", "FERMER");
    fermerButton_.setToolTipText("Fermer le projet");

    validationButton_.addActionListener(this);
    fermerButton_.addActionListener(this);

    buttonPanel.add(validationButton_);
    buttonPanel.add(fermerButton_);

    /** Ajout des deux pr�c�dents panneaux dans le panneau sud. */

    panelSud.add(validPanel);
    panelSud.add(buttonPanel);

    this.add(panelNord);
    this.add(panelSud);

  }

  /**
   * 
   */

  public void changeOnglet(final ChangeEvent _evt) {}

  /**
   * 
   */
  private void fermer() {
    getImplementation().actionPerformed(new ActionEvent(this, 1, "FERMER"));
  }

  /**
   * 
   */
  private void valider() {
    ((AlbeFilleParametres) getParentComponent()).valider();
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {
    Object src = _evt.getSource();
    if (src == fermerButton_)
      fermer();
    else if (src == validationButton_)
      valider();
    else if (src == aideButton_)
      aide();
  }

}
