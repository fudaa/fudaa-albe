package org.fudaa.fudaa.albe;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import org.fudaa.dodico.corba.albe.SDefense;
import org.fudaa.dodico.corba.albe.SPointDefense;
import org.fudaa.dodico.corba.albe.VALEUR_NULLE;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

/**
 * Description de l'onglet 'Caract�ristiques pieu'.
 * 
 * @author Sabrina Delattre
 */

public class AlbeDefenseParametres extends AlbeAbstractOnglet {

  JComboBox presenceCombo_, nbPointsCombo_;

  BuPanel panel_;

  BuTextField hauteur_;

  BuTextField[] deform_;

  BuTextField[] effort_;

  BuTextField[] energie_;

  BuButton courbeButton_, aideButton_;

  JLabel nbPointsLabel_;

  double deformMax_;

  int nbPoints_;

  // Utilisation du gestionnaire SpringLayout pour placer les composants
  SpringLayout layout_ = new SpringLayout();

  /**
   * bool�en indiquant si la valeur de la d�formation maximale peut �tre
   * r�cup�r�e (ie si les champs 'deform' sont tous saisis, valides, croissants
   * et compris entre 0 et 100).
   */
  boolean deformMaxRecuperable_;

  /**
   * r�f�rence vers la fen�tre parente.
   */

  AlbeFilleParametres fp_;

  /**
   * Constructeur.
   * 
   * @param _fp
   */

  public AlbeDefenseParametres(final AlbeFilleParametres _fp) {

    super(_fp.getImplementation(), _fp, AlbeMsg.URL005);

    fp_ = _fp;
    deformMaxRecuperable_ = false;
    nbPoints_ = 2;

    this.setLayout(new BuVerticalLayout());
    this.add(createPanelNord());

    // Panneau vide si pas de d�fense
    BuPanel emptyPanel = new BuPanel();

    // Panneau contenant les �l�ments n�cessaires si il y a une d�fense
    BuPanel defensePanel = new BuPanel();
    defensePanel.setLayout(new BuVerticalLayout());
    defensePanel.add(createComposantsPanel());

    // Panneau contenant la boite combo 'Nombre de points'
    BuPanel p1 = new BuPanel();
    p1.setLayout(new FlowLayout());

    // Initialisation de la boite combo
    nbPointsLabel_ = new JLabel("Nombre de points (de 2 � "
        + AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION + ")");
    nbPointsCombo_ = new JComboBox();
    for (int i = 1; i < AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION; i++) {
      nbPointsCombo_.addItem(Integer.toString(i + 1));
    }
    nbPointsCombo_.addActionListener(this);
    p1.add(nbPointsLabel_);
    p1.add(nbPointsCombo_);
    BuPanel tempPanel = new BuPanel(new BorderLayout());
    tempPanel.add(p1, BorderLayout.WEST);

    // Panneau contenant la remarque
    BuPanel p2 = AlbeLib.titledPanel("Remarque", AlbeLib.ETCHED_BORDER);
    p2.setLayout(new BuVerticalLayout());
    JLabel lab = new JLabel(
        "Il n'est pas n�cessaire d'introduire le point de d�flexion et r�action nulles.");
    p2.add(lab);

    // Panneau concernant les points de description de la courbe
    BuPanel p = AlbeLib.titledPanel("Points de description de la courbe", AlbeLib.ETCHED_BORDER);
    p.setLayout(new BuVerticalLayout());
    p.add(tempPanel);
    p.add(p2);
    p.add(createTextFieldPanel());

    defensePanel.add(p);

    CardLayout c = new CardLayout();
    panel_ = new BuPanel(c);
    panel_.add("pandefense", defensePanel);
    panel_.add("pannodefense", emptyPanel);
    this.add(panel_);

  }// Fin constructeur

  /**
   * cr�e et retourne le panneau nord comportant la boite combo 'Pr�sence' et le
   * bouton 'aide'.
   */

  public BuPanel createPanelNord() {

    BuPanel p1 = new BuPanel();
    p1.setLayout(new BorderLayout());

    BuPanel p2 = new BuPanel();
    p2.setLayout(new FlowLayout());

    String[] ouiNon = { "Oui", "Non" };
    presenceCombo_ = new JComboBox(ouiNon);
    presenceCombo_.addActionListener(this);

    JLabel presenceLabel = new JLabel("Pr�sence");
    p2.add(presenceLabel);
    p2.add(presenceCombo_);

    aideButton_ = AlbeLib.aideButton();
    aideButton_.addActionListener(this);

    p1.add(p2, BorderLayout.CENTER);
    p1.add(aideButton_, BorderLayout.EAST);

    return p1;

  }

  /**
   * cr�e et retourne le panneau comportant la zone de texte 'hauteur' et le
   * bouton 'courbe'.
   */

  public BuPanel createComposantsPanel() {

    BuPanel p = new BuPanel(new BuVerticalLayout());

    // Panneau 1 : zone de texte 'Hauteur'
    BuPanel p1 = new BuPanel();
    p1.setLayout(new FlowLayout());

    JLabel hauteurLabel = new JLabel("Hauteur (mm)");
    hauteur_ = AlbeLib.doubleField(AlbeLib.FORMAT002);
    hauteur_.setColumns(5);
    hauteur_.setHorizontalAlignment(SwingConstants.CENTER);
    hauteur_.addKeyListener(this);
    hauteur_.addFocusListener(this);
    courbeButton_ = AlbeLib.buttonId("Courbes def-R def-W", "graphe");
    courbeButton_
        .setToolTipText("Visualiser les courbes d�flexion-r�action et d�flexion-absorption d'�nergie de la d�fense");
    courbeButton_.addActionListener(this);

    p1.add(hauteurLabel);
    p1.add(hauteur_);

    // Panneau 2 : bouton 'Courbe'
    BuPanel p2 = new BuPanel();
    p2.setLayout(layout_);
    p2.add(courbeButton_);

    layout_.putConstraint(SpringLayout.NORTH, courbeButton_, 10, SpringLayout.NORTH, p2);
    layout_.putConstraint(SpringLayout.WEST, courbeButton_, 550, SpringLayout.WEST, p2);
    layout_.putConstraint(SpringLayout.EAST, p2, 40, SpringLayout.EAST, courbeButton_);
    layout_.putConstraint(SpringLayout.SOUTH, p2, 10, SpringLayout.SOUTH, courbeButton_);

    p.add(p1);
    p.add(p2);

    return p;
  }

  /**
   * cr�e et retourne le panneau contenant tous les champs texte pour la saisie
   * des points.
   */

  public BuPanel createTextFieldPanel() {

    BuPanel p = new BuPanel();
    p.setLayout(layout_);

    deform_ = new BuTextField[AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION];
    effort_ = new BuTextField[AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION];
    energie_ = new BuTextField[AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION];

    for (int i = 0; i < AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION; i++) {
      deform_[i] = AlbeLib.doubleField(AlbeLib.FORMAT001);
      effort_[i] = AlbeLib.doubleField(AlbeLib.FORMAT001);
      energie_[i] = new BuTextField();
      deform_[i].setHorizontalAlignment(SwingConstants.CENTER);
      effort_[i].setHorizontalAlignment(SwingConstants.CENTER);
      energie_[i].setHorizontalAlignment(SwingConstants.CENTER);
      energie_[i].setForeground(Color.red);
      deform_[i].addKeyListener(this);
      effort_[i].addKeyListener(this);
      deform_[i].addFocusListener(this);
      effort_[i].addFocusListener(this);
      energie_[i].setFocusable(false);
    }

    /**
     * Au d�part, 2 est s�lectionn� dans la JComboBox donc les champs 2 � 7 sont
     * non �ditables.
     */
    for (int i = 2; i < AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION; i++) {
      deform_[i].setEditable(false);
      effort_[i].setEditable(false);
      energie_[i].setEditable(false);
    }
    /** Les champs �nergies ne sont pas �ditables. */
    for (int i = 0; i < AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION; i++) {
      energie_[i].setEditable(false);
    }

    // Ajout des labels et des champs texte
    p.add(new JLabel(""));
    p.add(new JLabel("   DEFORM. (en %)   "));
    p.add(new JLabel("     EFFORT (en kN)     "));
    p.add(new JLabel("     ENERGIE (en kJ)     "));
    for (int i = 1; i < AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION + 1; i++) {
      p.add(new JLabel("Point n�" + i));
      p.add(deform_[i - 1]);
      p.add(effort_[i - 1]);
      p.add(energie_[i - 1]);
    }

    SpringUtilities.makeCompactGrid(p, AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION + 1, 4, 40, 20, 60,
        13);

    BuPanel tempPanel = new BuPanel(new BorderLayout());
    tempPanel.add(p, BorderLayout.WEST);

    return tempPanel;
  }

  /**
   * retourne vrai si tous les champs �ditables sont remplis et faux sinon.
   */
  public boolean fullField(BuTextField[] _tf) {

    for (int i = 0; i < nbPoints_; i++) {
      if (AlbeLib.emptyField(_tf[i]))
        return false;
    }
    return true;
  }

  /**
   * retourne vrai si tous les champs �ditables sont valides et faux sinon.
   */
  public boolean validField(BuTextField[] _tf) {

    for (int i = 0; i < nbPoints_; i++) {
      if (AlbeLib.textFieldConversionDouble(_tf[i]) == null)
        return false;
    }
    return true;
  }

  /**
   * retourne vrai si les valeurs des d�formations sont comprises entre 0 et
   * 100.
   */

  public boolean deformationsValides() {
    for (int i = 0; i < nbPoints_; i++) {
      if (AlbeLib.textFieldDoubleValue(deform_[i]) < 0
          || AlbeLib.textFieldDoubleValue(deform_[i]) > 100)
        return false;
    }
    return true;
  }

  /**
   * retourne vrai si les valeurs des efforts sont positifs ou nuls.
   */

  public boolean effortsPositifs() {
    for (int i = 0; i < nbPoints_; i++) {
      if (AlbeLib.textFieldDoubleValue(effort_[i]) < 0)
        return false;
    }
    return true;
  }

  /**
   * retourne vrai si les valeurs rentr�es dans les champs �ditables sont
   * croissantes.
   */

  public boolean champsCroissants(BuTextField[] _tf) {

    for (int i = 0; i < nbPoints_ - 1; i++) {
      if (AlbeLib.textFieldDoubleValue(_tf[i]) >= AlbeLib.textFieldDoubleValue(_tf[i + 1]))
        return false;
    }
    return true;
  }

  /**
   * rend �ditables les _index premiers champs 'deform', 'effort' et 'enrgie' et
   * non �ditables les suivants.
   * 
   * @param _index index de l'item s�lectionn� dans la boite combo 'Nombre de
   *          points'
   */
  public void textFieldSetEditable(int _index) {

    for (int i = 0; i < _index + 2; i++) {
      deform_[i].setEditable(true);
      effort_[i].setEditable(true);
    }

    for (int i = _index + 2; i < AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION; i++) {
      deform_[i].setEditable(false);
      effort_[i].setEditable(false);
    }
  }

  /**
   * retourne la valeur de la deformation maximale (valeur du dernier champ
   * �ditable).
   */

  public double getDeformMax() {

    return AlbeLib.textFieldDoubleValue(deform_[nbPoints_ - 1]);
  }

  /**
   * retourne un tableau de double contenant les ordonn�es de la courbe
   * d�flexion-absorption d'�nergie.
   */

  public double[] getEnergies() {
    int nbPts = getNbPoints();
    double[] energies = new double[nbPts];

    // R�cup�ration de la valeur de la hauteur (saisie en mm) et conversion en
    // m�tres
    double hauteur = AlbeLib.textFieldDoubleValue(hauteur_) / 1000;

    energies[0] = (AlbeLib.textFieldDoubleValue(deform_[0]) / 100) * hauteur
        * (AlbeLib.textFieldDoubleValue(effort_[0]) / 2);

    for (int i = 1; i < nbPts; i++) {
      double num = ((AlbeLib.textFieldDoubleValue(deform_[i]) - AlbeLib
          .textFieldDoubleValue(deform_[i - 1])) / 100)
          * hauteur
          * (AlbeLib.textFieldDoubleValue(effort_[i]) + AlbeLib
              .textFieldDoubleValue(effort_[i - 1]));
      energies[i] = num / 2 + energies[i - 1];
    }
    return energies;
  }

  /**
   * appel�e lors de la s�lection de la pr�sence de d�fense dans la boite de
   * liste.
   */
  public void choixPresenceDefense() {
    /** Si pr�sence de d�fense. */
    if (presenceCombo_.getSelectedIndex() == 0) {

      // Affichage du panneau
      CardLayout c = (CardLayout) panel_.getLayout();
      c.show(panel_, "pandefense");

      // Activation des lignes de d�flexion dans l'onglet 'Crit�res de
      // dimensionnement'
      fp_.dim_.elsRareDeflexionLabel_.setEnabled(true);
      fp_.dim_.eluFondaDeflexionLabel_.setEnabled(true);
      fp_.dim_.eluAccDeflexionLabel_.setEnabled(true);
      fp_.dim_.deflexionElsRare_.setEditable(true);
      fp_.dim_.deflexionEluAcc_.setEditable(true);
      fp_.dim_.deflexionEluFonda_.setEditable(true);
      fp_.dim_.perCentLabel1_.setEnabled(true);
      fp_.dim_.perCentLabel2_.setEnabled(true);
      fp_.dim_.perCentLabel3_.setEnabled(true);

      // On rend visible le tableau sur les coefficients de la d�fense
      fp_.coef_.defensePanel_.setVisible(true);

      majMessages();

    }

    /** Si absence de d�fense. */
    else if (presenceCombo_.getSelectedIndex() == 1) {

      // Affichage du panneau vide
      CardLayout c = (CardLayout) panel_.getLayout();
      c.show(panel_, "pannodefense");

      // D�sactivation des lignes de d�flexion dans l'onglet 'Crit�res de
      // dimensionnement'.
      fp_.dim_.elsRareDeflexionLabel_.setEnabled(false);
      fp_.dim_.eluFondaDeflexionLabel_.setEnabled(false);
      fp_.dim_.eluAccDeflexionLabel_.setEnabled(false);
      fp_.dim_.deflexionElsRare_.setEditable(false);
      fp_.dim_.deflexionEluAcc_.setEditable(false);
      fp_.dim_.deflexionEluFonda_.setEditable(false);
      fp_.dim_.perCentLabel1_.setEnabled(false);
      fp_.dim_.perCentLabel2_.setEnabled(false);
      fp_.dim_.perCentLabel3_.setEnabled(false);

      // On rend invisible le tableau sur les coefficients de la d�fense
      fp_.coef_.defensePanel_.setVisible(false);

      try {
        final AlbeParamEventView mv = getImplementation().getParamEventView();
        mv.removeAllMessagesInCategory(AlbeMsg.CAT005);
      }
      catch (final NullPointerException _e) {}
    }
  }

  /**
   * affichage des valeurs des �nergies dans les champs texte.
   */
  public void afficheEnergies(double[] _energie) {

    for (int i = 0; i < nbPoints_; i++) {
      energie_[i].setText("" + AlbeLib.arronditDouble(_energie[i]));
    }
    for (int i = nbPoints_; i < AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION; i++) {
      energie_[i].setText("");
    }
  }

  /**
   * appel�e lors d'un clic sur le bouton 'Courbe'.
   */

  public void clicBoutonCourbe() {

    // On v�rifie que le champ texte 'Hauteur' est rempli
    if (AlbeLib.emptyField(hauteur_))
      AlbeLib.dialogError(getApplication(), AlbeMsg.ERR001);

    // On v�rifie que la valeur de la hauteur est valide
    else if (AlbeLib.textFieldConversionDouble(hauteur_) == null)
      AlbeLib.dialogError(getApplication(), AlbeMsg.ERR002);

    // On v�rifie que la valeur de la hauteur est strictement positive
    else if (AlbeLib.textFieldDoubleValue(hauteur_) <= 0)
      AlbeLib.dialogError(getApplication(), AlbeMsg.ERR015);

    // On v�rifie que les champs 'd�formation' et 'effort' sont tous
    // renseign�s
    else if (!fullField(deform_) || !fullField(effort_))
      AlbeLib.dialogError(getApplication(), AlbeMsg.ERR003);

    // On v�rifie que les champs 'd�formation' et 'effort' contiennent des
    // nombres
    else if (!validField(deform_) || !validField(effort_))
      AlbeLib.dialogError(getApplication(), AlbeMsg.ERR004);

    // On v�rifie que les valeurs des d�formations sont comprises entre 0 et
    // 100
    else if (!deformationsValides())
      AlbeLib.dialogError(getApplication(), AlbeMsg.ERR007);

    // On v�rifie que les valeurs des efforts sont positifs ou nuls
    else if (!effortsPositifs())
      AlbeLib.dialogError(getApplication(), AlbeMsg.ERR008);

    // On v�rifie que les valeurs des d�formations sont rentr�es dans l'ordre
    // strictement croissant
    else if (!champsCroissants(deform_))
      AlbeLib.dialogError(getApplication(), AlbeMsg.ERR005);

    // On v�rifie que les valeurs des efforts sont rentr�es dans l'ordre
    // strictement croissant.
    else if (!champsCroissants(effort_))
      AlbeLib.dialogError(getApplication(), AlbeMsg.ERR006);

    else {
      // Calcul et affichage des �nergies
      double[] energies = getEnergies();
      afficheEnergies(energies);

      /**
       * affichage des graphiques.
       */
      AlbeLib.showGraphViewer(getApplication(), getImplementation(),
          "Repr�sentation graphique de la courbe d�flexion-r�action", getScriptReaction());
      AlbeLib.showGraphViewer(getApplication(), getImplementation(),
          "Repr�sentation graphique de la courbe d�flexion-absorption de l'�nergie de la d�fense",
          getScriptEnergie(energies));
    }
  }

  /**
   * essaye de calculer les valeurs des �nergies (si les donn�es ne comportent
   * pas d'erreurs, on affiche les �nergies dans les champs texte, sinon on
   * "vide" ces champs).
   */

  public void tryCalculEnergie() {
    try {
      if (AlbeLib.textFieldDoubleValue(hauteur_) > 0 && fullField(deform_) && fullField(effort_)
          && deformationsValides() && effortsPositifs() && champsCroissants(deform_)
          && champsCroissants(effort_))
        // Calcul et affichage des �nergies
        afficheEnergies(getEnergies());
      else {
        for (int i = 0; i < nbPoints_; i++) {
          energie_[i].setText("");
        }
      }
    }
    catch (final NullPointerException _e) {
      for (int i = 0; i < nbPoints_; i++) {
        energie_[i].setText("");
      }
    }
  }

  /**
   * 
   */
  public String getScriptCommun() {
    final StringBuffer s = new StringBuffer(4096);
    s.append("  legende oui\n");
    s.append("  animation non\n");
    s.append("  marges\n{\n");
    s.append("    gauche 80\n");
    s.append("    droite 120\n");
    s.append("    haut 50\n");
    s.append("    bas 30\n");
    s.append("  }\n");
    s.append("  \n");
    s.append("  axe\n{\n");
    s.append("    titre \"d�formation\"\n");
    s.append("    unite \"%\"\n");
    s.append("    orientation horizontal\n");
    s.append("    graduations oui\n");
    s.append("    minimum 0\n");
    s.append("    maximum 100 \n");
    s.append("    pas 10\n");
    s.append("  }\n");
    s.append("  axe\n{\n");
    return s.toString();
  }

  /**
   * 
   */
  public String getScriptReaction() {
    final StringBuffer s = new StringBuffer(4096);
    s.append("graphe \n{\n");
    s.append("  titre \"D�fenses d'accostage\"\n");
    s.append("  sous-titre \"Courbe d�flexion-r�action\"\n");
    s.append(getScriptCommun());
    s.append("    titre \"effort\"\n");
    s.append("    unite \"kN\"\n");
    s.append("    orientation vertical\n");
    s.append("    graduations oui\n");
    s.append("    minimum 0\n");
    s.append("    maximum " + getMaxR() + "\n");
    s.append("    pas " + getPasR() + "\n");
    s.append("  }\n");
    s.append("  courbe\n{\n");
    s.append("    aspect\n{\n");
    s.append("    contour.couleur 009999\n");
    s.append("    }\n");
    s.append("    valeurs\n{\n");
    s.append("      " + 0 + " " + 0 + "\n");
    for (int i = 0; i < AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION; i++) {
      if (deform_[i].isEditable()) {
        s.append("      " + AlbeLib.textFieldDoubleValue(deform_[i]) + " "
            + AlbeLib.textFieldDoubleValue(effort_[i]) + "\n");
      } else
        break;
    }
    s.append("    }\n");
    s.append("  }\n");
    return s.toString();

  }

  /**
   * 
   */
  public String getScriptEnergie(double[] _energies) {
    final StringBuffer s = new StringBuffer(4096);
    s.append("graphe \n{\n");
    s.append("  titre \"D�fenses d'accostage\"\n");
    s.append("  sous-titre \"Courbe d�flexion-absorption d'�nergie\"\n");
    s.append(getScriptCommun());
    s.append("    titre \"�nergie\"\n");
    s.append("    unite \"kJ\"\n");
    s.append("    orientation vertical\n");
    s.append("    graduations oui\n");
    s.append("    minimum 0\n");
    s.append("    maximum  " + getMaxA() + "\n");
    s.append("    pas " + getPasA() + "\n");
    s.append("  }\n");
    s.append("  courbe\n{\n");
    s.append("    type nuage\n");
    s.append("    valeurs\n{\n");
    s.append("      " + 0 + " " + 0 + "\n");
    for (int i = 0; i < AlbeLib.NBMAX_POINTS_COURBE_DEF_REACTION; i++) {
      if (deform_[i].isEditable()) {
        s.append("      " + AlbeLib.textFieldDoubleValue(deform_[i]) + " " + _energies[i] + "\n");
      } else
        break;
    }
    s.append("  }\n");
    s.append("  }\n");
    return s.toString();
  }

  /**
   * retourne le pas de l'axe des efforts de la courbe def-reaction.
   */
  public double getPasR() {

    return AlbeLib.getPas(AlbeLib.textFieldDoubleValue(effort_[getNbPoints() - 1]), 0);
  }

  /**
   * retourne l'ordonn�e maximale � afficher sur l'axe des efforts de la courbe
   * def-r�action.
   */
  public double getMaxR() {

    return 10 * getPasR();
  }

  /**
   * retourne le pas de l'axe des efforts de la courbe def-absorption.
   */
  public double getPasA() {

    return AlbeLib.getPas(getEnergies()[getNbPoints() - 1], 0);
  }

  /**
   * retourne l'ordonn�e maximale � afficher sur l'axe vertical de la courbe
   * def-absorption.
   */
  public double getMaxA() {
    return 10 * getPasA();
  }

  /**
   * retourne le nombre de points des courbes.
   */
  public int getNbPoints() {
    return nbPointsCombo_.getSelectedIndex() + 2;
  }

  /**
   * retourne une structure de donn�es contenant les informations du point _n.
   * retourne null si le point n'existe pas.
   * 
   * @param _n num�ro du point � retourner (commence � 2)
   */
  public SPointDefense getPoint(final int _n) {
    SPointDefense p = null;
    try {
      p = (SPointDefense) AlbeLib.createIDLObject(SPointDefense.class);
      p.deformation = AlbeLib.textFieldDoubleValue(deform_[_n - 2]);
      p.effort = AlbeLib.textFieldDoubleValue(effort_[_n - 2]);
    }
    catch (final ArrayIndexOutOfBoundsException _e1) {
      System.err.println(AlbeMsg.CONS006);
    }
    return p;
  }

  /**
   * retourne une structure de donn�es contenant les informations de tous les
   * points.
   */

  public SPointDefense[] getPoints() {

    SPointDefense[] p = null;

    final int nb = getNbPoints();
    try {
      p = new SPointDefense[nb];
    }
    catch (final NegativeArraySizeException _e2) {
      System.err.println(AlbeMsg.CONS007);

    }

    for (int i = 0; i < nb; i++) {
      try {
        p[i] = getPoint(i + 2);
      }
      catch (final NullPointerException _e1) {
        System.err.println(AlbeMsg.CONS008);
      }
      catch (final ArrayIndexOutOfBoundsException _e2) {
        System.err.println(AlbeMsg.CONS009);
        return null;
      }
    }

    return p;
  }

  /**
   * renvoie une structure contenant les donn�es actuellement affich�es dans
   * l'onglet 'D�fenses d'accostage'.
   */

  public SDefense getParametresDefense() {
    final SDefense p = (SDefense) AlbeLib.createIDLObject(SDefense.class);

    // Si d�fense
    if (presenceCombo_.getSelectedIndex() == 0) {
      p.presenceDefense = AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI;
      p.hauteur = AlbeLib.textFieldDoubleValue(hauteur_);
      p.nombrePoints = nbPointsCombo_.getSelectedIndex() + 2;
      p.points = getPoints();
    }

    // Si pas de d�fense
    else if (presenceCombo_.getSelectedIndex() == 1)
      p.presenceDefense = AlbeLib.IDL_DEFENSE_ACCOSTAGE_NON;

    return p;
  }

  /**
   * charge les points sp�cifi�s dans les champs de texte correspondants.
   * 
   * @param _p tableau contenant les points � importer
   */
  public synchronized void setPoints(final SPointDefense[] _p) {
    int n = 0;
    try {
      n = _p.length;
      for (int i = 0; i < n; i++) {
        if (_p[i].deformation != VALEUR_NULLE.value)
          deform_[i].setValue(new Double(_p[i].deformation));
        if (_p[i].effort != VALEUR_NULLE.value)
          effort_[i].setValue(new Double(_p[i].effort));
      }

    }
    catch (final NullPointerException _e1) {
      System.err.println(AlbeMsg.CONS010);
    }
  }

  /**
   * importe les donn�es contenues dans la structure pass�e en param�tres dans
   * l'onglet 'D�fenses d'accostage'.
   * 
   * @param _p structure de donn�es contenant les param�tres � importer
   */
  public synchronized void setParametresDefense(SDefense _p) {
    if (_p == null)
      _p = (SDefense) AlbeLib.createIDLObject(SDefense.class);

    // Si d�fense
    if (_p.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI)) {
      presenceCombo_.setSelectedIndex(0);
      if (_p.hauteur != VALEUR_NULLE.value)
        hauteur_.setValue(new Double(_p.hauteur));

      nbPointsCombo_.setSelectedIndex(_p.nombrePoints - 2);
      setPoints(_p.points);
      tryCalculEnergie();
    }

    // Si pas de d�fense
    else if (_p.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_NON))
      presenceCombo_.setSelectedIndex(1);
  }

  /**
   * 
   */
  public boolean pointDefenseModified(SPointDefense _p, int _n) {
    SPointDefense p2 = getPoint(_n);
    if (_p.deformation != p2.deformation || _p.effort != p2.effort)
      return true;
    return false;
  }

  /**
   * 
   */

  public boolean pointsDefenseModified(SPointDefense[] _p) {
    for (int i = 0; i < _p.length; i++) {
      if (pointDefenseModified(_p[i], i + 2))
        return true;
    }
    return false;
  }

  /**
   * 
   */

  public boolean paramsDefenseModified(SDefense _p) {
    SDefense p2 = getParametresDefense();
    if (!_p.presenceDefense.equals(p2.presenceDefense))
      return true;

    if (_p.presenceDefense.equals(AlbeLib.IDL_DEFENSE_ACCOSTAGE_OUI))
      if (_p.hauteur != p2.hauteur || _p.nombrePoints != p2.nombrePoints
          || pointsDefenseModified(_p.points))
        return true;

    return false;
  }

  /**
   * 
   */
  public void actionPerformed(ActionEvent _evt) {

    Object src = _evt.getSource();

    /** Gestion pr�sence de d�fense ou non. */
    if (src == presenceCombo_)
      choixPresenceDefense();

    /** Gestion du choix du nombre de points dans la boite combo. */
    else if (src == nbPointsCombo_) {
      int index = nbPointsCombo_.getSelectedIndex();
      nbPoints_ = index + 2;
      textFieldSetEditable(index);
      majMessages();
      tryCalculEnergie();
    }

    /** Clic sur le bouton 'Courbe'. */
    else if (src == courbeButton_)
      clicBoutonCourbe();

    /** Clic sur le bouton 'Aide'. */
    else if (src == aideButton_)
      aide();
  }

  /**
   * 
   */
  public ValidationMessages validation() {
    final ValidationMessages vm = new ValidationMessages();

    /**
     * Lancement de la validation seulement s'il y a des d�fenses (sinon rien �
     * v�rifier) et si structure soumise � un accostage.
     */
    if (presenceCombo_.getSelectedIndex() == 0 && fp_.action_.actionCombo_.getSelectedIndex() == 0) {

      // V�rification champ 'Hauteur'
      if (AlbeLib.emptyField(hauteur_))
        vm.add(AlbeMsg.VMSG131);
      else if (AlbeLib.textFieldDoubleValue(hauteur_) == VALEUR_NULLE.value)
        vm.add(AlbeMsg.VMSG132);
      else if (AlbeLib.textFieldDoubleValue(hauteur_) <= 0)
        vm.add(AlbeMsg.VMSG133);

      deformMaxRecuperable_ = false;
      // V�rification des champs 'D�formations'
      if (!fullField(deform_))
        vm.add(AlbeMsg.VMSG134);
      else if (!validField(deform_))
        vm.add(AlbeMsg.VMSG136);
      else {
        if (!deformationsValides())
          vm.add(AlbeMsg.VMSG140);
        if (!champsCroissants(deform_))
          vm.add(AlbeMsg.VMSG138);
        else {
          deformMaxRecuperable_ = true;
          // R�cup�ration de la valeur de la d�formation maximale
          deformMax_ = getDeformMax();
        }
      }

      // V�rification des champs 'Effort'
      if (!fullField(effort_))
        vm.add(AlbeMsg.VMSG135);
      else if (!validField(effort_))
        vm.add(AlbeMsg.VMSG137);
      else if (!effortsPositifs())
        vm.add(AlbeMsg.VMSG141);
      else if (deformMaxRecuperable_ && !champsCroissants(effort_))
        vm.add(AlbeMsg.VMSG139);
    }
    return vm;
  }

  /**
   * 
   */
  public void focusLost(FocusEvent _evt) {
    majMessages();
    tryCalculEnergie();
  }

}
