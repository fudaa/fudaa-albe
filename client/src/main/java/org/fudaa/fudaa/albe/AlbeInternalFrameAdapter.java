/*
 * @creation 11 oct. 06
 * @modification $Date: 2007-05-11 09:00:04 $
 * @license GNU General Public License 2
 * @copyright (c) 1998-2001 CETMEF 2 Bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr 
 */
package org.fudaa.fudaa.albe;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.print.Printable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.fudaa.ebli.impression.EbliPageable;
import org.fudaa.ebli.impression.EbliPageableDelegate;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaParamListener;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuResource;

/**
 * affiche une fen�tre dans l'application Fudaa-Albe (Albe). cette classe sert
 * de classe m�re dont toutes les fen�tres � afficher dans Albe doivent d�river.
 * elle contient les m�thodes "vides" que ces fen�tres pourront surcharger.
 * 
 * @version $Version$
 * @author Sabrina Delattre
 */

public class AlbeInternalFrameAdapter extends BuInternalFrame implements FudaaProjetListener,
    FudaaParamListener, ChangeListener, EbliPageable, PropertyChangeListener {
  /**
   * projet en cours.
   */
  FudaaProjet project_;

  /**
   * conteneur Swing principal de la fen�tre.
   */
  JComponent content_;

  /**
   * r�f�rence vers l'application. permet d'acc�der � tous les composants de
   * l'application.
   */
  BuCommonInterface appli_;

  int currenttab_;

  EbliPageableDelegate delegator_;

  /**
   * constructeur de la fenetre. construit une fen�tre vide avec un titre, un
   * projet associ�, une ic�ne.
   * 
   * @param _title titre de la fen�tre
   * @param _iconkeyword mot cl� � utiliser pour rechercher l'ic�ne
   * @param _appli application � laquelle cette fen�tre sera rattach�e
   * @param _project projet en cours auquel cette fen�tre sera rattach�e
   */
  public AlbeInternalFrameAdapter(final String _title, final String _iconkeyword,
      final BuCommonInterface _appli, final FudaaProjet _project) {
    super(_title, false, true, true, true);
    appli_ = _appli;
    project_ = _project;
    currenttab_ = 0;
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
    project_.addFudaaProjetListener(this);
    delegator_ = new EbliPageableDelegate(this);
    setFrameIcon(BuResource.BU.getIcon(_iconkeyword));

  }

  public AlbeInternalFrameAdapter getInternalFrame(final int _frame) {
    AlbeInternalFrameAdapter f = null;
    switch (_frame) {
    case 1: {
      f = ((AlbeImplementation) getApplication().getImplementation()).getFilleParametres();
      break;
    }
    
    case 2: {
      f = ((AlbeImplementation) getApplication().getImplementation()).getFilleNoteDeCalculs();
      break;
    }

    }
    return f;
  }

  /**
   * m�thode appell�e lors d'un changement de propri�t� espionn�e par ce
   * composant.
   * 
   * @param _evt �v�nement qui d�crit le changement de propri�t�
   */
  public void propertyChange(final PropertyChangeEvent _evt) {}

  /**
   * 
   */
  protected void setCurrentTab(final int _index) {
    currenttab_ = _index;
  }

  /**
   * 
   */
  protected int getCurrentTab() {
    return currenttab_;
  }

  /**
   * permet d'acc�der � l'application g�n�rale.
   * 
   * @return l'objet application contenant toutes les informations g�n�rales sur
   *         le logiciel
   */
  public BuCommonInterface getApplication() {
    return appli_;
  }

  /**
   * permet d'acc�der � l'implementation g�n�rale.
   * 
   * @return l'objet impl�mentation contenant toutes les informations g�n�rales
   *         sur le logiciel
   */
  public AlbeImplementation getImplementation() {
    return (AlbeImplementation) appli_.getImplementation();
  }

  /**
   * permet d'acc�der � l'objet projet, contenant toutes les informations sur le
   * projet ouvert en cours.
   * 
   * @return l'objet projet contenant toutes les informations g�n�rales sur le
   *         projet ouvert en cours
   */
  public FudaaProjet getProjet() {
    return project_;
  }

  /**
   * permet d'acc�der � l'objet conteneur swing principal.
   * 
   * @return l'objet conteneur swing principal
   */
  public JComponent getContent() {
    return content_;
  }

  /**
   * supprime la fen�tre du bureau du logiciel.
   */
  public void delete() {
    FudaaParamEventProxy.FUDAA_PARAM.removeFudaaParamListener(this);
    if (project_ != null) {
      project_.removeFudaaProjetListener(this);
    }
    project_ = null;
  }

  /**
   * rattache la fen�tre au projet sp�cifi� et charge les param�tres du projet.
   * 
   * @param _project projet auquel la fen�tre sera rattach� et duquel les
   *          donne�s seront import�es
   */
  public void setProjet(final FudaaProjet _project) {
    project_ = _project;
    updatePanels();
  }

  /**
   * execution de l'action �cout�e survenue.
   * 
   * @param _e �v�nement ayant engendr� l'action
   */
  public void actionPerformed(final ActionEvent _e) {
    final String _action = _e.getActionCommand();
    if (_action.equals("IMPRIMER")) {
      imprimer();
    }
    if (_action.equals("PREVISUALISER")) {
      previsualiser();
    } else {
      action(_action);
    }
  }

  public int print(final java.awt.Graphics _g, final java.awt.print.PageFormat _format,
      final int _numPage) {
    final boolean _bordure = false;
    final Component _c = this;
    if (_numPage != 0) {
      return Printable.NO_SUCH_PAGE;
    }
    // Sauvegarde de Clip pour restauration finale.
    final Shape s = _g.getClip();
    // Utilisation de methode de Graphics2d.
    final Graphics2D g2d = (Graphics2D) _g;
    // Initialisation du graphics et translation de l 'origine.
    final double[] coord = EbliPrinter.initGraphics(g2d, _format);
    // Calcul du facteur d'echelle.
    final int wComp = _c.getWidth();
    final int hComp = _c.getHeight();
    final double facteur = EbliPrinter.echelle(_format, wComp, hComp);
    final double wCompDessine = wComp * facteur;
    final double hCompDessine = hComp * facteur;
    // definition de la zone d'impression
    double xCentre = (_format.getImageableWidth() - wCompDessine) / 2;
    double yCentre = (_format.getImageableHeight() - hCompDessine) / 2;
    g2d.translate(xCentre, yCentre);
    g2d.clip(new Rectangle.Double(-5D, -5D, wCompDessine + 10, hCompDessine + 10));
    // mise a l'echelle et impression
    g2d.scale(facteur, facteur);
    _c.printAll(_g);
    if (_bordure) {
      g2d.draw(new Rectangle.Double(0D, 0D, wComp, hComp));
    }
    // Restauration du graphics.
    g2d.scale(1 / facteur, 1 / facteur);
    g2d.translate(-xCentre, -yCentre);
    g2d.translate(-coord[0], -coord[1]);
    g2d.setClip(s);
    return Printable.PAGE_EXISTS;
  }

  public com.memoire.bu.BuInformationsDocument getInformationsDocument() {
    return new BuInformationsDocument();
  }

  public com.memoire.bu.BuInformationsSoftware getInformationsSoftware() {
    return getApplication().getInformationsSoftware();
  }

  public org.fudaa.ebli.impression.EbliPageFormat getDefaultEbliPageFormat() {
    return delegator_.getDefaultEbliPageFormat();
  }

  public int getNumberOfPages() {
    return 1;
  }

  public java.awt.print.PageFormat getPageFormat(final int _i) {
    return delegator_.getPageFormat(_i);
  }

  public java.awt.print.Printable getPrintable(final int _i) {
    return delegator_.getPrintable(_i);
  }

  /**
   * 
   */
  protected void action(final String _action) {}

  /**
   * 
   */
  protected void imprimer() {}

  /**
   * 
   */
  protected void previsualiser() {}

  /**
   * 
   */
  protected void sauvegarder() {}

  /**
   * appell�e automatiquement lorsque une structure de donn�es est cr��e dans le
   * projet en cours. la fen�tre est mise � jour uniquement si elle n'est pas la
   * source de l'�v�nement.
   * 
   * @param _e �v�nement qui a d�clench� l'appel
   */
  public synchronized void paramStructCreated(final FudaaParamEvent _e) {}

  /**
   * appell�e automatiquement lorsque une structure de donn�es est supprim�e
   * dans le projet en cours. la fen�tre est mise � jour uniquement si elle
   * n'est pas la source de l'�v�nement.
   * 
   * @param _e �v�nement qui a d�clench� l'appel
   */
  public synchronized void paramStructDeleted(final FudaaParamEvent _e) {}

  /**
   * appell�e automatiquement lorsque une structure de donn�es est modifi�e dans
   * le projet en cours. la fen�tre est mise � jour uniquement si elle n'est pas
   * la source de l'�v�nement.
   * 
   * @param _e �v�nement qui a d�clench� l'appel
   */
  public synchronized void paramStructModified(final FudaaParamEvent _e) {}

  /**
   * appell�e automatiquement lorsque un �v�nement projet surgit. Il peut s'agir
   * d'un ajout de param�tres ou d'une importation de param�tres dans le projet.
   * 
   * @param _e �v�nement qui a d�clench� l'appel
   */
  public synchronized void dataChanged(final FudaaProjetEvent _e) {}

  /**
   * appell�e automatiquement lorsque que le projet change d'�tat. le changement
   * d'�tat peut �tre une ouverture ou une fermeture de projet. la fen�tre est
   * mise � jour uniquement si elle n'est pas la source de l'�v�nement
   * 
   * @param _e �v�nement qui a d�clench� l'appel
   */
  public synchronized void statusChanged(final FudaaProjetEvent _e) {}

  /**
   * 
   */
  protected void changeOnglet(final ChangeEvent _evt) {}

  /**
   * appell�e automatiquement lorsque l'utilisateur change d'onglet principal.
   * 
   * @param _evt �v�nement qui a d�clench� l'appel
   */
  public void stateChanged(final ChangeEvent _evt) {
    changeOnglet(_evt);
  }

  /**
   * mise � jour du contenu des onglets de la fen�tre. cette fonction est
   * appell�e automatiquement lorsqu'une modification de l'�tat ou du contenu du
   * projet est notifi�e.
   */
  protected void updatePanels() {}
}
